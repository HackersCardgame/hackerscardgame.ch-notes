window.YTD.ad_engagements.part0 = [ {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1315577981377667072",
              "tweetText" : "In welchen Städten ist das Risiko einer Immobilienblase am grössten? Was sind die Trends auf dem Wohnungsmarkt? Diesen Fragen ist UBS mit einer Analyse von Wohnimmobilienpreisen in 25 Grossstädten nachgegangen. Mehr erfahren: https://t.co/5R9GCRz27n\n#realestate #bubbleindex https://t.co/UWhDRsXS7R",
              "urls" : [ "https://t.co/5R9GCRz27n" ],
              "mediaUrls" : [ "https://t.co/UWhDRsXS7R" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-14 14:05:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-14 14:05:57",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1314549513634357253",
              "tweetText" : "Neues Stellenangebot: LEITER/IN TREUHAND &amp; UNTERNEHMENSBERATUNG\nArbeitsort: Raum Bern\n#job #stelle #karriere #geschäftsführung #wirtschaftsprüfung #direktor #bereichsleiter",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Xeloba",
              "screenName" : "@xeloba_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "CEO"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Sicherheit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Personality"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeiten in der schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Mitarbeitermotivation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Integrität"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-14 23:28:48"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-14 23:28:51",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319607744618483714",
              "tweetText" : "A family brings a lifetime of joy, but also responsibility. Our network of experts helps you proactively plan for the future, so you can enjoy life’s most important milestones together.\n\nFind out more: https://t.co/hw9CU394Cr",
              "urls" : [ "https://t.co/hw9CU394Cr" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Julius Baer",
              "screenName" : "@juliusbaer"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-26 00:42:58"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-26 00:42:59",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319601358899642374",
              "tweetText" : "Gutes tun tut gut. Auf #UBSHelpetica, finden Sie viele nachhaltige Projekte im sozialen Bereich, die Ihre Unterstützung brauchen. Ergreifen Sie jetzt die Möglichkeit und tun Sie etwas Gutes für Ihre Mitmenschen, tun Sie etwas Gutes für sich: https://t.co/OkKswJUhwC\n\n#shareUBS",
              "urls" : [ "https://t.co/OkKswJUhwC" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-25 17:15:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-25 17:15:56",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319004746863104000",
              "tweetText" : "RT @QuickTake: ⚡️ “How diversity across industries is helping shape a better future\" https://t.co/npw517Lp4T",
              "urls" : [ "https://t.co/npw517Lp4T" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "SAS Software",
              "screenName" : "@SASsoftware"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Gartner_inc"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CiscoSecure"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Cisco"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IBM"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ronald_vanLoon"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@msftsecurity"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "IBM"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "IBM Cloud"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Cisco"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-26 17:29:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-26 17:29:58",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1320628881477521408",
              "tweetText" : "SARON oder Festhypothek? Unser Hypothekenexperte und Berater Lukas Ammann gibt Ihnen alle Infos und Insights zur key4 SARON Hypothek. \n\nHaben Sie noch Fragen?\n\nDie Vorteile einer key4 Festhypothek erklären wir Ihnen hier: https://t.co/hrhxmNK3V4\n#makingbankingsimple https://t.co/02MQnIph56",
              "urls" : [ "https://t.co/hrhxmNK3V4" ],
              "mediaUrls" : [ "https://t.co/02MQnIph56" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-27 11:01:47"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-27 11:01:48",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-27 11:00:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-27 11:00:54",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1320628881477521408",
              "tweetText" : "SARON oder Festhypothek? Unser Hypothekenexperte und Berater Lukas Ammann gibt Ihnen alle Infos und Insights zur key4 SARON Hypothek. \n\nHaben Sie noch Fragen?\n\nDie Vorteile einer key4 Festhypothek erklären wir Ihnen hier: https://t.co/hrhxmNK3V4\n#makingbankingsimple https://t.co/02MQnIph56",
              "urls" : [ "https://t.co/hrhxmNK3V4" ],
              "mediaUrls" : [ "https://t.co/02MQnIph56" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-27 07:36:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-27 07:36:34",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634334186897408",
              "tweetText" : "Übrigens, wir helfen nicht nur bei ÖV-Ausfall. Entdecken Sie weitere TCS-Leistungen wie Pannenhilfe, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CSU"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CDU"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-27 18:08:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-27 18:08:58",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 05:35:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 05:35:02",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634334186897408",
              "tweetText" : "Übrigens, wir helfen nicht nur bei ÖV-Ausfall. Entdecken Sie weitere TCS-Leistungen wie Pannenhilfe, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CSU"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CDU"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 05:25:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 05:25:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1320998680535703559",
              "tweetText" : "Im Hypothekar-Dschungel den Überblick behalten ist nicht leicht, selbst für Branchenkenner. \nZu welchen Hypothekarthemen wollten Sie schon lange einen Tipp? Unser Experte, Mario Ramseier, gibt Ihnen wertvolle Antworten in den nächsten 48h. \n#makingbankingsimple #realestate https://t.co/gF7xxspBvE",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/gF7xxspBvE" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "immobilienkredit"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 05:14:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 05:14:14",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634265643692034",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Velopannen. Weitere TCS-Leistungen: E-Mobilitäts-Expertise, ÖV-, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CSU"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CDU"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 07:07:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 07:07:19",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634399811018752",
              "tweetText" : "Übrigens, wir helfen nicht nur bei heiklen Rechtsfragen. Weitere TCS-Leistungen: Pannenhilfe, E-Mobilitäts-Expertise und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CSU"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CDU"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 04:17:42"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 04:17:44",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184733245902069760",
              "tweetText" : "Ultraschnelles und sehr sicheres Webhosting abgestimmt auf diverse CMS Anbieter. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "developers"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 13:28:19"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 13:28:21",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634465162465280",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Autopannen. Weitere TCS-Leistungen: E-Mobilitäts-Expertise, ÖV-, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 13:07:48"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 13:07:50",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634399811018752",
              "tweetText" : "Übrigens, wir helfen nicht nur bei heiklen Rechtsfragen. Weitere TCS-Leistungen: Pannenhilfe, E-Mobilitäts-Expertise und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-28 18:59:19"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-28 18:59:21",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321376210572759043",
              "tweetText" : "Das Umweltbundesamt hat neue offizielle Zahlen zum #Verpackungsabfall und dessen Verwertung veröffentlicht. --&gt; https://t.co/NZZow5yANS Interessant ist auch die Verteilung auf einzelne Materialien. #Verpackung #Abfall #Recycling #Kreislaufwirtschaft @IK_Verband @PlasticsEuropeD https://t.co/YWopN0BvRv",
              "urls" : [ "https://t.co/NZZow5yANS" ],
              "mediaUrls" : [ "https://t.co/YWopN0BvRv" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Newsroom.Kunststoffverpackungen",
              "screenName" : "@News_Kunststoff"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@faznet"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@plastik_umwelt"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Verpackung"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-30 02:34:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-30 02:34:51",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321018061869240321",
              "tweetText" : "Vom 1. bis 3.11.2020 feiern wir die Schweizer Digitaltage. Online erzählen wir, wie wir die Menschen begleiten, die Digitalisierung zu nutzen oder erklären in Learning Labs wie TikTok funktioniert und bieten kostenlose Academy-Schulungen. → https://t.co/5FwiZLLxuu https://t.co/Vjz7XsEFtu",
              "urls" : [ "https://t.co/5FwiZLLxuu" ],
              "mediaUrls" : [ "https://t.co/Vjz7XsEFtu" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "eltern"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "mutter"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parent"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "kids"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "educations"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "familie"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "education"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parenting"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parents"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tochter"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 11:07:34"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 11:07:36",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321754844672282626",
              "tweetText" : "In den sozialen Medien verbreiten sich seit Wochen sogenannte Deepfake-Bilder. Dabei setzt eine Künstliche Intelligenz den eigenen Kopf auf einen fremden Körper – und das verblüffend echt. Das darf natürlich nicht leichtsinnig geschehen, aber zugegeben: Witzig ist es auch! https://t.co/dOIFETRqYV",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/dOIFETRqYV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "parents"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "educations"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "familie"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tochter"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parent"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "education"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "mutter"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "kids"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "eltern"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parenting"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 07:25:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 07:25:16",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 07:39:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 07:39:58",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321791138018283522",
              "tweetText" : "Tun Sie etwas Gutes und geben Sie Ihr Wissen an Andere weiter. Melden Sie sich jetzt auf unserer Freiwilligen-Plattform #UBSHelpetica an und unterstützen Sie ein Projekt im Bereich Bildung: https://t.co/YdyG7uNz6o\n\n#shareUBS #gutestuntutgut #freiwilligenarbeit #volunteering",
              "urls" : [ "https://t.co/YdyG7uNz6o" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 07:23:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 07:24:01",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1321791138018283522",
              "tweetText" : "Tun Sie etwas Gutes und geben Sie Ihr Wissen an Andere weiter. Melden Sie sich jetzt auf unserer Freiwilligen-Plattform #UBSHelpetica an und unterstützen Sie ein Projekt im Bereich Bildung: https://t.co/YdyG7uNz6o\n\n#shareUBS #gutestuntutgut #freiwilligenarbeit #volunteering",
              "urls" : [ "https://t.co/YdyG7uNz6o" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 22:03:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 22:03:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 21:14:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 21:14:57",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1301037181759303680",
              "tweetText" : "Wie kommunizieren Sie in besonderen Ereignissen mit Ihren Mitarbeitenden? Mit #eCall erreichen Sie Ihre Belegschaft ortsunabhängig per #SMS. Und das bequem aus Ihrem Mailprogramm. Jetzt mehr dazu erfahren:\n► https://t.co/fbuFj4MKPl\n\n#BusinessMessaging  #Mitarbeiterkommunikation https://t.co/nNLxzt3eXG",
              "urls" : [ "https://t.co/fbuFj4MKPl" ],
              "mediaUrls" : [ "https://t.co/nNLxzt3eXG" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "F24 Schweiz AG",
              "screenName" : "@f24_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-31 18:14:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-31 18:14:13",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321362556540379136",
              "tweetText" : "Fonds ganz einfach online zeichnen – jederzeit und überall.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-01 13:01:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-01 13:01:26",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634265643692034",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Velopannen. Weitere TCS-Leistungen: E-Mobilitäts-Expertise, ÖV-, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "families"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-15 03:44:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-15 03:44:59",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634399811018752",
              "tweetText" : "Übrigens, wir helfen nicht nur bei heiklen Rechtsfragen. Weitere TCS-Leistungen: Pannenhilfe, E-Mobilitäts-Expertise und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-15 08:03:42"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-15 08:03:43",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-02 15:19:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-02 15:19:04",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321362556540379136",
              "tweetText" : "Fonds ganz einfach online zeichnen – jederzeit und überall.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-02 18:04:09"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-02 18:04:12",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321743191247364098",
              "tweetText" : "An alle Lehrpersonen: Der #Zukunftstag wurde abgesagt – #Roche bietet ein spannendes, virtuelles Programm! Erhalten Sie Einblicke in die Berufswelt der Roche und diskutieren Sie mit Ihrer Klasse beim Turmgespräch zum Thema Berufseinstieg mit. Anmeldung:  https://t.co/yPtWXgMslD https://t.co/NTDFK8oHgO",
              "urls" : [ "https://t.co/yPtWXgMslD" ],
              "mediaUrls" : [ "https://t.co/NTDFK8oHgO" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Roche Schweiz",
              "screenName" : "@roche_schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Bildung"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "lehrer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "schule"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-02 18:15:09"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-02 18:15:10",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-02 18:25:29"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-02 18:25:31",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323176070305193985",
              "tweetText" : "Marco Hahn ist bei uns für maximale Start-up-power am Start. Täglich gibt er alles für mehr Neuland und weniger alte Denke. Mehr über key4 by UBS erfahren: https://t.co/duMPl3PC0V \n#FacesofInnovation https://t.co/HhEVqmmO5p",
              "urls" : [ "https://t.co/duMPl3PC0V" ],
              "mediaUrls" : [ "https://t.co/HhEVqmmO5p" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-03 14:55:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-03 14:55:13",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321362556540379136",
              "tweetText" : "Fonds ganz einfach online zeichnen – jederzeit und überall.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-04 07:33:17"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-04 07:33:19",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323618459624873987",
              "tweetText" : "Das Ziel unserer #Klimastrategie: der komplette Umstieg von Kohle auf #Wasserstoff durch den Einsatz einer #Direktreduktionsanlage mit Einschmelzer. Das gelingt nicht von heute auf morgen, sondern muss schrittweise erfolgen. Mehr zu unserer Klimastrategie: https://t.co/hF6X1cFeDN https://t.co/29ZA3Rb5FY",
              "urls" : [ "https://t.co/hF6X1cFeDN" ],
              "mediaUrls" : [ "https://t.co/29ZA3Rb5FY" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "thyssenkrupp",
              "screenName" : "@thyssenkrupp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Renewable Energies"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-04 23:59:31"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-04 23:59:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321362556540379136",
              "tweetText" : "Fonds ganz einfach online zeichnen – jederzeit und überall.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-04 23:27:06"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-04 23:27:07",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319002734851461121",
              "tweetText" : "Wir investieren für unsere Kunden in ganz Europa. Davon profitieren auch Städte und Gemeinden. Erfahren Sie mehr zur Windkraftanlage an der Westküste Norwegens, deren Bau wir unterstützen: 👇 #BLKNachhaltigkeit https://t.co/QlPivbGpaS",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/QlPivbGpaS" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BlackRock Switzerland",
              "screenName" : "@BlackRock_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Government"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Government/Education"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Journalist"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-05 00:25:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-05 00:25:51",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321775360116215819",
              "tweetText" : "Wertorientierte Kapitalanlage und Vermögenssicherung mit dem Max #Otte Vermögensbildungsfonds (WKN: #A1J3AM). Ohne Ausgabeaufschlag bei https://t.co/JcEbUxwsW1 https://t.co/9R5n2UftMV",
              "urls" : [ "https://t.co/JcEbUxwsW1" ],
              "mediaUrls" : [ "https://t.co/9R5n2UftMV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Der Privatinvestor",
              "screenName" : "@DPrivatinvestor"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-05 00:43:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-05 00:43:34",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184733303632531456",
              "tweetText" : "Webhosting mit vielen Tools für neue Möglichkeiten. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "wordpress"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-04 21:38:29"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-04 22:45:03",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323257888643702785",
              "tweetText" : "The perfect Match: Unsere neue Debitkarte #NextGenDebit ist DIE Ergänzung zur Kreditkarte. Vom Bargeldbezug bis zum kontaktlosen Bezahlen ist alles drin. Jetzt informieren: https://t.co/DUAqIJq6R3 https://t.co/brUGcbL3Pm",
              "urls" : [ "https://t.co/DUAqIJq6R3" ],
              "mediaUrls" : [ "https://t.co/brUGcbL3Pm" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@amazon"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Facebook"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Microsoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Google"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@YouTube"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Apple"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitter"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@netflix"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@AppleMusic"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@intel"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@TechCrunch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WhatsApp"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "PayPal"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-05 01:31:26"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-05 01:31:28",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321362556540379136",
              "tweetText" : "Fonds ganz einfach online zeichnen – jederzeit und überall.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "PostFinance",
              "screenName" : "@PostFinance"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-05 02:10:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-05 02:10:52",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184733303632531456",
              "tweetText" : "Webhosting mit vielen Tools für neue Möglichkeiten. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "wordpress"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-05 09:21:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-05 09:21:56",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319607744618483714",
              "tweetText" : "A family brings a lifetime of joy, but also responsibility. Our network of experts helps you proactively plan for the future, so you can enjoy life’s most important milestones together.\n\nFind out more: https://t.co/hw9CU394Cr",
              "urls" : [ "https://t.co/hw9CU394Cr" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Julius Baer",
              "screenName" : "@juliusbaer"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-06 01:58:30"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 01:58:32",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324375512828026882",
              "tweetText" : "Gutes tun tut Gut. Auf #UBSHelpetica, finden Sie viele nachhaltige Projekte im Bereich Unternehmertum, die Ihre tatkräftige Unterstützung brauchen. Tun Sie etwas Gutes für die Chancengleichheit, tun Sie etwas Gutes für Alle: https://t.co/Upum5pXFj7\n#shareUBS #gutestuntutgut",
              "urls" : [ "https://t.co/Upum5pXFj7" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-06 10:49:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 10:49:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324276322730848256",
              "tweetText" : "Meet DJI Mini 2.\nWith a 4K camera and OcuSync 2.0, Mini 2 is the perfect drone to make your moments fly - and yes, it still weighs less than 249 g.\n\nDiscover more: https://t.co/lkyirov6hy",
              "urls" : [ "https://t.co/lkyirov6hy" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "DJI",
              "screenName" : "@DJIGlobal"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Travel news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ArianaGrande"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ladygaga"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Eminem"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@katyperry"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@jtimberlake"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@metmuseum"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@justinbieber"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@lonelyplanet"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@kendricklamar"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arts"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "art"
            }, {
              "targetingType" : "Website Activity",
              "targetingValue" : "Purchase"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-06 04:22:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 05:07:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324276153444524032",
              "tweetText" : "Meet DJI Mini 2.\nWith a 4K camera and OcuSync 2.0, Mini 2 is the perfect drone to make your moments fly - and yes, it still weighs less than 249 g.\n\nDiscover more: https://t.co/lkyirov6hy",
              "urls" : [ "https://t.co/lkyirov6hy" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "DJI",
              "screenName" : "@DJIGlobal"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Travel news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ArianaGrande"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ladygaga"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Eminem"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@katyperry"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@jtimberlake"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@metmuseum"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@justinbieber"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@lonelyplanet"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@kendricklamar"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "art"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arts"
            }, {
              "targetingType" : "Website Activity",
              "targetingValue" : "Purchase"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-06 04:33:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 04:33:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "computer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-06 06:14:20"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 06:14:21",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324231219345870848",
              "tweetText" : "LANXESS bleibt trotz Belastungen durch die Corona-Krise auf Kurs. Wir haben die Prognose für das Gesamtjahr bestätigt und weiter präzisiert. Der Umsatz im 3. Quartal lag bei 1,46 Mrd. €, das operative Ergebnis (EBITDA pre) bei 193 Mio. €. Details 👇\nhttps://t.co/Ts18MFjXL1",
              "urls" : [ "https://t.co/Ts18MFjXL1" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "LANXESS DE",
              "screenName" : "@LANXESS_DEU"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "chemie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-06 19:08:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 19:08:08",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323912318829420544",
              "tweetText" : "Die stärksten Warnsignale für eine Immobilienblase zeigen München und Frankfurt. Dicht gefolgt von Paris und Amsterdam. Finden Sie heraus, wie es um die Schweiz steht: https://t.co/67O2zBI5RJ\n#bubbleindex #realestate https://t.co/mtIqbFbA8N",
              "urls" : [ "https://t.co/67O2zBI5RJ" ],
              "mediaUrls" : [ "https://t.co/mtIqbFbA8N" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-06 19:19:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 19:19:58",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321066303596220416",
              "tweetText" : "Eine katalytische Investition von mindestens 5 Milliarden Dollar heute könnte die Volkswirtschaften der @GPforEducation-Partnerländer mit bis zu 164 Milliarden Dollar stärken und 18 Millionen Menschen aus der Armut befreien.\n\n#RaiseYourHand #FundEducation",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Global Partnership for Education",
              "screenName" : "@GPforEducation"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            } ],
            "impressionTime" : "2020-11-06 19:10:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-06 19:10:13",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1324375512828026882",
              "tweetText" : "Gutes tun tut Gut. Auf #UBSHelpetica, finden Sie viele nachhaltige Projekte im Bereich Unternehmertum, die Ihre tatkräftige Unterstützung brauchen. Tun Sie etwas Gutes für die Chancengleichheit, tun Sie etwas Gutes für Alle: https://t.co/Upum5pXFj7\n#shareUBS #gutestuntutgut",
              "urls" : [ "https://t.co/Upum5pXFj7" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 08:09:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 08:09:03",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 08:55:35"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 08:55:36",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324628963285753856",
              "tweetText" : "Für Gabriele ist Innovation ein Weg und ein Mindset – und er ist stolz, alle Voraussetzungen für innovative Lösungen in seinem Team key4 by UBS vorzufinden. Mehr über key4 by UBS erfahren: https://t.co/z40s0jgldQ #FacesofInnovation https://t.co/pAWFztG2Nz",
              "urls" : [ "https://t.co/z40s0jgldQ" ],
              "mediaUrls" : [ "https://t.co/pAWFztG2Nz" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "sparen"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 08:27:42"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 08:27:44",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 07:32:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 07:32:17",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 10:42:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 10:42:09",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324375512828026882",
              "tweetText" : "Gutes tun tut Gut. Auf #UBSHelpetica, finden Sie viele nachhaltige Projekte im Bereich Unternehmertum, die Ihre tatkräftige Unterstützung brauchen. Tun Sie etwas Gutes für die Chancengleichheit, tun Sie etwas Gutes für Alle: https://t.co/Upum5pXFj7\n#shareUBS #gutestuntutgut",
              "urls" : [ "https://t.co/Upum5pXFj7" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 10:41:37"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 10:41:45",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-07 18:31:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-07 18:42:35",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324375512828026882",
              "tweetText" : "Gutes tun tut Gut. Auf #UBSHelpetica, finden Sie viele nachhaltige Projekte im Bereich Unternehmertum, die Ihre tatkräftige Unterstützung brauchen. Tun Sie etwas Gutes für die Chancengleichheit, tun Sie etwas Gutes für Alle: https://t.co/Upum5pXFj7\n#shareUBS #gutestuntutgut",
              "urls" : [ "https://t.co/Upum5pXFj7" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-08 02:35:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-08 02:35:53",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634465162465280",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Autopannen. Weitere TCS-Leistungen: E-Mobilitäts-Expertise, ÖV-, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-15 23:01:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-15 23:01:57",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323886033122009090",
              "tweetText" : "Neues Stellenangebot: TEAMLEITER/IN KRANKENTAGGELD- &amp; UNFALLVERSICHERUNG\nUnternehmen: Ausgleichskasse panvica\nArbeitsort: Münchenbuchsee\n#job #stelle #karriere #sozialversicherungsleiter #versicherungsverwalter #versicherung #spezialist #abteilungsleiter",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Xeloba",
              "screenName" : "@xeloba_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Sicherheit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "KMU"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-09 01:37:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-09 02:04:24",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321775360116215819",
              "tweetText" : "Wertorientierte Kapitalanlage und Vermögenssicherung mit dem Max #Otte Vermögensbildungsfonds (WKN: #A1J3AM). Ohne Ausgabeaufschlag bei https://t.co/JcEbUxwsW1 https://t.co/9R5n2UftMV",
              "urls" : [ "https://t.co/JcEbUxwsW1" ],
              "mediaUrls" : [ "https://t.co/9R5n2UftMV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Der Privatinvestor",
              "screenName" : "@DPrivatinvestor"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-08 16:46:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-08 16:46:08",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "computer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-08 21:12:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-08 21:12:34",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321743191247364098",
              "tweetText" : "An alle Lehrpersonen: Der #Zukunftstag wurde abgesagt – #Roche bietet ein spannendes, virtuelles Programm! Erhalten Sie Einblicke in die Berufswelt der Roche und diskutieren Sie mit Ihrer Klasse beim Turmgespräch zum Thema Berufseinstieg mit. Anmeldung:  https://t.co/yPtWXgMslD https://t.co/NTDFK8oHgO",
              "urls" : [ "https://t.co/yPtWXgMslD" ],
              "mediaUrls" : [ "https://t.co/NTDFK8oHgO" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Roche Schweiz",
              "screenName" : "@roche_schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Education news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-08 18:14:26"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-08 18:14:28",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191972811710464",
              "tweetText" : "«Ist mir heute ins Klo gefallen und funktioniert immer noch.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-09 03:11:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-09 03:11:35",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324652773305188354",
              "tweetText" : "Was tun wir, wenn sich #CO2 Emissionen nicht direkt vermeiden lassen? Wir bei #thyssenkrupp setzen das Prinzip der #CarbonCaptureAndUsage um: https://t.co/2JIvgeVqTO #klimaneutral #CCU https://t.co/QgiJpgI9xl",
              "urls" : [ "https://t.co/2JIvgeVqTO" ],
              "mediaUrls" : [ "https://t.co/QgiJpgI9xl" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "thyssenkrupp",
              "screenName" : "@thyssenkrupp"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Chemie"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-10 04:01:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 04:05:20",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1318804099673063424",
              "tweetText" : "With #Talanx, the coal phase-out is already implemented. In underwriting, we basically no longer pose any risks for newly planned coal-fired power plants and mines. The same applies to oil sands. \nhttps://t.co/F5ufcd522k\n#TalanxTakesCare #sustainability #ClimateChange #insurance https://t.co/eDk4HJhV5R",
              "urls" : [ "https://t.co/F5ufcd522k" ],
              "mediaUrls" : [ "https://t.co/eDk4HJhV5R" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Talanx Group",
              "screenName" : "@talanx_en"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "versicherung"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-10 04:05:20"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 04:05:22",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663410014441475",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-10 09:51:37"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 09:51:39",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324278021893881858",
              "tweetText" : "Die #Swissbau, @bauendigital_CH und der @sia_schweiz präsentieren das #SwissbauInnovationLab «on Tour». Sichern Sie sich jetzt Ihr Ticket für den Online Live-Event vom Donnerstag, 26. November 2020 und erleben Sie die geballte Power der Branche: https://t.co/Z59LDRv4BC #Swissbau https://t.co/8TH8ylscOi",
              "urls" : [ "https://t.co/Z59LDRv4BC" ],
              "mediaUrls" : [ "https://t.co/8TH8ylscOi" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swissbau",
              "screenName" : "@Swissbau"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Government"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-10 09:40:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 09:40:52",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321775360116215819",
              "tweetText" : "Wertorientierte Kapitalanlage und Vermögenssicherung mit dem Max #Otte Vermögensbildungsfonds (WKN: #A1J3AM). Ohne Ausgabeaufschlag bei https://t.co/JcEbUxwsW1 https://t.co/9R5n2UftMV",
              "urls" : [ "https://t.co/JcEbUxwsW1" ],
              "mediaUrls" : [ "https://t.co/9R5n2UftMV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Der Privatinvestor",
              "screenName" : "@DPrivatinvestor"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-10 21:53:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 21:53:08",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321775360116215819",
              "tweetText" : "Wertorientierte Kapitalanlage und Vermögenssicherung mit dem Max #Otte Vermögensbildungsfonds (WKN: #A1J3AM). Ohne Ausgabeaufschlag bei https://t.co/JcEbUxwsW1 https://t.co/9R5n2UftMV",
              "urls" : [ "https://t.co/JcEbUxwsW1" ],
              "mediaUrls" : [ "https://t.co/9R5n2UftMV" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Der Privatinvestor",
              "screenName" : "@DPrivatinvestor"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-10 18:02:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 18:03:10",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663410014441475",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Investing"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-10 17:43:31"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-10 17:43:32",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326120867231911936",
              "tweetText" : "« Photography involves humanity ».\nWatch Sabine Weiss, winner of the 2020 Women In Motion Award for photography, share her experience.\n@rencontresarles\n#WomenInMotion",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Kering",
              "screenName" : "@KeringGroup"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@voguemagazine"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-11 12:17:15"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-11 12:17:17",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1325573817364869122",
              "tweetText" : "An den Schweizer Digitaltagen wurde ich in einem Interview gefragt, ob man irgendwann alles rund um Medienkompetenz wissen kann. Ich glaube: Neue Entwicklungen wird es immer geben. https://t.co/7QlFSPIkh6",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/7QlFSPIkh6" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "datenschutz"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-11 14:13:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-11 14:13:56",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1325715324025647109",
              "tweetText" : "\"BETTER THAN A BUCKET LIST\" - the latest episode of my podcast available now ! \nhttps://t.co/TfFWpfarry \nvia @TheTimeExpert",
              "urls" : [ "https://t.co/TfFWpfarry" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Anna Jelen",
              "screenName" : "@TheTimeExpert"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@DeepakChopra"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            } ],
            "impressionTime" : "2020-11-11 13:25:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-11 13:25:08",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1325573817364869122",
              "tweetText" : "An den Schweizer Digitaltagen wurde ich in einem Interview gefragt, ob man irgendwann alles rund um Medienkompetenz wissen kann. Ich glaube: Neue Entwicklungen wird es immer geben. https://t.co/7QlFSPIkh6",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/7QlFSPIkh6" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-12 00:19:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-12 00:19:24",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191809024184321",
              "tweetText" : "«Funktioniert gut, jedoch haben meine Eltern gedacht ich nehme Drogen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-12 15:40:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-12 15:40:59",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-12 19:22:12"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-12 19:22:14",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-13 20:33:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 20:33:52",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324685462557102080",
              "tweetText" : "Bei vielen unserer Geschäfte zeigen sich inzwischen Anzeichen für eine Wende zum Besseren. Die Nachfrage aus wichtigen Kundenindustrien hat im Vergleich zum zweiten Quartal wieder angezogen. Positive Impulse kommen insbesondere aus China und den USA.\nhttps://t.co/6qVAXiwfk0",
              "urls" : [ "https://t.co/6qVAXiwfk0" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "LANXESS DE",
              "screenName" : "@LANXESS_DEU"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Chemische Industrie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-13 22:09:25"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 22:09:27",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1321047655779307522",
              "tweetText" : "Jedes Mädchen in der Schule würde praktisch die Kinderehe beenden, die Kindersterblichkeit halbieren und frühe Schwangerschaften drastisch reduzieren. Eine voll finanzierte GPE könnte 46 Millionen weitere Mädchen eine Ausbildung ermöglichen.  #RaiseYourHand #FundEducation",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Global Partnership for Education",
              "screenName" : "@GPforEducation"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WorldBank"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            } ],
            "impressionTime" : "2020-11-13 23:02:09"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 23:02:12",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-13 23:20:29"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 23:20:34",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-14 00:22:20"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-14 00:22:22",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326120867231911936",
              "tweetText" : "« Photography involves humanity ».\nWatch Sabine Weiss, winner of the 2020 Women In Motion Award for photography, share her experience.\n@rencontresarles\n#WomenInMotion",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Kering",
              "screenName" : "@KeringGroup"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Photography"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-13 21:24:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 21:24:13",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-13 21:37:46"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-13 21:37:50",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663410014441475",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-14 04:32:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-14 04:32:40",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326863834674556928",
              "tweetText" : "#CallofDuty Modern Warfare ist das Spiel der Stunde. Somit wird die Schlacht um den dritten #digitecPlayground im fiktiven Kriegsgebiet von Verdanks ausgetragen. Melde dich jetzt an, um am Cup teilzunehmen. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-14 04:35:26"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-14 04:35:28",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-14 17:24:46"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-14 17:24:48",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634399811018752",
              "tweetText" : "Übrigens, wir helfen nicht nur bei heiklen Rechtsfragen. Weitere TCS-Leistungen: Pannenhilfe, E-Mobilitäts-Expertise und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "families"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-21 13:26:48"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-21 13:26:51",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-15 23:51:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-15 23:51:23",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326120867231911936",
              "tweetText" : "« Photography involves humanity ».\nWatch Sabine Weiss, winner of the 2020 Women In Motion Award for photography, share her experience.\n@rencontresarles\n#WomenInMotion",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Kering",
              "screenName" : "@KeringGroup"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arts"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 10:02:06"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 10:02:15",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 13:11:13"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 13:11:15",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1327305834846695425",
              "tweetText" : "Nimm an der Heist-Herausforderung teil &amp; erhalte ein Gratisgeschenk von 1.000.000 GTA$\n \nWenn ihr gemeinsam bis zum 18.11. 100 Milliarden GTA$ stehlt, belohnen wir die Community mit einem besonderen neuen Fahrzeug, das im Dezember für begrenzte Zeit gratis erhältlich sein wird.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Rockstar Games",
              "screenName" : "@RockstarGames"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ubisoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Steam"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Xbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@PlayStation"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IGN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@EA"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#games"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gaming"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "backward compatible"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamers"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-028-gtaops4xb1pc-lapsedplayers-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-031-gtaops4xb1pc-activecasino-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-032-gtaops4xb1pc-activecasino-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-029-gtaops4xb1pc-lapsedplayers-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "000_GTAO_Payers-Lifetime_18ce54s7sud"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 13:08:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 13:09:23",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1327305834846695425",
              "tweetText" : "Nimm an der Heist-Herausforderung teil &amp; erhalte ein Gratisgeschenk von 1.000.000 GTA$\n \nWenn ihr gemeinsam bis zum 18.11. 100 Milliarden GTA$ stehlt, belohnen wir die Community mit einem besonderen neuen Fahrzeug, das im Dezember für begrenzte Zeit gratis erhältlich sein wird.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Rockstar Games",
              "screenName" : "@RockstarGames"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ubisoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Steam"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Xbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@PlayStation"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IGN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@EA"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#games"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gaming"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "backward compatible"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamers"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-028-gtaops4xb1pc-lapsedplayers-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-031-gtaops4xb1pc-activecasino-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-032-gtaops4xb1pc-activecasino-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-029-gtaops4xb1pc-lapsedplayers-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "000_GTAO_Payers-Lifetime_18ce54s7sud"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 22:30:23"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 22:30:24",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1327305834846695425",
              "tweetText" : "Nimm an der Heist-Herausforderung teil &amp; erhalte ein Gratisgeschenk von 1.000.000 GTA$\n \nWenn ihr gemeinsam bis zum 18.11. 100 Milliarden GTA$ stehlt, belohnen wir die Community mit einem besonderen neuen Fahrzeug, das im Dezember für begrenzte Zeit gratis erhältlich sein wird.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Rockstar Games",
              "screenName" : "@RockstarGames"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Ubisoft"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Steam"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Xbox"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@PlayStation"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@IGN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@EA"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#games"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "gamer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gaming"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "xbox"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "backward compatible"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#gamers"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-028-gtaops4xb1pc-lapsedplayers-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-031-gtaops4xb1pc-activecasino-eu_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-011-032-gtaops4xb1pc-activecasino-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "gtao-010-029-gtaops4xb1pc-lapsedplayers-latamasia_18ce54ypdu8"
            }, {
              "targetingType" : "List",
              "targetingValue" : "000_GTAO_Payers-Lifetime_18ce54s7sud"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 22:32:45"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 22:32:46",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 22:01:44"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 22:01:46",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 22:35:43"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 22:35:47",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 21:16:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 21:16:09",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1323191972811710464",
              "tweetText" : "«Ist mir heute ins Klo gefallen und funktioniert immer noch.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 16:04:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 16:04:51",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 15:41:46"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 15:41:48",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1327203306817658881",
              "tweetText" : "Denise ist Changemanagerin und wo könnte sie besser die stetige Veränderung pushen, als in einem hochmotivierten Team wie unserem? Mehr über key4 by UBS erfahren: https://t.co/8RnHAbmPqo #FacesofInnovation https://t.co/PAfSkGGlxb",
              "urls" : [ "https://t.co/8RnHAbmPqo" ],
              "mediaUrls" : [ "https://t.co/PAfSkGGlxb" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 23:15:23"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 23:15:25",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326863834674556928",
              "tweetText" : "#CallofDuty Modern Warfare ist das Spiel der Stunde. Somit wird die Schlacht um den dritten #digitecPlayground im fiktiven Kriegsgebiet von Verdanks ausgetragen. Melde dich jetzt an, um am Cup teilzunehmen. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-16 23:25:27"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-16 23:25:28",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 01:31:46"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 01:31:47",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1325814128817082370",
              "tweetText" : "90% des Lebens verbringen drinnen. Digitale Zwillinge sorgen dafür, dass Smart Buildings positive Erlebnisse am Arbeitsplatz unterstützen. Wie dies geht, zeigt @Siemens_Switzerland im Digital Use Case für das #SwissbauInnovationLab «on Tour». https://t.co/TTsKC1FGSp #Swissbau https://t.co/WulTTqN5ez",
              "urls" : [ "https://t.co/TTsKC1FGSp" ],
              "mediaUrls" : [ "https://t.co/WulTTqN5ez" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swissbau",
              "screenName" : "@Swissbau"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 00:05:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 00:05:35",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-17 21:57:43"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-17 21:57:44",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663353357737989",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Photography"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@MuseumModernArt"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@FinancialTimes"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Financial Times"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "art"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "#art"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-17 22:00:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-17 22:00:08",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-17 23:15:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 00:05:32",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191809024184321",
              "tweetText" : "«Funktioniert gut, jedoch haben meine Eltern gedacht ich nehme Drogen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 08:59:08"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 08:59:10",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 08:37:35"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 08:37:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323630242783764483",
              "tweetText" : "Wir investieren das Kapital unserer Kunden in Unternehmen, die auf eine #Kreislaufwirtschaft setzen. Investitionen wie diese tragen zum Schutz der Umwelt und ihrer natürlichen Ressourcen bei. Erfahren Sie mehr: https://t.co/23LjxG04LF \n#Nachhaltigkeit",
              "urls" : [ "https://t.co/23LjxG04LF" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BlackRock Switzerland",
              "screenName" : "@BlackRock_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Government/Education"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 08:45:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 08:45:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326120867231911936",
              "tweetText" : "« Photography involves humanity ».\nWatch Sabine Weiss, winner of the 2020 Women In Motion Award for photography, share her experience.\n@rencontresarles\n#WomenInMotion",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Kering",
              "screenName" : "@KeringGroup"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "arts"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "cultures"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "culture"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 08:40:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 08:40:42",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 08:41:53"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 08:41:54",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1328664106627452929",
              "tweetText" : "Profitieren auch Sie von der Schweizer #Datensicherheit:\n👉 https://t.co/rFUq7MvBMx\n\n#swissmade #passwortmanager #passwortmanagement #datenschutz https://t.co/NBO0FvjhFt",
              "urls" : [ "https://t.co/rFUq7MvBMx" ],
              "mediaUrls" : [ "https://t.co/NBO0FvjhFt" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "ALPEIN Software SWISS AG",
              "screenName" : "@ALPEINSoftSWISS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-11-18 10:10:25"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 10:10:27",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 10:10:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 10:10:26",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326863834674556928",
              "tweetText" : "#CallofDuty Modern Warfare ist das Spiel der Stunde. Somit wird die Schlacht um den dritten #digitecPlayground im fiktiven Kriegsgebiet von Verdanks ausgetragen. Melde dich jetzt an, um am Cup teilzunehmen. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 04:06:23"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 04:06:25",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 04:46:29"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 04:46:32",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1327251320659718145",
              "tweetText" : "Neues Stellenangebot: Technischer Leiter Bau (m/w) - Aesch\nUnternehmen: Rofra Bau AG\n#jobs #karriere #baugewerbe #technischerleiter #bau #bautechniker #hochbau #tiefbau",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "aviva selection",
              "screenName" : "@AvivaSelection"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UniBasel"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeit stellen"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "jobsuche stellen"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "job arbeit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "stellen-angebote"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeitsmarkt"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeit job"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 04:10:19"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 04:10:20",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-18 15:31:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-18 15:31:14",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326863834674556928",
              "tweetText" : "#CallofDuty Modern Warfare ist das Spiel der Stunde. Somit wird die Schlacht um den dritten #digitecPlayground im fiktiven Kriegsgebiet von Verdanks ausgetragen. Melde dich jetzt an, um am Cup teilzunehmen. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-19 08:17:20"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-19 08:17:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-19 12:36:38"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-19 12:36:39",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191972811710464",
              "tweetText" : "«Ist mir heute ins Klo gefallen und funktioniert immer noch.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-19 12:34:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-19 12:34:20",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663410014441475",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-20 02:21:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 02:21:55",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324231219345870848",
              "tweetText" : "LANXESS bleibt trotz Belastungen durch die Corona-Krise auf Kurs. Wir haben die Prognose für das Gesamtjahr bestätigt und weiter präzisiert. Der Umsatz im 3. Quartal lag bei 1,46 Mrd. €, das operative Ergebnis (EBITDA pre) bei 193 Mio. €. Details 👇\nhttps://t.co/Ts18MFjXL1",
              "urls" : [ "https://t.co/Ts18MFjXL1" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "LANXESS DE",
              "screenName" : "@LANXESS_DEU"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Chemische Industrie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 15:12:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 15:13:02",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 15:18:17"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 15:18:18",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 21:47:09"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 21:47:12",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329721550451470337",
              "tweetText" : "Sichern Sie sich jetzt vollautomatischen Email-Schutz bereits ab 19,95 € im Jahr.\npEp für Email schützt Unternehmen effizient vor Cyber-Angriffen und DSGVO-Problemen.\nVoll kompatibel mit Exchange, Office 365, G Suite, etc. - Auf Outlook, Mac und Mobile. https://t.co/HWCRjDb3Oj",
              "urls" : [ "https://t.co/HWCRjDb3Oj" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Pipo Langstrumpf",
              "screenName" : "@LangstrumpfPipo"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 21:44:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 23:10:20",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326863834674556928",
              "tweetText" : "#CallofDuty Modern Warfare ist das Spiel der Stunde. Somit wird die Schlacht um den dritten #digitecPlayground im fiktiven Kriegsgebiet von Verdanks ausgetragen. Melde dich jetzt an, um am Cup teilzunehmen. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 23:15:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 23:15:23",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 23:16:28"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 23:16:30",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 19:45:25"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 19:45:27",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-20 19:59:44"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-20 19:59:47",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-21 05:23:39"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-21 05:23:40",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324685462557102080",
              "tweetText" : "Bei vielen unserer Geschäfte zeigen sich inzwischen Anzeichen für eine Wende zum Besseren. Die Nachfrage aus wichtigen Kundenindustrien hat im Vergleich zum zweiten Quartal wieder angezogen. Positive Impulse kommen insbesondere aus China und den USA.\nhttps://t.co/6qVAXiwfk0",
              "urls" : [ "https://t.co/6qVAXiwfk0" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "LANXESS DE",
              "screenName" : "@LANXESS_DEU"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Chemische Industrie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-21 05:22:30"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-21 05:22:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326796928051449856",
              "tweetText" : "Kartentipp: Am 27. November startet die Schnäppchenjagd des Jahres! Denken Sie daran, Ihre Limite rechtzeitig zu erhöhen und gewinnen Sie mit etwas Glück einen von drei @Zalando Gutscheinen im Wert von je 300.– Hier: https://t.co/ynlkTErvym #BlackFriday #limitenerhöhung #viseca https://t.co/OdO1ycsa26",
              "urls" : [ "https://t.co/ynlkTErvym" ],
              "mediaUrls" : [ "https://t.co/OdO1ycsa26" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Viseca Card Services",
              "screenName" : "@Viseca_Cards"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-21 05:16:39"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-21 05:16:42",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1318875882564571136",
              "tweetText" : "Was ist die beste Methode, Kindern etwas beizubringen – gerade zu Covid-Zeiten? Einige Schweizer Klassenzimmer haben den Willen zur digitalen Transformation, andere setzen weiterhin auf Tafel &amp; Kreide. Es ist an der Zeit, altbekannte Methoden gemeinsam zu hinterfragen! https://t.co/hiSu5wVL1C",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/hiSu5wVL1C" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "parenting"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parents"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parent"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "eltern"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-21 22:19:29"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-21 22:19:30",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-21 23:25:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-21 23:25:06",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "domain"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-21 15:16:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-21 15:16:03",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-22 13:13:28"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-22 13:15:47",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-22 23:18:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-22 23:18:06",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1330405655694487552",
              "tweetText" : "From polluting waterways, to poisoning children and using child labour, there have been a number of serious and false allegations made against us. Read the facts here: https://t.co/i9xXP9Axa0",
              "urls" : [ "https://t.co/i9xXP9Axa0" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Glencore",
              "screenName" : "@Glencore"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CreditSuisse"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CSschweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBSschweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UBS"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-22 19:18:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-22 19:19:01",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323663410014441475",
              "tweetText" : "Discover the Brioni Fall/Winter 2020 Collection portrayed by House ambassador Brad Pitt #Brioni #TailoringLegends",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Brioni",
              "screenName" : "@Brioni_Official"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Startups"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Leadership"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer networking"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Gender",
              "targetingValue" : "Men"
            } ],
            "impressionTime" : "2020-11-23 04:39:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-23 04:39:55",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1326214662652878854",
              "tweetText" : "Der Schleier zwischen Leben und Tod ist zerrissen. Wagt euch am 24. November ins Jenseits.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-23 10:44:13"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-23 10:44:15",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329311431750086656",
              "tweetText" : "Mit #SmartMechatroniX in die #FactoryOfTheFuture: Jetzt mehr darüber erfahren, wie du mit #BoschRexroth #Lineartechnik das nächste Level der #Fabrikautomation erreichst: https://t.co/uPY0hyeaOx https://t.co/DpqKI0CRzS",
              "urls" : [ "https://t.co/uPY0hyeaOx" ],
              "mediaUrls" : [ "https://t.co/DpqKI0CRzS" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bosch Rexroth DACH",
              "screenName" : "@RexrothDACH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Science news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technologie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-23 23:18:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-23 23:18:53",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329414520515796993",
              "tweetText" : "Sieh dir jetzt unseren Watch Dogs: Legion Black Friday Deal an! Es ist nie zu spät, sich dem Widerstand anzuschließen!\n#watchdogslegion\nhttps://t.co/YmYkdK4Y4v",
              "urls" : [ "https://t.co/YmYkdK4Y4v" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Watch Dogs: Legion",
              "screenName" : "@watchdogsgame"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Halo"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Call of Duty"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-23 23:52:02"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-23 23:52:04",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329786047081963521",
              "tweetText" : "Fabian und sein Team machen Dinge technisch möglich, die einen klar erkennbaren Mehrwert haben – wie unsere innovative Hypothekarplattform key4 by UBS. Mehr darüber erfahren: https://t.co/cf4i9XsXRJ #FacesofInnovation https://t.co/G1Yuufr4ki",
              "urls" : [ "https://t.co/cf4i9XsXRJ" ],
              "mediaUrls" : [ "https://t.co/G1Yuufr4ki" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Haus"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "immobilien"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "sparen"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-24 00:24:07"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-24 00:24:09",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329095601984860165",
              "tweetText" : "Nachhaltige Engagements sind gefragt. Mit UBS Helpetica, der neuen Plattform für Freiwilligenarbeit, kann man unkompliziert aktiv werden. Dank den UBS Vitainvest Anlagefonds bieten wir zudem auch in der Vorsorge die Möglichkeit, nachhaltig zu investieren. https://t.co/9jTQnIZpfG",
              "urls" : [ "https://t.co/9jTQnIZpfG" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "nachhaltig"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-24 14:01:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-24 14:01:03",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "computer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-24 14:05:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-24 14:06:00",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191972811710464",
              "tweetText" : "«Ist mir heute ins Klo gefallen und funktioniert immer noch.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-24 14:04:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-24 14:05:00",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331162173146607616",
              "tweetText" : "Welche Karte passt zu mir? Unser neues Kartenangebot ist so individuell wie Sie. Digital, sicher, nachhaltig und international: Für jedes Bedürfnis die passende Karte. So bezahlt man heute: https://t.co/UlLYQcunNk https://t.co/UTmPDKk9aZ",
              "urls" : [ "https://t.co/UlLYQcunNk" ],
              "mediaUrls" : [ "https://t.co/UTmPDKk9aZ" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "PayPal"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-24 15:20:40"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-24 15:20:42",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329803406756638734",
              "tweetText" : "Mehr Raum für Ideen! Dank den neuen funktionsintegrierten Profilen von #BoschRexroth! Die komplett innenliegende #Kabelführung erlaubt die sichere &amp; platzsparende Verlegung von Kabeln erstmals durch die #Profilverbindungen hindurch https://t.co/8HeqDpgSO3 https://t.co/xPDqfc2vgm",
              "urls" : [ "https://t.co/8HeqDpgSO3", "https://t.co/xPDqfc2vgm" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bosch Rexroth DACH",
              "screenName" : "@RexrothDACH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Science news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technologie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 02:51:39"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 02:51:41",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1324309194154135555",
              "tweetText" : "Congratulations to @EricVanraes and the team for their work with the Strategic Bond Opportunities Fund, now ranked #1 by @cwireselector for standard deviation and maximum drawdown within the Global Corporates category https://t.co/xGOyTixOwI    \n#CorporateBonds #USTreasuries https://t.co/4QCfyOOiVi",
              "urls" : [ "https://t.co/xGOyTixOwI" ],
              "mediaUrls" : [ "https://t.co/4QCfyOOiVi" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Eric Sturdza Investments",
              "screenName" : "@EricSturdza_Inv"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@nytimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ForbesTech"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@business"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@FinancialTimes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WIRED"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WSJ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CNN"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@TechCrunch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@TheEconomist"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Forbes"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@washingtonpost"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "21 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 03:54:17"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 03:54:19",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1327270851541164032",
              "tweetText" : "Sanitär- und Heizungsleitungen vorgefertigt anzuliefern, ist dank dem Use Case Management von @buildingSMARTIn keine Utopie mehr. Das zeigt @bauendigitalCH im #Swissbau Innovation Lab «on Tour». https://t.co/LUJKJ7YfUH #Swissbau https://t.co/qw2V1R5yu9",
              "urls" : [ "https://t.co/LUJKJ7YfUH" ],
              "mediaUrls" : [ "https://t.co/qw2V1R5yu9" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Swissbau",
              "screenName" : "@Swissbau"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 03:41:49"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 03:41:51",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1328729655340273665",
              "tweetText" : "✨Von einer Schmiede zum digitalen High-tech Unternehmen: #BoschRexroth blickt auf ein 225-jährige, kraftvolle Geschichte zurück. Auch das dritte Unternehmensjahrhundert werden wir mit innovativen Ideen prägen. \nWe keep on moving! 🎶 https://t.co/63qKb4AP5F https://t.co/7YPeMX7miO",
              "urls" : [ "https://t.co/63qKb4AP5F" ],
              "mediaUrls" : [ "https://t.co/7YPeMX7miO" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bosch Rexroth DACH",
              "screenName" : "@RexrothDACH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Science news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technologie"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 03:58:08"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 03:58:09",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 17:19:55"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 17:19:58",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 19:01:49"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 19:01:53",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329799098719997955",
              "tweetText" : "Sie spazieren an einem Hofladen vorbei, haben aber kein Bargeld? Mit TWINT können Sie unterdessen bei mehr als 2’000 Hofläden digital und unkompliziert bezahlen. Bei UBS nutzen bereits mehr als 70% der digitalen Kunden dieses Angebot. Mehr zu TWINT: https://t.co/GS1LVhfyAb https://t.co/YgveDwBtoh",
              "urls" : [ "https://t.co/GS1LVhfyAb" ],
              "mediaUrls" : [ "https://t.co/YgveDwBtoh" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "digital"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-25 18:50:06"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-25 18:50:07",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329095601984860165",
              "tweetText" : "Nachhaltige Engagements sind gefragt. Mit UBS Helpetica, der neuen Plattform für Freiwilligenarbeit, kann man unkompliziert aktiv werden. Dank den UBS Vitainvest Anlagefonds bieten wir zudem auch in der Vorsorge die Möglichkeit, nachhaltig zu investieren. https://t.co/9jTQnIZpfG",
              "urls" : [ "https://t.co/9jTQnIZpfG" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Green solutions"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "guter zweck"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 06:58:54"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 06:58:56",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298633886935781376",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Fragen rund um die E-Mobilität. Entdecken Sie weitere TCS-Leistungen wie Rechtsschutz u.v.m.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "families"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-22 12:00:57"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-22 12:00:59",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1298633886935781376",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Fragen rund um die E-Mobilität. Entdecken Sie weitere TCS-Leistungen wie Rechtsschutz u.v.m.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-22 13:54:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-22 13:54:24",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331162173146607616",
              "tweetText" : "Welche Karte passt zu mir? Unser neues Kartenangebot ist so individuell wie Sie. Digital, sicher, nachhaltig und international: Für jedes Bedürfnis die passende Karte. So bezahlt man heute: https://t.co/UlLYQcunNk https://t.co/UTmPDKk9aZ",
              "urls" : [ "https://t.co/UlLYQcunNk" ],
              "mediaUrls" : [ "https://t.co/UTmPDKk9aZ" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 23:47:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 23:47:34",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 23:50:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 23:50:22",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331162173146607616",
              "tweetText" : "Welche Karte passt zu mir? Unser neues Kartenangebot ist so individuell wie Sie. Digital, sicher, nachhaltig und international: Für jedes Bedürfnis die passende Karte. So bezahlt man heute: https://t.co/UlLYQcunNk https://t.co/UTmPDKk9aZ",
              "urls" : [ "https://t.co/UlLYQcunNk" ],
              "mediaUrls" : [ "https://t.co/UTmPDKk9aZ" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 23:47:31"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 23:47:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191809024184321",
              "tweetText" : "«Funktioniert gut, jedoch haben meine Eltern gedacht ich nehme Drogen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 18:57:47"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 18:57:50",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331883169424044032",
              "tweetText" : "In World of Warcraft: Shadowlands könnt ihr zu dem Helden werden, der in euch schlummert. #shadowlands #warcraft",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW active 2-11"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW churn 2-11-20"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 18:37:36"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 18:37:38",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729141859508224",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 18:29:56"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 18:37:36",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331162173146607616",
              "tweetText" : "Welche Karte passt zu mir? Unser neues Kartenangebot ist so individuell wie Sie. Digital, sicher, nachhaltig und international: Für jedes Bedürfnis die passende Karte. So bezahlt man heute: https://t.co/UlLYQcunNk https://t.co/UTmPDKk9aZ",
              "urls" : [ "https://t.co/UlLYQcunNk" ],
              "mediaUrls" : [ "https://t.co/UTmPDKk9aZ" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-26 20:56:48"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-26 20:56:50",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329937863237009408",
              "tweetText" : "Gehe einen Bund mit dem Symbionten ein ... Wir sind Venom. \n\nSchnapp dir Venom jetzt im Item-Shop!",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Fortnite",
              "screenName" : "@FortniteGame"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 to 34"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 01:24:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 01:24:18",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329799098719997955",
              "tweetText" : "Sie spazieren an einem Hofladen vorbei, haben aber kein Bargeld? Mit TWINT können Sie unterdessen bei mehr als 2’000 Hofläden digital und unkompliziert bezahlen. Bei UBS nutzen bereits mehr als 70% der digitalen Kunden dieses Angebot. Mehr zu TWINT: https://t.co/GS1LVhfyAb https://t.co/YgveDwBtoh",
              "urls" : [ "https://t.co/GS1LVhfyAb" ],
              "mediaUrls" : [ "https://t.co/YgveDwBtoh" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UBS Schweiz",
              "screenName" : "@UBSschweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-27 18:54:45"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-27 18:54:46",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184733245902069760",
              "tweetText" : "Ultraschnelles und sehr sicheres Webhosting abgestimmt auf diverse CMS Anbieter. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "developers"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 09:26:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 09:26:18",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191909779709958",
              "tweetText" : "«Macht viel Spass, die ganze Wohnung mit Hue vollzustopfen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 09:28:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 09:28:51",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "computer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 12:33:40"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 12:33:58",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331883169424044032",
              "tweetText" : "In World of Warcraft: Shadowlands könnt ihr zu dem Helden werden, der in euch schlummert. #shadowlands #warcraft",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW active 2-11"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW churn 2-11-20"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 12:34:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 12:34:02",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331516555209871360",
              "tweetText" : "Du schiesst dir in #CallOfDutyWarzone nur selbst in die Füsse? Wir geben dir professionelle Schützenhilfe, damit du am #digitecPlayground Chancen auf den grossen Sieg hast. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-28 12:24:01"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-28 12:24:17",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1184729702256263168",
              "tweetText" : "Auf der Suche nach Deiner individuellen Domain? Verfügbarkeit prüfen &amp; registrieren. #Hostpoint",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Hostpoint",
              "screenName" : "@hostpoint"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "laptops"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "tvs"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "computer"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 10:58:33"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 10:58:35",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329430052111605761",
              "tweetText" : "Wir investieren das Kapital unserer Kunden, um Unternehmen in ganz Europa dabei zu helfen, sich für die Zukunft nachhaltiger aufzustellen. #Nachhaltigkeit",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BlackRock Switzerland",
              "screenName" : "@BlackRock_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Government"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Government/Education"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "CEO"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 04:58:34"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 04:58:36",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1331516555209871360",
              "tweetText" : "Du schiesst dir in #CallOfDutyWarzone nur selbst in die Füsse? Wir geben dir professionelle Schützenhilfe, damit du am #digitecPlayground Chancen auf den grossen Sieg hast. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 09:40:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 09:40:11",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329394098470486019",
              "tweetText" : "Ihr habt unsere Live-Show #ConnectedHydraulics verpasst? Kein Problem, wir haben einen ersten Sneak Peak für Euch - seht Euch die Highlights an! Die volle Version wird demnächst auf unserem YouTube-Kanal verfügbar sein: https://t.co/Td05zTvGJy #RexrothONAIR #BoschRexroth https://t.co/Joar1zON7Y",
              "urls" : [ "https://t.co/Td05zTvGJy" ],
              "mediaUrls" : [ "https://t.co/Joar1zON7Y" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Bosch Rexroth DACH",
              "screenName" : "@RexrothDACH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Business news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Science news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "technology"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 03:27:28"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 03:27:30",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331516555209871360",
              "tweetText" : "Du schiesst dir in #CallOfDutyWarzone nur selbst in die Füsse? Wir geben dir professionelle Schützenhilfe, damit du am #digitecPlayground Chancen auf den grossen Sieg hast. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 03:33:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 03:33:52",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331883169424044032",
              "tweetText" : "In World of Warcraft: Shadowlands könnt ihr zu dem Helden werden, der in euch schlummert. #shadowlands #warcraft",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW active 2-11"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW churn 2-11-20"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-29 05:05:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-29 05:05:23",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1332347989063569412",
              "tweetText" : "Neues Stellenangebot: GESCHÄFTSFÜHRER/IN ZÜRICH &amp; NORDOSTSCHWEIZ\nUnternehmen: Arnold AG\nArbeitsort: Fehraltorf\n#job #karriere #stelle #direktor #generalmanager #betriebsleiter #manager",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Xeloba",
              "screenName" : "@xeloba_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UZH_ch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Sicherheit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Motivation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Geschäftsführung"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeiten in der schweiz"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-30 02:49:24"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-30 03:03:28",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191909779709958",
              "tweetText" : "«Macht viel Spass, die ganze Wohnung mit Hue vollzustopfen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-30 14:53:15"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-30 14:53:30",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191909779709958",
              "tweetText" : "«Macht viel Spass, die ganze Wohnung mit Hue vollzustopfen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-30 14:53:16"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-30 14:53:19",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331883169424044032",
              "tweetText" : "In World of Warcraft: Shadowlands könnt ihr zu dem Helden werden, der in euch schlummert. #shadowlands #warcraft",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW active 2-11"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW churn 2-11-20"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-30 03:06:51"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-30 03:06:53",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329937863237009408",
              "tweetText" : "Gehe einen Bund mit dem Symbionten ein ... Wir sind Venom. \n\nSchnapp dir Venom jetzt im Item-Shop!",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Fortnite",
              "screenName" : "@FortniteGame"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 to 34"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-11-30 03:03:28"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-11-30 03:03:31",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1333536580032716810",
              "tweetText" : "Der Count Down läuft:\nWir starten wieder mit 24 Fenstern zur Nachhaltigkeit.\nWegweisendes, Nachdenkliches, Überraschendes...\nhttps://t.co/HnTgCUS6g1\n.\nMit etwas Glück gewinnt ihr nachhaltige Geschenke im Quiz. https://t.co/1g3iKvk56D",
              "urls" : [ "https://t.co/HnTgCUS6g1" ],
              "mediaUrls" : [ "https://t.co/1g3iKvk56D" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "24xNachhaltig",
              "screenName" : "@24xNachhaltig"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@konzern_vi"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@umweltnetz"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 02:59:59"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 03:00:01",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331885710681509888",
              "tweetText" : "Nicht den Anforderungen entsprechende E-Mails landen in der Spam-Quarantäne. Unsere E-Mail-Security verhindert, dass wichtige Daten in die falschen Hände gelangen. Mehr dazu: https://t.co/iMgDMPnlk1 https://t.co/DnrrufqmVm",
              "urls" : [ "https://t.co/iMgDMPnlk1" ],
              "mediaUrls" : [ "https://t.co/DnrrufqmVm" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Infortix",
              "screenName" : "@infortix_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "B2B"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "KMU"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 02:21:13"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 02:21:14",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1332347989063569412",
              "tweetText" : "Neues Stellenangebot: GESCHÄFTSFÜHRER/IN ZÜRICH &amp; NORDOSTSCHWEIZ\nUnternehmen: Arnold AG\nArbeitsort: Fehraltorf\n#job #karriere #stelle #direktor #generalmanager #betriebsleiter #manager",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Xeloba",
              "screenName" : "@xeloba_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "Zürich"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Verhalten"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Geschäftsführung"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Mitarbeitermotivation"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "KMU"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeiten in der schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Infrastruktur"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UZH_ch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 01:58:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 01:58:13",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1332276736864751618",
              "tweetText" : "Daniel hat mit 16 Jahren schon Segelflugzeuge durch den Himmel gesteuert. Mit unserem Team steuert er nun die operative Abwicklung von Transaktionen sowie Change-Initiativen. Erfahren Sie jetzt mehr über key4 by UBS: https://t.co/eZyckINcfb #FacesofInnovation https://t.co/175woQfGuA",
              "urls" : [ "https://t.co/eZyckINcfb" ],
              "mediaUrls" : [ "https://t.co/175woQfGuA" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Haus"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "immobilien"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 01:05:52"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 01:05:54",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191909779709958",
              "tweetText" : "«Macht viel Spass, die ganze Wohnung mit Hue vollzustopfen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 06:00:11"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 06:00:12",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331883169424044032",
              "tweetText" : "In World of Warcraft: Shadowlands könnt ihr zu dem Helden werden, der in euch schlummert. #shadowlands #warcraft",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "World of Warcraft",
              "screenName" : "@Warcraft"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "World of Warcraft"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW active 2-11"
            }, {
              "targetingType" : "List",
              "targetingValue" : "WoW churn 2-11-20"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 05:50:58"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 05:51:00",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1329430052111605761",
              "tweetText" : "Wir investieren das Kapital unserer Kunden, um Unternehmen in ganz Europa dabei zu helfen, sich für die Zukunft nachhaltiger aufzustellen. #Nachhaltigkeit",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BlackRock Switzerland",
              "screenName" : "@BlackRock_CH"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Government"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Politics"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Entrepreneurship"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Government/Education"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 03:42:53"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 03:42:58",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1332276736864751618",
              "tweetText" : "Daniel hat mit 16 Jahren schon Segelflugzeuge durch den Himmel gesteuert. Mit unserem Team steuert er nun die operative Abwicklung von Transaktionen sowie Change-Initiativen. Erfahren Sie jetzt mehr über key4 by UBS: https://t.co/eZyckINcfb #FacesofInnovation https://t.co/175woQfGuA",
              "urls" : [ "https://t.co/eZyckINcfb" ],
              "mediaUrls" : [ "https://t.co/175woQfGuA" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "immobilien"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investment"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "Haus"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "investments"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 03:19:18"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 03:19:20",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331516555209871360",
              "tweetText" : "Du schiesst dir in #CallOfDutyWarzone nur selbst in die Füsse? Wir geben dir professionelle Schützenhilfe, damit du am #digitecPlayground Chancen auf den grossen Sieg hast. #digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 03:43:06"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 03:43:07",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1330786232381661185",
              "tweetText" : "Event-Psychologie: Vier sofort wirksame Erfolgs-Booster für digitale Events. Jetzt lesen auf: https://t.co/9lMGNTq7I5 #VirtualEvent #DigitalEvent #Eventmanagement #Marketing #Psychologie #Event https://t.co/9yWRxfJdKA",
              "urls" : [ "https://t.co/9lMGNTq7I5" ],
              "mediaUrls" : [ "https://t.co/9yWRxfJdKA" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "AdCoach",
              "screenName" : "@adcoach_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "event"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "eventprofs"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "25 to 54"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-02 01:42:22"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-02 01:42:30",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1323191909779709958",
              "tweetText" : "«Macht viel Spass, die ganze Wohnung mit Hue vollzustopfen.» \n\n#digitec",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "digitec",
              "screenName" : "@digitec_de"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-02 02:41:10"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-02 02:41:13",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1333364087301033985",
              "tweetText" : "Mit einer Hypothek von key4 by UBS sparen Sie monatlich Geld: dank smartem Anbietermix für attraktive Zinsen. Simona kann so am See wohnen. ⛰ Wovon träumen Sie? \n\nhttps://t.co/4H63dqfA3J https://t.co/FjC5xsWW2s",
              "urls" : [ "https://t.co/4H63dqfA3J" ],
              "mediaUrls" : [ "https://t.co/FjC5xsWW2s" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-01 18:07:04"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-01 18:07:06",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1331957596526403586",
              "tweetText" : "This year, give yourself the gift of more streaming, gaming and video calls! 600 Mbit/s incl. UPC TV App now for just CHF 39 a month.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "UPC Schweiz",
              "screenName" : "@UPC_Switzerland"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-03 06:24:30"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-03 06:24:32",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1318875882564571136",
              "tweetText" : "Was ist die beste Methode, Kindern etwas beizubringen – gerade zu Covid-Zeiten? Einige Schweizer Klassenzimmer haben den Willen zur digitalen Transformation, andere setzen weiterhin auf Tafel &amp; Kreide. Es ist an der Zeit, altbekannte Methoden gemeinsam zu hinterfragen! https://t.co/hiSu5wVL1C",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/hiSu5wVL1C" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Michael In Albon, Jugendmedienschutz-Beauftragter",
              "screenName" : "@MichaelInAlbon"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BMJV_Bund"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@netzpolitik"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BMFSFJ"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parenting"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "familie"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parents"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "parent"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 to 49"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-23 01:06:00"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-23 01:06:02",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1318896992458768397",
              "tweetText" : "Neues Stellenangebot: \nGeschäftsführerin / Geschäftsführer KMU\nArbeitsort: Nähe Stadt Zürich\n#job #karriere #stelle #geschäftsführung #direktor #ceo #finanzbranche #digitalisierung",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Andreas Martin",
              "screenName" : "@andreas_martin1"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@RailService"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SFV_ASF"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@UZH_ch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Handelszeitung"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@srf3"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@ETH"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@InsideParade"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tagesanzeiger"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@20min"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BILANZ"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@CH_Wochenende"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Blickch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@BeobachterRat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@srfnews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZSchweiz"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@watson_news"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NZZaS"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Swisscom_de"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@SRF"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "stellen-angebote"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "ich suche arbeit"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "suche arbeit in der schweiz"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "job finden"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "arbeiten in der schweiz"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-23 01:07:39"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-23 01:07:40",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1298634465162465280",
              "tweetText" : "Übrigens, wir helfen nicht nur bei Autopannen. Weitere TCS-Leistungen: E-Mobilitäts-Expertise, ÖV-, Rechts- und Reiseschutz.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Touring Club Schweiz",
              "screenName" : "@TCS_Schweiz"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Auto Manufacturer"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "auto"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "families"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-22 18:18:47"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-22 18:18:48",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1336332938133131264",
              "tweetText" : "Die besonderen und wichtigen Momente in diesem Jahr haben wir nicht verpasst, nur waren sie anders als gewohnt. In einer Zeit, in der Social Distancing die neue Norm geworden ist, helfen wir dabei mit den Menschen in Kontakt zu bleiben, die uns wichtig sind.  #KeepOnConnecting",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Huawei Mobile",
              "screenName" : "@HuaweiMobile"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Samsung USA"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Android"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Photos"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Cloud"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Samsung Mobile"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Samsung Mobile US"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Sony"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Chrome"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Samsung "
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Financial Technology"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Samsung Mobile UK"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Maps"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Cast"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google Chromecast"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Google "
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Technology"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Tech news"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer programming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer reviews"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WIRED"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@engadget"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            }, {
              "targetingType" : "Platforms",
              "targetingValue" : "Desktop"
            } ],
            "impressionTime" : "2020-12-21 01:03:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-21 01:03:52",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1341348196438687745",
              "tweetText" : "Der perfekte Platz, um auch mal abzuschalten. Mehr Golf als je zuvor. Der neue Golf Alltrack. \n\n#mehrgolfalsjezuvor #vwgolfvariant #volkswagen \n\nVideo zeigt Sonderausstattungen gegen Mehrpreis.\n\nWeitere Informationen: https://t.co/ZZVViDojVE",
              "urls" : [ "https://t.co/ZZVViDojVE" ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Volkswagen News",
              "screenName" : "@volkswagen"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Toyota"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "18 and up"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Germany"
            } ],
            "impressionTime" : "2020-12-26 06:37:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-26 06:37:43",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1341166796318015490",
              "tweetText" : "RT @disneyplus: Bis zum 31. Dezember 2020 schenkt Fortnite dir 30 Tage Disney+, wenn du im Spiel etwas mit Echtgeld kaufst. Mit Ablauf des…",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "publisherInfo" : {
              "publisherName" : "Fortnite",
              "screenName" : "@FortniteGame"
            },
            "advertiserInfo" : {
              "advertiserName" : "Fortnite",
              "screenName" : "@FortniteGame"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Gaming news and general info"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Computer gaming"
            }, {
              "targetingType" : "Interests",
              "targetingValue" : "Online gaming"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Fortnite"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "13 to 49"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Germany"
            } ],
            "impressionTime" : "2020-12-26 06:38:32"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-26 06:38:33",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "SearchTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1335940600122789890",
              "tweetText" : "Da ist für jeden Geschmack etwas dabei: Jetzt mit noch mehr Auswahl und monatlich kündbar. MagentaTV - das beste TV Deutschlands.\n#MehrFürAlle #MagentaTV #erlebenwasverbindet  #wirsindmagenta",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "MagentaTV",
              "screenName" : "@MagentaTV"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Facebook"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@tumblr"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@YouTube"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitter"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Pinterest"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Twitch"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Snapchat"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@netflix"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@instagram"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@Skype"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@WhatsApp"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Germany"
            } ],
            "impressionTime" : "2020-12-26 06:41:37"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-26 06:41:38",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "ProfileTweets",
            "promotedTweetInfo" : {
              "tweetId" : "1331274642661789696",
              "tweetText" : "Ihr Lieblingsstück online schnell und einfach versichern.",
              "urls" : [ ],
              "mediaUrls" : [ ]
            },
            "advertiserInfo" : {
              "advertiserName" : "BaloiseCH",
              "screenName" : "@baloise_ch"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Computer reviews"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Insurance"
            }, {
              "targetingType" : "Conversation topics",
              "targetingValue" : "Insurance"
            }, {
              "targetingType" : "Age",
              "targetingValue" : "35 and up"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "English"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-12-30 10:37:41"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-12-30 10:37:42",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319346479685898240",
              "tweetText" : "Für das Kino Cinewil in Wil ist die Qualität der Reinigung zentral. Dazu gehört auch, dass sich die Reinigungsfachkräfte gut auf Deutsch verständigen können. https://t.co/ogmIG1lwdt #GAVLehrgang #Reinigungsbranche https://t.co/2F9SmwKowy",
              "urls" : [ "https://t.co/ogmIG1lwdt" ],
              "mediaUrls" : [ "https://t.co/2F9SmwKowy" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Reine Profis",
              "screenName" : "@reineprofis"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NataschaWey"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@salesman_ch"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-23 15:44:13"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-23 15:44:15",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319346479685898240",
              "tweetText" : "Für das Kino Cinewil in Wil ist die Qualität der Reinigung zentral. Dazu gehört auch, dass sich die Reinigungsfachkräfte gut auf Deutsch verständigen können. https://t.co/ogmIG1lwdt #GAVLehrgang #Reinigungsbranche https://t.co/2F9SmwKowy",
              "urls" : [ "https://t.co/ogmIG1lwdt" ],
              "mediaUrls" : [ "https://t.co/2F9SmwKowy" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "Reine Profis",
              "screenName" : "@reineprofis"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@NataschaWey"
            }, {
              "targetingType" : "Follower look-alikes",
              "targetingValue" : "@salesman_ch"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-23 20:29:15"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-23 20:29:17",
            "engagementType" : "ChargeableImpression"
          } ]
        }, {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319532976292745216",
              "tweetText" : "Festhypothek oder lieber SARON – was ist eigentlich der Unterschied? Unser Hypothekenexperte &amp; Berater Lukas Ammann gibt Ihnen alle Infos &amp; Insights zur Festhypothek. Was SARON ist &amp; welche Vorteile es bringt, erfahren Sie am Montag. #makingbankingsimple #Festhypothek https://t.co/fwuLhrdzBb",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/fwuLhrdzBb" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Keywords",
              "targetingValue" : "immobilien"
            }, {
              "targetingType" : "Keywords",
              "targetingValue" : "sparen"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-23 17:04:50"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-23 18:10:39",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
}, {
  "ad" : {
    "adsUserData" : {
      "adEngagements" : {
        "engagements" : [ {
          "impressionAttributes" : {
            "deviceInfo" : {
              "osType" : "Desktop"
            },
            "displayLocation" : "TimelineHome",
            "promotedTweetInfo" : {
              "tweetId" : "1319655027020005376",
              "tweetText" : "Das lang ersehnte Traumhaus soll nicht länger ein Luftschloss bleiben. Demnächst beraten wir nämlich nicht nur Ablöser, sondern helfen auch Neukäufern dabei, Hauseigentümer zu werden. \n \nWorauf legen Sie Wert bei einer Hypotheken-Beratung? \n#dreambig #eigenheim https://t.co/HWsHdex4IA",
              "urls" : [ ],
              "mediaUrls" : [ "https://t.co/HWsHdex4IA" ]
            },
            "advertiserInfo" : {
              "advertiserName" : "key4 by UBS",
              "screenName" : "@key4byUBS"
            },
            "matchedTargetingCriteria" : [ {
              "targetingType" : "Interests",
              "targetingValue" : "Financial planning"
            }, {
              "targetingType" : "Languages",
              "targetingValue" : "German"
            }, {
              "targetingType" : "Locations",
              "targetingValue" : "Switzerland"
            } ],
            "impressionTime" : "2020-10-24 16:13:21"
          },
          "engagementAttributes" : [ {
            "engagementTime" : "2020-10-24 16:13:23",
            "engagementType" : "ChargeableImpression"
          } ]
        } ]
      }
    }
  }
} ]