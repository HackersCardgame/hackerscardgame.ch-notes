window.YTD.direct_messages.part0 = [ {
  "dmConversation" : {
    "conversationId" : "14252828-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Irgendwo sind wir uns (im Chaos-Umfeld?) auch schonmal IRL über den Weg gelaufen. Das Gesicht auf Twitter kommt mir jedenfalls bekannt vor. Vielleicht sollten wir uns mal IRL treffen (vielleicht am CoSin oder Technik-Camp Bodensee?), das macht's sicher einfacher. Kannst Dich gerne auch per E-Mail an abe@debian.org melden. Twitter schaue ich nur sehr unregelmässig rein.",
        "mediaUrls" : [ ],
        "senderId" : "14252828",
        "id" : "1134186156105580548",
        "createdAt" : "2019-05-30T19:53:46.481Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "15840592-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/cAVB7Vk3v3",
          "expanded" : "http://rluettke.bplaced.net/",
          "display" : "rluettke.bplaced.net"
        }, {
          "url" : "https://t.co/BeQkEs1jdr",
          "expanded" : "https://www.xing.com/profile/Robert_Luettke",
          "display" : "xing.com/profile/Robert…"
        } ],
        "text" : "seine seite? https://t.co/cAVB7Vk3v3 und sein linkedIN / xing? https://t.co/BeQkEs1jdr ersteres sicher, letzteres müsstest Du ja wissen da Du ihn kennst...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217972485452455941",
        "createdAt" : "2020-01-17T00:50:44.076Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich weiss leider nichts weil wir seit 2014 nicht mehr gesprochen haben. es kann sein, dass er gerade nicht in der psychiatrie ist",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217969641227718660",
        "createdAt" : "2020-01-17T00:39:25.958Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "noch eine frage, weisst du grad in welcher psychiatrie er ist / war, bzw sonst frage ich ihn einfach....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217967590682824708",
        "createdAt" : "2020-01-17T00:31:17.070Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "urks :D ne, das leckere. ohne fisch. :-)",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217966182076207110",
        "createdAt" : "2020-01-17T00:25:41.217Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hoffentlich nicht sushi im glas (ZacMc Cracken)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217965844820566021",
        "createdAt" : "2020-01-17T00:24:20.827Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich bin mal afk, wir machen sushi",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217965735403802634",
        "createdAt" : "2020-01-17T00:23:54.721Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217965412874432523",
        "createdAt" : "2020-01-17T00:22:37.823Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok.  die email die ich benutzt hab ist\ncontact@leonard-ritter.com",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217965327147044868",
        "createdAt" : "2020-01-17T00:22:17.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hast du mir noch Deine Email? Es ist für patienten echt kontraproduktiv, wenn sie merken, dass hintendruch geredet wird, das fördert deren paranoia, ich würde dich dann ins cc tun, mit offenen karten spielen hilft patieten",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217965006442192901",
        "createdAt" : "2020-01-17T00:21:00.919Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du hast da sicher recht.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217961311058960392",
        "createdAt" : "2020-01-17T00:06:19.872Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich denke es ist möglich, \"zu\" intelligent zu werden. also so intelligent, dass man schon wieder blöd ist.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217961172886007818",
        "createdAt" : "2020-01-17T00:05:46.936Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich denke intelligenz ist nur die halbe story, es kommt auch drauf an von wem und mit welchen quellen man \"gefüttert\" wird um sich richtig viel ärger einzuhandeln...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217960990010150916",
        "createdAt" : "2020-01-17T00:05:03.328Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "er ist definitiv klug aber kein megabrain",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217960747688431622",
        "createdAt" : "2020-01-17T00:04:05.566Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein aber er spielt Kontrabass, vor allem Jazz",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217960137899565065",
        "createdAt" : "2020-01-17T00:01:40.168Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also der bruder von deiner frau",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217960082367045637",
        "createdAt" : "2020-01-17T00:01:26.935Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "auch informatik begabt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217960041778688004",
        "createdAt" : "2020-01-17T00:01:17.276Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Liebeskummer ist sehr prägend. Das Mädchen aus dem Lied, was du zitiert hast, dem hab ich sieben Jahre hinterhergetrauert.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217959835674775556",
        "createdAt" : "2020-01-17T00:00:28.146Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Er raucht auch sehr viel.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217959724299292677",
        "createdAt" : "2020-01-17T00:00:01.563Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "von meiner Frau ihr Bruder wurde auch mit Schizophrenie diagnostiziert. Wenn er seine Medikamente nicht nimmt, fällt er zurück in Sachen die schon lang vorbei sind und versucht eine Frau zu kontaktieren die er vor über 10 Jahren das letzte Mal gesehen hat.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217959408027820036",
        "createdAt" : "2020-01-16T23:58:46.155Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, das wird mit der Behandlung auch besser.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217959105194795013",
        "createdAt" : "2020-01-16T23:57:33.959Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958971111366660",
        "createdAt" : "2020-01-16T23:57:01.988Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja um deinen Track zu zitieren auch so ein \"ewiger pfeil in meinem Brustkorb\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217958940903968773",
        "createdAt" : "2020-01-16T23:56:54.805Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hexe -&gt; häckse ist witzig :)",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958904044359684",
        "createdAt" : "2020-01-16T23:56:45.999Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wen meinst du?",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958803183939592",
        "createdAt" : "2020-01-16T23:56:21.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wer ist deine Herrin?",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958784020242438",
        "createdAt" : "2020-01-16T23:56:17.382Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hör grad aus der regie ich dürfe meine herrin nicht häckse nennen... :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217958448383627270",
        "createdAt" : "2020-01-16T23:54:57.366Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Siehst du.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958111325118470",
        "createdAt" : "2020-01-16T23:53:36.998Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "viel zu viel",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217958095466500101",
        "createdAt" : "2020-01-16T23:53:33.277Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Rauchst du?",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958076134952964",
        "createdAt" : "2020-01-16T23:53:28.615Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "huch, zum glück sind diese leitungen unidirektional... aber ich werde mal deinem kollegen mailen, das medizin zeugs macht meine unibas häckse...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217958063577223172",
        "createdAt" : "2020-01-16T23:53:25.622Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es gibt einen Indikator für die Infektion: Leute, die infiziert wurden, werden oft starke Raucher.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217958048444174340",
        "createdAt" : "2020-01-16T23:53:22.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie sonst kannst du deine Arme und Beine bewegen.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957749176328196",
        "createdAt" : "2020-01-16T23:52:10.671Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, definitiv.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957606934958092",
        "createdAt" : "2020-01-16T23:51:36.742Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es gibt auch spezialisierte Psychopharmaka die dagegen helfen können, aber die sind unausgereift.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957575108460548",
        "createdAt" : "2020-01-16T23:51:29.162Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "überwinden nervenbahnen die bluthirnschranke?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217957564341792772",
        "createdAt" : "2020-01-16T23:51:26.595Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du bist nicht der einzige, der infiziert wurde. Neurologen kämpfen gegen diese Bakterien.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957469126909956",
        "createdAt" : "2020-01-16T23:51:03.898Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es gibt kein völlig wirksames Medikament dagegen, aber Antibiotika scheinen zu helfen",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957351573147652",
        "createdAt" : "2020-01-16T23:50:35.867Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "In deinem Gehirn bilden sie giftige Proteine und stören deine Gedankenabläufe",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957126892589061",
        "createdAt" : "2020-01-16T23:49:42.302Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Völlig narbenfrei",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957055534858244",
        "createdAt" : "2020-01-16T23:49:25.278Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Falls es dir hilft, stell dir einfach vor der NSA hat Bakterien mit CRISPR genetisch verändert so dass sie die Blut-Gehirn Schranke überwinden können",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217957039697289222",
        "createdAt" : "2020-01-16T23:49:21.507Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "eben ab hier kann es ungemütlich werden, loop { faildef spinn }",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217956950522126345",
        "createdAt" : "2020-01-16T23:49:00.240Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Junge",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217956835669479428",
        "createdAt" : "2020-01-16T23:48:32.874Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/FyyShnpBsL",
          "expanded" : "https://twitter.com/messages/media/1217956788907233290",
          "display" : "pic.twitter.com/FyyShnpBsL"
        } ],
        "text" : "falsche frage, wie \"minimalinvasiv\" und in welchem zeitraum lässt sich so etwas montieren, die auf der münze, und müsste man den schädel aufschneide, oder käme man z.B. über die nase auch da rein? https://t.co/FyyShnpBsL",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1217956788907233290/1217956782972309506/uzzNKbhW.jpg" ],
        "senderId" : "75128838",
        "id" : "1217956788907233290",
        "createdAt" : "2020-01-16T23:48:21.893Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wir sind alle aus Materie, und so sind es auch Schimmelpilze und Bakterien",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217956721076973572",
        "createdAt" : "2020-01-16T23:48:05.538Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Welt besteht eben nicht nur aus Menschen, die sich gegenseitig böses antun",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217956556748263429",
        "createdAt" : "2020-01-16T23:47:26.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was ist plausibler? Dass man dir bei geheimen Versuchen etwas zur Gedankenkontrolle implantiert hat, oder dass du an einer Gehirninfektion erkrankt bist?",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217956372236636164",
        "createdAt" : "2020-01-16T23:46:42.364Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "drogen ist eine objektfixierung die vom implantat ablnekt, auch viele medikamente",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217955999606362117",
        "createdAt" : "2020-01-16T23:45:13.538Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und achtung ab dieser information wird es allenfalls ungemühtlihc",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217955928496058372",
        "createdAt" : "2020-01-16T23:44:56.569Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hatte auch mal ne kurze drogeninduzierte Psychose, die mich in die geschlossene Abteilung gebracht hat für drei Tage, aber ich habs wieder hinbekommen.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217955891716161541",
        "createdAt" : "2020-01-16T23:44:47.813Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schweizer Psychiatien (sie en.wikipedia, donald ewen cameron, CTRL-F burghölzli) haben an Verdingkindern / Schwabenkindern (hiess es in detuschalnd) medikamentenexperimente gemacht (so die offizielle meldung in der presse) ich denke aber da wurden ab 1950 auch experimente mit stimoceiver was glaub dieses jahr mit elon musks neuralink auf den markt kommt. was war dazwischen???",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217955800318119940",
        "createdAt" : "2020-01-16T23:44:26.031Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Er hat mich kontaktiert weil er wusste dass ich auch was im Hirn habe.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217955433438117892",
        "createdAt" : "2020-01-16T23:42:58.545Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "er sagt er schätzt sich auf über 150, und das glaube ich ihm auch. was genau das problem ist.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217955202466242564",
        "createdAt" : "2020-01-16T23:42:03.473Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hat er mal einen IQ test gemacht?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217954974338027524",
        "createdAt" : "2020-01-16T23:41:09.084Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "----",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217954530521952266",
        "createdAt" : "2020-01-16T23:39:23.278Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Einem der drin war, glaubt man unangehmes normal nicht mehr.\nWeil ich das vorher wusste, habe ich das trotzdem hinbekommen. Quasi aus Langeweile. Ich sag mal so, ich wollte Drogen kriegen. Legal. Hat nicht geholfen. Ich denke immer so quer und schnell.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217954517570007046",
        "createdAt" : "2020-01-16T23:39:20.182Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nur ging das 6+ Jahre. Ich habe im Prinzip ein riesiges soziales Gedankenexperiment gemacht, mit über 200 Beteiligten. Es war wie einmal in die Matrix rein und wieder raus, allerdings war die Matrix die Psychiatrie.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217954200379887625",
        "createdAt" : "2020-01-16T23:38:04.567Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mein Leben ist eine Mischung aus Matrix und Sherlock Holmes \"Reichenbach Fall\".",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217954080213041156",
        "createdAt" : "2020-01-16T23:37:35.904Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(das ist nur ein auszug)",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217954072927576068",
        "createdAt" : "2020-01-16T23:37:34.177Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "er hat mir damals geschrieben:",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217953527206752260",
        "createdAt" : "2020-01-16T23:35:24.062Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217953456041943045",
        "createdAt" : "2020-01-16T23:35:07.108Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, besser ist es. er weiss es durch meine ankündigung.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217953426425942024",
        "createdAt" : "2020-01-16T23:35:00.041Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "soll / darf ich erwähnen dass du mri die email adresse gegeben hast?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217953346184732676",
        "createdAt" : "2020-01-16T23:34:40.897Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber die Idee ist gut.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217953220120731652",
        "createdAt" : "2020-01-16T23:34:10.842Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Karte, die du verlinkt hast, enthält viel zu viele Informationen %)",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217953208921993220",
        "createdAt" : "2020-01-16T23:34:08.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "15840592",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/1vrcCsV0Xa",
          "expanded" : "https://twitter.com/messages/media/1217953133416132614",
          "display" : "pic.twitter.com/1vrcCsV0Xa"
        } ],
        "text" : "mein hackerscardgame ist so etwas wie ein \"geekcode\" einzelne kraten machen kenen sinn, mein lebensweg bisher... https://t.co/1vrcCsV0Xa",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1217953133416132614/1217953129070809088/R1RHr8aQ.jpg" ],
        "senderId" : "75128838",
        "id" : "1217953133416132614",
        "createdAt" : "2020-01-16T23:33:50.266Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hab ihm nochmal geschrieben, und ihm gesagt dass ich dir seine email gegeben hab, damit er bescheid weiss.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217952951219650565",
        "createdAt" : "2020-01-16T23:33:06.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "seine e-mail ist rluettke@gmail.com",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217952816234401797",
        "createdAt" : "2020-01-16T23:32:34.551Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Er hat mich so 2014 angeschrieben, seit dem ist sehr viel Zeit vergangen.",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217952762434027529",
        "createdAt" : "2020-01-16T23:32:21.748Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sein Name ist Robert",
        "mediaUrls" : [ ],
        "senderId" : "15840592",
        "id" : "1217952716263129095",
        "createdAt" : "2020-01-16T23:32:10.716Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-94813779",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also richtige Facebook-Mails oder Mails die nicht wirklich von Facebook stammen?",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1177109676850388996",
        "createdAt" : "2019-09-26T06:36:31.290Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich denke ich hab mich nur \"zu schnell bewegt\" um es esotherisch auszudrücken und die \"netten leute\" versuchen bei mir das PTBS zu reaktivieren, mich einzuschüchtern, und gestern haben die noch mein laptop gebrickt, also habe ich heute irgend 200 nicht bearbeitete mails... und seher viel \"zufälligen\" spam der lustigerweise von facebook kommt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1177076096489152516",
        "createdAt" : "2019-09-26T04:23:05.171Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "🙂",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176409601924837380",
        "createdAt" : "2019-09-24T08:14:40.431Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1176409555758198788",
        "createdAt" : "2019-09-24T08:14:29.420Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":-)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176409543426936836",
        "createdAt" : "2019-09-24T08:14:26.474Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176409530059694084",
        "createdAt" : "2019-09-24T08:14:23.329Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, habe den Tweet gesehen  :-&amp;\ndeshalb frage ich ja\n\nich wünsche Dir viel Kraft!",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176409520047906820",
        "createdAt" : "2019-09-24T08:14:20.900Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/hN1KxqPxI0",
          "expanded" : "https://twitter.com/FailDef/status/1176398193556447232",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "ja danke der Nachfrage, hab wohl irgend ein kleineres PTBS: https://t.co/hN1KxqPxI0",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1176408548877766660",
        "createdAt" : "2019-09-24T08:10:29.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wieder alles okay bei Dir? :-/\nDu wirkst gerade recht verschreckt...",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176406171986644996",
        "createdAt" : "2019-09-24T08:01:02.663Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/SviIUZ5Guu",
          "expanded" : "https://twitter.com/FailDef/status/1175887632733265921",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "ich war in den Ferien und habe Deine Twitter-Anfrage erst jetzt gesehen\nkann Dir auf Anhieb keine Antwort geben; müsste mich vertieft damit befassen\nhttps://t.co/SviIUZ5Guu",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1176199682344849411",
        "createdAt" : "2019-09-23T18:20:31.729Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "super danke für den Hinweis :)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1103738781738434565",
        "createdAt" : "2019-03-07T19:26:46.738Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/lXLVIRz6Vw",
          "expanded" : "https://twitter.com/katierosman/status/1103109217626415105",
          "display" : "twitter.com/katierosman/st…"
        } ],
        "text" : "hey das wär ja allenfalls auch noch ein bild dass in deinen Vortrg zum thema \"unterbrechung durch Phone\" passen würde... https://t.co/lXLVIRz6Vw",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1103737695245910024",
        "createdAt" : "2019-03-07T19:22:27.766Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das mit dem Zert: muss mal erwägen einen andere Lösung zu suchen\nWeiss nicht ob das mein Anbieter ohne weiteres unterstützt",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1065656878565416964",
        "createdAt" : "2018-11-22T17:22:53.512Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, ich weiss\nMeine Websites müsste ich dringend überarbeiten",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1065656736751775749",
        "createdAt" : "2018-11-22T17:22:19.713Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pin-nnen hab ich eigentlich nicht gemeint sondern wirklich deine Homepage. da fehlt im übrigen das zertifikat :) und das aktuelle twitter foto finde ich auch besser als das auf deiner webseite... :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1065615354934759428",
        "createdAt" : "2018-11-22T14:37:53.532Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1065481826884571140",
        "createdAt" : "2018-11-22T05:47:17.926Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gute Idee. Done :)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1065481808110919690",
        "createdAt" : "2018-11-22T05:47:13.438Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du meinst gepinnt?",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1065481760744587268",
        "createdAt" : "2018-11-22T05:47:02.161Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Coole Tweets. hey, mach doch diesen letzten Multi-Tweets-Tweet zu Deiner Homepage...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1065378912128167941",
        "createdAt" : "2018-11-21T22:58:21.186Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1006104106174484484",
        "createdAt" : "2018-06-11T09:21:26.123Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "habe Raoul nochmal angeschrieben",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006103025214197765",
        "createdAt" : "2018-06-11T09:17:08.393Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MH6symj782",
          "expanded" : "https://www.cosin.ch/fahrplan/2018/events/113.html",
          "display" : "cosin.ch/fahrplan/2018/…"
        } ],
        "text" : "https://t.co/MH6symj782",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1006100455359959046",
        "createdAt" : "2018-06-11T09:06:55.719Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sowie das Freitag ungünstig sei und ich deshalb verschieben sollte",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006100443372621828",
        "createdAt" : "2018-06-11T09:06:52.845Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "er hat mir gesagt Sonntag gegen 11:15",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006100369590640644",
        "createdAt" : "2018-06-11T09:06:35.240Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann hat Raoul denn noch nicht verschoben",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006100334958260229",
        "createdAt" : "2018-06-11T09:06:26.993Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, aber dein vortrag ist Freitag eingetragen???",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1006100155144302596",
        "createdAt" : "2018-06-11T09:05:44.118Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich werde einfach am Sonntag vormittag kommen\nich bin recht ausgebucht mit meiner masterarbeit dieses Weekend",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006099917197193220",
        "createdAt" : "2018-06-11T09:04:47.388Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, dann reist du am freitag zurück und samstag wieder hin?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1006099710879428613",
        "createdAt" : "2018-06-11T09:03:58.203Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Biel: Vermutlich nicht",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1006099601731047428",
        "createdAt" : "2018-06-11T09:03:32.186Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(Y)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1002122794216427524",
        "createdAt" : "2018-05-31T09:41:07.425Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "94813779",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Wc8HfJ9eQ1",
          "expanded" : "https://twitter.com/messages/media/1001877528859086852",
          "display" : "pic.twitter.com/Wc8HfJ9eQ1"
        } ],
        "text" : " https://t.co/Wc8HfJ9eQ1",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1001877528859086852/1001877520889864193/sxOs8svS.jpg" ],
        "senderId" : "75128838",
        "id" : "1001877528859086852",
        "createdAt" : "2018-05-30T17:26:31.695Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ij1PFp2Jy1",
          "expanded" : "https://twitter.com/FailDef/status/1001510321838845952",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "https://t.co/ij1PFp2Jy1\nund was war die Antwort? ;)",
        "mediaUrls" : [ ],
        "senderId" : "94813779",
        "id" : "1001735167709515780",
        "createdAt" : "2018-05-30T08:00:50.037Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-119799252",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xFKrip49Mn",
          "expanded" : "https://twitter.com/FailDef/status/747895928493867008",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "⚠ https://t.co/xFKrip49Mn ☠: ★★★★★ (5/5)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "747896419055509508",
        "createdAt" : "2016-06-28T20:56:16.267Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich mutmasse (sorry welcher Ghostwriter formuliert so geschwollen) dass man allenfalls ein handelsübliches handy zu ner basis station oder imsi-catcher upgraden kann, aber dafür bin ich zu wenig 1337 deshalb hab ich das mal dem denis geschickt, dem seine master arbeit war so was in diese richtung...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "719661686634999811",
        "createdAt" : "2016-04-11T23:01:31.517Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, mit ner Basisstation kann ich wenig anfangen...",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "719410577165197316",
        "createdAt" : "2016-04-11T06:23:42.348Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/bFexrDpERH",
          "expanded" : "https://www.facebook.com/marc.landolt.9/posts/10209485056470256?pnref=story",
          "display" : "facebook.com/marc.landolt.9…"
        } ],
        "text" : "Idee: Spielzeug wo die ZH-Piraten und BE-Piraten zusammen damit spielen könnten: https://t.co/bFexrDpERH",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "718622150924640260",
        "createdAt" : "2016-04-09T02:10:46.843Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "570317431048179714",
        "createdAt" : "2015-02-24T20:20:46.235Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Viel Spass :3 Und bitte mich nicht erwähnen, danke.",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "570316238108426241",
        "createdAt" : "2015-02-24T20:16:01.812Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mal bewerben, TNX",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "570315585025937411",
        "createdAt" : "2015-02-24T20:13:26.118Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/zLuVWpCx9k",
          "expanded" : "https://pub.refline.ch/845721/3726/++publications++/2/index.html",
          "display" : "pub.refline.ch/845721/3726/++…"
        } ],
        "text" : "https://t.co/zLuVWpCx9k",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "570315161074065408",
        "createdAt" : "2015-02-24T20:11:45.045Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Erst. Zuvor hatte ich schon mal genippt. Übrigensbin absolut Troll freundlich: Interaktion ist förderlich für die interagierenden individuen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "492820170612961281",
        "createdAt" : "2014-07-25T23:54:19.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ach so :3 erste Mate?",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "492818816834220033",
        "createdAt" : "2014-07-25T23:48:56.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja auf dem Parkplatz als du den Mate Kasten davon getragen hast.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "492818618296848384",
        "createdAt" : "2014-07-25T23:48:09.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich hab dir ne gratis Mate gegeben?",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "492817397691453440",
        "createdAt" : "2014-07-25T23:43:18.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ahhh Psycho-Ecke, übrigens nochmals danke für das Gratis Mate von der PV in Aarau-Rohr...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "492800719276474369",
        "createdAt" : "2014-07-25T22:37:01.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich komm selber aus der \"Psycho-Ecke\" :3 Lass dich nicht trollen",
        "mediaUrls" : [ ],
        "senderId" : "119799252",
        "id" : "492800137022824449",
        "createdAt" : "2014-07-25T22:34:43.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "119799252",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schizos so wie auch z.B. Authisten etc. haben oftmals neben ihrem Schaden auch einen Begabung, z.B. John Nash, Schizo und Nobelpreisträger",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "492788054151933952",
        "createdAt" : "2014-07-25T21:46:42.000Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-180758192",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thanks, man! 👍🙏✔️",
        "mediaUrls" : [ ],
        "senderId" : "180758192",
        "id" : "1100788514701168644",
        "createdAt" : "2019-02-27T16:03:28.284Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I know, nun open... pls retweet...",
        "mediaUrls" : [ ],
        "senderId" : "180758192",
        "id" : "1100786832676175877",
        "createdAt" : "2019-02-27T15:56:47.244Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "180758192",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/LPca3ruA6T",
          "expanded" : "https://twitter.com/messages/media/1100780526812704773",
          "display" : "pic.twitter.com/LPca3ruA6T"
        } ],
        "text" : " https://t.co/LPca3ruA6T",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1100780526812704773/1100780517618843648/e5WXOQAW.png" ],
        "senderId" : "75128838",
        "id" : "1100780526812704773",
        "createdAt" : "2019-02-27T15:31:44.069Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "180758192",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja ich würd dann dein account so einstellen dass man dinge retweeten kann, dann hätt ichs schon lange ge-re-tweeted...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1100780291034173444",
        "createdAt" : "2019-02-27T15:30:47.596Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kannst Du bitte mal was 'allgemein verträglich Positives' auf #paa100 tweeten... Thanks landev. 💯Wir brauchen Promo😉",
        "mediaUrls" : [ ],
        "senderId" : "180758192",
        "id" : "1100779951341690884",
        "createdAt" : "2019-02-27T15:29:26.597Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-196135360",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, danke. ich stehe gerne rede und antwort wenn mein Handy eingeschaltet ist. Sonst einfach eine SMS auf 078 674 15 32, dann sehe ich es, sobald ich es einschalte...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178951684564963333",
        "createdAt" : "2019-10-01T08:36:00.152Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc, meine Kollegin hat dir telefonisch bestätigt, dass wir deine Direkt-Nachricht erhalten haben. Ich werde deine Informationen an die zuständige Stelle weiterleiten. Beste Grüsse ^Yvonne",
        "mediaUrls" : [ ],
        "senderId" : "196135360",
        "id" : "1178950870106595334",
        "createdAt" : "2019-10-01T08:32:45.979Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "10093870",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178946990283079685",
        "createdAt" : "2019-10-01T08:17:20.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hätten sie allenfalls rasch eine Telefonnummer, damit ich anrufen kann um zu überprüfen, ob sie das erhalten haben, denn seit snwoden wissen wir ja, dass wir dem Internet und vielleicht somit auch twitter nicht zu fest trauen sollten",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178946059055304708",
        "createdAt" : "2019-10-01T08:13:38.933Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/9WCkoi5Q2o",
          "expanded" : "https://marclandolt.ch/Hackers-Cardgame/jpg/DE/",
          "display" : "marclandolt.ch/Hackers-Cardga…"
        } ],
        "text" : "und die Lernkarten die beim Entlarven helfen könnten wären die hier: https://t.co/9WCkoi5Q2o",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178944123870613508",
        "createdAt" : "2019-10-01T08:05:58.369Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BOQQk5LLBa",
          "expanded" : "https://twitter.com/messages/media/1178943590107668485",
          "display" : "pic.twitter.com/BOQQk5LLBa"
        } ],
        "text" : "ich muss davon ausgehen, dass die Psychiatrie Königsfelden dazu benutzt wird Menschen die z.B. Dinge über Mord, Whistleblowing, Hacking etc. wissen -- also z.B. Autisten -- psychotisch zu machen. Ihnen explizit Fehlinformationen einzuspeisen um sie davon abzuhalten Dinge aufzudecken. Mögliche Taktich wäre Zielperson z.B. Schizoprhenie anhängen, ihr Psychopharmaka verschreiben die sie gar nicht benötigt die Intelligenzmindernd wirkt. Auch führt die Diagnose Schizophrenie dazu, dass sich sowohl die ganze Familie als auch der komplette Freundeskreis abwendet, was unweigerlich auch zu Isolation und PTBS führt.\n\nSomit sollten Sie als Krankenkasse allenfalls mal alle Kunden die massiv Kosten verursachen (die könnten selber auch nichts dafür, z.B. mit 20 Jahren ist man hilflos ausgeliefert) überprüfen.\n\nMeiner Meinung nach gibt es zwei sorten von Schizophrenie Patieten\n\na) Solche die mit Hilfe der Psychiatrie psychotisch gemacht werden um ihnen so auch die möglichkeit zu nehmen in ihrer Klicke weitere fragen zu stellen, damit der Autist nicht noch mehr Puzzleteile zum mutmasslich induzierten Selbstmord von Tobias Moser zu finden. Autisten haben vermutlich eine Zwangsneurose das Puzzle zum gesammtbild zusammen setzen zu wollen (da könnten sie mich als Person nehmen für diese Gruppe von Patienten)\n\nb) Solche die nur tun als hätten sie Schizophrenie und seien Psychotisch und den fehlbaren Psychiatern helfen. Kommunikationskanäle noch unklar. Meiner Meinung nach wäre da der Orlando Martins (da weiss ich nicht bei welcher Krankenkasse er ist) den man für diese Gruppe nehmen könnte\n\nDies ergäbe dann mit Formeln für Netzwerkgraphen (Mathe 5. Informatik Semester) wohl ein bild. Allenfalls würde sich dann immer einer von b an einen von a heften.\n\nDas ganze ist noch viel komplizierter als dass man das so einfach erklären könnte. Und ich würde auch vor Gericht aussagen, was ich einfach nicht selber stämmen kann ist selber gegen so eine riesen Juristische Person wie z.B. eine ganze Psychiatrie vorzugehen. Und dass das ja nicht einfach ist, zeigt auch der Fall Mollath in Deutschland.\n\nIch hab so eine Art Tarot gemacht, bzw. sind es Lernkarten, die ungefähr aufzeigen welche Schachzüge die besagte Juristische Person machen\n\nDes weitern kommt dazu, dass sobald die Krankenkasse sagt, dass sie die Behandlung eines Patienten nicht mehr zahle (Aussage einer ihrer jüngeren Kunden die ich zu schützen versuche) dass man dann sofort aus der Psychiatrie geschmissen wird. Da wäre es mir eigentlich auch recht, wenn Sie jeweils der Psychiatrie sagen würden, dass sie mich entlassen sollen, bzw. nicht zahlen sollen. Ich hoffe dass ich dann auch entlassen würde, und wenn nicht, wäre dann auch klar, dass die Psychiatrie oder deren Entscheidungsträger mich aus den selben Gründen in die Psychiatrie interniert hat wie den Fall Gustl Mollath.\n\nMeiner Meinung nach ist einer der Zentralen Figuren dieses Systems der Hr. Dr. Hansjürg Pfisterer und die Astrid Blum einfach eine seiner Gehilfen. Leider habe ich zwar massenhaft Indizien aber praktisch keinen Handfesten beweis.\n\nMit freundlichen Grüssen\nMarc Landolt https://t.co/BOQQk5LLBa",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1178943590107668485/1178943582872444928/COsdCnw5.jpg" ],
        "senderId" : "75128838",
        "id" : "1178943590107668485",
        "createdAt" : "2019-10-01T08:03:50.634Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bin noch am schreiben, irgendwie hab ich grad ein bisschen mühe damit, aber werde alles schicken",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178940043278389252",
        "createdAt" : "2019-10-01T07:49:44.651Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc",
        "mediaUrls" : [ ],
        "senderId" : "196135360",
        "id" : "1178939906812448772",
        "createdAt" : "2019-10-01T07:49:12.126Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "196135360",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178937938283028484",
        "createdAt" : "2019-10-01T07:41:22.799Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-216038112",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "216038112",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo, die adresse ist ticktak@bluewin.ch (eines computer unbegabten kollegen) aber das problem scheint schon serversitig. 0792910787",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "565556028025352192",
        "createdAt" : "2015-02-11T17:00:39.282Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-233903268",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mach dir Mal es paar online dating profil uf lovoo und tinder.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1312444149518524420",
        "createdAt" : "2020-10-03T17:27:23.971Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du wirst glaube von wrong havenodromes und other technostate Multiverses humanoiden und yesterday aliens angechannelt.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1306664887062597642",
        "createdAt" : "2020-09-17T18:42:40.423Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schreib Mal ne Freimaurer lounge oder ne dianetics zentrale in deiner nähe an.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1306657037225517061",
        "createdAt" : "2020-09-17T18:11:28.877Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zieh dir Mal Rick and Morty rein hnd science papers ueber preons um quanten. Die NPCs sind microversianische systems und wohl auch microblackholemultiverses bioentitäten.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1303738098715701253",
        "createdAt" : "2020-09-09T16:52:39.720Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the weets looks like a encoded medium expression.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1291470263486418961",
        "createdAt" : "2020-08-06T20:24:39.879Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wieder so nen drecks Italo durch meinen garten geloffen. Soger die judo cheffin is ne drecks Italo Kriminelle Vatikan gehirngewaschene trulla. Und wieder nen drecks cablecom Telefon call center stress anruf.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1283404422329044997",
        "createdAt" : "2020-07-15T14:13:53.474Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hoffe die drecks Schweiz, mit Ihrem dreckigen korrupten verfassungswiedrigen staatsschergen passen sich Mal Der legalität, Der Wahrheit und Der gerechtigkeit an. Sonst halt Mal freimaurern, aber auch diese Vollhonks sind corrupte unwissende Idioten.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1283394647658815492",
        "createdAt" : "2020-07-15T13:35:03.013Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Schweiz ist halt so nen verschissene krimineller italomafia und HellsAngels legal state voller osteuropäer und schweizer links und rechts Faschisten kriminalität und Gewalt.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1283394213737103364",
        "createdAt" : "2020-07-15T13:33:19.569Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Evtl. Sinnvoll in einen land mit Rechtsstaat ziehen",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1280798365593337860",
        "createdAt" : "2020-07-08T09:38:21.132Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Evtl. Druid of Stonehenge is aber hardcofe",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1280797749814984708",
        "createdAt" : "2020-07-08T09:35:54.317Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Am besten selber nen blackhole,/whitehole mit kleinen enterprises drin proggen. Auch wenns zu labge dauert.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1280796667478974468",
        "createdAt" : "2020-07-08T09:31:36.265Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also alles Voll Scheiss e",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1280796475644088325",
        "createdAt" : "2020-07-08T09:30:50.529Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich Weiss scientology und die Freimaurerei sind die grössten Italo illu fasho säue. Evtl. Pastafarian ausprobieren, is aber auch so Vatikan fasho Scheiss. Cabbalah könnte helfen is aber so was wie hewbrww exocricsm.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1280796412180119556",
        "createdAt" : "2020-07-08T09:30:35.413Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Der brannt, mazehaxxors",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279742097982132229",
        "createdAt" : "2020-07-05T11:41:07.306Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "In America gabs auch so prostituierte, welche bspw. Ganzezeit mediziener verarscht, runtergemobbt und ausgebautet haben.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279166241593229316",
        "createdAt" : "2020-07-03T21:32:52.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Auch die deutschen sind da involviert. Ü bet gesichtsanalyse usw. findet man diese kriminellen Frauen auch auf bordelle websites.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279165450237198340",
        "createdAt" : "2020-07-03T21:29:43.770Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Lass dich ja nicht Von denen verarschen, das sind Manipulationen und lügnerinnen, welche vorgeben Jungfrau zu sein. Auch die UZH und Der Ch ausgang ist vill mit solchen unseriösen schweizerinnen.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279163039951007748",
        "createdAt" : "2020-07-03T21:20:09.125Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Anscheinend eine mediamatikerin aus Zürich und eine pedagogischefhstudentin aus Winterthur oder so.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279162498424471558",
        "createdAt" : "2020-07-03T21:18:00.018Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hattest du auch ein problem mit irgend welchen Linksradikalen Italo und Russen mafia Prostituierten(schweizerinnen, sogar genetisch blond) in Realifecafee oder so und wirst deswegen von Der schweizer, Italo und Russen mafia angegriffen?",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1279162288428273670",
        "createdAt" : "2020-07-03T21:17:09.935Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du wirst glaube von aquanischen und snaggoiden adventure times microverseclonekriegern verteidigt.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274368057352257540",
        "createdAt" : "2020-06-20T15:46:36.176Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Teilen ist schwer doch mit holodeck wird teilen was Anders herrschen ist schwer führen und leiten auch.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274367860916314118",
        "createdAt" : "2020-06-20T15:45:49.343Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kauf dir ne Sonne I'm internet und reservist dir die antimaterie und Mach microverse raumschiffe draus mit blackhole whitehole programming",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274367486239088644",
        "createdAt" : "2020-06-20T15:44:20.012Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Everything is cyberspace, think at a quark, than at a universes, than at a spaceshuttle in the relation of a quarks to an universes.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274367273059389444",
        "createdAt" : "2020-06-20T15:43:29.185Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Multiversian diplomat amohibioide, furroide und flexitarische insektoide,",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274367069845426180",
        "createdAt" : "2020-06-20T15:42:40.733Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "233903268",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Divide et impera mein Herr? Alle in den selben Topf werfen? Vorschlagen dass die Schweiz einen Bürgerkrieg anfängt? Twitterst Du im Auftrag einer Studentenverbindung?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1274366871094136838",
        "createdAt" : "2020-06-20T15:41:53.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Microverseraumshiffe aller rickformation und fehlerhafte raumzeitaechitektur vorhandene menthaler etc. Gilden. Als inteligente person einfach Mal Von Der Schweiz und Ihrem mongo kriminellen Netzwerk distanzieren.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1274365885092937734",
        "createdAt" : "2020-06-20T15:37:58.324Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Versuch Mal wormholes zum ehemaligen bnd boss zu Machen und nach kanada zu diesen HellsAngels medlein entführ pedos",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235777686263541766",
        "createdAt" : "2020-03-06T04:02:14.831Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Evtl. You have a commoliton on your shoulder or a quthulu(die säue ficken ziegen und kinder in Deutschen kirchen und schlössern, Siehe doku mit den zwei schwestern) or a thief of hell. Evtl. A Illu bastard and a Scientology brainwash outcash bastard.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235776825667842052",
        "createdAt" : "2020-03-06T03:58:49.657Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hetz Mal gegen Italo Mafia und Vatikan, glaube diese vollmongo kriminalos denken du seist pedo und hacken die Menthale juristische instanz durch Ihre blosse kriminelle fremdhegemonierende Menge und brutality out of there stupidy.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235775193148162058",
        "createdAt" : "2020-03-06T03:52:20.407Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Stell dir Mal nen Blackhole for, drin nen whitehole mit nem Microverse, dann wormholes aus nem microverses um deine quarks, manchmal nur synapsen manchmal ganzer body vor. Bzgl. Himmel und so.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235767509082652676",
        "createdAt" : "2020-03-06T03:21:48.387Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "In einem Paralleluniversum wurde ich durch die schwarzengrade als kleines kind auf einer brücker in Zürich albisguetli durch SnB/Templer/Seraphimtolegy wegteleporiert und executiert...(Meine Armen eltern...)",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235763377483497477",
        "createdAt" : "2020-03-06T03:05:23.334Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich wurde vor ein paar Jahren durch zwei kanadier quthulu schweine, welche nur lügen sprechen und lügen hören erschossen, wegen Frauen und so halt...",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235763082737201158",
        "createdAt" : "2020-03-06T03:04:13.099Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Anhand Der Paralleluniversen Daten der weissenzwerge, sieht es so aus als würdest du in mindestens einem Paralleluniversum in den suizid hypnotisiert Worden sein.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235762647091552262",
        "createdAt" : "2020-03-06T03:02:29.205Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ziehn dir Mal Rick and Morty und es paar Kurzgesagt YouTube clips ine, ich glaube en Brian and study timetravelstau attackt dich. Nebe de psychi wo schliesst, nid Scientology, d usländermafias, d corruption politiker und d microverses vergesse.",
        "mediaUrls" : [ ],
        "senderId" : "233903268",
        "id" : "1235762082097901573",
        "createdAt" : "2020-03-06T03:00:14.501Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-235421369",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BGw57RS0JL",
          "expanded" : "https://twitter.com/FailDef/status/915780064519839744",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "hey, happened randomly when i got home from a 2wk trip just after contracting for gov, events thereafter proved to me it wasnt me, tolerable levels now https://t.co/BGw57RS0JL",
        "mediaUrls" : [ ],
        "senderId" : "235421369",
        "id" : "939047676389203974",
        "createdAt" : "2017-12-08T08:23:07.491Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-246581263",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "246581263",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, Du bist doch beim CCC, haben die schon Irgendwas um rauszufinden ob man das Equation Group Zeugs auf Platte hat/entfernen kann?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "575050762973265920",
        "createdAt" : "2015-03-09T21:49:20.496Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-257127397",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "257127397",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "965268106988998660",
        "createdAt" : "2018-02-18T16:53:45.306Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, es werden noch 1 oder 2 solcher Nachrichten kommen. Der Melina habe ich schon per Signal geschrieben, dass ihre PP Mailbox voll ist, aber es kümmert sie wahrscheinlich nicht. Bis jetzt habe ich nichts unternommen, aber wahrscheinlich müsste man die Leute aus der MDB löschen. Können wir ja am Dienstag besprechen.",
        "mediaUrls" : [ ],
        "senderId" : "257127397",
        "id" : "965255644776423428",
        "createdAt" : "2018-02-18T16:04:14.014Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "257127397",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/VcHY8GgAYM",
          "expanded" : "https://twitter.com/messages/media/965253868295741444",
          "display" : "pic.twitter.com/VcHY8GgAYM"
        } ],
        "text" : " https://t.co/VcHY8GgAYM",
        "mediaUrls" : [ "https://ton.twitter.com/dm/965253868295741444/965253859273773056/NnXVuXhP.jpg" ],
        "senderId" : "75128838",
        "id" : "965253868295741444",
        "createdAt" : "2018-02-18T15:57:10.664Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "257127397",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kleine Frage: Bist Du am Mails verschicken?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "965253837006233604",
        "createdAt" : "2018-02-18T15:57:03.039Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "257127397",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ups, schon unterwegs...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962312856871071748",
        "createdAt" : "2018-02-10T13:10:38.702Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kommst du auch noch?",
        "mediaUrls" : [ ],
        "senderId" : "257127397",
        "id" : "962312462099009541",
        "createdAt" : "2018-02-10T13:09:04.559Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-304459163",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "304459163",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "998332747155374084",
        "createdAt" : "2018-05-20T22:40:49.746Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hoi Marc. Das freut mich sehr. \"Hab's mir kurz vorlesen lassen\" Du bist ja geil 😂  Manche Bekannte haben es bis heute nicht geschafft, es überhaupt fertig zu lesen ;) Vielen Dank für die Infos und Links, schaue ich mir sehr gerne an. Das Buch war der Stand meiner Sichtweise, den ich vor ca. 2 - 3 Jahren hatte. Ich musste all die neuen Informationen und auch den feministischen Aspekt irgendwie verarbeiten. Bei mir kam damals eine Vergewaltigung hoch, die ich verdrängt hatte und auch unbewusst im Buch verarbeitet habe. Heute würde ich sicher nicht mehr alles genau so schreiben. Vielen Dank auf jeden Fall für deine spannenden Inputs! :D Bis bald und liebs Grüessli, Sylvia",
        "mediaUrls" : [ ],
        "senderId" : "304459163",
        "id" : "998332500232622085",
        "createdAt" : "2018-05-20T22:39:50.933Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "304459163",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gruss Marc",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "998324509252911108",
        "createdAt" : "2018-05-20T22:08:05.795Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "304459163",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/vrDSPsyd5s",
          "expanded" : "https://www.google.ch/search?q=fuck+sabu&safe=active&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjWuszsopXbAhWO_aQKHcePAUYQ_AUICigB&biw=1920&bih=987#imgrc=ZLV81l1rDuZK7M",
          "display" : "google.ch/search?q=fuck+…"
        }, {
          "url" : "https://t.co/hgVdjLFzef",
          "expanded" : "https://www.ofv.ch/sachbuch/detail/natogeheimarmeen-in-europa/3193/",
          "display" : "ofv.ch/sachbuch/detai…"
        }, {
          "url" : "https://t.co/ygFV0fh3XL",
          "expanded" : "https://www.amazon.com/Illegale-Kriege-NATO-Länder-sabotieren-Chronik-ebook/dp/B01M1UN8TH/ref=sr_1_4?s=books&ie=UTF8&qid=1526853391&sr=1-4&refinements=p_27%3ADaniele+Ganser",
          "display" : "amazon.com/Illegale-Krieg…"
        }, {
          "url" : "https://t.co/xiQxaaA2Z6",
          "expanded" : "https://www.amazon.de/IBM-Holocaust-Strategic-Alliance-Corporation/dp/0914153277",
          "display" : "amazon.de/IBM-Holocaust-…"
        }, {
          "url" : "https://t.co/hCKhDwCL5B",
          "expanded" : "https://www.youtube.com/watch?v=pMmxOF7rMco",
          "display" : "youtube.com/watch?v=pMmxOF…"
        } ],
        "text" : "Halli Hallo\n\nich hab Dein Buch rasch \"gelesen\", naja ich war zu faul und habs von Google ge-scraped, alle Seiten mit einem Script gescreenshottet, dann mit Tesseract in Text umgewandelt und mir dann von einem TTS Programm mit dreifacher Geschwindigkeit vorlesen lassen, geht irgendwie schneller als lesen... :)\n\nich hätte paar Bemerkungen dazu:\n\n-Anonymous ist nicht mehr wirklich Elite, das sind nur noch ein paar Spinner die immer wieder die selben Dinge posten, zwar grafisch immer relativ toll gemacht, aber halt nicht mehr Elite seit Sabu die Elite Hackergruppe Lulzec beim FBI verpfiffen hat. https://t.co/vrDSPsyd5s: deren neue Heilige Gral ist #QANON... naja...\n\nDie ganze Anonymous Sache sollte man sowieso ein bisschen differenziert betrachten, wir hatten bei den Piraten Aargau mal ein Mitglied (Rafi) der im Namen von Anonymous gegen/bei Scientology demonstrieren ging, weil er (das war vor den Snowden Leaks) vermutet hat dass er von Scientology verfolgt wird. Ich hatte vor 10 Jahren ähnliche Gedanken und jetzt mit den richtigen Medikamenten muss ich mir eingestehen dass das Wahnvorstellungen waren. Ich denke es gibt zwar berechtigte Fälle wo man gegen Scientology demonstriert aber meist ist es eine Art kollektive Psychose weil Scientology irgendwie misteriös wirkt wenn man sich nicht eingehend damit beschäftigt hat\n\n-Zu verdeckten CIA operationen sind die Beiden Bücher von Ganser (den Du ja auch erwähnst) zu empfehlen, und für lesefaule wie mich gibt's die auch als Hörbücher:\nhttps://t.co/hgVdjLFzef\nhttps://t.co/ygFV0fh3XL\n\nDann zum Thema Juden und heutige Technologie wäre das Buch von Edwin Black: \"IBM und der Holocaust\" noch zu Empfehlen, wo beschrieben wir wie damals mit Lochkarten die Juden \"sortiert\" wurden ob sie nach Auschwiz oder in eine Fabrik als Sklave kommen. Da sehe ich Parallelen zur heutigen Überwachungsdiskussion, denn ein schönes Beispiel war wenn die Lochkarte z.B. bei \"Mechanische Fertigkeiten\" ein Loch hatte dass man dann nicht nach Auschwiz kam sondern in eine Fabrik.https://t.co/xiQxaaA2Z6 (Wenn Du es nicht bekommst hab ich sonst ne Version denn ich vermute mal da hat IBM dafür gesorgt dass es nicht mehr gedruckt wird)\n\nDen Bartoschek hast du ja auch im Twitter, sein Vortrag an der UNI finde ich auch noch toll wenn du den noch nicht gesehen hast: https://t.co/hCKhDwCL5B er beläuchtet das ganze ein bisschen aus einer anderen Perspektive als Ganser, bei diesem Video Konkret über Verschwörungstheorien und mit Deinem Buch würden Dich Deine Leser vermutlich auch die meisten Leute in die Verschwörungsecke stellen\n\nDann im letzten fünftel die lobeserede auf Schweizer und Europäer finde ich ein bisschen zu dick aufgetragen :) so supertoll sind wir Schweizer auch nicht... :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "998324498662293510",
        "createdAt" : "2018-05-20T22:08:03.285Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "17200324-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thanks for following!",
        "mediaUrls" : [ ],
        "senderId" : "17200324",
        "id" : "788745658756952067",
        "createdAt" : "2016-10-19T14:16:33.924Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-440188706",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/oj2n2emhWA",
          "expanded" : "http://l.instagram.com/?FailDef-722-35555=02c08cd33e76e14e4111df1d422e09a4&e=ATPTa4ohshrpM86RFnEyrGjlDQFJJxv_rvDPBHF9qVarN0ElpnNRI8swWeGmxyg&u=http://linkis.com/instagram.com/3433/orL3WQg#28960",
          "display" : "l.instagram.com/?FailDef-722-3…"
        } ],
        "text" : "https://t.co/oj2n2emhWA ​​​​​​​​Marc Landolt jun !",
        "mediaUrls" : [ ],
        "senderId" : "440188706",
        "id" : "910583079609266183",
        "createdAt" : "2017-09-20T19:14:58.727Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/B1vZoCRjLq",
          "expanded" : "http://truetwit.com/vy477140036",
          "display" : "truetwit.com/vy477140036"
        } ],
        "text" : "lissakr11 uses TrueTwit validation. To validate click here: https://t.co/B1vZoCRjLq",
        "mediaUrls" : [ ],
        "senderId" : "440188706",
        "id" : "867259695081426947",
        "createdAt" : "2017-05-24T06:03:18.944Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-594915995",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "OK kein Problem :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174638198649163780",
        "createdAt" : "2019-09-19T10:55:44.973Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mein Teddy meint wir hätten kein Geld und keine Nerven für so eine grosse Reise...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174543829556518916",
        "createdAt" : "2019-09-19T04:40:45.636Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und bring teddy und die Stimmen mit, das wird sicher lustig. Familientreffen :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174421700009746442",
        "createdAt" : "2019-09-18T20:35:27.694Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Komm nach Berlin, ich würde dich gerne mal persönlich kennenlernen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174420387603632132",
        "createdAt" : "2019-09-18T20:30:14.787Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das mit den debug Infos mache ich auch aber die idioten kapieren es leider oft nicht und legen es mir als Unsicherheit aus",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174420282301435908",
        "createdAt" : "2019-09-18T20:29:49.674Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ach endlich mal wer mit Hirn &lt;3",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174420158284271625",
        "createdAt" : "2019-09-18T20:29:20.109Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich glaube ich hab neben der Schizophrenie auch noch n bisschen Autismus, was erklären würde, weshalb ich es Hasse Unwahrheit sagen zu müssen... bzw. es ist auch logisch möglichst viele debug-Information von sich zu geben: user@debian:~ landev -d -v -v -v (landev ist mein möchtegern hacker name)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174400008650723333",
        "createdAt" : "2019-09-18T19:09:16.058Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und ich finde es gut, dass du so offen mit der Krankheit umgehst",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174398185856479237",
        "createdAt" : "2019-09-18T19:02:01.479Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich habe da auch ein Exemplar :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174398063072489476",
        "createdAt" : "2019-09-18T19:01:32.202Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schön, dass der teddy bei dir ist :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174398012140994565",
        "createdAt" : "2019-09-18T19:01:20.068Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Viele von den Fotos sind doch voll ok beispielsweise das Gekritzel :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174397954284838918",
        "createdAt" : "2019-09-18T19:01:06.264Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/XqMnRJiy12",
          "expanded" : "https://marclandolt.ch/krankheit/",
          "display" : "marclandolt.ch/krankheit/"
        } ],
        "text" : "naja und manchmal wenn man zu viel Mist twitter endet es irgendwie so: (achtung nichts für ganz schwache nerven) https://t.co/XqMnRJiy12 (aber meistens war mein Teddy-Bär (mit dem ich ja rede) bei mir...) keine angst das ist zeugs von 2014, ich bin heute ein bisschen vorsichtiger. (mein Teddy-Bär sagt: \"Gehäuchelt\")",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174130202248654852",
        "createdAt" : "2019-09-18T01:17:10.089Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oha :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174075254211252228",
        "createdAt" : "2019-09-17T21:38:48.576Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/JvGLncRPrJ",
          "expanded" : "http://bfbcs.com/stats_pc/landev",
          "display" : "bfbcs.com/stats_pc/landev"
        } ],
        "text" : "Eben drum sage ich \"idee\" und vielleicht sieht es irgendwer und denkt er macht mal ne Studie dazu um diesbezüglich Gewisshet zu erlangen, und falls dem so wäre wären dann wären Rankings wie das hier: https://t.co/JvGLncRPrJ wo nicht nur alte 41 jährige Säcke wie ich drin stehe sonern auch ganz viele Kinder nicht so super toll.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174062651753881609",
        "createdAt" : "2019-09-17T20:48:44.929Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber wie immer Korrelation ist nicht Kausalität",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174061379935973380",
        "createdAt" : "2019-09-17T20:43:40.683Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Klingt nach ziemlich vielen zusammen hängenden Ideen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174061322406965258",
        "createdAt" : "2019-09-17T20:43:26.971Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/wz6A25SD9P",
          "expanded" : "https://twitter.com/FailDef/status/1174017738546790400",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "lustig haben, über zeugs auf hohem niveau diskutieren mit dem Gremium-der-Seele (Terry Pratchet; Scheibenwelt) und dieses Zeugs auf hohem Niveau auf sehr tiefen Niveau kommentieren. hier zB. https://t.co/wz6A25SD9P wäre meine Hypothese dass Leute mit einem hohen K/D (Kill Death Ratio) eher soziophaten sind und die mit einem ganz tiefen Altruisten. Wie bei Psychologie so üblich wäre so eine Theorie natürlich nur zu 80% richtig, denn es gibt sicher auch Soziophaten die einfach grotten-schlechte Gamer sind. Wäre es ein Persönlichkeitsmerkmal wäre es Normalverteilt aber viel wichtiger wäre, dass z.B. MKULTRA wenn es das mal gegeben hat oder das  sogar noch heute gibt so z.B. Menschen gefunen werden könnten die eher bereit sind einen Mord zu begehen. Technologie (Technokratie) ist grundsätzlich neutral und falls diese Idee stimmt könnte man nicht nur potenzielle Attentäter und Morder für irgendwelche Aufträge finden, sondern auch z.B als Bundespolizei rechtzeitig auf Leute zugehen (helfen, ausbilden, zeigen wie schön flauschig es unter guten Menschen sein kann) und Dinge korrigieren.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174060625124888582",
        "createdAt" : "2019-09-17T20:40:40.800Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Definiere \"maximal blöd tun\"",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174043666823962628",
        "createdAt" : "2019-09-17T19:33:17.545Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hahaha",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174043635123466244",
        "createdAt" : "2019-09-17T19:33:09.998Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maximal blöd tun ausser es ist irgendetwas wichtiges, dann weren sie recht schnell vernünftig...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1174042861525983243",
        "createdAt" : "2019-09-17T19:30:05.605Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was machen deine imaginären Freunde so? :-)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174042621951561732",
        "createdAt" : "2019-09-17T19:29:08.430Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Na, wie gehts dir?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1174042580138504197",
        "createdAt" : "2019-09-17T19:28:58.472Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sweet :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1168470615407378437",
        "createdAt" : "2019-09-02T10:27:58.542Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xI8htWc83q",
          "expanded" : "https://twitter.com/messages/media/1167924649738457099",
          "display" : "pic.twitter.com/xI8htWc83q"
        } ],
        "text" : " https://t.co/xI8htWc83q",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1167924649738457099/1167924636027240449/URg3CjEA.png" ],
        "senderId" : "75128838",
        "id" : "1167924649738457099",
        "createdAt" : "2019-08-31T22:18:30.353Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/tc6S6tBlCt",
          "expanded" : "https://www.facebook.com/groups/GNUAndLinux/permalink/10162670115950019/",
          "display" : "facebook.com/groups/GNUAndL…"
        } ],
        "text" : "https://t.co/tc6S6tBlCt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1167924616691535878",
        "createdAt" : "2019-08-31T22:18:22.460Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey allenfalls was für Dich:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1167924613814222852",
        "createdAt" : "2019-08-31T22:18:21.626Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "moin",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154375222117765124",
        "createdAt" : "2019-07-25T12:57:54.901Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Morgen :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154372537255698437",
        "createdAt" : "2019-07-25T12:47:14.787Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bestechende Argumente",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154372518695907334",
        "createdAt" : "2019-07-25T12:47:10.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schaf auch gut",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154206581103431685",
        "createdAt" : "2019-07-25T01:47:47.754Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "von irgend 06:00 bis 16:00, bzw. immer Montag und Freitag einigermassen normale Tag-Nacht Zeiten... ich war jetzt auch wieder 2h draussen mit dem Fahrrad, Nachts läuft man keinen keinen Menschen übern weg und somit auch keinen Idioten. Und man ist nicht klitsch Nass wie bei Tags mit 40°C und holt sich so auch keinen Hautkrebs...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154206552968114180",
        "createdAt" : "2019-07-25T01:47:41.073Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gute Nacht und schlaf schön",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154199695876612105",
        "createdAt" : "2019-07-25T01:20:26.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Von wann bis wann schläfst du normalerweise so?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154199550967603204",
        "createdAt" : "2019-07-25T01:19:51.652Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gerne doch :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154198790758457348",
        "createdAt" : "2019-07-25T01:16:50.413Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber danke für das angebot",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154198723959934980",
        "createdAt" : "2019-07-25T01:16:34.467Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, zuerst sollte man sich vorbereiten bevor man die Zeit eines Experten/Expertin verschwendet...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154198709661515786",
        "createdAt" : "2019-07-25T01:16:31.061Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sag bescheid, wenn du mal Lust hast, mit mir zu telefonieren per Skype",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154198527645499396",
        "createdAt" : "2019-07-25T01:15:47.671Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Alles gut :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154198442358530058",
        "createdAt" : "2019-07-25T01:15:27.331Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in das ganze quanten-zeugs wollte ich mich auch mal einarbeiten, hab sogar Quantentheorie für dummies gekauft, aber konnte mich bisher noch nciht dazu überwinden anzufangen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154147411553476613",
        "createdAt" : "2019-07-24T21:52:40.642Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich find minkorrekt immer toll wenn sie über dieses thema reden",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154146716095926276",
        "createdAt" : "2019-07-24T21:49:54.834Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber irgendwie reagieren die menschen doch seltsam auf eine frau mit der figur von marilyn monroe, ihrer haarfarbe und der intelligenz von sheldon cooper :P",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154146712673488900",
        "createdAt" : "2019-07-24T21:49:53.998Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "\"quantenheilung\" :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154146625637326854",
        "createdAt" : "2019-07-24T21:49:33.265Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das trage ich auch so ein bisschen vor mir her, um die idioten wegzuhalten",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154146562962055177",
        "createdAt" : "2019-07-24T21:49:18.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "inzwischen bin ich als quantenphysikerin anerkannt und habe mir damit meinen ruf gemacht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154146501083504644",
        "createdAt" : "2019-07-24T21:49:03.558Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "war bei meinem ersten camp der fall, danach nie wieder",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154146449271197700",
        "createdAt" : "2019-07-24T21:48:51.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "alles gut :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154146414743756804",
        "createdAt" : "2019-07-24T21:48:42.975Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja aber das sagen diese jungs aus begeisterung -- meistens -- also ich mag intelligente frauen. und dass hacker ein bisschen unbeholfen sind mit sozialem ist ja auch bekannt. bzw. versucht man dann mit einem \"Rant\" aufmerksamkeit der frau auf sich zu ziehen, denn wo keine angriffsfläche auch keine berührungsfläche. ausserdem wenn es 9 häcker und eine häckerin ist, dann spielen da vermutlcih auch gruppendynamische prozesse von \"wie bekomme ich jetzt ihre aufmerksamkeit\" über \"wie steche ich die anderen aus\" oder \"ich hab ja eh keine chance\" aber vermutlich auch \"die frauen haben mich immer ignoriert, jetzt räche ich mich\". also ich will jetzt die hacker community nicht kollektiv in schutz nehmen, aber ich denke es hat da schon weniger idioten als sonst in gruppen/vereinien",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154145362564620293",
        "createdAt" : "2019-07-24T21:44:32.168Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich weiß nicht, wie oft ich in der hackerszene den spruch \"oh mein gott, du bist eine frau\" gehört habe",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154143405548691461",
        "createdAt" : "2019-07-24T21:36:45.526Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das hast du oft genug. auch bei hochbegabung, angststörung, frauen usw",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154143346316795909",
        "createdAt" : "2019-07-24T21:36:31.401Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "man gewöhnt sich dran, und man/ich beginnt dann einfach damit dies zu kompensieren mit (un)angebrachten zynischen sprüchen über Vorurteile oder zitiert irgendwelche wissenschaftlichen quellen, denn wenn es ein schizophrener sagt kann es ja nicht stimmen, aber wenn es eine wissenschaftliche studie sagt muss es ja stimmen. was einem dann aber wieder angelastet wird, dass man immer wissenschaftliche quellen zitieren muss. ich denke bei den einfachen Leuten geht es eigentlich mehr darum, dass der nicht-schizophrene das gefühl behält, dass sei besser als der schizophrene, aber das ist etwas was man nur sehr schlecht erklären kann, denn sobald man so etwas nur andeutet hat man schon den groll oder hass am hals. ich denke so ählich dürfte es auch z.B. minderheiten gehen, die z.B. auf ungerechtigkeit im lohn hinweisen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154135635277467652",
        "createdAt" : "2019-07-24T21:05:52.969Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Klar, aber scheiße ist sowas trotzdem",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154134083691327492",
        "createdAt" : "2019-07-24T20:59:43.021Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mach dir keine sorgen, man überlebt es... man muss einfach akzeptieren, dass man weniger rechte hat als ein nicht-schizophrener. Vorurteile, und deshalb bin ich auch meist auf der seite der minderheiten, die haben meist auch weniger rechte als \"der weisse mann\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154133559310819333",
        "createdAt" : "2019-07-24T20:57:38.009Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und das geht natürlcih gar nicht wenn so ein \"Schozspinner\" intelligenter ist als der polizist, da muss man erst recht noch eins reindrücken, denn \"Wir Polizisten sind ja über alle Zweifel erhaben\" bzw. sieht man ja immer wieder in der presse, es sind auch nur menschen. ausserdem gemäss einer amerikanischen studie ist die Häusliche gewalt durch polizisten massiv über dem durchschnitt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154133166610763780",
        "createdAt" : "2019-07-24T20:56:04.416Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und keine gefahr für die allgemeinheit darsellst",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132795318513669",
        "createdAt" : "2019-07-24T20:54:35.847Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "irgendwie tust du mir in dem punkt echt leid. ich meine, die müssen doch auch sehen, dass du relativ intelligent bist :-)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132762917519364",
        "createdAt" : "2019-07-24T20:54:28.123Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "irgendwo gesichert... ist aber auch schon 10 jahre her. ich hab dann versucht den polizisten zur rede zu stellen wegen dem abgeänderten protokoll und der hat dann einfach meiner psychiaterin angerufen und gesagt man solle mir mehr medikamente geben... also als randgruppen mensch, egal ob behinderung, ausländer etc. ist man eh immer im nachteil",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154132727291006981",
        "createdAt" : "2019-07-24T20:54:19.649Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "zum thema fehlende ausbildung von polizisten - das rechtfertigt nicht, menschen einfach so aus dem verkehr zu ziehen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132649130299400",
        "createdAt" : "2019-07-24T20:54:00.994Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "4 - das ist assi",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132513838813188",
        "createdAt" : "2019-07-24T20:53:28.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "3 - Datenschutz?! wtf?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132435371745284",
        "createdAt" : "2019-07-24T20:53:10.034Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zu 2) hast du noch den Wortlaut des Tweets?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132342518272004",
        "createdAt" : "2019-07-24T20:52:47.901Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zu 1 - wie dumm ist die denn?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154132277670100996",
        "createdAt" : "2019-07-24T20:52:32.430Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "&lt;zynismus&gt;ich armes armes opfer&lt;/zynismus&gt; und mittlerweile versuche ich einfach solchen situationen möglichst aus dem weg zu gehen. Irgendwer von der ETH Zürich der sich mit \"Spielestrategie\" auskennt hat mal gesagt, \"Egal ob Du als Kläger oder Angeklagter mit der Polizei zu tun hast, Du verlierst immer\" und Spielestrategie ist Wissenschaft, da kann man glaub drauf vertrauen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154132256379686916",
        "createdAt" : "2019-07-24T20:52:27.367Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "polizisten sind nicht ausgebildet in psychologie oder gar psychiatrie. die wissen nicht was machen mit jemandem der schizoprhenie hat, vertrauen auch nciht auf ihr eigenes urteil, also wird das \"problem\" (also ich) einfahc mal in die psychiatrie gebracht... und ich war noch nie gewalttätig (ausser vielleicht gegen mich selber) und hab auch noch nie irgendwem eine faust ins gesicht geschlagen oder so, aber dieses \"detail\" steht dann natrülich nicht im polizeicomputer... echt frustrieren...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154131648075583492",
        "createdAt" : "2019-07-24T20:50:02.346Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1x wurde mir meiner meinung nach zu unrecht gekündet, das habe ich (damals noch naiv und irgend 20 jahre alt) der polizei gemeldet -- logischerweise stand da im polizei computer dass ich schizophrenie habe -- und dann haben die mich wieder mal in die psychiatrie gebracht.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154131103294218244",
        "createdAt" : "2019-07-24T20:47:52.599Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1x wollte ich ins kantonsspital weil es mir nicht gut ging, und da wartete die polizei 200m vor dem spital, hat mich rausgenommen und mich einweisen lassen, obwohl ich ja schon auf dem weg ins spital war. damals wusste ich noch nicht, dass die im polizeicomputer nachschauen können dass ich schizophrenie habe...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154130690834702340",
        "createdAt" : "2019-07-24T20:46:14.111Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1x hat ein besorgter bürger einen tweet von mir als drohung interpretiert und die behörden alarmiert. dabei war es keine drohung. der polizist hat dann selbt gemerkt dass es keine drohung war und den tweet im protokoll so abgeändert dass es wie eine drohung aussah",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154130339159130116",
        "createdAt" : "2019-07-24T20:44:50.260Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1x hat die psychiaterin gesagt, ich müsse keine medikamente mehr nehmen, dann hab ich also keine mehr genommen, wurde psychotischer und dann hat sie mich einweisen lassen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154130071478628356",
        "createdAt" : "2019-07-24T20:43:46.443Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, verschiedene gründe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154129916083916809",
        "createdAt" : "2019-07-24T20:43:09.388Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "menschenrechte, rechtsstaat, wtf?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154111740092866564",
        "createdAt" : "2019-07-24T19:30:55.895Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "moment, die haben dich grundlos dahin geschleppt? wie geht das denn?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154111713706487818",
        "createdAt" : "2019-07-24T19:30:49.598Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja bisher wurde ich immer von der polizie gebracht. die können da scheinbar im polizeicomputer nachschauen, und da steht scheinbar auch drinn dass ich schizophrenie habe. und dass von den schizophrenen 90% nicht gewalttätig sind wird vermutlich in der polizeischule niemandem beigebracht... das ist eigentlich eine nicht legitime vorverurteilung. bzw. die reden sich dann immer raus mit \"ja aber 10% sind gewalttätig\" obwohl die ja auch da reinschreiben könnten dass ich zu den 90% nicht gewalttätigen schizos gehöre... und genau wegen solchen ereignissen in meinem leben, bin ich auch fast immer auf der seite der minderheiten. und setzt man sich dann z.B. für eine Minderheit ein, wird das dann wieder als akt der agression gewertet und da 10% der schizophrenen gewalttätig sind landet man dann halt wieder in der psychiatrie.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154105748466462725",
        "createdAt" : "2019-07-24T19:07:07.401Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "psychatrie wegen depression oder schizos?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154095410622976004",
        "createdAt" : "2019-07-24T18:26:02.644Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok verstehe",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154095372974854148",
        "createdAt" : "2019-07-24T18:25:53.665Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, da ich öfters mal fü4 3 monate in der psychiatrie bin udn im 10. stock wohne ist das doppelt keine gute idee, aber meine eltern hatten immer welche...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154093036852219908",
        "createdAt" : "2019-07-24T18:16:36.696Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hast du selbst welche?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154091774698283012",
        "createdAt" : "2019-07-24T18:11:35.771Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "genau, die wollte mir einfach gesellschaft leisten... ich bin eh ein katzenmensch, das merken sie glaub auch...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154091036311871492",
        "createdAt" : "2019-07-24T18:08:39.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, sehr schön. problem gelöst",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154090856573472773",
        "createdAt" : "2019-07-24T18:07:56.870Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "jetzt haben sie abgenommen, sie sei jung und deshalb so dünn. und seit einem tag weg. aber das ist normal das machen viele katzen mal 2 tage weg bleiben...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154088276577288196",
        "createdAt" : "2019-07-24T17:57:41.764Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Soanders bleibt sie erstmal eine Weile bei dir und du rufst regelmäßig da an",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154084925563375622",
        "createdAt" : "2019-07-24T17:44:22.811Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Tierheim?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154084865811390471",
        "createdAt" : "2019-07-24T17:44:08.560Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ach Mist",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154084857611476997",
        "createdAt" : "2019-07-24T17:44:06.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja aber es nimmt nieamand ab, vielleicht sind sie in den ferien und die katze ist wirklcih alleine und drum zu mir gekommen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1154036644309159940",
        "createdAt" : "2019-07-24T14:32:31.715Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hast du jetzt die Nummer angerufen?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1154020186045001734",
        "createdAt" : "2019-07-24T13:27:07.703Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, erst mal mach ich mir jetzt um die abgemagerte katze sorgen, denn ich bin immer nohc hier an der aare und die würde sich ja kaum zu einem wildfremden menschen hinlegen wenn sie sich nicht futter oder so erhoffen würde. wie bereits erwähnt, sie ist meiner meinung nach abgemagert...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153852286776553476",
        "createdAt" : "2019-07-24T02:19:57.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "So jetzt schlaf aber schön und viel Spaß mit dem teddy :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153851998984429573",
        "createdAt" : "2019-07-24T02:18:48.786Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hm wusste ich gar nicht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153851901932376069",
        "createdAt" : "2019-07-24T02:18:25.659Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja nach dem ich matrix gesehen habe war für mich auch alles nur eine simulation... ich denke auf solche themen sind menschen anfällig. das thema heisst ja glaub radikaler konstruktivismus",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153851082554126342",
        "createdAt" : "2019-07-24T02:15:10.297Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Äh ja, was ich eben geschrieben habe - gehört das schon in die Ecke schizo?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850800558546948",
        "createdAt" : "2019-07-24T02:14:03.076Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "blödes dilemma",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153850717599404036",
        "createdAt" : "2019-07-24T02:13:43.292Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, dann ist sie vieleiht nicht mehr hier",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153850685038968837",
        "createdAt" : "2019-07-24T02:13:35.516Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ruf morgen bei der Telefonnummer an",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850630873800708",
        "createdAt" : "2019-07-24T02:13:22.606Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und das Gefühl, dass die Welt nicht real ist, nur eine simulation und ich meine Familie umbringen müsse, damit ich in de Welt über dieser hier eine Zukunft habe - das war so bei mir der tiefpunkt",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850504222564357",
        "createdAt" : "2019-07-24T02:12:52.411Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich eigentlich auch. war noch kurz rasch an die aare (fluss) um ein bisschen in der natur zu programmieren. und dann ist eine katze gekommen die jetzt seit 2h bei mir ist und irgendwie sehr abgemagert aussieht. sie hat sogar eine telefonnummer am halsband, aber ich weiss jetzt nciht ob ich da anrufen soll und sie vermisst wird oder ob sie einfach einen grossen aktionsradius hat",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153850359892381707",
        "createdAt" : "2019-07-24T02:12:18.007Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Intelligenz korreliert übrigens stark mit psychischen Krankheiten",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850239780118532",
        "createdAt" : "2019-07-24T02:11:49.355Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "9h Schlaf und 6h Arbeit am Tag",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850091993784325",
        "createdAt" : "2019-07-24T02:11:14.148Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Optimum für mich ist um 11h aufstehen und um 2h ins Bett",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153850032287899653",
        "createdAt" : "2019-07-24T02:10:59.917Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wollte eigentlich gleich schlafen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153849941313359876",
        "createdAt" : "2019-07-24T02:10:38.204Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Studium :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153849893389250564",
        "createdAt" : "2019-07-24T02:10:26.785Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du hast ja auch sehr merkwürdige wachzeiten... :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153849578061467653",
        "createdAt" : "2019-07-24T02:09:11.595Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie süß :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153849440559648775",
        "createdAt" : "2019-07-24T02:08:38.820Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/bEzuiqey46",
          "expanded" : "https://www.facebook.com/photo.php?fbid=10218766694225399&set=a.1861732509994&type=3&theater",
          "display" : "facebook.com/photo.php?fbid…"
        }, {
          "url" : "https://t.co/Y25A6C8Muz",
          "expanded" : "https://www.facebook.com/photo.php?fbid=10213987373145359&set=pb.1441390726.-2207520000.1563921477.&type=3&theater",
          "display" : "facebook.com/photo.php?fbid…"
        } ],
        "text" : "danke, ich bin schon mitglied und ja diese leute haben irgendwie ein ähnliches mindset wie ich. war auch neulcih mit dem ccc schweiz das CERN anschauen gewesen, wusstest du dass im CMS eine Karze rum tigert? https://t.co/bEzuiqey46 ausserdem war ich im ZeTeCo, das ist wie das CCC-Camp, aber für die Schweiz also viel viel kleiner: https://t.co/Y25A6C8Muz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153797054121967626",
        "createdAt" : "2019-07-23T22:40:28.936Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zum Thema like minded people, guck dich vielleicht mal beim ccc um, der bildet gerade kleine Ableger in der Schweiz",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153773096928759812",
        "createdAt" : "2019-07-23T21:05:17.091Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke, ich mag dich auch.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153673498914672650",
        "createdAt" : "2019-07-23T14:29:31.066Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke für dein Vertrauen. Ich mag dich, soweit ich dich bisher kenne. Ich habe lange nach den richtigen Menschen gesucht, aber so langsam finde ich sie :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153626083780173829",
        "createdAt" : "2019-07-23T11:21:06.424Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja als ich noch gearbeitet habe hab ich 6000 verdient, das war schon besser. aber so kann ich 12h schlafen am tag, das hilft sehr bei meiner krankheit. keine chefs die rum-hitlern etc. schizophrene sind vulnerabler als normale menschen. aber wenn du das dann erklären willst, dann wirst du als man als Sissi ausgelacht etc. also sich irgendwie zurück ziehen zu können hilft. seit ich die IV bekomme bin ich weniger suizidal weil halt all die existenzängste wo man sich um job und geld sorgen machen muss einem weniger beschäftigen. und wenn man sich wohler fühlt, dann ist man auch weniger anfällig für psychosen. die vorurteile (nicht von allen menschen aber insbesondere von denen aus der tendenziell rechten ecke) bleiben halt. und wenn man mal eine schlechter phase hatte -- ich war zwar nie gewalttätig aber hab z.B. gemeint ich hätte einen chip im kopf -- dann wird einem das das lebenlang nachgetragen. aber es gibt auch gute in meinem leben, ich kann immer freitags und montags mit meinen beiden nichten spielen (1&amp;3 Jahre alt) und die mögen mich komplett und haben auch keine vorurteile. das ist echt toll. und z.B. auch die Piraten-Aargau leute haben auch keine vorurteile. aber solche leute sind selten. meist interessieren sich die leute so für \"ja wie sind dann diese stimmen, was sagen sie etc...\" aber für ich als person interessieren sich die leute nicht. bzw. nicht für dinge die mich interessieren. ich hab ja 5 semester informatik studiert und krieg auch ein neuronales netzwerk mit backpropagation hin. leute mit denen du über solche themen sprechen kannst sind extrem selten. aber ich vermute das geht Dir ja mit den Quanten Themen nicht anders...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153420628298489860",
        "createdAt" : "2019-07-22T21:44:42.043Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bzw bist du mit dem Status quo zufrieden?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153416518652301322",
        "createdAt" : "2019-07-22T21:28:22.185Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also soll bloß nicht offensive klingen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153416404852457479",
        "createdAt" : "2019-07-22T21:27:55.191Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oder am selbstständig machen?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153416285784592389",
        "createdAt" : "2019-07-22T21:27:26.685Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "In Wie fern hindert dich die Krankheit am Arbeiten?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153416252263731205",
        "createdAt" : "2019-07-22T21:27:18.688Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oh no",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153416183955230725",
        "createdAt" : "2019-07-22T21:27:02.395Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich arbeite nicht, ich hab IV (InValiden Rente) das ist in der schweiz rund 3000€ umgerechnet. da hab ich viel zeit für meinen teddy :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153413026202247172",
        "createdAt" : "2019-07-22T21:14:29.538Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja hab wegen meiner krankheit eh jedes jahr den job verloren",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153412863517741065",
        "createdAt" : "2019-07-22T21:13:50.762Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie geht es dir so? Job ok? Was machen deine imaginären Freunde?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412828759568388",
        "createdAt" : "2019-07-22T21:13:42.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja ich auch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153412766264430601",
        "createdAt" : "2019-07-22T21:13:27.583Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Normalerweise würde ich nicht jeden quark, den ich mal gemacht habe, in den cv schreiben, aber das machen andere Leute ja auch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412553831264263",
        "createdAt" : "2019-07-22T21:12:36.900Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und bin tutorin an der Uni. Atombau und chemische Bindung aka Quantenmechanik für Chemiker",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412379348295684",
        "createdAt" : "2019-07-22T21:11:55.304Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hatte dieses Jahr 4-5 Vorträge",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412347039539204",
        "createdAt" : "2019-07-22T21:11:47.616Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153412108429746181",
        "createdAt" : "2019-07-22T21:10:50.713Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja ab und an",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412092243927045",
        "createdAt" : "2019-07-22T21:10:46.849Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mathematics of quantum parity games ist neu",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153412053362757636",
        "createdAt" : "2019-07-22T21:10:37.581Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du bist redner?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153412017098842119",
        "createdAt" : "2019-07-22T21:10:28.937Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/nexR4oBM50",
          "expanded" : "https://berlinsides.org/index.html%3Fp=2912.html",
          "display" : "berlinsides.org/index.html%3Fp…"
        } ],
        "text" : "https://t.co/nexR4oBM50",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153411946907099140",
        "createdAt" : "2019-07-22T21:10:13.233Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah das",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153411865151770628",
        "createdAt" : "2019-07-22T21:09:52.728Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oder hier",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153411861091631108",
        "createdAt" : "2019-07-22T21:09:51.739Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153411852363272197",
        "createdAt" : "2019-07-22T21:09:49.663Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/WDbhxSGc6f",
          "expanded" : "https://www3.math.tu-berlin.de/combi/dmg/TES-Summer2019/final_workshop.html",
          "display" : "www3.math.tu-berlin.de/combi/dmg/TES-…"
        } ],
        "text" : "https://t.co/WDbhxSGc6f",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153411844373123082",
        "createdAt" : "2019-07-22T21:09:48.775Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guckst du hier :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153411829223350284",
        "createdAt" : "2019-07-22T21:09:44.149Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und erfolgreich?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153411675191746570",
        "createdAt" : "2019-07-22T21:09:07.426Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153411529116721157",
        "createdAt" : "2019-07-22T21:08:32.593Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "immer noch am quantenzeugs am studieren?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153411024885825540",
        "createdAt" : "2019-07-22T21:06:32.379Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hatte kaum Zeit die letzten Monate",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153410974797500424",
        "createdAt" : "2019-07-22T21:06:20.441Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sollte eh mal zu @sycramore auf github",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153410947006042118",
        "createdAt" : "2019-07-22T21:06:13.813Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ach ja",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1153410887107141639",
        "createdAt" : "2019-07-22T21:05:59.519Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xbGVXnOkeC",
          "expanded" : "https://github.com/redundanceredundance/onetimepad",
          "display" : "github.com/redundanceredu…"
        } ],
        "text" : "hey was ist eigentlich daraus geworden? https://t.co/xbGVXnOkeC",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1153007197862932484",
        "createdAt" : "2019-07-21T18:21:52.527Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Auf jeden Fall bist Du wohl jünger als ich gemäss Deiner Wortwahl und Deinem Foto. :D\n\nIch denke Du sprichst damit das an, was viele Schizophrene beschäftigt: Durch das verabreichen von Psychopharmaka wird auch die eigene Identität verändert. Man ist nicht mehr sich selber, nur weil die meisten Angst haben, dass der Schizophrene vielleicht gemäss nüchterner Statistik zu den 10% gewalttätigen Schizophrenen gehört. Aber wenn man zu den 90% nicht-gewalttätigen Schizos gehört, ist vielleicht genau diese Identität (zwischen Normalität und Wahnsinn) die die man selber viel lieber mag. Diese trockene und nüchterne Welt wo man von jedem schräg angeglupscht wird, wenn man halt mit 40 noch mit seinem Teddy (ᵔᴥᵔ) knuddelt. Oder mit ihm oder Leuten die nicht da sind redet. Das sind zu mir meist ganz tolle Gespräche. Man ist auch weniger alleine wenn man dieses Gremium seiner Seele dabei hat. Bei den meisten Psychischen Krankheiten wird man wegen Vorurteilen ausgegrenzt und bei den meisten Psychischen Krankheiten leidet man auch unter der Krankheit. Aber immer Leute dabei zu haben die auf eine liebevolle weise mit einem blöd tun ist super und ein Pflaster auf die Wunde.\n\nPS: es gibt ganz viele Schizophrenie Gruppen in Facebook, bist Du da auch zu finden?\n\nGruss\nMarc und von meinem (ᵔᴥᵔ)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1138207991000457221",
        "createdAt" : "2019-06-10T22:15:06.669Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und nein, ich halte dich nicht für gaga oder irgendwas. Krankheit ist Krankheit und die Stigmatisierung von psychisch Kranken ist voll für den Arsch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1138187986917629956",
        "createdAt" : "2019-06-10T20:55:37.292Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/w1CMQAkcyc",
          "expanded" : "https://twitter.com/FailDef/status/1138184181215834112",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "War Schizophrenie nicht wieder so ein biochemischer Müll, ein Resultat aus Dopaminmangel oder Dopaminüberschuss? Falls ja, Chemiekeule drauf und Ruhe. Bitte jetzt nicht austicken, ich habe selbst biochemische Probleme, die nur auf SSRI ansprechen und bei Psychotherapie \"fick dich\" sagen. Ich bin also nicht irgendsoeine Tante die rumheult a la ist ja alles nur Einbildung/deine Schuld und was es sonst noch so an grenzdebilen Punkten in der Debatte gibt. https://t.co/w1CMQAkcyc",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1138187886543724549",
        "createdAt" : "2019-06-10T20:55:13.418Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Empfang wird schlecht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018127386355826693",
        "createdAt" : "2018-07-14T13:37:39.505Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bin gleich erstmal off",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018127359382257668",
        "createdAt" : "2018-07-14T13:37:33.091Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Verständlich",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018123570701127686",
        "createdAt" : "2018-07-14T13:22:29.791Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich trau den providern nicht, in mein privates netz sollten die eigentlich nicht kommen, hab auch ne alte kiste als dmz host eingerichtet so dass wenn jemand versucht zuzugreifen dass er nur auf der alten kiste aber nicht in meinem netzwerk landet",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018120246140588037",
        "createdAt" : "2018-07-14T13:09:17.154Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das ist der grund weshalb ich den plaste router von denen retourniert hab und mir nen 500Fr firewall / router zugelegt habe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018119938295386116",
        "createdAt" : "2018-07-14T13:08:03.751Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oh nice",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018119802353745924",
        "createdAt" : "2018-07-14T13:07:31.378Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/mEGoVLBAtB",
          "expanded" : "https://www.hacking-lab.com/index.html",
          "display" : "hacking-lab.com/index.html"
        } ],
        "text" : "die schweiz hat sogar eine webseite wo man legal hacken darf https://t.co/mEGoVLBAtB und du punkte machen kannst",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018119764747673604",
        "createdAt" : "2018-07-14T13:07:22.387Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, aber scannen ohne Einwilligung des Besitzers kann trotzdem illegal sein",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018119762226839558",
        "createdAt" : "2018-07-14T13:07:21.861Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, den hacker paragraphen gibts in der schweiz nicht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018119649609834501",
        "createdAt" : "2018-07-14T13:06:54.941Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Computersabotage oder so",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018119584224817158",
        "createdAt" : "2018-07-14T13:06:39.354Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gibt's für Punkt 2 eine Klausel im Vertrag, die das erlaubt? Falls nein, könnte sowas vom hackerparagraphen erfasst werden. Sowas habt ihr doch sicher auch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018119537034715140",
        "createdAt" : "2018-07-14T13:06:28.100Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "es gibt 2 varianten, mit der ip adresse wurde gespamt und das opfer hat sich bei cablecom gemeldet, oder die cablecom (ist in der schweiz was wie kabel deutschland) scant aktiv die kunden ipadressen was ich recht frech fände",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018119215633588229",
        "createdAt" : "2018-07-14T13:05:11.466Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber wenn du das formular anschaust dann fehlt da z.B. die genaue uhrzeit etc... ausserdem wenn das an einen kunden vershcickt wird der kein plan von IT hat ist der hoffnungslos aufgeshcmissen und wirft hunderte von franken für einen spezialisten raus",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018118980492562436",
        "createdAt" : "2018-07-14T13:04:15.409Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber hat der Provider nicht eh Zugriff auf deine Kiste?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118745800232964",
        "createdAt" : "2018-07-14T13:03:19.486Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich vermute da ich die ip adresse erst seit einem monat habe haben die allenfalls den vorherigen besitzer der ip gemeitn",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018118735863926788",
        "createdAt" : "2018-07-14T13:03:17.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Merke ich",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118633946525700",
        "createdAt" : "2018-07-14T13:02:52.767Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hock zuhause und reg mich über meinen provider auf",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018118609619636228",
        "createdAt" : "2018-07-14T13:02:46.979Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bernau bei Berlin",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118539490807812",
        "createdAt" : "2018-07-14T13:02:30.266Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "arsch der welt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018118502882914310",
        "createdAt" : "2018-07-14T13:02:21.525Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und du?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118499833610244",
        "createdAt" : "2018-07-14T13:02:20.820Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und freue mich aufs Baden mit Freunden",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118488865550341",
        "createdAt" : "2018-07-14T13:02:18.183Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Auch gut. Sitze gerade in einer Bahn am Arsch der Welt",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018118443806183429",
        "createdAt" : "2018-07-14T13:02:07.475Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo, gut und selber?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018118009808973834",
        "createdAt" : "2018-07-14T13:00:23.965Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey, wie geht's dir?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "1018117410375782406",
        "createdAt" : "2018-07-14T12:58:01.043Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, viel Glück bei den Prüfungen :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "964399394614272004",
        "createdAt" : "2018-02-16T07:21:48.084Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Liebe Grüße, Alex",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "964215721181007877",
        "createdAt" : "2018-02-15T19:11:56.901Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sobald ich Zeit habe, schreibe ich dir",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "964215702608666628",
        "createdAt" : "2018-02-15T19:11:52.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey, erstmal vielen Dank für die Mühe. Ich habe momentan leider noch keine Zeit gehabt, mir den Code anzuschauen. Momentan ist Klausurenphase und hier geht alles drunter und drüber",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "964215652738392076",
        "createdAt" : "2018-02-15T19:11:40.598Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo\n\nich hab jetzt mal das ArgParse implementiert, das was ich das Gefühl hatte dass es das nicht unbedingt braucht. Es ist jeweils von Vorteil, wenn man alles in kleine Schritte unterteilt und für jeden kleinen Schritt eine Funktion macht, so behält man die übersicht besser (Divide et Impera)\n\nIch würde vorschlagen dass Du mal die Änderung anschaust wenn Du Zeit hast und dann versuchst die das Crypto zu übernehmen.\n\nich würde allenfalls generell in String verrechnen und dann nur bei der Darstellung Nullen und Einsen angeben, dafür könnte man eine Funktion machen printBits(textString) dann rechnest Du es intern nicht noch zuerst in eine Liste um und zurück... dabei muss man natürlich noch irgendwie die Länge angeben, ich würde da z.B. den Plaintex mass aller Dinge für die Länge nehmen, aber da weiss ich eben noch nicht sogenau was du vor hast, z.B. wenn der Schlüssel kürzer ist, würdest Du dann einfach den Schlüssel wiederholen wie bei der Cäsar oder besser Vigenere Verschlüsselung, also da seh ich noch nicht so ganz durch... :)\n\nGruss\nMarc",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962983837415497732",
        "createdAt" : "2018-02-12T09:36:52.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke für deine hilfe und gute nacht :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962841649339682827",
        "createdAt" : "2018-02-12T00:11:52.661Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so als mentale notizliste",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962840610766979076",
        "createdAt" : "2018-02-12T00:07:45.024Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zumindest kann ich jetzt mal alles forken, was ich schon ewig mal forken wollte",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962840579838226437",
        "createdAt" : "2018-02-12T00:07:37.668Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hat alles Zeit :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962840517615800324",
        "createdAt" : "2018-02-12T00:07:22.815Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Klar mach das",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962840500641386500",
        "createdAt" : "2018-02-12T00:07:18.777Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, ich geh dann mal pennen, danke für Deine Erläuterungen, ich werd mich morgen so gegen abend mal melden",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962839822934138884",
        "createdAt" : "2018-02-12T00:04:37.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also vom code her werde ich nicht alles perfekt korrigieren, ich werde schauen wo etwas falsch ist und nur das korrigieren sonst hast du einen riesen workload von änderungen was dann didaktisch nciht gerade hilfreich ist, aber dafür muss ich mal kurz hinhocken und den code genau anschuen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962838614840365067",
        "createdAt" : "2018-02-11T23:59:49.183Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, da muss ich dann wieder zugeben dass ich das (noch) nciht begreife :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962838342097391621",
        "createdAt" : "2018-02-11T23:58:44.136Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du siehst schon, überlegen kann ich, aber code sieht stellenweise echt noch furchtbar aus :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962838259620556804",
        "createdAt" : "2018-02-11T23:58:24.465Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und dem programm ist es egal, wo der schlüssel herkommt :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962838159880015876",
        "createdAt" : "2018-02-11T23:58:00.712Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also wenn du deinen schlüssel nicht aus dev/random generieren würdest, sondern dir die dinger als verschränkte qubits schickst mit deinem partner/deiner partnerin, dann hast du ein (theoretisch) unangreifbares protokoll",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962838104020279302",
        "createdAt" : "2018-02-11T23:57:47.392Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "2. auf dem ding basiert der quantenschlüsselaustausch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837956431175686",
        "createdAt" : "2018-02-11T23:57:12.185Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "es ist kryptographisch sicher, unter allen bedingungen, gegen quantencomputer und auch, wenn p=np sein sollte",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837920246845444",
        "createdAt" : "2018-02-11T23:57:03.556Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ansonsten, was an dem ding gut ist",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837836138471429",
        "createdAt" : "2018-02-11T23:56:43.596Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gehört in die lange liste von kram, den ich irgendwann mal machen wollte, aber nie geschafft habe",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837796259016709",
        "createdAt" : "2018-02-11T23:56:34.071Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das ist schon länger in der kategorie projekte bei mir drin",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837724721008644",
        "createdAt" : "2018-02-11T23:56:16.942Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "vielen vielen dank",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837679292473348",
        "createdAt" : "2018-02-11T23:56:06.116Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962837668366348294",
        "createdAt" : "2018-02-11T23:56:03.507Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich mach mal kommentare rein, dann solltest du nach dem ich den pull request gemacht haben shcön sehen in welchen zeilen ich änderungen gemacht habe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962836120005472261",
        "createdAt" : "2018-02-11T23:49:54.378Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, ich schau mir das mal in ruhe an aber vermutlcih eerst morgen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835905408065540",
        "createdAt" : "2018-02-11T23:49:03.268Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schon mal vielen dank für die intro in github",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835848571080709",
        "createdAt" : "2018-02-11T23:48:49.635Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mir selbst fehlt leider momentan die zeit",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835818686701572",
        "createdAt" : "2018-02-11T23:48:42.505Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und sobald man das mal geschnallt hat wie einfach das eigentlich ist will man nichts anderes mehr",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835794594533380",
        "createdAt" : "2018-02-11T23:48:36.767Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öh ja, also wenn du an dem ding was schrauben möchtest, immer gerne :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835785748754436",
        "createdAt" : "2018-02-11T23:48:34.652Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835724637818885",
        "createdAt" : "2018-02-11T23:48:20.095Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also das war das 101 mit github",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835692236759044",
        "createdAt" : "2018-02-11T23:48:12.367Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "alles gut",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835670057279494",
        "createdAt" : "2018-02-11T23:48:07.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835650927046661",
        "createdAt" : "2018-02-11T23:48:02.526Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öh ja, hat funktioniert",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835624914046980",
        "createdAt" : "2018-02-11T23:47:56.305Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich habe die teile eigentlich über die oberfläche hochgeladen :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835467342438405",
        "createdAt" : "2018-02-11T23:47:18.746Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann kannst du die README . md mit dem editor öffnen und solltest meinen text da drinn haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835339311288329",
        "createdAt" : "2018-02-11T23:46:48.379Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dort wo du das git add -A &amp;&amp; git commit -m \"initial uplaod\" &amp;&amp; git push gemacht hast",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962835224974516228",
        "createdAt" : "2018-02-11T23:46:20.988Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in dem geclonten verzeichnis das git pull machen?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835126915948548",
        "createdAt" : "2018-02-11T23:45:57.583Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich hatte das ja geclont",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962835092916850693",
        "createdAt" : "2018-02-11T23:45:49.477Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und jetzt willst Du die änderung ja noch auf deiner festplatte also musst du in dem verzeichnis ein \"git pull\" machen dann hast du es auch auf deiner festpaltte",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962834841619324932",
        "createdAt" : "2018-02-11T23:44:49.582Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also so zu sagen das was dem handwerker sein schraubenschlüssel ist",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962834659926249476",
        "createdAt" : "2018-02-11T23:44:06.253Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und so kann man sehr pragmatisch udn effektiv kolaborativ an software arbeiten",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962834602799878148",
        "createdAt" : "2018-02-11T23:43:52.625Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/fkL3cPU7hA",
          "expanded" : "http://README.md",
          "display" : "README.md"
        } ],
        "text" : "jetzt siehst du jetzt hast du die https://t.co/fkL3cPU7hA die ich abgeändert habe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962834489805365253",
        "createdAt" : "2018-02-11T23:43:25.712Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Got it",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962834453327437830",
        "createdAt" : "2018-02-11T23:43:16.980Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja hat geklappt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962834410545598468",
        "createdAt" : "2018-02-11T23:43:06.788Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber ich glaub es ist einfach der grüne knopf new pull request",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962833812479725582",
        "createdAt" : "2018-02-11T23:40:44.196Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "klick mal auf readme, geht da ein fenster auf?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962833446447013892",
        "createdAt" : "2018-02-11T23:39:16.931Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/IFBcG1SWMZ",
          "expanded" : "https://twitter.com/messages/media/962833113083719684",
          "display" : "pic.twitter.com/IFBcG1SWMZ"
        } ],
        "text" : " https://t.co/IFBcG1SWMZ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/962833113083719684/962832859194183680/2pA9QA9g.jpg" ],
        "senderId" : "594915995",
        "id" : "962833113083719684",
        "createdAt" : "2018-02-11T23:37:57.653Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also das müsstest du jetzt unter den meldungen haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962832854555287557",
        "createdAt" : "2018-02-11T23:36:55.805Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mach mal nen screenshot vom github",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962832725278420996",
        "createdAt" : "2018-02-11T23:36:24.981Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962832426673287174",
        "createdAt" : "2018-02-11T23:35:13.782Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "äh, wie integriere ich den in den master branch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962832423074529284",
        "createdAt" : "2018-02-11T23:35:12.956Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was ich jetzt gemacht habe ist eine pull request, den musst du jetzt annehmen und hast meine änderungen auch in dienem repository",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962831087415242757",
        "createdAt" : "2018-02-11T23:29:54.487Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und jetzt solltest du das wieder in Deinem git sehen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962830970381635589",
        "createdAt" : "2018-02-11T23:29:26.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und jetzt sende ich das über das webinterface wieder zu dir",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962830711580430341",
        "createdAt" : "2018-02-11T23:28:24.936Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xd7zub9REA",
          "expanded" : "https://twitter.com/messages/media/962830657729843204",
          "display" : "pic.twitter.com/xd7zub9REA"
        } ],
        "text" : " https://t.co/xd7zub9REA",
        "mediaUrls" : [ "https://ton.twitter.com/dm/962830657729843204/962830646862282752/3_eI3Dva.jpg" ],
        "senderId" : "75128838",
        "id" : "962830657729843204",
        "createdAt" : "2018-02-11T23:28:12.460Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann hab ich jetzt das readme geändert",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962830625861496836",
        "createdAt" : "2018-02-11T23:28:04.442Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schon klar :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962830254380404740",
        "createdAt" : "2018-02-11T23:26:35.883Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du siehst jetzt ist da braindef also mein benutzer",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962830215184633860",
        "createdAt" : "2018-02-11T23:26:26.531Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962830189087678468",
        "createdAt" : "2018-02-11T23:26:20.315Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/mbPgguSR4m",
          "expanded" : "https://twitter.com/messages/media/962830147668848646",
          "display" : "pic.twitter.com/mbPgguSR4m"
        } ],
        "text" : " https://t.co/mbPgguSR4m",
        "mediaUrls" : [ "https://ton.twitter.com/dm/962830147668848646/962830139666128896/TwTYtv5W.jpg" ],
        "senderId" : "75128838",
        "id" : "962830147668848646",
        "createdAt" : "2018-02-11T23:26:10.612Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann hole ich das projekt dass ich von dir ge-fork-t habe bei mir",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962830106887622660",
        "createdAt" : "2018-02-11T23:26:00.719Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ein fork",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962830064835538949",
        "createdAt" : "2018-02-11T23:25:50.707Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "allenfalls musst du F5 drücken",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829850384977924",
        "createdAt" : "2018-02-11T23:24:59.572Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829790960062468",
        "createdAt" : "2018-02-11T23:24:45.397Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das solltest du jetzt in git sehen dass ich das ge-fork-t habe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829778968567814",
        "createdAt" : "2018-02-11T23:24:42.530Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich meine forken",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829725273030661",
        "createdAt" : "2018-02-11T23:24:29.764Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und jetzt clone ich das repository",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829628548280324",
        "createdAt" : "2018-02-11T23:24:06.680Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/nNuoLOgiPc",
          "expanded" : "https://twitter.com/messages/media/962829521845149700",
          "display" : "pic.twitter.com/nNuoLOgiPc"
        } ],
        "text" : " https://t.co/nNuoLOgiPc",
        "mediaUrls" : [ "https://ton.twitter.com/dm/962829521845149700/962829512093458432/OcRSurzH.jpg" ],
        "senderId" : "75128838",
        "id" : "962829521845149700",
        "createdAt" : "2018-02-11T23:23:41.385Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das macht es jetzt für mich einfacher:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962829488903131142",
        "createdAt" : "2018-02-11T23:23:33.377Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gut, wenn ich das ding auf meinem rechner ändern möchte, nehme ich git clone ?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962829415783821316",
        "createdAt" : "2018-02-11T23:23:15.933Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wieso bin ich nie auf diese idee gekommen? irgendwie fehlt mir die wand zum kopf dagegenschlagen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962829330895310853",
        "createdAt" : "2018-02-11T23:22:55.689Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/iH24GUFIAr",
          "expanded" : "https://github.com/redundanceredundance/onetimepad",
          "display" : "github.com/redundanceredu…"
        } ],
        "text" : "https://t.co/iH24GUFIAr",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962829265782951940",
        "createdAt" : "2018-02-11T23:22:40.205Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh, gute idee",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962827627701657604",
        "createdAt" : "2018-02-11T23:16:09.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mach nen zweiten account",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962827604050014212",
        "createdAt" : "2018-02-11T23:16:04.003Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mir ist das ding einfach so peinlich, dass ich nicht will, das irgendwer das sieht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962827563398778884",
        "createdAt" : "2018-02-11T23:15:54.345Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "verwende github, du wirst es lieben, es ist so viel toller als mit textdateien rumzufummeln",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962827370477563909",
        "createdAt" : "2018-02-11T23:15:08.366Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann kann man mit git add -A &amp;&amp; git commit -m \"initial upload\" &amp;&amp; git push das verzeichnis hochladen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962827271122898948",
        "createdAt" : "2018-02-11T23:14:44.606Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich würde jetzt mal rasch empfehlen per button bei github ein neues repository zu machen, dann den code den github ausgibt im verzeichnis wo die dateien sind ausführen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962827156563865605",
        "createdAt" : "2018-02-11T23:14:17.307Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber um mit schlüsseln zu arbeiten, brauche ich die schlüssel im binärformat, bevor ich da irgendwelche bits abschneide und/oder ändere",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962827029950488580",
        "createdAt" : "2018-02-11T23:13:47.120Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das klappt auch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826929559822341",
        "createdAt" : "2018-02-11T23:13:23.174Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "der zufallszahlengenerator haut mir ja asciicode raus und den will ich irgendwie in binär konvertieren",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826921464745989",
        "createdAt" : "2018-02-11T23:13:21.244Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "problem beim programm ist neben dem fehlenden parser vor allem die inkompabilität von ascii und binärcode",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826850333536260",
        "createdAt" : "2018-02-11T23:13:04.445Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hatte mit 2 einfachen testfiles mal getestet, ob die \"verschlüsselung\" schon funktioniert",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826744045752325",
        "createdAt" : "2018-02-11T23:12:38.941Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öh, momentan noch nicht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826655625547782",
        "createdAt" : "2018-02-11T23:12:17.898Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hatte nur noch nicht die zeit, das zu modifizieren und hochzuladen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826615200903172",
        "createdAt" : "2018-02-11T23:12:08.224Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kannst du sonst mal rasch das ganze verzeichnis in github hochladen, da braucht ja noch andere dateien oder?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962826606048837636",
        "createdAt" : "2018-02-11T23:12:06.067Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das ist neben ein paar quantensachen so das einzige, wo ich wirklich stolz drauf bin",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826558900723716",
        "createdAt" : "2018-02-11T23:11:54.813Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "da soll als erstes mein eines skript aus der ba hoch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826498670579718",
        "createdAt" : "2018-02-11T23:11:40.439Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sycramore",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826435550466053",
        "createdAt" : "2018-02-11T23:11:25.399Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie twitter",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826428051050502",
        "createdAt" : "2018-02-11T23:11:23.608Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öh ja, aber da ist noch nichts drauf",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962826412582473732",
        "createdAt" : "2018-02-11T23:11:19.916Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hast du einen github account?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962825998164258823",
        "createdAt" : "2018-02-11T23:09:41.123Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "relativ trivial, einfach binärcode des klartextes mit dem schlüssel xor machen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962825466016157701",
        "createdAt" : "2018-02-11T23:07:34.254Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was ein one time pad ist, weißt du?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962825386232090628",
        "createdAt" : "2018-02-11T23:07:15.296Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "klar",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962825356511252487",
        "createdAt" : "2018-02-11T23:07:08.132Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, ich les den code mal durch, dauert rasch eine weile",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962824570142101509",
        "createdAt" : "2018-02-11T23:04:00.657Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "u've got mail :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962823433426997252",
        "createdAt" : "2018-02-11T22:59:29.629Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn man es nicht hochlädt sieht niemand die fehelr dann gibt auch neimand verbesserungs tips",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962822402878136324",
        "createdAt" : "2018-02-11T22:55:23.949Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber das ist eigentlich kontraprudktiv",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962822311039598596",
        "createdAt" : "2018-02-11T22:55:02.082Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "haben immer alle so hemmungen code zu veröffentlichen auf github",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962822257163882502",
        "createdAt" : "2018-02-11T22:54:49.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mail@marclandolt.ch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962822133943545863",
        "createdAt" : "2018-02-11T22:54:19.816Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich sag immer allen, lädt es einfach hoch, erstens schauts eh niemand an, zweitens falls doch wenn du glück hast hilft dir ncoh wer beim korrigieren",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962822118504312836",
        "createdAt" : "2018-02-11T22:54:16.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hast du ne mail adresse?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821982571122697",
        "createdAt" : "2018-02-11T22:53:43.726Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gibt da auch noch andere probleme",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821952833511429",
        "createdAt" : "2018-02-11T22:53:36.688Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "noch nicht. war mir bisher zu peinlich, das hochzuladen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821911452487684",
        "createdAt" : "2018-02-11T22:53:26.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821865327783941",
        "createdAt" : "2018-02-11T22:53:15.773Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hast du es auf github?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821863041912841",
        "createdAt" : "2018-02-11T22:53:15.236Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ja, momentan aktuellstes problem ist mein eines one time pad, was ein paar parameter vom terminal nehmen soll und bisher leicht beknackt aussieht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821801159127045",
        "createdAt" : "2018-02-11T22:53:00.484Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "argparse muss man nur die sdk lesen, ist da noch so ein bisschen tricky mit den parametern ich verwende meist stackexchange und nehm beispiele bin schneller als mit sdk",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821792552407044",
        "createdAt" : "2018-02-11T22:52:58.450Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pipe und co kenne ich :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821635366637574",
        "createdAt" : "2018-02-11T22:52:20.957Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dann muss man noch den pipe kennen | wo man mit cat datei.txt | cut -d -f1 z.b. die erste spalte nimmt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821582384193541",
        "createdAt" : "2018-02-11T22:52:08.309Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öh ja, jedenfalls bin ich bei argparse verzweifelt",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821564352868357",
        "createdAt" : "2018-02-11T22:52:04.012Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das ist alles :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821516718215173",
        "createdAt" : "2018-02-11T22:51:52.659Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mir fehlt einfach die übung in bash",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821489430089732",
        "createdAt" : "2018-02-11T22:51:46.200Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das ist mir klar :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821436732772356",
        "createdAt" : "2018-02-11T22:51:33.588Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "shell script ist einfach das aneinanderreihen von seriell ablaufenden befeheln",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821394382848004",
        "createdAt" : "2018-02-11T22:51:23.496Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "python wäre die erste frage, wie ich bei nem skript die kommandos vom terminal parsen kann",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821378624966661",
        "createdAt" : "2018-02-11T22:51:19.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und wer lineare algebra schnallt der hat eh kein problem mit shellscript",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821289244274693",
        "createdAt" : "2018-02-11T22:50:58.479Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "linux passt einigermaßen, python auch",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821265613574148",
        "createdAt" : "2018-02-11T22:50:52.779Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sehr schön",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821232327569414",
        "createdAt" : "2018-02-11T22:50:44.847Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "geil, geil, geil, geil, geil",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962821214384386052",
        "createdAt" : "2018-02-11T22:50:40.571Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn Du fragen zu Shell-Script, pyhton, c++, linux, windows, osx oder so hast nur fragen, da komme ich ein bisschen draus",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821169698234372",
        "createdAt" : "2018-02-11T22:50:29.921Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke für die links, da hab ich was wo ich mich damit beschäftigen kann",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962821034738110474",
        "createdAt" : "2018-02-11T22:49:57.750Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/nAghfFq9LT",
          "expanded" : "https://www.youtube.com/watch?v=uTbXlq5ttHA",
          "display" : "youtube.com/watch?v=uTbXlq…"
        } ],
        "text" : "https://t.co/nAghfFq9LT",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820804495933444",
        "createdAt" : "2018-02-11T22:49:02.890Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "habe ich auch noch was für dich",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820797176958980",
        "createdAt" : "2018-02-11T22:49:01.097Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und wenn du langeweile hast und einem mädchen im blumenkleid zuhören möchtest",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820771683930116",
        "createdAt" : "2018-02-11T22:48:55.037Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "klar",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820722858094596",
        "createdAt" : "2018-02-11T22:48:43.415Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, ich muss da einen strategischen stop vorschlagen, denn sonst hab ich nen buffer overflow in meinem gehirn, muss zuerst das was du bisher gesagt hast sichten",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962820689010089990",
        "createdAt" : "2018-02-11T22:48:35.378Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(mach dir keine sorge, was ich dir in einer stunde zu erklären versuche, hat für mich 7 jahre und ein physikstudium gebraucht um es zu verstehen)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820568482447365",
        "createdAt" : "2018-02-11T22:48:06.594Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "klar kann ich dir auch was über qubits erzählen, aber ich habe es halt seinerzeit erst über lina kapiert",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820470193164292",
        "createdAt" : "2018-02-11T22:47:43.144Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oder videos geschaut habe :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962820437322461190",
        "createdAt" : "2018-02-11T22:47:35.300Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber im normalfall schnall ich es dann schon wenn ich alles nachgelesen habe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962820401222094852",
        "createdAt" : "2018-02-11T22:47:26.711Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schau dir einfach die videos an :D die machen spaß",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820399699562502",
        "createdAt" : "2018-02-11T22:47:26.359Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in der schweiz sagen wir: \"Ich versteh nur Bahnhof\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962820326160850948",
        "createdAt" : "2018-02-11T22:47:08.824Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "öhm ja, und wenn du über dwave reden willst, dann redest du über optimierungsprobleme",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820295978639364",
        "createdAt" : "2018-02-11T22:47:01.605Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber in der realität sind das auch nur 2, da ein parameter durch die normierung (für die wahrscheinlichkeitsinterpretation) in der qm bestimmt ist und der andere durch diese komische globale phase, von der alle reden und die nie jemand kapiert :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820191150395396",
        "createdAt" : "2018-02-11T22:46:36.631Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "b) ansonsten hast du 4 freie parameter statt 2, wenn du dir zweidimensionale komplexe vektorräume anschaust",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962820025362145284",
        "createdAt" : "2018-02-11T22:45:57.090Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "a) sie sind algebraisch abgeschlossen, du findest also (fast) immer eine diagonalform",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819913445408772",
        "createdAt" : "2018-02-11T22:45:30.406Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also komplexe vektorräume haben ein paar eigenschaften",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819809716076548",
        "createdAt" : "2018-02-11T22:45:05.714Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und auch für die basics aus quanteninfo mit den logischen gattern",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819723770646532",
        "createdAt" : "2018-02-11T22:44:45.185Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah da muss ich noch schauen, dass hatte ich nie gemacht, lineare algebra mit complexen zahlen, diese kenne ich einzeln aber ncith kombiniert",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962819723703586821",
        "createdAt" : "2018-02-11T22:44:45.178Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819656804438020",
        "createdAt" : "2018-02-11T22:44:29.214Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn du die videos durchhast, solltest du mathematisch fit genug sein für so ziemlich alles, was die physiker in qm im studium (bachelor) machen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819649703464964",
        "createdAt" : "2018-02-11T22:44:27.531Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/yG21YPUPIn",
          "expanded" : "https://www.youtube.com/watch?v=M6aH84622Ms",
          "display" : "youtube.com/watch?v=M6aH84…"
        } ],
        "text" : "https://t.co/yG21YPUPIn",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819423626321926",
        "createdAt" : "2018-02-11T22:43:33.653Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und noch mehr über skalarprodukte",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819418693816324",
        "createdAt" : "2018-02-11T22:43:32.444Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "zum thema shellskript - momentan nichts, ich habe nur bisher keine gelegenheit zum üben gehabt, die ich vielleicht bald kriegen werde :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819335944310789",
        "createdAt" : "2018-02-11T22:43:12.747Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "funktionen als vektoren - das kapieren die meisten physiker nicht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819233855033354",
        "createdAt" : "2018-02-11T22:42:48.378Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Cie8QgJkCc",
          "expanded" : "https://www.youtube.com/watch?v=-hGBYv7fui8",
          "display" : "youtube.com/watch?v=-hGBYv…"
        } ],
        "text" : "https://t.co/Cie8QgJkCc",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819184710373384",
        "createdAt" : "2018-02-11T22:42:36.702Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und hier gehts ans verstehen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819180926992390",
        "createdAt" : "2018-02-11T22:42:35.761Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was ist Dir unklar?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962819136824008708",
        "createdAt" : "2018-02-11T22:42:25.259Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja shellscripts sind voll easy",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962819106872479748",
        "createdAt" : "2018-02-11T22:42:18.097Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also linalg hab ich schon gerafft, ist einfach schon verdammt lange her",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962819042007543812",
        "createdAt" : "2018-02-11T22:42:02.658Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "da hakts bei mir noch ziemlich",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819026656382981",
        "createdAt" : "2018-02-11T22:41:58.971Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du kannst mir vielleicht irgendwann mit shellskripten helfen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962819007178035204",
        "createdAt" : "2018-02-11T22:41:54.324Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool, dass du das nachgearbeitet hast. nicht schlecht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818954526969860",
        "createdAt" : "2018-02-11T22:41:41.792Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/t3TAYyAo6h",
          "expanded" : "https://www.youtube.com/watch?v=adosVCpcl5Q",
          "display" : "youtube.com/watch?v=adosVC…"
        } ],
        "text" : "https://t.co/t3TAYyAo6h",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818896003846148",
        "createdAt" : "2018-02-11T22:41:27.884Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ka wie gut das video ist",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818886814064644",
        "createdAt" : "2018-02-11T22:41:25.667Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja war bei mir auch so, nach dem hochkantigen rausfligen aus der FH hab ich auch alles noch nachgearbeitet, bekifft in der mathe hocken war trotzdem toll :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818729666019332",
        "createdAt" : "2018-02-11T22:40:48.179Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "der zusammenhang von linearer abbildung und matrix",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818720748929032",
        "createdAt" : "2018-02-11T22:40:46.052Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Bm4EGqdER8",
          "expanded" : "https://www.youtube.com/watch?v=KKo5wRfpQXg",
          "display" : "youtube.com/watch?v=KKo5wR…"
        } ],
        "text" : "https://t.co/Bm4EGqdER8",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818658174152709",
        "createdAt" : "2018-02-11T22:40:31.179Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(ich habe es die ersten Semester auch nicht gerafft. Ich bin damals in Lina für Mathematiker knapp durchgefallen und bei Lina 2 mit ner 4,0 durchgerutscht. Und meine Quantenklausur war auch nicht so der Bringer. Ich habe das später nachgeholt und mir Stück für Stück zusammengepusselt, weil mich das Thema nie losgelassen hat)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818494202023940",
        "createdAt" : "2018-02-11T22:39:52.059Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was zum Thema Dimension und Basis",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818282121322501",
        "createdAt" : "2018-02-11T22:39:02.116Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/HwdykXDJzH",
          "expanded" : "https://www.youtube.com/watch?v=UfJgVy0W0QU",
          "display" : "youtube.com/watch?v=UfJgVy…"
        } ],
        "text" : "https://t.co/HwdykXDJzH",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818252941426694",
        "createdAt" : "2018-02-11T22:38:54.535Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818228295790596",
        "createdAt" : "2018-02-11T22:38:48.634Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hexe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818199862603780",
        "createdAt" : "2018-02-11T22:38:41.862Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hexe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818194477146116",
        "createdAt" : "2018-02-11T22:38:40.568Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BwWrHM2tKc",
          "expanded" : "https://www.youtube.com/watch?v=GGbd_yCvTIQ",
          "display" : "youtube.com/watch?v=GGbd_y…"
        } ],
        "text" : "https://t.co/BwWrHM2tKc",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818191771820037",
        "createdAt" : "2018-02-11T22:38:39.952Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hexe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818188697366533",
        "createdAt" : "2018-02-11T22:38:39.200Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kleine Einführung in Lina",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818186398896132",
        "createdAt" : "2018-02-11T22:38:38.653Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "für mich ist's jetzt schon hexerei :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962818149036052484",
        "createdAt" : "2018-02-11T22:38:29.760Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(bis auf den Dichtematrixformalismus, aber den brauchst du nur, wenn du theoretische Quanteninfo machen willst. Da wirds dann eklig)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962818019562016772",
        "createdAt" : "2018-02-11T22:37:58.874Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962817961957494788",
        "createdAt" : "2018-02-11T22:37:45.128Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Anwendung ist dann relativ easy :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817928646348804",
        "createdAt" : "2018-02-11T22:37:37.188Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich schicke dir erstmal ein paar schöne Mathevideos",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817889614094340",
        "createdAt" : "2018-02-11T22:37:27.883Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Der Rest ist einfach Anwendung der linearen Algebra. Du kannst die Matrizen nur bei endlichdimensionalen Vektorräumen bringen, aber das Skalarprodukt funktioniert auch für Funktionenräume",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817853014659076",
        "createdAt" : "2018-02-11T22:37:19.168Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das wars eigentlich auch schon",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817699742175236",
        "createdAt" : "2018-02-11T22:36:42.674Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du wählst eine Basis, die dir dein Experiment darstellt und schaust dir den Zustand vorher an. Dieser Zustand ist eine Linearkombination aus Basisvektoren und kann in der von dir gewählten Basis geschrieben werden. Das Skalarprodukt (und für QM ist es das ganz billige Standartskalarprodukt oder auch euklidisches Skalarprodukt) gibt dir die Projektion. Für reale Vektorräume ist das recht leicht ersichtlich. Für komplexe Vektorräume gilt das so naja, nicht direkt, aber für QM interpretiert man es so",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817638824103940",
        "createdAt" : "2018-02-11T22:36:28.119Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also Quantenmechanik geht zu 90% um Projektionen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817240730161162",
        "createdAt" : "2018-02-11T22:34:53.179Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Daher jetzt mal so",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817181896634373",
        "createdAt" : "2018-02-11T22:34:39.171Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(es sei denn, man hat Lina für Mathematiker und Quanten bei einem ziemlich coolen Prof für Quanteninfo gehört :D)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817142759542788",
        "createdAt" : "2018-02-11T22:34:29.811Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Physiker wissen nicht, wo sie herkommt und die Mathematiker kennen sie nicht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962817046437355526",
        "createdAt" : "2018-02-11T22:34:06.853Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die von dir erwähnte Dirac-Notation ist eine recht clevere und kompakte Notation, welche einige Eigenschaften der kanonischen dualen Basis ausnutzt, um Skalarprodukte darzustellen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816989214466052",
        "createdAt" : "2018-02-11T22:33:53.240Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also erstens : Superposition ist ein komisches Wort von Physikern für die ganz normale Linearkombination aus der Mathevorlesung im ersten Semester",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816808427446276",
        "createdAt" : "2018-02-11T22:33:10.162Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja das ist mir klar &lt;1|0&gt; notation oder so",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962816718220480518",
        "createdAt" : "2018-02-11T22:32:48.599Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oder ich erzähle dir ein paar Dinge über lineare Algebra",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816673639272453",
        "createdAt" : "2018-02-11T22:32:38.002Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja ich werd das was du geschrieben hast danach eh noch genauer googeln",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962816637333360644",
        "createdAt" : "2018-02-11T22:32:29.333Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also ich könnte dir jetzt ewig irgendwelchen Kram über Superposition und so erzählen und Katze lebendig, Katze tot usw",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816627883626500",
        "createdAt" : "2018-02-11T22:32:27.064Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber Projektion klingt schon mal nicht schlecht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816536011530244",
        "createdAt" : "2018-02-11T22:32:05.164Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, bei QM hast du es meistens nur mit Endomorphismen (n mal n Matrix) zu tun",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816481863008261",
        "createdAt" : "2018-02-11T22:31:52.258Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also projektion eines n dimensionalen irgendwas auf etwas n-1 dimensionals?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962816372282667012",
        "createdAt" : "2018-02-11T22:31:26.142Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber wie man aus der Abbildung die Matrixdarstellung berechnet?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816363357261828",
        "createdAt" : "2018-02-11T22:31:23.999Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also klar, die Matrix ist eine Darstellung einer linearen Abbildung bezüglich einer gewissen Basis",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816314875301897",
        "createdAt" : "2018-02-11T22:31:12.429Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Weißt du, wie der Zusammenhang zwischen linearer Abbildung und Matrix ist?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816219442237445",
        "createdAt" : "2018-02-11T22:30:49.690Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Passt doch erstmal",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962816147866423300",
        "createdAt" : "2018-02-11T22:30:32.628Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich kenn gatter und ich ken matrixen von linearer algebra aber gatter in matrix form nicht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962816078874324999",
        "createdAt" : "2018-02-11T22:30:16.206Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Logische Gatter in Matrixform, um genau zu sein",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815915204268044",
        "createdAt" : "2018-02-11T22:29:37.143Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Standartmodell : Circuit Model = logische Gatter",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815876188819461",
        "createdAt" : "2018-02-11T22:29:27.846Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Normale Quantenmechanik = lineare Algebra",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815825664147462",
        "createdAt" : "2018-02-11T22:29:15.812Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Normaler Quantencomputer = lineare Algebra",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815790868320260",
        "createdAt" : "2018-02-11T22:29:07.509Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zum Thema Mathe",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815762477051908",
        "createdAt" : "2018-02-11T22:29:00.740Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Zweite Sache - der Rekord liegt für universelle Quantencomputer bei ca 50 Qubits (IBM), für adiabatische Quantencomputer bei ca 512 Qubits (oder sind die inzwischen bei 1024?)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815732538015749",
        "createdAt" : "2018-02-11T22:28:53.608Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also so zu sagen das qubit-CRC :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962815681388564485",
        "createdAt" : "2018-02-11T22:28:41.419Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also topologische Qubits sind die Qubits, auf denen man dann so einigermaßen rechnen kann. Die erstrecken sich meistens über mehrere physische Qubits, eins zum Rechnen und x für Fehlerkorrektur",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815554791923716",
        "createdAt" : "2018-02-11T22:28:11.256Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich hatte mir die vor einiger Zeit mal angeschaut, kriege die Details aber nicht mehr auf die Reihe",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815356585771012",
        "createdAt" : "2018-02-11T22:27:23.968Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also so ein \"quantencomputer für dummies\" wär glaub das ricthige für mich :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962815347668799494",
        "createdAt" : "2018-02-11T22:27:21.832Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Da stehen 5Qubit und 7 Qubit Fehlerkorrekturcodes erklärt drin.",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815281725878277",
        "createdAt" : "2018-02-11T22:27:06.177Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es gibt so ein Standartlehrbuch, den Nielsen-Chuang von 1997 (?, jedenfalls aus den 90ern)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815219046277124",
        "createdAt" : "2018-02-11T22:26:51.167Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "äh ja, jedenfalls gibts für Quantencomputer (im circuit model, das ist so das standartmodell) Fehlerkorrekturcodes, die zusätzliche Qubits brauchen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962815120408829956",
        "createdAt" : "2018-02-11T22:26:27.653Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(ok, ich will dir keinen Kaffee abschwatzen :D)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814988401434629",
        "createdAt" : "2018-02-11T22:25:56.176Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Solltest du Interesse haben und mal nach Berlin kommen, spendier mir nen Cafe und ich kann ihn dir erklären. Und auch sonst so ein paar Sachen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814895166246916",
        "createdAt" : "2018-02-11T22:25:33.956Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das No-Cloning Theorem kannst du mit etwas linearer Algebra gut verstehen. Da die meisten Infos auch an der Uni relativ wenig mathe machen und Quanten auf Lina 2 für Mathematiker aufsetzt (also auch die Physiker oft die Mathe nur in der Theorievorlesung lernen), erspare ich dir den Beweis",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814796583325700",
        "createdAt" : "2018-02-11T22:25:10.456Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Im Quantencomputer geht das NICHT (aufgrund des No-Cloning-Theorems).",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814592513708037",
        "createdAt" : "2018-02-11T22:24:21.826Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Im normalen Computer speicherst du Bits ja oft redundant oder dreifach zur Fehlerkorrektur",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814533806018567",
        "createdAt" : "2018-02-11T22:24:07.829Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Topologische Qubits nehmen in Kauf, dass die physikalischen Qubits ziemlich noisy sind und man Fehlerkorrekturcodes braucht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814468161003524",
        "createdAt" : "2018-02-11T22:23:52.142Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also physikalische Qubits sind die Teile, die du physikalisch direkt realisierst. Also Kreisströme in Supraleitern, Photonen, Atome in ultrakalten Fallen usw",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814360300158982",
        "createdAt" : "2018-02-11T22:23:26.494Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es gibt physikalische Qubits und topologische Qubits. Davon wirst du bei den CCC-Vorträgen über QM aber wenig hören, die covern eher die Basics",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814229093998596",
        "createdAt" : "2018-02-11T22:22:55.186Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also 1 und 2 hängen zusammen",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814094184198148",
        "createdAt" : "2018-02-11T22:22:22.985Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Quantenheilung? Please not seriously :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962814069748260868",
        "createdAt" : "2018-02-11T22:22:17.150Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wenn ich das Glück habe einen Quanten(heilungs)spezialisten im Chat zu haben hätte ich schon ein paar fragen:\n\n1. Wieviele QBITS kriegen die heute hin\n\n2. Stimmt dass dass wegen dem Quantenrauschen die Anzahl der QBITS begrenzt ist\n\n3. Wie heftig die Mathe damit man Quantencomputer überhaupt nutzen kann, da gibts ja spezielle Algorithmen um die Zustände mit mehreren Durchläufen auszulesen etc...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962813882086707204",
        "createdAt" : "2018-02-11T22:21:32.434Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich finde die zwei toll :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962813875996545029",
        "createdAt" : "2018-02-11T22:21:30.966Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ach der. Ja von dem habe ich schon gehört",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962813668240052228",
        "createdAt" : "2018-02-11T22:20:41.440Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also den @Icewalker1974 und den @ReinhardRemfort, die zwei machen noch nen coolen wissenschafts podcast: @MInkorrekt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962811885182771211",
        "createdAt" : "2018-02-11T22:13:36.329Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "science slam? ja, kenne ich",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962811526418661380",
        "createdAt" : "2018-02-11T22:12:10.772Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "äh ja, also ich bin auf meine alten tage dann über den umweg über chemie irgendwie doch bei physik gelandet ^^",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962811504042094596",
        "createdAt" : "2018-02-11T22:12:05.477Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kennst du ?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962811485876576261",
        "createdAt" : "2018-02-11T22:12:01.401Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gute idee :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962811410542710788",
        "createdAt" : "2018-02-11T22:11:43.225Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "haha",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962811397393567749",
        "createdAt" : "2018-02-11T22:11:40.015Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/v8i0bo9sze",
          "expanded" : "https://www.youtube.com/watch?v=WPKb7WCu1D8",
          "display" : "youtube.com/watch?v=WPKb7W…"
        } ],
        "text" : "dann wär das was für dich: https://t.co/v8i0bo9sze",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962811335867338758",
        "createdAt" : "2018-02-11T22:11:25.392Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bringt mir im studium allerdings eher weniger :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810866860220425",
        "createdAt" : "2018-02-11T22:09:33.534Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und allem, wo man labern konnte",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810824430706693",
        "createdAt" : "2018-02-11T22:09:23.424Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dafür ne eins in geschichte ^^",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810789865447435",
        "createdAt" : "2018-02-11T22:09:15.225Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie gesagt ich war in der schule auch oft (typisch mädchen) oft voll die niete in mathe oder physik",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810747133874186",
        "createdAt" : "2018-02-11T22:09:04.978Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und fh ist ja viel stärker angewandt als die uni",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810641122840581",
        "createdAt" : "2018-02-11T22:08:39.707Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber zum thema fh - es ist nie zu spät",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810601834729476",
        "createdAt" : "2018-02-11T22:08:30.346Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du darfst gerne raten, was ich jetzt studiere :D",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810266865033221",
        "createdAt" : "2018-02-11T22:07:10.493Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ansonsten - ich habe in der schule in der 11. klasse physik hardcore versemmelt und danach dieses scheißfach in die tonne gekloppt. in der schule war ich auch oft voll die niete in mathe (ohne kiffen, einfach die textaufgaben nicht geschnallt)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962810236582223876",
        "createdAt" : "2018-02-11T22:07:03.281Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ja, ich bin im entsprechenden umfeld, kenne die beiden von dir genannten personen allerdings nicht",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962809943672967172",
        "createdAt" : "2018-02-11T22:05:53.666Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "da du ja scheinbar auch im ccc umfeld bist kennst du ja sicher den nicolas worl und der reinhold remford, ich hab einfach von denen so ein bisschen von NV-Zentren zeugs gehört aber mcih nie so wirklich mit quanten computern auseinandergesetz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962809479187361797",
        "createdAt" : "2018-02-11T22:04:02.837Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja es war nicht so ein richtiges gymnasium, in der schweiz heisst es matura und es gibt eine berufsmatura auf der dann der Stoff in der FH aufbaut, aber wenn man dann in 2 Wochen den 4 Jahre durchgenommenen Stoff begreifen soll in der FH merkt man dass es zwar toll war bekifft im mathe unterricht zu hocken aber dass man den anschluss versifft hat",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962809214799400964",
        "createdAt" : "2018-02-11T22:02:59.650Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(erklären, keinen access)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962809069370298374",
        "createdAt" : "2018-02-11T22:02:25.004Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich kann höchstens quantencomputer anbieten :)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962809043206180868",
        "createdAt" : "2018-02-11T22:02:18.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "zum thema ML - ich kenne da ein paar menschen, die mir da noch was erklären sollen und irgendwann hoffe ich auch, die zeit dafür zu haben",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962809006413828100",
        "createdAt" : "2018-02-11T22:02:09.962Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was hat denn im gymnasium bekifft im matheunterricht hocken mit späterem erfolg an der fh zu tun?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962808869012549636",
        "createdAt" : "2018-02-11T22:01:37.214Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "594915995",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mal informaitk studiert, hochkant rausgefolgen weil ich im gymnasium immer bekifft im Matheunterricht gehockt bin, dann als tellerwascher und fabrikarbeiter gearbeitet mir die Technikerschule für Informatik finanziert, dann Technikerschule abgeschlossen und informatik stellen gemacht, aber ich leide immer noch unter dem versagen an der Fachhochschule und kompensiere das mit irgendwelchem rumgehacke, zur zeit versuch ich mir grad in künstlichen intelligenz algorithmen, gensim ist einfach und noch recht spannend, zur zeit versuch ich mir grad Bayes’sches Netzwerke zu lernen und wenn Du jemanden kennst der mir Alpha Go Zero erklären kann wäre ich noch so dankbar :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "962808602602934276",
        "createdAt" : "2018-02-11T22:00:33.766Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(ich tippe auf letzteres) :-)",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962799450749132806",
        "createdAt" : "2018-02-11T21:24:11.759Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was machst du eigentlich so? Entwicklung? Sysadminfoo?",
        "mediaUrls" : [ ],
        "senderId" : "594915995",
        "id" : "962799418641731588",
        "createdAt" : "2018-02-11T21:24:04.059Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-600137781",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/704ESfHre3",
          "expanded" : "https://twitter.com/FailDef/status/1290989578950762496",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "Halli Hallo, ich wollte rasch fragen, ob es sein kann, dass Dein Bruder z.B. zuviel berechtigt kritische Fragen z.B. zum Arbeitgeber oder zu Familien Angelegenheiten gestellt hat vor der Ersthopitalisierung in der Psychiatrie. War er für irgendwer / irgendwas etwa als Sündenbock deklariert worden, allenfalls auch nur nonverbal? Gab es in seinem Leben irgend einen Zeitraum / Ereignis wo sich sein Verhalten krass geändert hat (dy/dt) und könnte es sein, dass ihm damals etwas wie ein prä-2020-Neuralink verpasst worden ist? https://t.co/704ESfHre3 dann frage zur Pflegerin in der Ausbildung, ich hab sie glaub \"ein bisschen\" überfordert?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1291000903017930758",
        "createdAt" : "2020-08-05T13:19:35.648Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kann ich zuvor eine liste der themen haben damit ich auch quellen bereitlegen kann?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1278491830108258310",
        "createdAt" : "2020-07-02T00:53:00.217Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja kann ich...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1278414117003550726",
        "createdAt" : "2020-07-01T19:44:11.980Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Marc, würdest Du Dich im Rahmen einer psychiatrie-pflegerischen Ausbildung meiner Freundin für ein anonymes Interview zur Verfügung stellen? Danke für Antwort!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1278388782048428037",
        "createdAt" : "2020-07-01T18:03:31.644Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "System-Fehler abgefedert werden.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1275923556254265349",
        "createdAt" : "2020-06-24T22:47:36.017Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, es gibt nur den Rechts-Staat, der durch alle Instanzen jemanden schuldig oder unschuldig sprechen kann. Durch den Instanzenlauf sollten",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1275923496862994436",
        "createdAt" : "2020-06-24T22:47:21.872Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo der Inder, kleine sachlich nüchterne Frage ohne viel Emotionen: Falls der Art 59 StGB (Kleine Verwahrung) dazu verwendet würde um Zeugen, Täter, Opfer an der Justiz vorbei zu schmuggeln oder vor der Gerichtsverhandlung handlungsunfähig, unglaubwürdig zu machen und zu diskreditieren, gibt es da irgend ein Gesetz um dieses Sicherheitsloch im Rechtssystem zu stopfen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1275839266174746633",
        "createdAt" : "2020-06-24T17:12:39.711Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, Du hast mir ja mal erklärt was Haschaschinen sind, woher das Wort kommt, meiner Meinung nach ist das Wort suboptimal und suggestiv (Stichwort Neusprech) weisst Du grad noch woher Du das Wort hattest, von wem oder von welcher Quelle. Das war ja glaub damals die Zeit als Assessins Creed so super in war...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1265380182715703312",
        "createdAt" : "2020-05-26T20:31:59.894Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok, schon gedacht du meinst schloss und rigel :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1245373674892472325",
        "createdAt" : "2020-04-01T15:33:16.717Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das können nur meine Follower, das sind 228 Menschen lesen. Das heisst, meine Nachrichten können auch nicht retweetet werden.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1245369466977910788",
        "createdAt" : "2020-04-01T15:16:33.469Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hinter dem schhloss?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1245368410411864068",
        "createdAt" : "2020-04-01T15:12:21.564Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, keine Sorge. Ich schreibe einfach nur allen möglichen Müll, nur um ihn dann wieder zu löschen. Zudem: Ich bin hinter dem Schloss.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1245283144103837700",
        "createdAt" : "2020-04-01T09:33:32.488Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, du twitterst ja mehr als ich, muss man sich sorgen machen? oder fällt dir nur die decke auf den kopf wegen corona?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1245201099533045770",
        "createdAt" : "2020-04-01T04:07:31.546Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "vermutlich nie mehr... ich bin ja ein böser böser wahrheits-terrorist, bzw. ein whistle-blow-er... :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1228224496886153225",
        "createdAt" : "2020-02-14T07:48:34.062Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ok, wann entlassen sie Dich?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1228183016318492676",
        "createdAt" : "2020-02-14T05:03:44.331Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "H7.2, eigentlich, hab aber verpennt weil ich rasch 2h hinliegen wollte als ich die Post in der Telli holen wollte und bin dann erst um 21:00 aufgewacht, jetzt muss ich 09:00 erst wieder dort sein...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1228182609429057541",
        "createdAt" : "2020-02-14T05:02:07.331Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mein Bruder ist momentan nicht mehr dort. In welcher bist Du gerade?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1228179880702955525",
        "createdAt" : "2020-02-14T04:51:16.773Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, auf welcher abteilung ist dein bruder? wenn ihc schon mal in königsfelden bin könnte ich mal veruschen ihn zu besuchen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1228065871270154245",
        "createdAt" : "2020-02-13T21:18:14.771Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, ist gut. Hatte Stress mit der Freundin.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1227857228008427525",
        "createdAt" : "2020-02-13T07:29:10.333Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und immer noch beschäftigt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1227707764392255492",
        "createdAt" : "2020-02-12T21:35:15.445Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1217747137691312139",
        "createdAt" : "2020-01-16T09:55:16.964Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Marc, ich bin im Moment mit mir selber sehr stark beschäftigt. Deshalb kann Dir im Moment nur wünschen, dass sich alles zum Guten wendet für Dich.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1217747008070569989",
        "createdAt" : "2020-01-16T09:54:46.058Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/mTZtW5F98x",
          "expanded" : "https://twitter.com/messages/media/1213507159172026373",
          "display" : "pic.twitter.com/mTZtW5F98x"
        } ],
        "text" : "und ja, vielleicht ist es auch ein bisschen meine eigene Schuld, ist aber nur eine Frage und fragen wird man wohl noch dürfen https://t.co/mTZtW5F98x",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1213507159172026373/1213507151802650624/Kxxs_QPE.jpg" ],
        "senderId" : "75128838",
        "id" : "1213507159172026373",
        "createdAt" : "2020-01-04T17:07:07.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/RwfQ3vPBHf",
          "expanded" : "https://twitter.com/messages/media/1213506960823336964",
          "display" : "pic.twitter.com/RwfQ3vPBHf"
        } ],
        "text" : "Also ich vermute die Frau Dr. Hanno will mich gar nicht in die Psychiatrie tun, sie muss aber weil Hansjürg Pfisterer und Urs Blum gegen Patienten Droht, so etwas wie ganz viele STGB185 https://t.co/RwfQ3vPBHf",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1213506960823336964/1213506954120826885/B2PfqEEu.jpg" ],
        "senderId" : "75128838",
        "id" : "1213506960823336964",
        "createdAt" : "2020-01-04T17:06:20.161Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/gqPeo6kPT3",
          "expanded" : "https://twitter.com/messages/media/1213506573336825860",
          "display" : "pic.twitter.com/gqPeo6kPT3"
        } ],
        "text" : "allenfalls sachdienlich https://t.co/gqPeo6kPT3",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1213506573336825860/1213506566550441987/grLBtsf2.png" ],
        "senderId" : "75128838",
        "id" : "1213506573336825860",
        "createdAt" : "2020-01-04T17:04:47.995Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was sind SoMe aktivitäten? Muss ich jetzt wie der Snowden oder der Jacob Applebaum das Land verlassen wenn ich keine Intelligenzmindernde Medikamente verpasst bekommen will?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1213500688631771140",
        "createdAt" : "2020-01-04T16:41:24.622Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hoi Marc! Ich verstehe nur die Hälfte. Weswegen wird Dir ein FU angedroht? Geht es um die Verweigerung von Medikamenten oder um Deine Inaussichtstellung eines Leaks? Was ich auf jeden Fall herauslesen kann, ist, dass Du und Deine SoMe-Aktivitäten auf dem Radar der Behörden sind. Ferner kenne ich den Fall Desirée Lötscher nicht und weiss nicht einmal, ob ich überhaupt etwas davon wissen darf. Wie kann ich Dich also konkret unterstützen?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1213361806162432004",
        "createdAt" : "2020-01-04T07:29:32.519Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/tdlEr7MkuQ",
          "expanded" : "https://twitter.com/messages/media/1213079740686491653",
          "display" : "pic.twitter.com/tdlEr7MkuQ"
        } ],
        "text" : " https://t.co/tdlEr7MkuQ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1213079740686491653/1213079733845581824/quEje0zp.jpg" ],
        "senderId" : "75128838",
        "id" : "1213079740686491653",
        "createdAt" : "2020-01-03T12:48:42.937Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Ek3BGcMvs9",
          "expanded" : "https://twitter.com/messages/media/1213079686273740805",
          "display" : "pic.twitter.com/Ek3BGcMvs9"
        } ],
        "text" : "Halli Hallo, kannst Du mir allenfalls bei einem Rechtsfall helfen, ich kann Dir allenfalls auch etwas bezahlen für Deine Aufwände aber viel Geld habe ich nicht? Was soeben passiert ist ( Screenshot 1 -&gt; Screenshot 2) https://t.co/Ek3BGcMvs9",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1213079686273740805/1213079679546077186/pwJz9uZY.jpg" ],
        "senderId" : "75128838",
        "id" : "1213079686273740805",
        "createdAt" : "2020-01-03T12:48:30.014Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/zVMFSSiXGt",
          "expanded" : "https://twitter.com/messages/media/1193345581474824196",
          "display" : "pic.twitter.com/zVMFSSiXGt"
        } ],
        "text" : "du hast ja kein fb mehr... https://t.co/zVMFSSiXGt",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1193345581474824196/1193345569479102464/lz4IZBpF.jpg" ],
        "senderId" : "75128838",
        "id" : "1193345581474824196",
        "createdAt" : "2019-11-10T01:52:12.842Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/R4Kyl7YdKP",
          "expanded" : "https://twitter.com/FailDef/status/1184782695634735104",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "https://t.co/R4Kyl7YdKP",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184783355277119496",
        "createdAt" : "2019-10-17T10:48:58.805Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/R4Kyl7YdKP",
          "expanded" : "https://twitter.com/FailDef/status/1184782695634735104",
          "display" : "twitter.com/FailDef/status…"
        }, {
          "url" : "https://t.co/NOflRT5PHo",
          "expanded" : "https://twitter.com/FailDef/status/1184565804437790721",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "nbö, mir gehts so weit gut. siehe meinen letzten tweet, ich kann schon selber stehen, aber die jungen wie lukas weber brauchen auch hilfe... https://t.co/R4Kyl7YdKP und diesen Tweet hier https://t.co/NOflRT5PHo",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184783217246822407",
        "createdAt" : "2019-10-17T10:48:25.911Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Dann geht es um diesen Lukas Weber, dem Koks angeboten wurde.... Er hat dankend abgelehnt. Eine verbale Eskalation scheint stattgefunden zu haben. Lukas scheint glimpflich davon gekommen zu sein.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1184760592181202948",
        "createdAt" : "2019-10-17T09:18:31.657Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also, es geht da um zwei Dinge... Das erste ist Deine Geschichte. Da scheint es um eine Medikation zu gehen, die Du nicht wünschst. Du hast Deinen Willen dahingehend ausgedrückt. So stellt sich also die Frage, ob sie irgend eine Handhabe gegen Dich haben, um bei Dir eine Zwangsmedikation vorzunehmen (z.B Selbst- od. Fremdgefährdung).",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1184759912850739204",
        "createdAt" : "2019-10-17T09:15:49.692Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich habe das unaufgefordert zugeschickt bekommen, und ich denke so etwas nicht zu melden wäre dann wohl ein straftatbestand von mir und da ich vertrauen habe zu dir chrisitan und vermutlich in solothurn ist hoffe ich du bist nicht böse auf mich dass ich das Dir weiterleite",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184556134339928070",
        "createdAt" : "2019-10-16T19:46:05.111Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Name dejenigen der mir das geschickt hat ist Lukas Weber, war gar in Bern in der Psychiatrie",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184554223683407876",
        "createdAt" : "2019-10-16T19:38:29.582Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YsgnQvYLhD",
          "expanded" : "https://twitter.com/messages/media/1184554024214958084",
          "display" : "pic.twitter.com/YsgnQvYLhD"
        } ],
        "text" : "an christian Tanner, cc Dominik Zschokke sorry dass ich Dir das so weiterleite, aber da das meiner Meinung nach im Kanton Solothurn ist wärst Du oder Dein Chef wohl doch die richtige Anlaufstelle, ich werde es auch dominik zschokke senden https://t.co/YsgnQvYLhD",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1184554024214958084/1184554018758189058/tDaEr317.png" ],
        "senderId" : "75128838",
        "id" : "1184554024214958084",
        "createdAt" : "2019-10-16T19:37:42.131Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/XrRUcDjkp6",
          "expanded" : "https://twitter.com/messages/media/1184007943395708932",
          "display" : "pic.twitter.com/XrRUcDjkp6"
        } ],
        "text" : " https://t.co/XrRUcDjkp6",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1184007943395708932/1184007933782364161/3j_hetTk.png" ],
        "senderId" : "75128838",
        "id" : "1184007943395708932",
        "createdAt" : "2019-10-15T07:27:46.421Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "any idea was man bei sowas macht?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184007926345846788",
        "createdAt" : "2019-10-15T07:27:42.135Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, dann ruf ich a mal an",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1183027732747296772",
        "createdAt" : "2019-10-12T14:32:45.776Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Habe von Rafi überhaupt nichts mehr gehört.... Ich habe nur den Vermittler gespielt.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1183027669610442757",
        "createdAt" : "2019-10-12T14:32:30.728Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und rafi?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1183027529818529796",
        "createdAt" : "2019-10-12T14:31:57.396Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Eines der entscheidenden Punkte scheint beim Korsakov die Mangelernährung zu sein, da echt krasse Alkis sich sozusagen nur noch flüssig ernähren. Der Kaloriengehalt von Alk scheint für eine \"Aufrechterhaltung des Betriebs\" anscheinend ausreichend. Dein Teddy sagt richtig, dass wir am Stamm jeweils zu viel Alk konsumieren. Wenn das sonst aber nicht zur Regel wird und die Konsumenten ein sonst gewöhnliches Essverhalten an den Tag legen, wird sich kein Korsakov ergeben. Zudem steht bei den krassen Alkoholikern nicht Bier im Vordergrund. Die wollen nicht so viel Flüssigkeit aufnehmen, um auf ihren Pegel zu kommen. So greifen die meisten dann zu Wodka. Der ist eben auch geruchsneutral, was z.B. auf der Arbeit noch entscheidend sein könnte.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1183027273454309386",
        "createdAt" : "2019-10-12T14:30:56.275Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CDcRVDaCgX",
          "expanded" : "https://www.youtube.com/watch?v=JsIl7Tp2RV4&list=PLWsX0-AjlNWx0OSnVHjA61sKGRcaPpSIa&index=17&t=1m",
          "display" : "youtube.com/watch?v=JsIl7T…"
        } ],
        "text" : "Ich find den Wikipedia Artikel recht untoll, allenfalls weil das afaik eher selten ist. Wäre er an irgendwas (z.B. an der beschissenen Welt die wir haben) zerbrochen oder war er irgendwie zeuge von irgendwas dass ihn so aus der bahn geworfen hat, dass er zuflucht im alkohol gesucht hat? dann findet man auch nichts zum Thema früherkennung und vermeidung von korsakow im Artikel (mein teddy meint: apropos die meisten am piraten stammtisch &gt;6 Bier) und die wichtigste frage wäre, gibt es eine möglichkeit den schaden wieder zu beheben, da steht auch ncihts im artikel? um in bildern zu sprechen: https://t.co/CDcRVDaCgX (sprungmarke bei 1:00)   [dann noch rasch anderes thema, ich hab von rafi nichts gehört, du?]",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1183025201631629317",
        "createdAt" : "2019-10-12T14:22:43.344Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Korsakov ist eine Hirnschädigung infolge lang andauerndem Vitamin C(Irgendwas)-Mangel, welcher wiederum durch lang anhaltenden übermässigen Alk-Konsum entsteht.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1183019830858924038",
        "createdAt" : "2019-10-12T14:01:21.817Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "tönt aber iregendwie auch nach autismus, ist krosakov eine differenzialdiagnose von autismus?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1183018693565079557",
        "createdAt" : "2019-10-12T13:56:50.662Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "😅 Sehr schön, danke. Übrigens noch zu meinem Bruder, den ich gerade besucht habe, kann ich sagen, dass er unter eine Dissozialen Persönlichkeitsstörung in Verbindung mit Korsakov leidet. Das heisst er reagiert nicht auf irgend welche Belohnungssysteme oder Strafen. Er zieht sein Ding einfach durch. Das ist bei ihm angeboren. Nicht angeboren sind seine mentalen Defizite, die er sich als Folge von massivem Alkoholmissbrauchs zugezogen hat. Sein Zustand ist dementsprechend ziemlich reduziert. Die Möglichkeit, ihn zurückzuholen, schätzen nicht nur die Ärzte, sondern auch ich als sehr gering ein.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1183018431160995845",
        "createdAt" : "2019-10-12T13:55:48.127Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/XNRQMd5c98",
          "expanded" : "https://marclandolt.ch/Hackers-Cardgame/jpg/EN/",
          "display" : "marclandolt.ch/Hackers-Cardga…"
        }, {
          "url" : "https://t.co/cDWHRiJghK",
          "expanded" : "https://twitter.com/messages/media/1183010111796711429",
          "display" : "pic.twitter.com/cDWHRiJghK"
        } ],
        "text" : "hey, falls du nicht gerad etwas zu mir gesagt hast, dann gibt es von Dir einen Touring-Vollständigen Avatar (Akkustisch/Sprachsynthese) https://t.co/XNRQMd5c98 https://t.co/cDWHRiJghK",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1183010111796711429/1183010100841197568/P4-ay14g.jpg" ],
        "senderId" : "75128838",
        "id" : "1183010111796711429",
        "createdAt" : "2019-10-12T13:22:44.979Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "2:55",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1179807956747071493",
        "createdAt" : "2019-10-03T17:18:31.356Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/adMUqXLUKA",
          "expanded" : "https://www.youtube.com/watch?v=xpWo1_-QI6Y#t=2m55s",
          "display" : "youtube.com/watch?v=xpWo1_…"
        } ],
        "text" : "https://t.co/adMUqXLUKA \"To /burry/ people alive\" also Microsoft wäre der Pflug...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1179807937239375877",
        "createdAt" : "2019-10-03T17:18:26.774Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gut die v/o Antenne ist immer noch am Leben, ich vermute ich habe\n\na) ihn erschreckt\nb) seine Mêre erschreckt\nc) sowas wie eine disassembly protection, die dann einfach ganz viel Aufträge und Geld reinspuhlt\n\n\"scheinbar\" geht es ihm gut, ist souverän, es erinnert mich aber an meine Zeit bei ABB wo ich Tagelang irgendwelche Windows Probleme gewälzt habe und mir nebenbei Malware auf verschiedenen ebene aufgespielt wurden, und das braucht kein 20 Jähriger, und das braucht auch kein 40 Jähriger...\n\nviel sachebne, kein leben / sinn + blöden chef-lobster",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1179804926190456836",
        "createdAt" : "2019-10-03T17:06:28.817Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, Du hast mir mal im persönlichen gesagt, dass dein Bruder auch in der Psychiatrie ist und -- ich weiss den namen nicht mehr der krankheit -- er so irgendwie soziophatisch geworden ist. könnte es sein dass du mal ganz viel vertrauen in ihn gehabt hast und dass er das gar nicht vom alkohl hätte sondern von medikamenten tests oder gar nur eine induzierte Dissoziative Identitätsstörung wäre? und er einfach so eine art 2 betriebsmodi hätte und du ihn erst \"switchen\" müsstest um deinen original bruder wieder zurück zu haben?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178875351063244805",
        "createdAt" : "2019-10-01T03:32:40.839Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey Du hast mir ja gesagt ich solle mit Raphi Hegglin reden, der meldet sich schon eine ganze weile nicht mehr... hast Du was gehört?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178699913204568069",
        "createdAt" : "2019-09-30T15:55:33.187Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, so weit schon, das leben ist leider nur ein bisschen komplizierter als ich gedacht habe. allenfalls habe ich vielleicht noch eine dissoziative identitäts störung. das macht dann nochmals alles komplizierter...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178256501833687045",
        "createdAt" : "2019-09-29T10:33:35.678Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, ich habe mir ein wenig Sorgen um Dich gemacht. Habe gedacht, dass ich irgendwie so ein paar bekannte Tendenzen ausgemacht hätte. Deshalb wollte ich mit Dir auch einen Kaffee trinken gehen. Das war kein Komplott oder so. Einfach nur die Frage, ob alles ok bei Dir.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1178234788068708356",
        "createdAt" : "2019-09-29T09:07:18.712Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke fürs Vorbeischicken von Markus, scheinbar hat er den Tweet mit diesem Video &amp;fbclid=IwAR3rvHiwyn3qdfd-ggP_c2mKXZYTE4qw-6GuLknx6xSRtjd5OQlIBlRFdXs#t=29m20s (sprungmarke bei 29:20) nicht bekommen, den ich euch beiden geschickt habe, war das bei dir auch so?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1178066516333469700",
        "createdAt" : "2019-09-28T21:58:39.609Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Scheinbar hat die doofe Antenne ferien gemäss seinen 2 +/- gleichaltrigen / jüngeren kollegen. Ich hoff \"ich\" hab ihn nicht psychotisch gemacht...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1177234270848524293",
        "createdAt" : "2019-09-26T14:51:36.829Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bin erst grad aufgestanden, werde also sicher bis irgend 06:00 oder 10:00 wach sein und dann pennen, dann am Abend wollte ich mal schauen wie es Philipe Kurz v/o Antenne geht, denn von dem hab ich zu lange nichts mehr gehört...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1176920702823403524",
        "createdAt" : "2019-09-25T18:05:36.397Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Morgen Nachmittag auf einen Kaffee oder was auch immer?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1176916063742844939",
        "createdAt" : "2019-09-25T17:47:10.354Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "huch, dann bin ich ja beruhigt, hab schon Angst gehabt, dass Zynismus bei der UN-Holy-Morning unagebracht wäre... ich dachte nur ich erwähne das \"rechtzeitig\" dass ich darauf hingewisen wurde, dass mir das hätte früher auffallen sollen, aber allenfalls ist man da als weisser man wie ich der als kind \"fast\" nur weisse kinder gekannt hat allenfalls einfach ein häuchler...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1176908103809417221",
        "createdAt" : "2019-09-25T17:15:32.557Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich denke, wir haben eine enorme «Biodiversität» in der United Nations. Da sind fast alle Arten vertreten, wobei mittlerweile ja einige seltenen Arten das warme Klima des trauten Heimes bevorzugen.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1176787081739362308",
        "createdAt" : "2019-09-25T09:14:38.645Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo Inder, wie sieht es eigentlich mit der \"Biodiversität\" in den UN Holy Morning Vollversammlung aus?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1176663256649388038",
        "createdAt" : "2019-09-25T01:02:36.423Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hab ne sms und in twitter geschreiebne",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175478411055288326",
        "createdAt" : "2019-09-21T18:34:27.234Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kannst Du mal Raphael Hegglin kontaktieren. Er hat irgendwie Probleme mit seinem Raspberry Pie...\n+41791265025",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1175423311309942788",
        "createdAt" : "2019-09-21T14:55:30.418Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/aHbQLaRUh7",
          "expanded" : "https://github.com/braindef/jamulus",
          "display" : "github.com/braindef/jamul…"
        } ],
        "text" : "https://t.co/aHbQLaRUh7",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1148655145590427652",
        "createdAt" : "2019-07-09T18:08:22.401Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/vUgN7VG0n8",
          "expanded" : "https://www.crowdify.net/de/projekt/prozessor",
          "display" : "crowdify.net/de/projekt/pro…"
        } ],
        "text" : "https://t.co/vUgN7VG0n8",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1146152143159779332",
        "createdAt" : "2019-07-02T20:22:20.142Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok, nur gedacht, ist irgendwie sehr untypisch dass du nicht twitterst...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1145285626847211524",
        "createdAt" : "2019-06-30T10:59:06.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein, nein, alles gut. Ich unterrichte gerade wieder und das Semester nähert sich dem Ende. Deshalb gibt es noch viel zu tun. Ich komme am 2. Juli an die PV und werde gewiss wieder aktiver twittern. 😉",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1145233985704009733",
        "createdAt" : "2019-06-30T07:33:54.322Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, du hast seit ein paar tagen nichts mehr getweeted... nicht mehr auf twitter unterwegs?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1145052417135915013",
        "createdAt" : "2019-06-29T19:32:25.005Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/tcqagRneVL",
          "expanded" : "https://www.youtube.com/watch?v=LUK8oxKwh9M",
          "display" : "youtube.com/watch?v=LUK8ox…"
        } ],
        "text" : "hey, was hältst du davon: https://t.co/tcqagRneVL im normalfall sind ja politisch Rechte eher gegen den Klimaschutz. Will der Sellner nur mehr Follower mit diesem Move oder kommen die Rechten (zumindest diesbezüglich) zur vernunft. bzw. deucht mich er versucht jetzt irgnedwie durch die Blume zu sagen: \"Wählt politisch rechts wenn ihr den Planeten schützen wollt\"? Denn noch vor einem Monat hat er komplett gegen Fridays for the Future etc gemötzelt.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1135830932647219204",
        "createdAt" : "2019-06-04T08:49:31.789Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128688243212419076",
        "createdAt" : "2019-05-15T15:47:01.914Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sie könnten Dich natürlich mit einer einstweiligen  Verfügung stoppen, aber ich denke, Du wirst leider nicht die empfindliche Reichweite erreichen. Ich sage mal, da geschieht nix, wenn Du die Fakten aus Deiner Sicht darlegst.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1128687909513703428",
        "createdAt" : "2019-05-15T15:45:42.332Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also umformuliert dürfen die mir nicht verbieten die information zu veröffentlichen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128687233031180297",
        "createdAt" : "2019-05-15T15:43:01.043Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok oder @ salt :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128687127263436805",
        "createdAt" : "2019-05-15T15:42:35.882Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und ihc hab recherchieert, es gibt sehr viele kunden die genau deswegen gekündigt haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128687072078901253",
        "createdAt" : "2019-05-15T15:42:22.672Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, wenn Du das Problem zusammenfasst und in eigene Worte fasst, sehe kein Problem, diese Informationen zum Beispiel in einem Blog zu veröffentlichen.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1128686977832955909",
        "createdAt" : "2019-05-15T15:42:00.205Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und ipv6 sollten ja die dann schon irgendwann haben, die deppen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686508926558214",
        "createdAt" : "2019-05-15T15:40:08.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich miene ich hab denen 2 möglcihe lösungansätze aufgezählt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686453796610053",
        "createdAt" : "2019-05-15T15:39:55.258Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja ich kann auch einfach alles sleber formulieren und noch so draufschreiben, dass salt mir verboten hat das mail zu veröffentlichen also fasse ich es technisch zusammen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686399698493446",
        "createdAt" : "2019-05-15T15:39:42.361Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und somit kannst du diverse dinge nicht machen die z.B. UPNP brauchen, z.B. games oder auch nitendo switch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686215803363333",
        "createdAt" : "2019-05-15T15:38:58.519Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also du hast auf deinem handy und wlan 192.x.x.x und dann gibt es noch ein privates dazwischen 10.x.x.x und erst dann kommt eine öffentliche ip",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686134966484996",
        "createdAt" : "2019-05-15T15:38:39.255Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "geht dadrum dass ja die meisten natel-anbieter ein double-NAT haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128686026631929861",
        "createdAt" : "2019-05-15T15:38:13.424Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie willst Du das veröffentlichen?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1128685996634181637",
        "createdAt" : "2019-05-15T15:38:06.258Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du bist ja nicht öffentlcih :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128685963805384708",
        "createdAt" : "2019-05-15T15:37:58.460Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/s74EcGQI3k",
          "expanded" : "https://twitter.com/messages/media/1128685942280208388",
          "display" : "pic.twitter.com/s74EcGQI3k"
        } ],
        "text" : " https://t.co/s74EcGQI3k",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1128685942280208388/1128685936974422016/xRAhUgBT.png" ],
        "senderId" : "75128838",
        "id" : "1128685942280208388",
        "createdAt" : "2019-05-15T15:37:53.475Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö um unfähigkeit des netzwerkteams bei denen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128685815922606084",
        "createdAt" : "2019-05-15T15:37:23.182Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Geht es um eine kritische Lücke?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1128685760792670213",
        "createdAt" : "2019-05-15T15:37:10.035Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wenn Du Ihnen mit Falschinformationen keinen Schaden zufügst, wovon ich ausgehe, und Du keine Personen erkennbar bezeichnet, brauchst Du kein Einverständnis von Salt. Sie tun natürlich so, als ob Du auf ihre Zustimmung angewiesen bist. Nun, bedenke einfach, dass Du Kunde dieser Firma bist.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1128685574397755396",
        "createdAt" : "2019-05-15T15:36:25.636Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und da sie einsehen dass das ein gröberes problem ist haben dich auch einen ganze fucking monat gebraucht bis sie die antwort zusammengebrösmelet haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128684536844423172",
        "createdAt" : "2019-05-15T15:32:18.223Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Bc3qnwSHnf",
          "expanded" : "https://twitter.com/messages/media/1128684245461999620",
          "display" : "pic.twitter.com/Bc3qnwSHnf"
        } ],
        "text" : " https://t.co/Bc3qnwSHnf",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1128684245461999620/1128684241989132293/9g5C2H1B.png" ],
        "senderId" : "75128838",
        "id" : "1128684245461999620",
        "createdAt" : "2019-05-15T15:31:08.953Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, du kennst dich ja mit Recht recht gut aus. Was kann passieren wenn ich etwas bei Salt beanstande, die den Fehler auch zugeben und dann das trotzdem veröffentliche. Aber folgendes steht noch im Antowort-Mail:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1128684239388651527",
        "createdAt" : "2019-05-15T15:31:07.383Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "😉 no problem.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1116085594835308550",
        "createdAt" : "2019-04-10T21:08:36.381Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "uups falscher empfänger....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116085479227777028",
        "createdAt" : "2019-04-10T21:08:08.817Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ArK7qygJlS",
          "expanded" : "https://www.facebook.com/marc.landolt.9/videos/10219207927535956/",
          "display" : "facebook.com/marc.landolt.9…"
        } ],
        "text" : "today i found out how to livestream to facebook or youtube: https://t.co/ArK7qygJlS (you can do that with OBS-Studio, a Streaming Sofzware that is free...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116085418590703626",
        "createdAt" : "2019-04-10T21:07:54.426Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no problemo",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1113124910975647749",
        "createdAt" : "2019-04-02T17:03:54.332Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sorry, dass ich so  kurzangebunden war. Unterzuckerung ist unschön... Ich behandle mich jetzt mit Schokolade. Cu soon.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1113069341157752837",
        "createdAt" : "2019-04-02T13:23:05.469Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hmmm... das passt schon, aber diese Wörter müsste man in einem zusammenhang erklären um den Sachverhalt zu erklären, aber würde eigentlich gehen. \"Axe wird als Amulett angepriesen dass einem Frauen höhrig macht\"... ja das könnte gehen :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1112115838562324492",
        "createdAt" : "2019-03-30T22:14:12.735Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Fetisch, Amulett, Talisman?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1112115243033997316",
        "createdAt" : "2019-03-30T22:11:50.735Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey Du bist ja einer der gebildetsten Menschen die ich kenne, ich wollte rasch fragen ob Du weisst ob es da ein Wort dafür gibt: wenn man z.B. einen iPod gekauft hat und dann das gefühl hat er verleihe einem einen besonderen Status oder Magie, bzw. generell ein Wort für Technik, dass dem Wort Blutmagie gleicht. Also bei Blutmagie ist ja der Aberglaube, dass z.B. das Trinken von Blut einem Kraft, Energie, Jugend etc. gibt, und nun suche ich das Wort für Technik oder Konsumgüter die einem Macht Verleihen. Auch z.B. Gilett oder Axe verwenden ja in ihrer Werbung auch so suggestionen, dass man dann \"per Zauber\" (also wenn man das Produkt kauft) irgend ganz oben stünde bei Frauen....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1112114839097364487",
        "createdAt" : "2019-03-30T22:10:14.433Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "der text macht noch irgendwie sinn, dass gamer leicht beeinflussbar sind daran habe ich nicht gedacht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107745578258235396",
        "createdAt" : "2019-03-18T20:48:21.485Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/HFEqBvNges",
          "expanded" : "https://twitter.com/saschalobo/status/1107329997881200641",
          "display" : "twitter.com/saschalobo/sta…"
        } ],
        "text" : "https://t.co/HFEqBvNges",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107745413032042502",
        "createdAt" : "2019-03-18T20:47:42.098Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Dir auch!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107228030496374795",
        "createdAt" : "2019-03-17T10:31:48.474Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schönen sonntag ncoh :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107227845712060421",
        "createdAt" : "2019-03-17T10:31:04.422Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hmmm, ok. Selten genug. Hey, Marc, wir sehen uns. Danke für den Gedankenaustausch!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107227586873171972",
        "createdAt" : "2019-03-17T10:30:02.751Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, gute eigentlich nur echt selten...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107227336217378820",
        "createdAt" : "2019-03-17T10:29:02.948Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Manchmal auch gute! Aber Du hast schon recht.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107227240348168196",
        "createdAt" : "2019-03-17T10:28:40.075Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, das leben hat immer wieder böse überraschungen bereit... da bin ich mir sicher...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107227134970523653",
        "createdAt" : "2019-03-17T10:28:14.960Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das passiert ja jetzt glücklicherweise nicht mehr, hoffe ich.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107226990321561604",
        "createdAt" : "2019-03-17T10:27:40.465Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich glaub ich mach das immer wenn ich gezwungen bin mein leben zu überdenken und über das bisherige reflexiere... also wenn ich dazu gezwungen werde und wieder mal mit hanschellen ind die psychiatrie verschleppt wurde",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107226827314155525",
        "createdAt" : "2019-03-17T10:27:01.624Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und das mit dem windows logo das soll der serverraum darstelle wo ich mit 20 in zürich gearbeitet habe bei abb flexibler automation die s4c roboter steuerungen hatten...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107226616013438980",
        "createdAt" : "2019-03-17T10:26:11.232Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es sieht schon nach viel aus, ist aber noch viel mehr, weil komprimiert.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107226350446936070",
        "createdAt" : "2019-03-17T10:25:07.925Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "z.B. das unten rechts ist eine maslow-pyramite",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107226345979944965",
        "createdAt" : "2019-03-17T10:25:06.865Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wobei die symbole nicht für ein einzelnes wort steht sondern für z.B. ein Lebenseregnis oder eine ganze technishe abhandlung",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107226180137164804",
        "createdAt" : "2019-03-17T10:24:27.384Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was war die Begründung dafür?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107226096007823364",
        "createdAt" : "2019-03-17T10:24:07.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oder ne steno",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107226057948712964",
        "createdAt" : "2019-03-17T10:23:58.188Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, hab ich in der psychiatrie gekritzelt weil ich keine computer haben drufte... das ist komplett voll fies... ich glaub die gienissen das ncoh dort den patienten dinge zu verbieten wie einem kleinen kind \"Nein du darfst das nciht\" oder \"mach erst den Bitti-Bätti Tanz\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225970967236613",
        "createdAt" : "2019-03-17T10:23:37.484Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, sieht nicht schlecht aus.. so etwas wie eine Soulmap, kein Mindmap...",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107225682499780612",
        "createdAt" : "2019-03-17T10:22:28.658Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225586886496260",
        "createdAt" : "2019-03-17T10:22:05.861Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mein \"kunstwerk\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225582817996804",
        "createdAt" : "2019-03-17T10:22:04.916Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ist aber schon 20 jahre alt...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225569538818053",
        "createdAt" : "2019-03-17T10:22:01.738Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und beim christchurch typ ist ja auch klar dass in die Vorstellung von \"Umvolkung\" wie es gewisse menschen in Deutschland nennen verfolgen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225472440721412",
        "createdAt" : "2019-03-17T10:21:38.576Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225370510721028",
        "createdAt" : "2019-03-17T10:21:14.276Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ist das von Dir?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107225350617157636",
        "createdAt" : "2019-03-17T10:21:09.532Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dinge die mich grad beschäftigt oder verfolgt haben",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225307378012166",
        "createdAt" : "2019-03-17T10:20:59.242Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MawLx7uZC3",
          "expanded" : "https://twitter.com/messages/media/1107225235403800580",
          "display" : "pic.twitter.com/MawLx7uZC3"
        } ],
        "text" : " https://t.co/MawLx7uZC3",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1107225235403800580/1107225228063764481/7ajX7SSe.png" ],
        "senderId" : "75128838",
        "id" : "1107225235403800580",
        "createdAt" : "2019-03-17T10:20:42.450Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "irgend so eine merkwürdige art von gedanken protokoll, so wie wenn man mit seiner seele direkt redet",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107225191246192645",
        "createdAt" : "2019-03-17T10:20:31.539Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sinn und Zweck?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107224684343504901",
        "createdAt" : "2019-03-17T10:18:30.688Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja aber heute machen das glaub meist nur noch schizos",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107224637212119044",
        "createdAt" : "2019-03-17T10:18:19.446Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das haben schon die alten Wikinger gemacht. ;)",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107223746979512325",
        "createdAt" : "2019-03-17T10:14:47.190Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber das beschriften der waffen (oder generell irgendwelcher dinge) das ist glaub ganz typisch schizophrener, das hab ich auch immer gemacht, wenn gleich auch nicht mit waffen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107223596575932420",
        "createdAt" : "2019-03-17T10:14:11.334Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "beim breivik war ja erst schizophrenie diagnostiziert worden, aber dann hätte man ihm irgendwie die schuld absprechen müssen und das wollte niemand dann hat glaub das 2. gutachten gesagt \"narzistische persönlichkeitsstörung\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107223454166728708",
        "createdAt" : "2019-03-17T10:13:37.385Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja das vom christchurch geisteskranken hab ich ein bisschen gelesen, weil mich würde shcon wunder nehmen, ob er schizo ist",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107223261551702021",
        "createdAt" : "2019-03-17T10:12:51.509Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, sagt schon einiges aus... Ja, lesen muss man das Ding wirklich nicht.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107223103468457988",
        "createdAt" : "2019-03-17T10:12:13.781Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Starting training using file breivik.lower.txt\nVocab size: 29931\nWords in train file: 787393\nAlpha: 0.000304  Progress: 99.88%  Words/thread/sec: 111.36k  \nreal    0m3.884s\nuser    0m35.607s\nsys    0m0.098s",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107223092257017860",
        "createdAt" : "2019-03-17T10:12:11.154Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "marc@debian:~/Daten.2019/code/deepLearning$ ls breivik.lower.txt.bin -lah\n-rw-r--r-- 1 marc marc 24M Mär 16 21:00 breivik.lower.txt.bin",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107222950497918980",
        "createdAt" : "2019-03-17T10:11:37.315Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "197 laws\n    197 nonmuslims\n    197 norway\n    197 russia\n    198 c\n    198 hours\n    198 traditional\n    198 turkey\n    199 act\n    199 black\n    199 conservatives\n    199 foreign\n    199 old\n    199 peoples\n    200 influence\n    200 past\n    200 problem\n    200 single\n    201 avoid\n    202 found\n    202 nothing\n    203 norwegian\n    203 person\n    203 put\n    205 parties\n    206 find\n    206 movements\n    206 perhaps\n    207 ethnic\n    207 here\n    207 times\n    207 traitors\n    208 violence\n    209 sweden\n    209 view\n    210 allowed\n    210 economic\n    210 warfare\n    211 although\n    211 israel\n    212 armed\n    212 union\n    213 city\n    213 g\n    213 little\n    214 former\n    214 live\n    214 multiculturalist\n    215 above\n    215 full\n    215 going\n    216 ensure\n    216 really\n    217 am\n    217 christianity\n    217 community\n    217 completely\n    217 major\n    218 british\n    218 think\n    219 required\n    219 research\n    219 templar\n    219 total\n    220 better\n    221 done\n    221 weapons\n    223 everything\n    223 organisations\n    224 best\n    224 far\n    225 muhammad\n    226 category\n    226 decades\n    226 her\n    226 level\n    226 policy\n    226 second\n    227 material\n    228 able\n    228 organisation\n    229 though\n    230 believe\n    230 common\n    231 continue\n    231 french\n    231 sharia\n    232 don’t\n    232 rule\n    232 she\n    233 american\n    233 “the\n    234 security\n    235 germany\n    235 rather\n    235 simply\n    235 usually\n    237 attack\n    238 similar\n    240 year\n    241 democracy\n    243 hard\n    243 turkish\n    244 allah\n    245 off\n    246 freedom\n    246 policies\n    246 prevent\n    247 especially\n    248 enough\n    248 nations\n    249 god\n    250 already\n    251 united\n    252 again\n    252 euro\n    253 different\n    254 killed\n    255 considered\n    257 empire\n    257 ideology\n    257 nation\n    257 term\n    258 human\n    258 it’s\n    259 come\n    259 general\n    259 yet\n    260 others\n    261 various\n    262 nuclear\n    262 primary\n    262 source\n    264 fight\n    264 mass\n    266 police\n    269 east\n    269 go\n    269 left\n    269 per\n    271 b\n    271 given\n    271 reason\n    272 cannot\n    272 control\n    272 individual\n    273 lebanon\n    275 always\n    276 instead\n    276 members\n    277 civil\n    279 means\n    280 based\n    281 knight\n    282 attacks\n    282 modern\n    282 once\n    283 leaders\n    284 group\n    285 battle\n    285 including\n    285 known\n    286 france\n    286 small\n    287 likely\n    287 therefore\n    289 free\n    290 man\n    290 operation\n    291 each\n    292 rights\n    292  \n    293 force\n    294 place\n    294 s\n    295 form\n    295 middle\n    295 might\n    295 public\n    296 process\n    299 possible\n    299 th\n    305 acid\n    309 jews\n    310 x\n    311 arab\n    312 school\n    313 certain\n    314 movement\n    315 forces\n    316 back\n    317 last\n    317 ottoman\n    318 say\n    318 themselves\n    319 cause\n    321 armour\n    321 resistance\n    322 three\n    323 result\n    324 often\n    325 great\n    326 justiciar\n    327 family\n    328 book\n    328 important\n    329 thus\n    331 later\n    335 less\n    336 million\n    337 water\n    338 number\n    340 majority\n    343 called\n    344 around\n    345 century\n    347 become\n    347 did\n    348 does\n    348 using\n    349 religion\n    349 work\n    352 case\n    353 within\n    356 create\n    358 among\n    360 example\n    362 law\n    363 death\n    363 knights\n    367 another\n    367 today\n    368 culture\n    369 religious\n    370 point\n    371 due\n    373 population\n    375 want\n    378 states\n    381 marxist\n    382 few\n    385 until\n    386 current\n    386 etc\n    389 following\n    396 church\n    396 end\n    396 social\n    397 kg\n    398 me\n    399 high\n    400 get\n    401 made\n    404 multiculturalism\n    404 too\n    405 good\n    407 large\n    407 since\n    411 life\n    412 children\n    412 every\n    413 media\n    414 west\n    418 phase\n    419 both\n    423 never\n    425 conservative\n    428 least\n    430 according\n    433 support\n    434 immigration\n    434 why\n    440 without\n    441 day\n    445 groups\n    447 eu\n    448 society\n    449 men\n    449 see\n    451 individuals\n    453 future\n    454 several\n    455 him\n    458 down\n    458 europeans\n    461 still\n    461 women\n    465 system\n    466 know\n    475 need\n    479 part\n    482 party\n    483 right\n    483 well\n    484 history\n    492 make\n    492 said\n    492 take\n    499 same\n    503 long\n    523 fact\n    539 much\n    545 could\n    549 before\n    570 those\n    593 way\n    598 between\n    605 jihad\n    613 national\n    617 over\n    621 under\n    623 being\n    627 during\n    633 order\n    641 government\n    644 while\n    645 two\n    648 used\n    652 power\n    657 state\n    660 military\n    674 own\n    684 however\n    685 first\n    694 through\n    696 like\n    700 just\n    702 how\n    712 then\n    714 use\n    719 country\n    729 years\n    763 now\n    777 new\n    780 where\n    782 very\n    791 because\n    824 christians\n    837 must\n    873 countries\n    892 after\n    903 may\n    912 war\n    931 time\n    934 such\n    959 into\n    968 some\n    969 even\n    988 up\n    993 out\n   1025 world\n   1036 what\n   1057 us\n   1064 christian\n   1082 also\n   1109 about\n   1158 do\n   1161 many\n   1166 my\n   1170 its\n   1177 political\n   1177 western\n   1179 against\n   1184 no\n   1187 most\n   1188 any\n   1214 than\n   1218 only\n   1301 so\n   1306 these\n   1308 islamic\n   1324 islam\n   1324 your\n   1329 had\n   1344 been\n   1358 cultural\n   1405 should\n   1421 people\n   1448 when\n   1487 muslims\n   1491 his\n   1507 would\n   1522 there\n   1550 other\n   1563 europe\n   1564 can\n   1579 them\n   1630 he\n   1649 one\n   1694 muslim\n   1739 were\n   1752 more\n   1794 if\n   1890 our\n   1890 who\n   1941 european\n   2015 has\n   2274 which\n   2352 at\n   2379 but\n   2475 all\n   2805 an\n   2819 we\n   2830 –\n   3141 their\n   3278 you\n   3385 was\n   3518 not\n   3578 from\n   3691 on\n   3735 they\n   3849 or\n   3893 i\n   3966 have\n   4120 will\n   4299 this\n   4518 by\n   4532 with\n   5083 be\n   5435 are\n   5611 it\n   6309 for\n   6585 as\n   7873 that\n   9473 is\n  15745 a\n  15878 in\n  21406 to\n  21956 and\n  25517 of\n  47115 the",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107222819375591429",
        "createdAt" : "2019-03-17T10:11:06.056Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was ist dabei rausgekommen?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107222810810830852",
        "createdAt" : "2019-03-17T10:11:03.999Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber gelesen hab ichs ncith",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107222796147609604",
        "createdAt" : "2019-03-17T10:11:00.500Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja habs gezogen... und schon druch meine NLP KI durchgefüttert und durch den wordcount",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1107222640207556612",
        "createdAt" : "2019-03-17T10:10:23.322Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "BTW-ja, ich habe den Link wieder gelöscht.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1107221824490950660",
        "createdAt" : "2019-03-17T10:07:08.860Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/D45vOCelAG",
          "expanded" : "http://g7hxvnavlen4w5rz.onion/amnesty-custard",
          "display" : "g7hxvnavlen4w5rz.onion/amnesty-custard"
        } ],
        "text" : "https://t.co/D45vOCelAG",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106902331168305157",
        "createdAt" : "2019-03-16T12:57:35.721Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bitte über Tor downloaden, hier Breiviks Machwerk.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106902294166147076",
        "createdAt" : "2019-03-16T12:57:26.895Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, könntest Du mir das Breivik gespinne mal noch auf nen Stick tun, das wollte ich aus wissenschaftlichem interesse schon lange mal lesen. Der hatte ja glaub auch Schizophrenie, zumindest gemäss einigen Presseratikeln und deshalb interessiert es mich umsomehr, da ich als ein Mitliged der 90% friedfertigen schizophrenen verstehen was im Kopf eines der 10% gewalttätigen Schizophrenen abgeht....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106662128440823813",
        "createdAt" : "2019-03-15T21:03:07.012Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i weiss jo ned wien er underem bart uusgseht :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106513719981412357",
        "createdAt" : "2019-03-15T11:13:23.572Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106513650670583812",
        "createdAt" : "2019-03-15T11:13:07.049Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das esch alt!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106513581976231940",
        "createdAt" : "2019-03-15T11:12:50.667Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "het nonig gantwortet",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106511619805364228",
        "createdAt" : "2019-03-15T11:05:02.850Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oder denn isches es alts foti...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106511592156553220",
        "createdAt" : "2019-03-15T11:04:56.259Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/s3xbJ9h3O1",
          "expanded" : "https://www.aargauerzeitung.ch/schweiz/piratenpartei-will-zwei-nationalratssitze-warum-ihre-themen-die-waehler-kalt-lassen-134208023",
          "display" : "aargauerzeitung.ch/schweiz/pirate…"
        } ],
        "text" : "https://t.co/s3xbJ9h3O1",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106511541443153924",
        "createdAt" : "2019-03-15T11:04:44.219Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aha!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106511538255527941",
        "createdAt" : "2019-03-15T11:04:43.407Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich glaub de kilian hett ändlich de bart rasiert hihi...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106511447692111878",
        "createdAt" : "2019-03-15T11:04:21.820Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Jo, secher, wenn sech d'PPAG weder e chli beruhigt het... Ohni Nominierige ond so Zügs...",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106511291731136518",
        "createdAt" : "2019-03-15T11:03:44.630Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also i meine am nöchschte in aarau, bin jo immer nur a däm in aarau",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106510999383928836",
        "createdAt" : "2019-03-15T11:02:34.926Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wo esch de?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106510869146599430",
        "createdAt" : "2019-03-15T11:02:03.879Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und am nöchschte stammtisch wider debii?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106510800502562821",
        "createdAt" : "2019-03-15T11:01:47.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ned krass.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106510664649138180",
        "createdAt" : "2019-03-15T11:01:15.131Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und fliisig gfiiret?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106510558310924292",
        "createdAt" : "2019-03-15T11:00:49.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "merci",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1106510405210435589",
        "createdAt" : "2019-03-15T11:00:13.262Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Happy Geburtstag...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1106178746984394757",
        "createdAt" : "2019-03-14T13:02:19.791Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yay :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1072527654165856260",
        "createdAt" : "2018-12-11T16:24:54.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Jep.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1072523450600865806",
        "createdAt" : "2018-12-11T16:08:11.850Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kommst du heute auch?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1072523336926814213",
        "createdAt" : "2018-12-11T16:07:44.763Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja ich wollte schon nochmals probieren ob ich es austricksen kann, hatte aber bisher noch keine zeit, bin recht mit meinem anderen sofware projekt beschäftigt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1043203146632372229",
        "createdAt" : "2018-09-21T18:19:46.670Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Habe noch einen kleinen Bug, der Dein Script endgültig ausschliesst. Weiss aber schon, wie ich das hinbiege. Dann sind jetzt auch alle Cookies heftigst verschlüsselt, inkl. Konsistenz-Check. Bald ist das Ding wie Fort Knox :) Nun, Du hast mir gezeigt, wie anfällig solche Dinger sind. Ich pflege auf solche Hinweise zu reagieren. Schönen Abend und Merci!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1043167535787651077",
        "createdAt" : "2018-09-21T15:58:16.424Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Jep.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1026860496652312580",
        "createdAt" : "2018-08-07T15:59:55.284Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, dann bis später",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1026860446144446468",
        "createdAt" : "2018-08-07T15:59:43.282Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja!",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "1026860360576520197",
        "createdAt" : "2018-08-07T15:59:22.823Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi, kommst du heute auch an den stammtisch?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1026860301503946756",
        "createdAt" : "2018-08-07T15:59:08.722Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BjwHU6zfYA",
          "expanded" : "https://github.com/braindef/deepLearning",
          "display" : "github.com/braindef/deepL…"
        } ],
        "text" : "falls du mal selber mit gensim und word2veg probieren willst: https://t.co/BjwHU6zfYA",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "928328510019653636",
        "createdAt" : "2017-11-08T18:28:59.076Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also dem ott...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "837389227298066439",
        "createdAt" : "2017-03-02T19:48:44.443Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/J8YDczYcWB",
          "expanded" : "https://github.com/braindef/Hackers-Cardgame/blob/master/TODO.txt#L367-L430",
          "display" : "github.com/braindef/Hacke…"
        } ],
        "text" : "kannst du das an den komischen stefan weiter leiten? https://t.co/J8YDczYcWB",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "837387952430977027",
        "createdAt" : "2017-03-02T19:43:40.566Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du wurdest weggesperrt? Mit welcher Begründung?",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "829275608593133571",
        "createdAt" : "2017-02-08T10:28:07.087Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/XE8xEN5Iqz",
          "expanded" : "https://twitter.com/messages/media/801472404644134915",
          "display" : "pic.twitter.com/XE8xEN5Iqz"
        } ],
        "text" : " https://t.co/XE8xEN5Iqz",
        "mediaUrls" : [ "https://ton.twitter.com/dm/801472404644134915/801472392568705024/0ZJY4V_e.jpg" ],
        "senderId" : "75128838",
        "id" : "801472404644134915",
        "createdAt" : "2016-11-23T17:08:07.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo, Du kennst Dich doch mit Recht aus, verbietet mir da jetzt der Schlichtungsrichter einen Anwalt hinzu zu ziehen? Blödes Rechtsverdreher-Kauderwelsch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "801472369382621187",
        "createdAt" : "2016-11-23T17:07:58.217Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/3Az2LQbvrX",
          "expanded" : "http://www.worldcat.org/title/funfzig-jahre-philafrikanische-mission-in-angola/oclc/721200854",
          "display" : "worldcat.org/title/funfzig-…"
        } ],
        "text" : "halli hallo, kennst Du dich mit Eduard von Okolski aus und ist das des selbe der die Pfadi Adler Aarau gegründet hat wo ja der Grabstein rituell ans Pfadiheim genagelt wurde nach den 80 Jahren auf dem Friedhof in aarau? https://t.co/3Az2LQbvrX",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "793002081103781892",
        "createdAt" : "2016-10-31T08:10:04.180Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/JfJEkgSP0x",
          "expanded" : "https://de.wikipedia.org/wiki/Woyzeck#Woyzeck_im_Film",
          "display" : "de.wikipedia.org/wiki/Woyzeck#W…"
        } ],
        "text" : "kleine Frage an den Rechtsspezialisten, eine Person in den Suizid zu treiben müsste ja auch ein Straftatbestand sein, hast Du eine Ahnung wo ich das in den Gesetzestexten finde? Konkretes Beispiel wäre z.B. das hier: https://t.co/JfJEkgSP0x",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "753349918375481347",
        "createdAt" : "2016-07-13T22:06:31.849Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/iRbvvEsUaH",
          "expanded" : "https://twitter.com/FailDef/status/752623867269701637",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "Die Formulierung \"Versäuchungsgebiet\" Aarau auf der PiratenSeite über das Freifunk in Aarau ist ev. eine ScheinKorrelation: https://t.co/iRbvvEsUaH",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "752676723385303043",
        "createdAt" : "2016-07-12T01:31:29.639Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "syn =&gt; ayn-ack und re-ack am nächsten stammtisch...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "747896657224863752",
        "createdAt" : "2016-06-28T20:57:13.024Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xFKrip49Mn",
          "expanded" : "https://twitter.com/FailDef/status/747895928493867008",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "⚠ https://t.co/xFKrip49Mn ☠: ★★★★★ (5/5)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "747896507454599172",
        "createdAt" : "2016-06-28T20:56:37.335Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no problemo, war gestern noch kurz in Brugg, wegen der ETH / FHNW Professoring, bin dann auch noch rasch im P8.1 vorbei gegangen, dein Bruder hat sich scheinbar gefreut, dass jemand der seine geschwister kennt vorbei kommt, wollte aber dann doch nciht mit mir reden als ich erwähnt hatte, dass ich auch ein spinner bin (ps: das ist in der psychiatrie wie in unis wo man mal war, da trampt man dann halt einfach malr rein... :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "697667870256406531",
        "createdAt" : "2016-02-11T06:25:57.052Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey, Marc, sorry, bin gestern nach Hause gegangen, da extrem müde. Hoffe, Du bist gut nach Hause gekommen! LG, Dominic a.k.a Satan ;)",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "697316615084490756",
        "createdAt" : "2016-02-10T07:10:11.236Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/pyMvaj4g6g",
          "expanded" : "http://www.eiz.uzh.ch/uploads/tx_seminars/16._upc_cablecom_lecture_04.02.2016.pdf",
          "display" : "eiz.uzh.ch/uploads/tx_sem…"
        } ],
        "text" : "allenfalls was für Dich: https://t.co/pyMvaj4g6g (eintritt frei...)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "694966797917933571",
        "createdAt" : "2016-02-03T19:32:51.193Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in firefox eingeloggt und im iceweasel ohne im facebook eingeloggt zu sein...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "658062347194494979",
        "createdAt" : "2015-10-24T23:27:44.528Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ähm da ist allenfalls was mit dem Facebook Eintrag kaputt...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "658062075382661123",
        "createdAt" : "2015-10-24T23:26:39.723Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/TAXcGStlDQ",
          "expanded" : "https://twitter.com/messages/media/658062000321335299",
          "display" : "pic.twitter.com/TAXcGStlDQ"
        } ],
        "text" : " https://t.co/TAXcGStlDQ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/658062000321335299/658062000677875712/9mRGFpoX.png" ],
        "senderId" : "75128838",
        "id" : "658062000321335299",
        "createdAt" : "2015-10-24T23:26:22.119Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich gebe niemanden Hinweise zu Dir oder anderen Personen.",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "390504780335812608",
        "createdAt" : "2013-10-16T15:49:29.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "600137781",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schade...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "275966587641679872",
        "createdAt" : "2012-12-04T14:15:36.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey Marc! Der Stammtisch von heute Abend im Chili Mex in Aarau fällt aus. Cu, Dominic",
        "mediaUrls" : [ ],
        "senderId" : "600137781",
        "id" : "275949742285000704",
        "createdAt" : "2012-12-04T13:08:40.000Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1149368594",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und musste MobileNr nicht bekannt geben😁",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "1126209777653178373",
        "createdAt" : "2019-05-08T19:38:29.678Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Habe das Feedback Formular genutzt und geschrieben, dass ich kein Bot sei.\nVorgestern nochmals nachgehakt und Verständnis für lange Bearbeitungszeit wegen momentaner Sperrwelle gezeigt und seit gestern Nacht wohl entsperrt",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "1126209656370663428",
        "createdAt" : "2019-05-08T19:38:00.763Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "musstest du irgend ein formular ausfüllen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1126200035895648260",
        "createdAt" : "2019-05-08T18:59:47.062Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MzkQ0eEBSB",
          "expanded" : "https://twitter.com/KilianBrogli/status/1126185190278168583?s=19",
          "display" : "twitter.com/KilianBrogli/s…"
        } ],
        "text" : "Mattscheibe auf dem Account ist vorbei. Danke für Deine Hinweise (und sehe nun die DMs wieder)\nhttps://t.co/MzkQ0eEBSB",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "1126189431550029828",
        "createdAt" : "2019-05-08T18:17:38.782Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/yuppasPwE1",
          "expanded" : "https://twitter.com/messages/media/1125187581493354500",
          "display" : "pic.twitter.com/yuppasPwE1"
        } ],
        "text" : " https://t.co/yuppasPwE1",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1125187581493354500/1125187571288543232/2mUD2P4r.jpg" ],
        "senderId" : "75128838",
        "id" : "1125187581493354500",
        "createdAt" : "2019-05-05T23:56:39.248Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "???",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1125187539176914949",
        "createdAt" : "2019-05-05T23:56:29.011Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "true",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "1097903124013109252",
        "createdAt" : "2019-02-19T16:57:57.514Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/iK6pv3JtYC",
          "expanded" : "https://twitter.com/AlecMuffett/status/1097157076823035905",
          "display" : "twitter.com/AlecMuffett/st…"
        } ],
        "text" : "https://t.co/iK6pv3JtYC",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1097898426686156810",
        "createdAt" : "2019-02-19T16:39:17.679Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/iRbvvEsUaH",
          "expanded" : "https://twitter.com/FailDef/status/752623867269701637",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "Allenfalls auch im DreiLänderEck der Plan? https://t.co/iRbvvEsUaH",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "752676150288191496",
        "createdAt" : "2016-07-12T01:29:12.992Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, danke, muss mal kurz überlegungen zu reha anstellen, war noch nie in einer Reha, kann mir da nichts drunter vorstellen, mit psychiatrien hingegen kenne ich mich aus, da war ich glaub ca 10x...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "744977800273268739",
        "createdAt" : "2016-06-20T19:38:43.350Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in rheinfelden ist viel Reha- und Wellness, etc. aber da wird sich sicher noch viel sonst umher sein.",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "744976782940573699",
        "createdAt" : "2016-06-20T19:34:40.763Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, ich glaub das ist nicht ne richtige psychiatrie sondern irgend so eine Klinik odre so? steht glaub auf seiner webseite...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "744976489804869635",
        "createdAt" : "2016-06-20T19:33:30.861Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hy marc...nein, in rheinfelden ist nichts, was mir bekannt wäre...gruesse, Kilian",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "744970483905462279",
        "createdAt" : "2016-06-20T19:09:38.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/FjaRAiPYQd",
          "expanded" : "http://marclandolt.ch/ml_buzzernet/2016/02/01/mutmasslicher-facebook-edgerank-algorithmus/",
          "display" : "marclandolt.ch/ml_buzzernet/2…"
        } ],
        "text" : "hmmm, irgendwie filtert es Dich in meinem Twitter Stream, Mutmassung:  https://t.co/FjaRAiPYQd",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "694017764017737731",
        "createdAt" : "2016-02-01T04:41:43.872Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber wichtiger, die Liste 10 zu nehmen und mit Kandiadten Deiner Wahl zu ergänzen und dann zu wählen (-&gt; und somit den svplern an das Bein zu gingen ;)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "654842051155116035",
        "createdAt" : "2015-10-16T02:11:26.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "grins - der Dom hatte diese idee auch....erschreckend, die SVP wählenden haben gar nie auf die Wahlbeilage geschaut - affront gegenüber der Demokratie ;(",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "654841643447799811",
        "createdAt" : "2015-10-16T02:09:48.876Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wahlcouvert: bin grad mit dem Gedanken am spielen die SVP Liste abzufackeln oder zu maltretieren??? :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "654841136406732804",
        "createdAt" : "2015-10-16T02:07:48.020Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/kXYVOGPOBH",
          "expanded" : "https://twitter.com/FailDef/status/654493170290569216",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "https://t.co/kXYVOGPOBH",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "654840595085705219",
        "createdAt" : "2015-10-16T02:05:38.917Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "servus marc...ok, habe dies nicht mitbekommen mit dem SP/PP etc. - hatte auf dem Printscreen Aargau und Aarau gsehen...\n\nund das Wahlcouvert schon eingeworfen?",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "654839859446730755",
        "createdAt" : "2015-10-16T02:02:43.535Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gern geschehnen...und  nun muss ich mal ab ins Bett...gn8 &amp; lg",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "609153648250253312",
        "createdAt" : "2015-06-12T00:22:01.957Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "TNX.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "609153234603741184",
        "createdAt" : "2015-06-12T00:20:23.331Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ftp://ftp.ff3l.net/gluon/stable/factory/ evtl. noch bei freifunk.de community für Deinen Nano (weiss nicht,ob jene bei uns die ricthige ist)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "609150516552142848",
        "createdAt" : "2015-06-12T00:09:35.296Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du allenfalls Link für OpenWRT Firmware für Freifunk, allenfalls probiere ich mal meine zwei Ubiquiti Networks - NanoBeam®M2-400 zu flashen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "609149171396911105",
        "createdAt" : "2015-06-12T00:04:14.587Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "woww, upgedated  und immer a jour, da muss ich morgen mal reinhören. thx for your inputs, die sind gold wert resp. unbezahlbar",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "609148240869650432",
        "createdAt" : "2015-06-12T00:00:32.743Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/0nhV342Moa",
          "expanded" : "http://chaosradio.ccc.de/cr203.html",
          "display" : "chaosradio.ccc.de/cr203.html"
        } ],
        "text" : "CCC Sendung über Freifunk, da haben die das erwähnt...\n\nhttp://t.co/0nhV342Moa",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "609147795459735552",
        "createdAt" : "2015-06-11T23:58:46.618Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, dies ist sie - cool, danke für den Tip, wusste bis jetzt noch nichts von der andere Map gebe ich gerade weiter. Wow, Berlin BOOMT",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "609147481478270977",
        "createdAt" : "2015-06-11T23:57:31.680Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/6S8Q44ZLmu",
          "expanded" : "http://map.freifunk-3laendereck.net/",
          "display" : "map.freifunk-3laendereck.net"
        }, {
          "url" : "http://t.co/4QyEGFzTKH",
          "expanded" : "http://openwifimap.net/",
          "display" : "openwifimap.net"
        } ],
        "text" : "Ist das eigentlich eure Karte? http://t.co/6S8Q44ZLmu\nallenfalls wärs noch schlau das auch noch im http://t.co/4QyEGFzTKH eintragen(lassen)?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "609146376371499009",
        "createdAt" : "2015-06-11T23:53:08.225Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber auf vielen Schultern verteilt,geht es für alle Ringer ;)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "592800508693377024",
        "createdAt" : "2015-04-27T21:20:29.668Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Er war am letzten Stamm in Aarau dabei. Morgen weiss ich nicht,ob er kommt...heute hat er was it mæssiges gemacht",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "592800357706772480",
        "createdAt" : "2015-04-27T21:19:53.654Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ist Stefan Ott auch da? oder hat er sich ganz zurück gezogen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "592793304925904897",
        "createdAt" : "2015-04-27T20:51:52.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Morgen Warren wir alle mal wieder in Aarau,ab 1930Uhr am gewohnten Ort. Bist dabei?gruessli,kili",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "592766899475349504",
        "createdAt" : "2015-04-27T19:06:56.615Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ahoi Master...eine Homepage sucht noch einen gewieften Wordpress Ersteller&amp;Modifizierer?will noch Shop sowie Crowdfunding integrieren",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "592766722349871105",
        "createdAt" : "2015-04-27T19:06:14.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "grins - ich frage alle...aber habe es mir notiert ;)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "575022722931146753",
        "createdAt" : "2015-03-09T19:57:55.228Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "NR, never ever!!!! :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "575022476603867137",
        "createdAt" : "2015-03-09T19:56:56.496Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "musst nicht pressieren, ausser wir dürfen Dich noch als NR auf die Liste setzen. ab 19.00Uhr AV und dann ab ca. 20Uhr gemütlicher Teil ;) lg",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "575022084327370752",
        "createdAt" : "2015-03-09T19:55:22.977Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "woww, hammer. da bin ich halt einfach spät dran und versuchte, Restaurant und Pubs davon zu überzeugen. aberr ist schwer,da ohne it knowhow",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "575021939791675392",
        "createdAt" : "2015-03-09T19:54:48.511Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn ich nicht verpenne morgen stammtisch, freifunk ist super, aarau hat ja jetzt, ausserdem meine wlans sind auch alle offen.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "575021556755230720",
        "createdAt" : "2015-03-09T19:53:17.196Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was hälst Du von den Freifunk?ich ätte sonst noch eines, welches noch geflascht werden müsste und in einer Stadt sich gut machen würde ;)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "575021156366970880",
        "createdAt" : "2015-03-09T19:51:41.768Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hy Lord...ich finds cool, dass Bewerbungen raushaust, egal welche...aber am besten wärst Du in der IT aufgehoben.Kommst morgen ins ChiliMex?",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "575020973310808064",
        "createdAt" : "2015-03-09T19:50:58.095Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "PS: sorry, isch min bock gsieh",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "557070862349000704",
        "createdAt" : "2015-01-19T07:03:38.143Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ups, sorry, han au de falschi arti faved. jetzt grad de tweet mit infos . i de vierte grossrubrik de siebti abschnitt",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "557070810675179521",
        "createdAt" : "2015-01-19T07:03:25.820Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sorry bin grad nicht herr meines geistes, was hast Du wo gelesen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "557064727080501248",
        "createdAt" : "2015-01-19T06:39:15.402Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "habe ich bei spon auch gelesen, take care und guten Wochenstart",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "557061780527407104",
        "createdAt" : "2015-01-19T06:27:32.863Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "perfekt, lightbeam war es. thx a lot",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "548816342426877952",
        "createdAt" : "2014-12-27T12:23:07.223Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "d videos sind söscht denn nochher sowiso online z.B. uf youtube oder uf ccc.de denn chasch söscht nocheluege",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "548816206585921536",
        "createdAt" : "2014-12-27T12:22:34.839Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/TNLBtcDLE1",
          "expanded" : "http://www.digitaltrends.com/computing/mozillas-collusion-plug-in-aims-to-expose-online-tracking/",
          "display" : "digitaltrends.com/computing/mozi…"
        } ],
        "text" : "http://t.co/TNLBtcDLE1 das?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "548815874061529089",
        "createdAt" : "2014-12-27T12:21:15.594Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "jepa, muend es noh aufschalten für den dienstag, anderes restaurant hatte zu.\nja, genau dieses, welche gerade im Saal6 präsentiert wurde ;)",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "548815402852446208",
        "createdAt" : "2014-12-27T12:19:23.215Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also das mit dene bubbles? Zischtig stamm? stoht aber ned uf de websiite piraten-aargau.ch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "548814630018383872",
        "createdAt" : "2014-12-27T12:16:18.956Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hmm, wie heisst das plugin von firefox um den traffic verkehr aufzuzeigen? danke und gruss, Kilian\nPS: am Dienstag ist stamm in Aarau,dabei?",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "548814279382937604",
        "createdAt" : "2014-12-27T12:14:55.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "merci gliichfalls",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "489183507370229760",
        "createdAt" : "2014-07-15T23:03:31.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "choenne und mit Dir plaudere,analysiere und philosophieren. Chum guet heimwaerts und bis bald,glg kili",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "489155486970028032",
        "createdAt" : "2014-07-15T21:12:10.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Vielen liebe Dank für DIN Besuch,Inputs,Infos und einfach alles...wenn die SBB APP richtig anzeigen würde,hät i noch bos Aarau sitze bliebe",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "489155199999963136",
        "createdAt" : "2014-07-15T21:11:02.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1149368594",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bi im zug und so i 20 min dete",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "489092933631873024",
        "createdAt" : "2014-07-15T17:03:36.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ahoi....wirsch au noh auf Wohlen cho?mir sind definitiv im Rest Bären....bis nachher&amp;LG,kili",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "489092692119666688",
        "createdAt" : "2014-07-15T17:02:39.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/Z4kHb2JsRE",
          "expanded" : "http://www.youtube.com/watch?v=2Bkpitdl95I",
          "display" : "youtube.com/watch?v=2Bkpit…"
        }, {
          "url" : "https://t.co/TND3JNroJz",
          "expanded" : "https://ton.twitter.com/1.1/ton/data/dm/428544578854338560/428544578871119872/BQZlvGha.jpg",
          "display" : "pic.twitter.com/TND3JNroJz"
        } ],
        "text" : "und das video dazu am 30c3 http://t.co/Z4kHb2JsRE https://t.co/TND3JNroJz",
        "mediaUrls" : [ "https://ton.twitter.com/dm/428544578854338560/428544578871119872/BQZlvGha.jpg" ],
        "senderId" : "1149368594",
        "id" : "428544578854338560",
        "createdAt" : "2014-01-29T15:06:04.000Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "jepa,Krankenakten gehören dem Patienten u nicht den Ärzten sowie Versicherungen.Hörte auch schon von schwarzer Liste bei Krankenkassen,pfui",
        "mediaUrls" : [ ],
        "senderId" : "1149368594",
        "id" : "428299112568852480",
        "createdAt" : "2014-01-28T22:50:40.000Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1206879247",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1206879247",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CbPxQY914d",
          "expanded" : "https://github.com/freebsdgirl/ggautoblocker",
          "display" : "github.com/freebsdgirl/gg…"
        } ],
        "text" : "is this like https://t.co/CbPxQY914d",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "833701908930035716",
        "createdAt" : "2017-02-20T15:36:39.251Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/3c33alBox7",
          "expanded" : "http://unfollowspy.com",
          "display" : "unfollowspy.com"
        } ],
        "text" : "Hey flatlanders: this Mainiac knows why the state of Ma een A is PRO nowinganced a thataway. \n\nTo all the Seals, Green Berets, spooks, and effin' US marines Semper Fi'!\n\nLet's take this suckin' planet back from these apocalyptic genocidal mfers!\n\nAnd the only enemy I've met is the NEUTRON BOMBLET!(excluding my Shin Beth pals)\n\nYesh Gval\n\n(Send custom messages to new followers via the https://t.co/3c33alBox7 website)",
        "mediaUrls" : [ ],
        "senderId" : "1206879247",
        "id" : "833700849226428420",
        "createdAt" : "2017-02-20T15:32:26.592Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1248765216",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oder es könnte auch einfach eine Eule sein...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971722834706882565",
        "createdAt" : "2018-03-08T12:22:32.239Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja da hat einfach ein nachbar seinen wasserkocher ein bisschen unsanft hingestellt oder so",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971720424605339654",
        "createdAt" : "2018-03-08T12:12:57.681Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "die Aufnahme in der Nacht ist echt! ich nahm im Traum auch Strukturen wahr alles abstrakt irgend etwas war das",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971719646880595972",
        "createdAt" : "2018-03-08T12:09:52.195Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "😀",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719423139700740",
        "createdAt" : "2018-03-08T12:08:59.103Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "beweise es",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719385860792324",
        "createdAt" : "2018-03-08T12:08:49.968Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich versuche aufzuklären so gut wie ich kann, weil ich darüber mehr weiß als der Rest.",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971719346903973893",
        "createdAt" : "2018-03-08T12:08:40.699Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber auch \"menschen\" udn \"gesundheit\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719258957852676",
        "createdAt" : "2018-03-08T12:08:19.707Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mkultra",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719123779641348",
        "createdAt" : "2018-03-08T12:07:47.517Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "targeted individual",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719096780886020",
        "createdAt" : "2018-03-08T12:07:41.067Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mind control",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719063897608196",
        "createdAt" : "2018-03-08T12:07:33.215Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "chemtrail",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971719021094633476",
        "createdAt" : "2018-03-08T12:07:23.044Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das sind die häufigsten Worte in Deinem Twitter Profil",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971718996604092420",
        "createdAt" : "2018-03-08T12:07:17.180Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/1T2i3Pvcx6",
          "expanded" : "https://twitter.com/messages/media/971718924831256580",
          "display" : "pic.twitter.com/1T2i3Pvcx6"
        } ],
        "text" : " https://t.co/1T2i3Pvcx6",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971718924831256580/971718912474861569/f4TZoTyU.jpg" ],
        "senderId" : "75128838",
        "id" : "971718924831256580",
        "createdAt" : "2018-03-08T12:07:00.319Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/sj1WhVEli2",
          "expanded" : "https://twitter.com/messages/media/971718877590753284",
          "display" : "pic.twitter.com/sj1WhVEli2"
        } ],
        "text" : " https://t.co/sj1WhVEli2",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971718877590753284/971718838608912385/scm9PbuD.jpg" ],
        "senderId" : "75128838",
        "id" : "971718877590753284",
        "createdAt" : "2018-03-08T12:06:49.183Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "uups das war das falsche bild",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971718784632410116",
        "createdAt" : "2018-03-08T12:06:26.628Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/8S5SFCjoho",
          "expanded" : "https://twitter.com/messages/media/971718703413891076",
          "display" : "pic.twitter.com/8S5SFCjoho"
        } ],
        "text" : " https://t.co/8S5SFCjoho",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971718703413891076/971718693871849472/96GSb7cx.jpg" ],
        "senderId" : "75128838",
        "id" : "971718703413891076",
        "createdAt" : "2018-03-08T12:06:07.446Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nein Herzrasen, Erschöpfung",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971718576393674757",
        "createdAt" : "2018-03-08T12:05:36.969Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hab jetzt nur Belastungsstörung und das Experiment beginnt in der Schlafphase remoteviewing usw...",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971718457392758788",
        "createdAt" : "2018-03-08T12:05:08.595Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also belastungsstörung = schizophreinie?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971718385636728836",
        "createdAt" : "2018-03-08T12:04:51.495Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja und das war nicht gut die Symptome Belastungsstörung und stimmen wurden nicht besser",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971717980139683846",
        "createdAt" : "2018-03-08T12:03:14.978Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "weil ich nehm mal an in der Psychiatrie msustest du welche nehmen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717827668381700",
        "createdAt" : "2018-03-08T12:02:38.460Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gibt's in Deutschland auch maximal 6 Monate zwangsmedikation?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717764779053063",
        "createdAt" : "2018-03-08T12:02:23.477Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "keine",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971717657593556996",
        "createdAt" : "2018-03-08T12:01:57.910Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bei mir auch, und dann hab ich seit neue xeplion und geht wieder besser damit, was hast Du für medis?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717605336772612",
        "createdAt" : "2018-03-08T12:01:45.454Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Paranoide Schizophrenie",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971717504220491780",
        "createdAt" : "2018-03-08T12:01:21.341Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und was wurde bei Dir diagnostiziert?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717435266093060",
        "createdAt" : "2018-03-08T12:01:04.913Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971717367079276548",
        "createdAt" : "2018-03-08T12:00:48.713Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber Du hast schon Psychiatrie Erfahrung?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717290910736392",
        "createdAt" : "2018-03-08T12:00:30.501Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717140591054852",
        "createdAt" : "2018-03-08T11:59:54.646Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nein aus Deutschland",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971717114779357188",
        "createdAt" : "2018-03-08T11:59:48.538Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bist du schweizer?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971717039567134724",
        "createdAt" : "2018-03-08T11:59:30.579Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schizophrenie hab ich schon seit 20 jahren, mich aber leider immer geweigert medikamente zu nehemen, was ein fehler war",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971716940455727108",
        "createdAt" : "2018-03-08T11:59:06.948Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "glaub mir Versuch es du wirst dich wundern",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971716908625035269",
        "createdAt" : "2018-03-08T11:58:59.350Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich glaub eher so an schulmedizin und eher weniger an homeopatie",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971716758892679174",
        "createdAt" : "2018-03-08T11:58:23.647Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "robert franz?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971716694388498436",
        "createdAt" : "2018-03-08T11:58:08.260Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich weis auch wie es ist, hast du versucht mit den Tipps von Robert Franz ? ich hab z.b eine schlimme Grippe nach einen Tag mit Citricidal weg bekommen. Naturheilmittel sind nicht zu unterschätzen",
        "mediaUrls" : [ ],
        "senderId" : "1248765216",
        "id" : "971716407829389316",
        "createdAt" : "2018-03-08T11:56:59.956Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1248765216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich schreib da mal besser ne PM: eben, ich will Dich Dann nicht irgendwie dumm darstellen oder so, ich hab nur in tweetdeck nach Menschen gesucht die ähnliches twittern als ich damals als ich meine Medikamente nicht genommen habe. Es ist dann schon auch nicht so dass da Vitamine helfen würden, aber bei mir hat dann Xeplion geholfen. Ich hatte auch das Gefühl dass Deine Tweets die Du schreibst schon ein bisschen nach Hilferuf tönen denn in den letzten Tagen und auch viel häufiger als sonst was bei mir immer eine grössere Krise war. Ich hab dann immer gedacht irgendwer würde meine Tweets sehen und mir helfen aber leider hat niemand geholfen damals...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971715057506807812",
        "createdAt" : "2018-03-08T11:51:38.040Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1357057818",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1357057818",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/mN3zrMBqKY",
          "expanded" : "https://twitter.com/messages/media/1332030752226234374",
          "display" : "pic.twitter.com/mN3zrMBqKY"
        } ],
        "text" : "danke für die antwort. Der Screenshot ist schon von euch afaik nur schon ein paar Jahre alt, Ihr könnt ja nichts dafür was für Stellen ausgeschrieben werden. Grunsätzlich müsste man aber viel \"clusterfuck\" aufräumen. nicht nur das Plastik im Meer oder die CO2 Problematik, es sieht in der Informatik genau so schlimm aus. Physiker würden sich im Grab-Rotieren (oder wenn sie noch leben sonst) wenn man das Wort Entropie für Chaos verwenden würde, Informatik ist aber eher \"prone\" für Entropie... https://t.co/mN3zrMBqKY",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1332030752226234374/1332029723371859970/z50-ZsZN.jpg" ],
        "senderId" : "75128838",
        "id" : "1332030752226234374",
        "createdAt" : "2020-11-26T18:37:34.090Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/rimBCjwIcV",
          "expanded" : "https://www.jobs.ch/de/stellenangebote/?term=cluster",
          "display" : "jobs.ch/de/stellenange…"
        }, {
          "url" : "https://t.co/g5UYqA7GTf",
          "expanded" : "http://jobs.ch",
          "display" : "jobs.ch"
        } ],
        "text" : "Hallo lieber Marc\nDanke für deinen Kommentar. Diese Seite ist leider nicht die aktuelle von uns :) Aber ich habe dir sehr gerne den Link rausgesucht, sodass du zu den passenden Stellen kommst.\nhttps://t.co/rimBCjwIcV\nIch wünsche dir alles Gute!\nLiebe Grüsse, Ajten von https://t.co/g5UYqA7GTf",
        "mediaUrls" : [ ],
        "senderId" : "1357057818",
        "id" : "1331906630422827012",
        "createdAt" : "2020-11-26T10:24:21.063Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1465707894",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/2z96H1yFDP",
          "expanded" : "http://truetwit.com/vy348602888",
          "display" : "truetwit.com/vy348602888"
        } ],
        "text" : "TicklishQuill uses TrueTwit validation. To validate click here: http://t.co/2z96H1yFDP",
        "mediaUrls" : [ ],
        "senderId" : "1465707894",
        "id" : "579718053719515136",
        "createdAt" : "2015-03-22T18:55:29.413Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1608199141",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/qH7xzwCyXR",
          "expanded" : "http://truetwit.com/vy347855047",
          "display" : "truetwit.com/vy347855047"
        } ],
        "text" : "DebMatheny uses TrueTwit validation. To validate click here: http://t.co/qH7xzwCyXR",
        "mediaUrls" : [ ],
        "senderId" : "1608199141",
        "id" : "578422921930022912",
        "createdAt" : "2015-03-19T05:09:05.875Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2304536324",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sippschaft ist auch so ein Typisches 1980er Wort #Altsprech, ich bleib bei der Pfisterer Hypothese.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "648945610964508676",
        "createdAt" : "2015-09-29T19:41:05.281Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ".",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "648629113922654212",
        "createdAt" : "2015-09-28T22:43:26.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ausserdem gibt es way to much social media das behauptet cb zu sein, mein achilles der am waltermerzweg programmiert und getestet wurde, nach dem mich urs &amp; astrid mit (passend auf psyprofil eines nerds, cia) einem computerkurs angelockt haben nur weil ich behauptet habe der cia unterwandere die abb chfac?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "648629027360653315",
        "createdAt" : "2015-09-28T22:43:05.771Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Dann loopt es auf US4877027 (Bundespolizist) ergo Seite mit Malware (z.B. das Hacking Team Zeugs was die Polizei Zürich illegalerweise angeschafft hat. Dann der Pädophilenfall in Aarau war wohl ein ¢IA Manöver um vom 1950er Pädoring abzulenken, wie in England mit den Pädo Politiker, die Russen wusstens die Amis wussens aledged Hr. P. Gibt ja Hanibals ähm Leute die genaustens wissen wie der Staatsaparat funktioniert, Zitat: \"Hahaha, ein Pädo hat es gar nicht schön im Knast\", und diese Leute haben dann wohl selber keine Pädophilie geübt, aber alle 1950er dazu angestiftet, ergo Macht. Ausserdem gratd recht offtopic und ausserhalb meines Ereignishorizonts, man sollte mal den Aarauer Sauladen aufräumen, da haben wir ja jetzt genug beweise...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "644242606105325571",
        "createdAt" : "2015-09-16T20:13:01.451Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, will nur nicht zu viel aufs mal veröffentlichen (Infos pro Zeiteinheit und davon die Steigung, dy/dx) was man mir angetan hat, damit sich die Täterschaft langsam daran gewöhnt und mich nicht gleich umbringt oder wie vor einer Woche halb zum Krüppel schlägt.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635730370944761859",
        "createdAt" : "2015-08-24T08:28:26.431Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so wie das Wikileaks Logo, von der Schwarzen Weltkugel langsam so viel Wahrheit veröffentlichen bis die schwarze Weltkugel ganz verschwunden ist, so dass kein schwarzes Schaf Amok läuft weil es ertappt wurde. dy/dx...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635507357624238083",
        "createdAt" : "2015-08-23T17:42:15.915Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "du bietest selbstjustiz, bist somit kein pertner mit dem ich \"geschäfte\" machen werde. AHMEN",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635504284927459332",
        "createdAt" : "2015-08-23T17:30:03.326Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Pflegeeltern eher nicht, vielleicht wurde ihnen das am anfang mal eingeredet, dann wurden über mich ihre sündendatenbank der \"Pflegeeltern\" die ihre kinder misshandeln (vater hat z.B. die Tochter geschlagen) ausgehändigt und am dem zeitpunkt haben sie auf angriffsmodus geschaltet. als laisser faire, sündendatenbank abfüllen, dann über ein halb so altes kind die taten einbenden, zufälligerweise hat das kind damals belege gefunden dass 1999 der CIA das ABB Firmennetzwerk unterwandert hat, wo dann mehrerer milliarden nach usa abgeflossen sind.\n\nund nein es wird sich niemand vorgeknöpft, die HUMINT Baupläne des CIA wollen dass man gewalt anwendet, protokollieren das und dann wirst Du auch fermdbestimmt!!!!!",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635501352962117636",
        "createdAt" : "2015-08-23T17:18:24.314Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "In dem kleinen Block wo ich wohne gab es scheinbar schon 6 \"miestriöse\" Tote. Da stimmt irgendwas nicht, z.B. das mit dem Andree Brunner, der Jürg terroristiert alle, inklusive mir oder der Sozialhilfeempfängerin, er geht gerne auf schwächere, ich habe wegen dem terror von jürg viele \"selbst\"mordversuche gemacht, auftraggeber wird urs&amp;astrid Blum sein in meinem fall und im fall von andree Brunner wohl der ehemalige arbeitgeber von ihm, urs&amp;astrid blum ist wohl auch auftragnehmer meines ehemaligen arbeitgebers, wo ich miseriöse machenschaften aufgedeckt habe, das wurde aber vom psychiater vertuscht und nie gerichtlihc bearbeitet, wie der fall #Mollath in deutschland, und ja es war halt so technologie involviert.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635477991217307652",
        "createdAt" : "2015-08-23T15:45:34.436Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/IhsR57Orpq",
          "expanded" : "http://chaosradio.ccc.de/cr198.html",
          "display" : "chaosradio.ccc.de/cr198.html"
        } ],
        "text" : "nö. bin noch da.\n\nZur ABB, damals wie heute ist die ABB Lernzentren, heute BBB (soll ja nicht auf den Konzern und von da aufs Militär zurückfallen ) einentlich ein Abrichtungsplatz für besagte Technologien, im neuen Gebäude hat es jetzt sogar Überwachungskameras, und 30 Sekunden nach dem man als Ehemaliger das Gebäude betritt, taucht auch schon ein Lehrer auf und hindert einem daran da was am Kiosk zu kaufen. Die geben auch damit an wie gut sie ihrer \"Roboter\" steurn können und Programmiert haben. Apropos gehackter Hardware, hör allenfalls mal das Chaos Radio über Hardware Hacking: http://t.co/IhsR57Orpq\nDie stelle mit der Losgrösse der Security Chips (\"für PayTV\"), wenn einer gehackt ist können somit hunderte wenn nicht tausende Kinder misbraucht, zu Amokläufern gemacht oder getötet werden. \"Nicht so super\".\n\nWie beim Backup ein bisschen paranoid zu sein nicht schadet, so schadet es auch nicht ein bisschen vorsichtig ähm paranoid zu sein.\n\nGruss",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "635022510787035139",
        "createdAt" : "2015-08-22T09:35:39.448Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nachtrag: geh nur von einem Kommunikationskanal/von einem Server/von einer Software aus, und Du bist genau so durch die anderen Kanäle gesteuert die Du noch nicht kennst.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634682600846684163",
        "createdAt" : "2015-08-21T11:04:58.579Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Fang an so Zeugs zu lesen und zu veröffentlichen wenn Du nicht CIA \"Herrenrasse\" bist [1] dann findest Du es vermutlich selber raus.\n\nDann zitierst Du den stimociever, als DAS Objekt was dazu verwendet wird, de facto gibt es tausende von möglichkeiten solche Technologien zu verwenden, alte, neue. Dann wenn Du dich mit dem Konzept von Objekt Orientierter Programmierung auskennst, kommst Du allenfalls schnell dahinter, dass man dann eine entsprechende \"Verwaltungs\"-Software schreiben könnte, die verschiedene Technologien über ein Interface(Java) gleichzeitig/abwechslend/alternativ verwendet.\n-Stimociver\n-US4877027\n-Wo2005055579A1\n-Akkustische Signale\n-Visuelle Signale\n-Chip im Visuellen Cortex\n-RetinaCCD Chip\n-Stuart Meloy Implantat\n-Active Denial System (\"weisse\" Folter)\n-Mikrowellen\"spektroskopie\"::NeuroEndokrine um die Hormone zu beeinflussen\n -&gt;Adernalin (Agression)\n -&gt;Serotonin (Glücksgefühl)\n -&gt;Dopamin (Überlegenheitsgefühl)\n -&gt;Ocytocin (langristige Päärchenbildung)\n\netc etc etc\n\n[1] Snowden: \"US Spies feel comfortable in Switzerland\", wenn es jemand komfortabel hat, leidet im Normalfall jeman anderes darunter.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634681728846700547",
        "createdAt" : "2015-08-21T11:01:30.680Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/PT4GbA4Xjl",
          "expanded" : "https://en.wikipedia.org/wiki/Jos%C3%A9_Manuel_Rodriguez_Delgado",
          "display" : "en.wikipedia.org/wiki/Jos%C3%A9…"
        } ],
        "text" : "Lies den Artikel über https://t.co/PT4GbA4Xjl (Das ist jetzt ca. 50 Jahre alt, könnte also sein, dass das mittlerweile so ein bisschen (Euphemismus) weiterentwickelt ist.\n\nThe stimoceiver could be used to stimulate emotions and control behavior. According to Delgado, \"Radio Stimulation of different points in the amygdala and hippocampus in the four patients produced a variety of effects, including pleasant sensations, elation, deep, thoughtful concentration, odd feelings, super relaxation, colored visions, and other responses (other Responses; da fehlt im Artikel, dass man so einen Aggressionsschub und z.B. einen Amoklauf triggern könnte).\" Delgado stated that \"brain transmitters can remain in a person's head for life. The energy to activate the brain transmitter is transmitted by way of radio frequencies.\" (Heute würde man das \"Energy Harvesting\" nennen)\n\nBehavioural:\nab einem bestimmten Schmerzpegel (Exponential-Kurve) bricht jeder\n\nManupulativ mit \"nur\" Bildmaterial, egal ob per TV oder RetinaCCD, Konditionierte Stimuli (Pavlow / Clark Hull) etc..:\nFrag mal einen 20 Jährigen ob er durch Werbung beeinflussbar ist, zu ~80% wird er sagen, dass er das nicht ist, er hat ja einen freien Willen. Frag ihn danach welchen Nassrasierer (zumindest in der Schweiz) er verwendet, ~70% werden sagen: \"Giletet\". Dann frag ihn wieder: \"bist du sicher dass Du nicht beeinflussbar bist?\" --&gt; dann flüchten sie jeweils -&gt; Kognitive Dissonanz weil er nicht gemerkt hat, dass durch das vertrauen das er in seinem Kinderzimmer hat, wo auch der TV steht, automatisch auch den Produkten vertraut die da angeboten werden.\n\n(Sorry für die langen Sätze und sogar für Schweizer Verhältnisse schlechte Rechtschreibung) :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634633666749181955",
        "createdAt" : "2015-08-21T07:50:31.792Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "lies die patente, das ist teilweise \"non-invasiv\", das heisst du brauchst kein implantat. sonst gibts rfid chips zumindest für die identifikation im KSA (Kantonsspital Aarau) afaik haben die ca. 100 stück am lager.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634480908503019523",
        "createdAt" : "2015-08-20T21:43:31.372Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "WARNUNG: wenn du so zeugs zu lesen beginnst wirst du automatisch Targeted Individual des CIA / NSA und so weiter...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634409023400452099",
        "createdAt" : "2015-08-20T16:57:52.625Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "diverse möglichkeiten, das fängt in den 1930er mit den \"liebenröhren\" (für Medizinische Zwecke an, geht dann über 1950er Jose Rodruiguez Manuel Delgado auf 1960er mit Frey Effect, Cornelson University, bis heute wo das so gut entwickelt ist dass man es praktisch nicht mehr wahrnimmt als nicht Autist.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634408852017053699",
        "createdAt" : "2015-08-20T16:57:11.791Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/NdYdFoIZwQ",
          "expanded" : "https://twitter.com/messages/media/634406865804394499",
          "display" : "pic.twitter.com/NdYdFoIZwQ"
        } ],
        "text" : "irgend so was hier, das macht man \"natürlich nicht\" mit menschen nur mit laborratten https://t.co/NdYdFoIZwQ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/634406865804394499/634406865858920448/MM03S1yC.png" ],
        "senderId" : "75128838",
        "id" : "634406865804394499",
        "createdAt" : "2015-08-20T16:49:18.506Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, eher gesteuerte 1960er Zombis, aber mit gluschtigen 18 jährigen Protokollassistentinnen. \"Gönnt man sich halt\" :/",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "634405427585941507",
        "createdAt" : "2015-08-20T16:43:35.306Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2304536324",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, jeder kann die gedanken von jedem lesen, das mit der Funkdisziplin müssen die aber noch lerenen: WO2005055579A1",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "633627888676376579",
        "createdAt" : "2015-08-18T13:13:55.580Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "19386137-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/fJqOa5OZzu",
          "expanded" : "https://twitter.com/messages/media/1320108342510051333",
          "display" : "pic.twitter.com/fJqOa5OZzu"
        } ],
        "text" : "Halli Hallo Christian\n\nich wollte fragen an wen man sich wenden kann, wenn man das Gefühl hat dass meine nicht-mehr-Mutter (Margrit Landolt-Gigax) zumindest mal geplant hat, dass meine jüngere Nichte auch als LaborRatte im Psychiatrie-Wesen landet? Also ungefähr so wie der Dr. Marvin Monroe (aus Simpsons) beweisen will, dass wenn man ein Kind (mich) 20 Jahre in einem KellerZimmer vergammeln lässt, dass es dann einen Dachschaden entwickelt.\n\nMeine beiden Nichten mögen mich Total, streiten sogar darum wer von ihnen Time-Slots mit mir bekommen, da versuche ich beiden beizubringen, dass man auch zu dritt spielen kann (Work in Progress)\n\nBis anhin vor ca. 5 Monaten wurde ich immer Montags eingeladen zu meinen Eltern, wenn die Nichten bei ihren Grosseltern waren. Als ich dann zu kritische Fragen gestellt / Blicke geworfen habe hat sie gesagt ich würde meinen Nichten etwas antun wollen um mich vor der ganzen Familie zu diskreditieren danach wurde ich wieder in die Psychiatrie gesperrt und mit intelligenzmindernder Psychopharmaka behandelt... die letzten 20-40 Jahre waren für mich die Hölle, und ich möchte verhindern, dass Jüngere auch als Laborratten missbraucht werden. \n\nEs geht mir nicht um Rache und Gerechtigkeit für mich, aber könnte man allenfalls im PolizeiComputer auf der Akte von Maja Landolt (3 jährig) diese vorerst spekulative Vermutung als solche eintragen, oder wird es dann eher schlimmer weil die wüssten, dass ihr Onkel das jetzt schon rausgefunden habe?\n\nDer passende Euphemismus daz: \"Verzogen\", bzw. irgend etwas geisteskrankes wie das Stanford Prison Experiment für Kinder oder wenn Du eher Film-Quellen zitieren möchtest \"Twins\" mit Schwarzenegger, aber halt mit Psychologie und nicht mit DNA.\n\nGruss\nMarc\n\nallenfalls im selben Kontext: Bild https://t.co/fJqOa5OZzu",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1320108342510051333/1320108309626687492/J5zZrpZt.jpg" ],
        "senderId" : "75128838",
        "id" : "1320108342510051333",
        "createdAt" : "2020-10-24T21:02:10.208Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/74Th99kr4s",
          "expanded" : "https://twitter.com/FailDef/status/1304412224606416897",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "Halli Hallo Christian\n\ngibt es eine legale Möglichkeit für Schweizer den Klarnamen zu einem Twitter Account zu bekommen?\n\nhttps://t.co/74Th99kr4s\nhttps://twitter .com/FailDef/status/1304412224606416897\n\nKurzanalyse der Vorhandenen Daten\n(Psychologie-Prädikatenlogik für Anfänger)\n\np1: Gemäss Profil Micorosoft Programmierer\np2: findet solchen Content lustig\np3: will solchen Content anpranger\n\np1 -&gt; p3: allenfalls Autist mit Inselbegabung auf Informatiker\n\np2 -&gt; p4: ev. selber opfer vo so etwas gewesen, dann würde man es lustig finden, weil man mal Ausnahmsweise nicht das Opfer ist und gar nicht unbdingt weil man irgend etwas wie ein Pädo-Sadist ist\n\np1 + p4 -&gt; p5: ungesehene dritte Hand hätte ihn mehr abgreichtet als erzogen\n\np3 würde die Gleichung ändern, aber um genaues sagen zu können definitiv zu wenig Daten, deshalb eben meine Frage, gibt es irgend eine legale Möglichkeit an seinen [ Namen | eMail | ... ] \n\np5 -&gt; p6: abrichtende Instanz hätte Zwangsneurose ihn isoliert zu halten und z.B. solche Fragen zu zensieren\n\np5 -&gt; p7: hätte entsprechend hohe Access Codes fürs Abrichten\n\np6 -&gt; p8: überprüfen ob mindestens mein Reply in Twitter angekommen ist\n\np7 -&gt; p9: Julian Assange sagt ja dass zensiert wird im Netz...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1304419083241353223",
        "createdAt" : "2020-09-11T13:58:39.110Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, kleine sachlich nüchterne Frage ohne viel Emotionen: Falls der Art 59 StGB (Kleine Verwahrung) dazu verwendet würde um Zeugen, Täter, Opfer an der Justiz vorbei zu schmuggeln oder vor der Gerichtsverhandlung handlungsunfähig, unglaubwürdig zu machen und zu diskreditieren, gibt es da irgend ein Gesetz um dieses Sicherheitsloch im Rechtssystem zu stopfen? Du und Dominic sind die einzigen die ich kenne die sich mit so etwas auskennen, deshalb frage ich Euch.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1275839536019451908",
        "createdAt" : "2020-06-24T17:13:44.046Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, ich nehm jetzt meine Medis (wenn auch nicht freiwillig), aber die eigentliche Frage wäre, wie findet man raus ob man in der PKK/PDAG ein IMSI in einem IMSI-Catcher ist, oder sonst irgendwie Telefongespräche umgeleitet werden? Weisst Du das allenfalls grad, weil afaik warst Du derjenige der mit den IMSI's angefangen hat...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1227707649380233221",
        "createdAt" : "2020-02-12T21:34:48.071Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, nur gedacht :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1216287267926544388",
        "createdAt" : "2020-01-12T09:14:16.887Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Dankeschön :)\nLieben Gruss an den Teddy. Aber ich habe überhaupt kein Bedürfnis, jünger auszusehen. Ich bin bald 50, sehe bald 50 aus und fühle mich dabei gut und gesund",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1216287140998500356",
        "createdAt" : "2020-01-12T09:13:46.645Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mein gleichaltriger \"Teddy Bär\" findet ich soll Dir wegen Deinem neueren Twitter Foto sagen, dass ich meinen Bart mit Haarfarbe aus dem Aldi färbe, man sieht dann grad 5-10 Jahre jünger aus... bzw. frabig wie der @breakthesystem ist auch toll...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1216128593098682372",
        "createdAt" : "2020-01-11T22:43:45.886Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, verstanden. Dann zu meiner Medikation, das geht soweit gut ohne. bzw. habe ich vorallem von der Psychiatrie selber etwas wie ein PTBS, denn die haben bei mir so richtig viel falsch gemacht. Und ja ich habe angst vor viele menschen, vor autos, und eben vor der psychiatrie, aber eine gesunde portion paranoja schaded allenfalls im leben genau so wenig wie beim backup. aber so weit alles gut und da ich ja jetzt in der telli wohne habe ich zum einkaufen nur 100 m zu fuss fussweg. Falls Du weisst, wo ich das mit der Militärjustiz nachlesen kann wäre nicht abgeneigt das mal durchzulesen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184708263981600772",
        "createdAt" : "2019-10-17T05:50:35.641Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein Marc.\n1. ich bin nicht Dein Privatpolizist\n2. ich bin nicht Polizist\n3. innere Sicherheit ist sowieso kantonales Hoheitsgebiet.\n4. militärische Vergehen sind in der Zuständigkeit der Militärjustiz.\n\nLass das jetzt bitte sein. \n\nErlaube mir die Frage, wie es mit Deiner Medikation steht. Du machst mir derzeit einen sehr unguten Eindruck",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1184700340458930180",
        "createdAt" : "2019-10-17T05:19:06.539Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Name dejenigen der mir das geschickt hat ist Lukas Weber, war gar in Bern in der Psychiatrie",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1184554182990352394",
        "createdAt" : "2019-10-16T19:38:19.874Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/VrKxeWpQMr",
          "expanded" : "https://twitter.com/messages/media/1184553904249430021",
          "display" : "pic.twitter.com/VrKxeWpQMr"
        } ],
        "text" : "sorry dass ich Dir das so weiterleite, aber da das meiner Meinung nach im Kanton Solothurn ist wärst Du oder Dein Chef wohl doch die richtige Anlaufstelle, ich werde es auch dominik zschokke senden https://t.co/VrKxeWpQMr",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1184553904249430021/1184553898125791232/UhbHjfsA.png" ],
        "senderId" : "75128838",
        "id" : "1184553904249430021",
        "createdAt" : "2019-10-16T19:37:13.553Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, wenn mich die psychiaterin wieder mal zwangsinterniert erfahre ihc das meist erst dann wenn die polizei mir handschellen anlegt... aber gut beim letzten mal hab ich auch massehaft so mails verschickt, aber nicht weil ich drohen wollte, sondern das war doch auch nur ein hilfeschrei den obwhol etwa 100 leute in dem verteiler war mir niemand den kleinen finger gereicht hat... aber mittlerweile weiss ich, dass das wort eines schizoprhenen überhaupt nichts gilt und dass es halt vorurteile gibt die man selbst wenn man zu den 90% nicht gewalttätigen schizoprhene gehört immer wie die 10% gewalttätigen behandlet wird. ist vermutlcih mit nicht-gewalttätigen ausländer auch öfters mals so...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158793638127779844",
        "createdAt" : "2019-08-06T17:35:07.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ha! \nErstens wüsste ich das nicht und\nZweitens wüsstest Du das vermutlich :p",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158792659827343365",
        "createdAt" : "2019-08-06T17:31:14.156Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no problemo, ich vermute wenn ich in dem systme ausgeschrieben wäre würdest du mich nicht auhc noch vorwarnen :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158792171308367877",
        "createdAt" : "2019-08-06T17:29:17.658Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und tut mir echt leid wegen der fedpol Geschichte. Das habe ich ungeschickt gehandhabt. Bitte um Entschuldigung",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158792016949633029",
        "createdAt" : "2019-08-06T17:28:40.848Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich denke, das ist keine Frage des Alters, sondern der Fahrtauglichkeit. Die allerdings mit zunehmendem Alter zunehmend abnimmt. Darum auch regelmässig geprüft werden sollte",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158791850762866692",
        "createdAt" : "2019-08-06T17:28:01.238Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hab auch keinen führerschein und bin gotten froh darüber, da mir dann auch nicht so ein mist passieren kann wie mit dem 60 jährigen, der eine mutter mit 2 töchter im anhänger überfährt. man sollte eh allen ab 60 das billet wengehmen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158791415771607045",
        "createdAt" : "2019-08-06T17:26:17.519Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158791165828902918",
        "createdAt" : "2019-08-06T17:25:17.926Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Den hat dafür meine Frau ;)",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158791150184095748",
        "createdAt" : "2019-08-06T17:25:14.193Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das macht ja irgendwie keinen sinn",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158791147831144452",
        "createdAt" : "2019-08-06T17:25:13.639Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Haha! Ich besitze ein Auto. Einfach keinen Führerschein",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158791068357419012",
        "createdAt" : "2019-08-06T17:24:54.683Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja den post hab ich gesehen... ich hab mich gefragt wie du das zeugs ohne auto nach hause transporteren kannst, aber ich hab dann gedacht ich belästige dich besser nicht wegen so was",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158790894570680326",
        "createdAt" : "2019-08-06T17:24:13.257Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und ne, ich denke in der aktuellen Konstellation interessieren mich die Aargauer Piraten noch etwas weniger als schon vorher",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158790466944585734",
        "createdAt" : "2019-08-06T17:22:31.299Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich mache Ferien ;)\nInklusive Ikea Pulte zusammenschrauben.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158790261255880708",
        "createdAt" : "2019-08-06T17:21:42.263Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "huch... btw. was macht du so? kommst du wieder mal an nen stammtisch?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158790034901950470",
        "createdAt" : "2019-08-06T17:20:48.300Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Definitiv nicht.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789864424443908",
        "createdAt" : "2019-08-06T17:20:07.644Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich muss mir keine sorgen mehr machen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158789749827670020",
        "createdAt" : "2019-08-06T17:19:40.339Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nochmals. Sorry für die Belästigung",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789660157587460",
        "createdAt" : "2019-08-06T17:19:18.943Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sie hatte mal mit einem anderen Tanner zu tun gehabt",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789517316374532",
        "createdAt" : "2019-08-06T17:18:44.905Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ist gut, habe mit ihr gesprochen.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789331781324804",
        "createdAt" : "2019-08-06T17:18:00.664Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja kein problem, bis in ca 3h zittert mein körper nicht mehr... :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158789261031854088",
        "createdAt" : "2019-08-06T17:17:43.796Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich hab jetzt mal alle Fr. landolts die ich kenne im facebook angeschrieben um zu fragen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158789196326277126",
        "createdAt" : "2019-08-06T17:17:28.360Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sorry für die Störung",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789155641593860",
        "createdAt" : "2019-08-06T17:17:18.660Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Andere Landolts",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789112205336584",
        "createdAt" : "2019-08-06T17:17:08.300Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Könnte Kontakt ausfindig machen",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789077153587204",
        "createdAt" : "2019-08-06T17:16:59.968Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "huch...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158789060997070855",
        "createdAt" : "2019-08-06T17:16:56.113Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ne, geht nicht um Dich.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158789012078940164",
        "createdAt" : "2019-08-06T17:16:44.433Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hab nicht das gefühl, dass diese Frau landolts aus meiner familie dich überhaupt kennen. ich hab deinen namen glaub nicht mal beiläufig erwähnt. udn es gibt glabu recht viele landolts. ich bin jetzt wegen dem grad in super panik modus. schizophrene sind vulnerabler, das heist die drehen ab so zeugs völlig ab. und da ich ja dich nie irgendwie explizit erwähnt habe oder die empfohlen oder so sagt mein unterbwusstsein jetzt irgendwas wie \"üble verschwörung gegen mcih\" -- vermutlcih heisst meine diagnose deshalb paranoide schizophrenie... also bin ich dann im ropol und wollte sie dass man mich da rauslöscht?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158788151965564934",
        "createdAt" : "2019-08-06T17:13:19.371Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Fahndungssystem der CH Polizeien",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158787271811899396",
        "createdAt" : "2019-08-06T17:09:49.527Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich kenne ausser Dir keine Ladolts. \nWenn das keine Verwandte von Dir waren -&gt; Sorry. Falls doch: Das geht so nicht.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158787200525524997",
        "createdAt" : "2019-08-06T17:09:32.523Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was ist RIPOL",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158787018278785034",
        "createdAt" : "2019-08-06T17:08:49.068Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich habe - in meinen Ferien, wohlgemerkt - eine Info erhalten, dass bei fedpol eine Frau Landolt angerufen habe, mich verlangt habe und gesagt habe, es sei dringend. Gehe um eine RIPOL Löschung.",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158786390852820996",
        "createdAt" : "2019-08-06T17:06:19.493Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Eine Frau Landolt hat beim Fedpol angerufen? Hab ich etwas falsch gemacht, du stürtzt mich grad in eine gröbere Psychose. Ich weiss nichts von einer Frau Landolt die beim Fedpol angerufen hat. Ich kenne nur 2 Frau Landolts, meine Schwester Ursula und meine Mutter Margrit. Könntest Du mich bitte von dieser Anspannung so schnell wie möglich befreien und mir sagen was los ist?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1158783751234695176",
        "createdAt" : "2019-08-06T16:55:50.169Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hoi Marc\n\nIst die Frau Landolt, die heute bei fedpol angerufen hat und mich verlangt hat, mit Dir verwandt?",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1158765579303608325",
        "createdAt" : "2019-08-06T15:43:37.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, danke für die antwort und euch tanners auch alles gute und gesundheit :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1048494283739860996",
        "createdAt" : "2018-10-06T08:44:52.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hoi Marc\n\nIch arbeite zwar bei der Bundespolizei, bin aber weder Polizist noch Jurist. Ich empfehle Dir, mit Deinen Fragen an einen Juristen zu gelangen. Der Exception ist da sicher ein besserer Kontakt als ich. \n\nOb die Gruppe vom deutschen Verfassungsschutz beobachtet wird habe ich keine Ahnung. In der Schweiz ist die Verantwortung hier zwischen NDB (Prävention) und fedpol (KOBIK - Strafverfolgung) geteilt. Allenfalls kommen auch noch die relevanten KAPOs zum Zug. \n\nZu Deinem geistigen Zustand: Es freut mich, dass es Dir besser geht. Geistige Ruhe und Gesundheit sind wichtig. \nPass auf Dich auf.\n\nCheers\nChris",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "1048278723655610372",
        "createdAt" : "2018-10-05T18:28:18.545Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, ich hätte da mal eine Frage\n\nWenn z.B. eine Facebook Gruppe wie \"Reale Verschwörungen!\" (meiner Meinung nach) einen auf rechte bis rechtsextreme Gesinnung und Rassenhetze macht findet. Da aus neugier mal beitritt.\n\n1. Frage: Macht man sich da strafbar (vermutlich nicht bevor man merkt was läuft)\n2. Frage: Werden diese (Deutsche Gruppe) und deren Mitglieder allenfalls vom Verfassungsschutz beobachtet und man selber als Mitglied gelistet?\n3. Frage: Wenn man es dann gemerkt hat muss man es melden oder macht man sich dann strafbar wenn man es nicht meldet?\n4. Frage: Wie meldet man eine Facebook Gruppe in der man gesperrt ist, da scheint Facebook gar keine Möglichkeit mehr anzubieten diese zu melden.\n(PS: Es ist jetzt nicht dass ich sauer wäre auf die Gruppe und mich rächen möchte, ich hab dann versucht vernünftig auf die Leute einzureden um vielleicht beim einen oder anderen positiven Einfluss nehmen zu können)\n\nUnd im Nachhinein schnalle ich auch weshalb da alles in Richtung Rassismus geht, die haben einfach alle die politisch eher Links sind ausgesperrt. Ausserdem fördert das das aufspalten der Gesellschaft in verschiedene Subgruppen und es gibt keinen Dialog. Das ist doch auch irgendwie behindert...\n\nZu meinem Zustand (ich hab ja mal recht schrott gepostet) ich hab jetzt Medikamente und bin wieder Vernünftig, und mir ist mittlerweile klar, dass Polizie etc. mich nicht verfolgt und dass es für Polizisten vermutlich selber recht mühsam sein muss sowohl wenn sie mit Links- als auch Rechts-Extremen zu tun hat. Mittlerweile weiss ich auch, dass 90% der Schizophrenen nie gewalttätig werden, dass die Polizei aber zur Sicherheit die ganzen 100% so behandelt als wären sie bei den 10% Gewalttätigen Schizophrenen (Was den Psychischen Zustand der 90% gewaltfreien Schizos verschlimmert).\n\nGruss\nMarc\n\nPS: Wär toll Dich wieder mal an einem Piraten-Stammtisch zu sehen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1047941649169625092",
        "createdAt" : "2018-10-04T20:08:53.714Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19386137",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "806384496891330568",
        "createdAt" : "2016-12-07T06:27:00.661Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nimm mich aus dem Verteiler raus",
        "mediaUrls" : [ ],
        "senderId" : "19386137",
        "id" : "806382155978932228",
        "createdAt" : "2016-12-07T06:17:42.618Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2398046887",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2398046887",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/DCCCcFg4Jk",
          "expanded" : "https://twitter.com/messages/media/1326013030849785861",
          "display" : "pic.twitter.com/DCCCcFg4Jk"
        } ],
        "text" : "i always follow all parties, on latin you say \"Audiatur et altera pars\", it does not help to make the people fight more or isolate more. i normaly expain this that way: there are Idiots in all coutries / religions, there are idiots in russia but also idiots in usa. the good thing is that there are also good people in all coutries. often no matter if right wing or left wing, black or white, older people tend to abuse their powers against younger people, so i found a way to work around this problem: screenshot https://t.co/DCCCcFg4Jk",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1326013030849785861/1326012998864031744/O_CjffDZ.jpg" ],
        "senderId" : "75128838",
        "id" : "1326013030849785861",
        "createdAt" : "2020-11-10T04:05:17.507Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "why do you follow GCHQ they are fully involved in targetting people",
        "mediaUrls" : [ ],
        "senderId" : "2398046887",
        "id" : "1325954684243337220",
        "createdAt" : "2020-11-10T00:13:26.457Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2461130078",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, wie gehts, alles klar bei Dir?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1309136924578324484",
        "createdAt" : "2020-09-24T14:25:40.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, wie gehts, alles klar bei Dir?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1177995415066730503",
        "createdAt" : "2019-09-28T17:16:07.745Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke vielmals",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175822237448003589",
        "createdAt" : "2019-09-22T17:20:41.828Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok... gute besserung",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175822204585611268",
        "createdAt" : "2019-09-22T17:20:33.990Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber ich habe die ganze woche noch ferien, wir finden also sicher noch zeit, mir ist das wichtig",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175822124470231045",
        "createdAt" : "2019-09-22T17:20:14.883Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wird heute nichts, habe mich total erkältet und werde mich ausruhen",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175821990906728452",
        "createdAt" : "2019-09-22T17:19:43.060Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und schon bereit?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175820985586585605",
        "createdAt" : "2019-09-22T17:15:43.357Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich geh dann mal ein paar stunden pennen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175657766435860484",
        "createdAt" : "2019-09-22T06:27:08.877Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ed0kQQP6LJ",
          "expanded" : "https://www.brack.ch/sandisk-usb-stick-ultra-fit-usb3-1-16-gb-701079",
          "display" : "brack.ch/sandisk-usb-st…"
        }, {
          "url" : "https://t.co/EJENY1jlLy",
          "expanded" : "https://github.com/braindef/installation-scripts",
          "display" : "github.com/braindef/insta…"
        } ],
        "text" : "falls du linux installieren möchtest, dann gehts sonst auch mit nem usb30 stick wie diesem hier: https://t.co/ed0kQQP6LJ und allenfalls ist das nützlich: https://t.co/EJENY1jlLy ./desktop/debian/buster/install.sh (script anschauen)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175657736526344196",
        "createdAt" : "2019-09-22T06:27:01.758Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/RDcoLVBfG8",
          "expanded" : "http://git.0x8.ch",
          "display" : "git.0x8.ch"
        } ],
        "text" : "ok, mumble ist allenfalls kaputter als ich gedacht habe, bereite auf deinem windows oder besser linux rechner als notfallplan mal jamulus vor, findest du auf sourceforge als windows binary oder in meinem githubacout als vorbereitetes deb für debian, ubuntu, mint ... (debianoide systeme), jamulus server läuft auch auf https://t.co/RDcoLVBfG8, ist unverschlüsselt, aber da die \"netten\" arschlöcher eh alles mitbekommen wenden wir hier einfach das konzept von \"Security by Obscurity\" an, was soviel heisst wie, man lernt funktionieren mit dem umstand, dass die arsche fast alles wissen....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175561306285498372",
        "createdAt" : "2019-09-22T00:03:52.023Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hahaha",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175517627185336324",
        "createdAt" : "2019-09-21T21:10:17.073Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/PTwIkjj9dq",
          "expanded" : "https://twitter.com/messages/media/1175517580389486602",
          "display" : "pic.twitter.com/PTwIkjj9dq"
        } ],
        "text" : "und schriib gross mich druuf, mech nervesi eh scho permanent: https://t.co/PTwIkjj9dq",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1175517580389486602/1175517574248972288/R0OFxaz-.png" ],
        "senderId" : "75128838",
        "id" : "1175517580389486602",
        "createdAt" : "2019-09-21T21:10:06.204Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Okay",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175517376483352581",
        "createdAt" : "2019-09-21T21:09:17.304Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dim alter würdi papiir bruuche, und schiib det druuf keini lüüt wo jünger sind, sie würde denn au uf die los goh...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175517318916530181",
        "createdAt" : "2019-09-21T21:09:03.583Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Eineweg würdi die software bzw datei ufemene lifeboot nutze ohni abindig a ergedwelchi netzwerk",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175516983728713733",
        "createdAt" : "2019-09-21T21:07:43.669Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Us welem grund denn?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175516779562582021",
        "createdAt" : "2019-09-21T21:06:54.982Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "egal weli absichte, wenn so öppis hesch wirsch massiv meh aagiffe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175516654928875525",
        "createdAt" : "2019-09-21T21:06:25.273Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oder so ala Profiling ohni bösi absichte, e bessere usdruck chund mir keine i sinn",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175516430604877829",
        "createdAt" : "2019-09-21T21:05:31.782Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Chli e art Social Networking",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175516192766943237",
        "createdAt" : "2019-09-21T21:04:35.088Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I wett ebe so en topologie mache u.A um mini bekannte u alli um mi ume besser z verstah, u ehres verhalte besser ischätze z chenne",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175516154279923722",
        "createdAt" : "2019-09-21T21:04:25.903Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/wfar2QnqD3",
          "expanded" : "https://marclandolt.ch/gespinne/display.php",
          "display" : "marclandolt.ch/gespinne/displ…"
        } ],
        "text" : "nö, so etwas musst du selber programmieren (Mathe: Netzwerkgraphen) oder mit Blatt und Stift machen... https://t.co/wfar2QnqD3",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175515946590658566",
        "createdAt" : "2019-09-21T21:03:37.414Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mit steckbrief u notizmöglichkeite",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175515784799510532",
        "createdAt" : "2019-09-21T21:02:57.824Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Übrigens, was i vergesse ha z fraage, kennsch du e software womer chan sini bekannte u allgemeine kontäkt iträge mit profilierigie u beziehige zunenand? Aso wie e netzwerktopologie für mensche?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175515682890498053",
        "createdAt" : "2019-09-21T21:02:33.525Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und sag mal was",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175511546111385604",
        "createdAt" : "2019-09-21T20:46:07.247Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hörst du mcih?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175511519758606343",
        "createdAt" : "2019-09-21T20:46:00.953Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175510430074228740",
        "createdAt" : "2019-09-21T20:41:41.152Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Jetzt wieder nicht",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175510288814215172",
        "createdAt" : "2019-09-21T20:41:07.472Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "immmer noch kein voice?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175510173676441607",
        "createdAt" : "2019-09-21T20:40:40.019Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich ha di iz es wili lang ghört",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175509782314278916",
        "createdAt" : "2019-09-21T20:39:06.712Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gott, die sind ja echt sauer darauf, dass ich dir das erkläre und müssen ganz viele cyberwaffen aufbrachen (voll in unserem interesse :D)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175509356072308740",
        "createdAt" : "2019-09-21T20:37:25.088Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Haha",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175507868537237517",
        "createdAt" : "2019-09-21T20:31:30.429Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "zum glück hab ich noch weitere laptops... moment rasch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175507735363948548",
        "createdAt" : "2019-09-21T20:30:58.729Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gut zu wissen",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175507249122480132",
        "createdAt" : "2019-09-21T20:29:02.749Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also so cyberwaffen brauchen sie selten, weil die sicherheitslöcher dann repariert werden",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175507154222100492",
        "createdAt" : "2019-09-21T20:28:40.135Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja das ist so",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175507076711424004",
        "createdAt" : "2019-09-21T20:28:21.644Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber das gute ist wenn sie so zeugs anwenden, dann zeigen sie auch wo wir was fixen können",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175507013704568836",
        "createdAt" : "2019-09-21T20:28:06.627Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kann sich also nur noch um stunden handeln :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175506924676276230",
        "createdAt" : "2019-09-21T20:27:45.402Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Verstehe",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506787912560644",
        "createdAt" : "2019-09-21T20:27:12.832Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "alles gleich unsicher, sie verwenden es einfach nur dann wenn würfeli anderen würfeli helfen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175506722745651204",
        "createdAt" : "2019-09-21T20:26:57.260Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hardwaretechnisch",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506651127922693",
        "createdAt" : "2019-09-21T20:26:40.205Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich meine wie sicher lenovo ist",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506628868751367",
        "createdAt" : "2019-09-21T20:26:34.892Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö acer",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175506487562657802",
        "createdAt" : "2019-09-21T20:26:01.190Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie sicher ist Lenovo",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506321690509322",
        "createdAt" : "2019-09-21T20:25:21.637Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175506261300961292",
        "createdAt" : "2019-09-21T20:25:07.245Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Lade mein handy für ein paar sekunden",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506209140613124",
        "createdAt" : "2019-09-21T20:24:54.807Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Okay",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175506151561158670",
        "createdAt" : "2019-09-21T20:24:41.069Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "moment rasch, selbe malware bei mri wie vorhin",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175506078299279364",
        "createdAt" : "2019-09-21T20:24:23.604Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175505942965866503",
        "createdAt" : "2019-09-21T20:23:51.351Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hörst du mih nicht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175505908996169733",
        "createdAt" : "2019-09-21T20:23:43.244Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/c2CXiozWbo",
          "expanded" : "https://www.youtube.com/watch?v=l5IaYfDS_Ms",
          "display" : "youtube.com/watch?v=l5IaYf…"
        } ],
        "text" : "https://t.co/c2CXiozWbo",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175498043485032454",
        "createdAt" : "2019-09-21T19:52:28.497Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Alternativ threema telefon oder telegram?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175492957341913093",
        "createdAt" : "2019-09-21T19:32:15.329Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja irgendwas stimmt nicht, mir zeigts an du redest, hör aber nichts",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175492577572872200",
        "createdAt" : "2019-09-21T19:30:44.780Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bin noch dran, liegt bei mir",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175492314787188744",
        "createdAt" : "2019-09-21T19:29:42.150Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "muss rasch neu booten",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175491392535179269",
        "createdAt" : "2019-09-21T19:26:02.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, ist was auf dme alsa afaik",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175491323647995910",
        "createdAt" : "2019-09-21T19:25:45.836Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "muss das 150 mikro erst konfigurieren, wait",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175491233805979648",
        "createdAt" : "2019-09-21T19:25:24.405Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich här dich",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175491188289429508",
        "createdAt" : "2019-09-21T19:25:13.558Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hörst du mcih",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175491102541078533",
        "createdAt" : "2019-09-21T19:24:53.117Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mein mic is ok",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175491077765312520",
        "createdAt" : "2019-09-21T19:24:47.200Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bzw. verwende gescheiter nen laptop nicht handy",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175490005806047238",
        "createdAt" : "2019-09-21T19:20:31.624Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "server is ok, ist allenfalls dein mic gemuted?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175489971274362885",
        "createdAt" : "2019-09-21T19:20:23.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Tiptop",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175489747101388807",
        "createdAt" : "2019-09-21T19:19:29.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, ich test rasch den server",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175489686208491529",
        "createdAt" : "2019-09-21T19:19:15.433Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175489565655797764",
        "createdAt" : "2019-09-21T19:18:46.682Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hör dich aber nicht hörst du mcih",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175489530780106756",
        "createdAt" : "2019-09-21T19:18:38.414Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sehe es",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175489513247903749",
        "createdAt" : "2019-09-21T19:18:34.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wäre drin",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175489493140475908",
        "createdAt" : "2019-09-21T19:18:29.393Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/pqGLfkyazV",
          "expanded" : "https://t.co",
          "display" : "t.co"
        } ],
        "text" : "der redirect von https://t.co/pqGLfkyazV wird nur über https gemacht, verwende die ip im screenshot",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175488349118828548",
        "createdAt" : "2019-09-21T19:13:56.702Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ZvjObhbllw",
          "expanded" : "https://twitter.com/messages/media/1175487932884500485",
          "display" : "pic.twitter.com/ZvjObhbllw"
        } ],
        "text" : " https://t.co/ZvjObhbllw",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1175487932884500485/1175487925208920064/YAS6E3oa.jpg" ],
        "senderId" : "2461130078",
        "id" : "1175487932884500485",
        "createdAt" : "2019-09-21T19:12:17.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Er zeigt",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175487792123645957",
        "createdAt" : "2019-09-21T19:11:43.865Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/aelryAIW99",
          "expanded" : "http://Git.0x8.ch",
          "display" : "Git.0x8.ch"
        } ],
        "text" : "https://t.co/aelryAIW99",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175487747085295620",
        "createdAt" : "2019-09-21T19:11:34.136Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/vuM3GRGYXQ",
          "expanded" : "https://twitter.com/messages/media/1175487679045275652",
          "display" : "pic.twitter.com/vuM3GRGYXQ"
        } ],
        "text" : " https://t.co/vuM3GRGYXQ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1175487679045275652/1175487673580081155/bTZ4kNlt.png" ],
        "senderId" : "75128838",
        "id" : "1175487679045275652",
        "createdAt" : "2019-09-21T19:11:17.223Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/RDcoLVBfG8",
          "expanded" : "http://git.0x8.ch",
          "display" : "git.0x8.ch"
        } ],
        "text" : "also was zeigt der dns resolver auf https://t.co/RDcoLVBfG8",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175487477525700612",
        "createdAt" : "2019-09-21T19:10:29.862Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ip",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175487405895430149",
        "createdAt" : "2019-09-21T19:10:11.758Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Scheint offline",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175487142782480388",
        "createdAt" : "2019-09-21T19:09:09.027Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und mumble hat geklapt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175486999723200517",
        "createdAt" : "2019-09-21T19:08:34.934Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wär ready",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175486920274653190",
        "createdAt" : "2019-09-21T19:08:15.980Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Okay",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175484895793221638",
        "createdAt" : "2019-09-21T19:00:13.327Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Server pw",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175484862851162117",
        "createdAt" : "2019-09-21T19:00:05.448Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Landev PW oder User?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175484598958116869",
        "createdAt" : "2019-09-21T18:59:02.534Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Landev",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175484479781183494",
        "createdAt" : "2019-09-21T18:58:34.150Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/p04WvfjgJk",
          "expanded" : "http://Git.0x8.ch",
          "display" : "Git.0x8.ch"
        } ],
        "text" : "https://t.co/p04WvfjgJk",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175484442657341450",
        "createdAt" : "2019-09-21T18:58:26.294Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ip&amp;Login?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175484411896373255",
        "createdAt" : "2019-09-21T18:58:17.936Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja, muss mein headset suchen",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175484311799259140",
        "createdAt" : "2019-09-21T18:57:54.066Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mumble?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175483568283357193",
        "createdAt" : "2019-09-21T18:54:56.796Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Salut marc! Ja ich wollte dich fragen ob du für einen security crashkurs zeit hast, ich fühle mich nicht mehr sicher.",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175483401907912708",
        "createdAt" : "2019-09-21T18:54:17.127Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2461130078",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo, bin erst jetzt aufgestanden, wie kann ich behilflich sein?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1175477716080087047",
        "createdAt" : "2019-09-21T18:31:41.542Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bräuchte ASAP deine hilfe",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175426602924158980",
        "createdAt" : "2019-09-21T15:08:35.218Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey marc, de zschökk wird der sicher scho gschribe haa, ha iz aber mal twitter abgelade dasi di direkt kontaktiere chan. Hettisch du ziit für mich?",
        "mediaUrls" : [ ],
        "senderId" : "2461130078",
        "id" : "1175425019410866181",
        "createdAt" : "2019-09-21T15:02:17.679Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2522555659",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2522555659",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Frau müsstest Du ja (ausser ich bin komplett falsch informiert) den Archetypen Kind integrativ behandeln. Devot halten ihm zu liebe?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "566388833001177088",
        "createdAt" : "2015-02-14T00:09:55.455Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2522555659",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du hältst z.B. den kleinen Raphael Hegglin verbal aber auch nonverbal auf Distanz z.B. am Stammtisch, für mich irgendwie unlogisch weil als",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "566388824855834625",
        "createdAt" : "2015-02-14T00:09:53.517Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2522555659",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "HalliHallo, hätte persönlihce Frage um mein querverwiesenes Weltbild zu vervollständigen,falls zu intime Frage akzeptiere ich auch schweigen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "566387936443514883",
        "createdAt" : "2015-02-14T00:06:21.708Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2613271452",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Cslr17utB2",
          "expanded" : "https://twitter.com/messages/media/1193866104818540548",
          "display" : "pic.twitter.com/Cslr17utB2"
        } ],
        "text" : "Hier mit Screenshot https://t.co/Cslr17utB2",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1193866104818540548/1193866095104540674/85sZEetg.png" ],
        "senderId" : "2613271452",
        "id" : "1193866104818540548",
        "createdAt" : "2019-11-11T12:20:35.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2613271452",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich sehe den tweet nciht, da stehet \"This Tweet is hidden because it’s from someone you don’t follow.\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1193840167519444996",
        "createdAt" : "2019-11-11T10:37:31.108Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/OE5A3I0nkc",
          "expanded" : "https://twitter.com/FailDef/status/1187637284188938241",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "Mir ist dein Tweet vom 25.10. erst jetzt aufgefallen: https://t.co/OE5A3I0nkc\nKurze Frage dazu: Was meintest du damit, dass dir Twitter immer das @BuergKomitee vorschlägt? Ich arbeite beim Sekretariat der @konzern_vi und bin dort zuständig für das Bürgerliche Komitee, und beantworte dir gerne alle Fragen dazu ;)",
        "mediaUrls" : [ ],
        "senderId" : "2613271452",
        "id" : "1193807637286203396",
        "createdAt" : "2019-11-11T08:28:15.381Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2737504151",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2737504151",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, irgendwie ein solo Kind mit einem Päärchen Profil zu favoriten ist für das solo Kind nicht so cool. :(",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "552563492440772608",
        "createdAt" : "2015-01-06T20:32:57.411Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2961920213",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und wie läuft Dein heiliger Krieg gegen den Islam, immer noch am ignorieren dass nach Deiner eigenen Statistik ~90% der Ausländer brave Bürger sind???",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971547551437656070",
        "createdAt" : "2018-03-08T00:46:01.455Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/7Rx0jP27b4",
          "expanded" : "https://twitter.com/DerHorus_/status/948478726874042368",
          "display" : "twitter.com/DerHorus_/stat…"
        }, {
          "url" : "https://t.co/e4RffD0x5H",
          "expanded" : "https://twitter.com/DerHorus_/status/948480573496012800",
          "display" : "twitter.com/DerHorus_/stat…"
        }, {
          "url" : "https://t.co/9VQ5cljXAy",
          "expanded" : "http://www.pewforum.org/2013/04/30/the-worlds-muslims-religion-politics-society-overview/",
          "display" : "pewforum.org/2013/04/30/the…"
        }, {
          "url" : "https://t.co/yhWnldHXFC",
          "expanded" : "http://12.Jetzt",
          "display" : "12.Jetzt"
        }, {
          "url" : "https://t.co/W5ODjw50BK",
          "expanded" : "http://psychiatrie-leaks.ch/",
          "display" : "psychiatrie-leaks.ch"
        } ],
        "text" : "1.Punkte ohne Inhalte. Ablenkungen, Zuschreibungen, Projektionen\n\n-&gt;nö Du siehst einfach die Zusammenhänge nicht\n\n2.Thema war Islam. Eine menschenverachtende faschistische Ideologie, die Sie mögen. Anhänger der Shariah sind für Sie „ganz liebe Menschen“\n\n-&gt;Du bist ja hier der der um die bösen bösen Schizos erweitert, ausserdem schilesst Du wieder von einer Gruppe (der Shariah Anhänger) auf die ganze Etnie)\n\n3.„Homöostase“ als Begriff, wird nicht nur in der Physik sondern auch in der Psychologie, Sozialwissenschaft verwendet. Grundlage aller heutigen Wissenschaften ist die Philosophie.\n\n-&gt;das Ahnenerbe der alten Griechen, ich bezweifle aber dass Du Dich mit Philosophie ausgiebig auseinandergesetzt hast und ich würde Dir jetzt weder den Sophisten noch den Philosophen anerkennen (Sophist im Sinne des Klassikers Platon Protagoras)\n\n4. Im  Zeitraum 2001-2016 gab es 94 rechte Tötungsdelikte &amp; 12600 durch Ausländer, ein nicht unerheblicher Teil durch Muslime.\n\n-&gt;Breivik hat an einem Tag soviele Menschen abgeschlachtet, er hat sich Christ gehäuchelt, aber dennoch jagen wir jetzt nicht alle Christen, nur ihn der ja vor Gericht kam und sinnvollerweise weggesperrt wurde\n\nPrävalenz (also 100000 Einwohner) für Tötungsdelikte\nDeutsche 2,3\nAusländer aus christlichen Herkunftsländer 7,7\nAusländer aus muslimischen Herkunftsländer 18,2\n\n-&gt;Du reduzierst ein Komplexes System auf 3 Zahlen runter, wenn z.B. ein Moslem wegen dem Krieg der Amis viel Gewalt erlebt hat ist logischerweise seine Gewaltbereitschaft grösser, würde man(n) das wirklich verhindern wollen, müsste man da ansetzen, Dein motiv scheint aber wie im 3. Reich einfach zu sein einen Sündenbock zu haben der an allem Schuld ist, de facto geht es aber dem Westen schlechter wegen der Wirtschaft. Ausserdem tust Du wenn Deine Zahlen überhaupt stimmen 81.8% (also der Mehrheit aller Moslems in Deutschland) unrecht, bzw. Du pinkelst einfach allen ans Bein die nicht Christen sind wie bei den Kreuzzügen.\n\n5.Extrem hohe Kriminalitätsprävalenzen (im Besonderen bei der Gewaltaffinität, die nicht sozioökonomisch erklärt werden können) für Muslime wurden in Deutschland immer wieder belegt. So Pfeifers/Baiers letzte Studie 2018: https://t.co/7Rx0jP27b4 (Plagiat von Dir aus einem Bericht, Du verkaufst die Arbeit eines Anderen als Deinige, man kann mit einem Leuchtstift recht gut aus jeder Seite der Bibel eine Terrordrohung basteln) https://t.co/e4RffD0x5H (ebd.)\n\n6.Statistik „richtig interpretieren“. Der Satz: „5% der Deutschen in der Schweiz sind Kriminell“, ist kriminologisch nicht belegbar bzw möglich, denn die Polizeiliche Kriminalstatistik, wie auch die Strafverfolgungsstatistik in Deutschland gleich der Schweizer, erfasst nicht, wie kriminell die Bevölkerung oder einzele Gruppen sind, dh. es gibt keine Analyse, die zeigt soviele Personen haben einen Eintrag im Führungszeugniss. Einzig sind Prävalenzen für ein Jahr zu benennen.\n\n-&gt;eben ich hab das schon gesagt, wenn Du aus einem Kriegsgebiet kommst, allenfalls noch zu unrecht vertreiben wurdest weil z.B. die Amis das Öl Deines Landes will oder sie gerne eine Pipeline durch Dein lang hätten, ist es logisch dass Du \"gewaltaffiner\" bist, ich erachte Dich auch als Gewaltaffin wenn soweit ich das beurteilen kann Du nur verbale Gewalt verübst, Öl ins Feuer giesst und Dich noch als super toller Brandstifter betrachtest.\n\n7.Statistik „fälschen“. Sie haben bzw kennen keine Statistiken, die Ihre „Erfahrungen“ bestätigen. Noch haben Sie die sozialwissenschaftlichen, methodischen Kenntnisse um Statistiken zu erarbeiten, nicht davon gesprochen, dass sie etwas von Kriminologie verstehen. Sonst, nur sonst, hätten Sie etwas dazu beigebracht.\n\n-&gt;ein weiteres mal die Frage: \"Ist das gerecht gegenüber derjenigen 81% die keine Verbrechen verüben?\"\n\n8.Hier eine der relevantesten Studien zur Shariah: https://t.co/9VQ5cljXAy\n\n-&gt; Hast Du die Studie gelesen oder nur die Grafiken angeschaut? Ausserdem hast Du wieder das Hänne-Ei Problem, vor 9/11, also vor dem War on Terror war der Moslem nicht das Feindbild sondern der böse böse Russe, was ich sagen will, Du hast in jedem Land, in jeder Etnie, in jeder Firma Idioten und gute Menschen, deshalb akzeptiere ich es nicht einfach alle in den selben Topf zu werfen.\n\n9.NSU. Wissenschaftlicher Fakt? Sie haben also Zugang, zu den bis ins Jahr 2134 unter Verschluss gehaltenen Akten.\n\n-&gt;ja aber ist ja einiges durchgesickert\n\n10.Ich diskutiere nicht über die USA &amp; ich bin an Ihrer Meinung über selbige nicht interessiert.“Heuchelei“ &amp; „I give a flying fuck about utopian communism.“ Denn ich unterstütze die Positionen der USA &amp; Israels!\n\n-&gt;da Zweifle ich, einerseits bist Du zu faul Dich weltpolitisch zu interessieren, andererseits Redest Du von USA in den besten Tönen, obwohl in den USA 50Mio einwohner von Lebensmittelmarken leben müssen und das ganze Geld in Ressourcen Kriege fliesst damit die Amis ihren Militärisch-Industriellen Komplex mit günstigem Öl betanken kann, obwohl Deutschland auch in solche Kriege verwickelt waren und sich leider nur eine Minderheit gegen den Krieg ausgesprochen hat im Deutschen Parlament, finde ich Deutschland dennoch das angehenehere Land als die USA, mal abgesehen von Leuten wie Dir :)\n\n11.Sind sie schon zum menschenverachtenden faschistischen Islam konvertiert oder einfach nur ein Soy Boy?\n\n-&gt;nö ich bin ein Christ, halte das Gebot \"Liebe Deinen Nächsten wie Dich selbst\" für einen wichtigen Eckpfeiler des Christlichen Glaubens\n\nhttps://t.co/yhWnldHXFC  halte ich Sie für dumm. (Der Spott ist umso größer, als ein Argument sein soll &amp; Sie nicht wissen, wie der IQ berechnet wird: mental age / chronological age * 100. Wenn Sie mit 16 einen IQ von 130 hatten dürften er heute 20 Jahre später bei 110 liegen.\n\n-&gt;wichtig wäre es Bildung nicht nur einseitig von der Hetzerseite aufzusaugen... aber das verhindert ja die Homöostase oder Filterblase wie man auch sagen könnte\n\n13.„Fehlbare Psychiatrie Fehlbares KESB und Familienrichter (…) selber von Schizophrenie betroffen“ https://t.co/W5ODjw50BK  gehen sie wieder in Therapie…\n\n-&gt;Daran erkenne ich, dass Du gar nicht an einer Vernünftigen Diskussion beteiligt bist, wie bereits erwähnt sind nicht alle Schizophrenen Massenmörder und Amokläufer. Ich hab das auch auf meiner Webseite weil es meiner Meinung nach etwas ist worüber diskutiert werden sollte, denn würde man rechtzeitig intervenieren (Breivik war ein Heimkind) könnte man wenn man die Zeichen nur verstehen würde rechtzeitig reagieren (präventiv) und könnte grösseres unheil verhindern, wenn aber Leute wie Du immer nur die Wirkung bekämpfen und nie die Ursache, dann ist es logisch dass sich die Situation nicht verbessert\n\nDein Punkt 13 sagt mir, dass Du jetzt weil Du eben nicht so tolle Argumente hast jetzt einen Makel suchst und mir den Vorwirfst, ohne auf die angesprochenen Punkte einzugenen, vielleicht bist Du noch ganz jung (deshalb habe ich ja gefragt) und ich tue Dir unrecht weil man in Jungen Jahren die Zusammenhänge noch nicht so gut sieht... also entschuldige meine Offenheit",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963362142320721927",
        "createdAt" : "2018-02-13T10:40:07.946Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/J3HdFMl5dX",
          "expanded" : "http://4.Im",
          "display" : "4.Im"
        }, {
          "url" : "https://t.co/i4PlP5EJxq",
          "expanded" : "https://twitter.com/DerHorus_/status/948478726874042368",
          "display" : "twitter.com/DerHorus_/stat…"
        }, {
          "url" : "https://t.co/UtxKGfLZ8w",
          "expanded" : "https://twitter.com/DerHorus_/status/948480573496012800",
          "display" : "twitter.com/DerHorus_/stat…"
        }, {
          "url" : "https://t.co/CFjixnuQYg",
          "expanded" : "http://www.pewforum.org/2013/04/30/the-worlds-muslims-religion-politics-society-overview/",
          "display" : "pewforum.org/2013/04/30/the…"
        }, {
          "url" : "https://t.co/oO0455FmQ4",
          "expanded" : "http://12.Jetzt",
          "display" : "12.Jetzt"
        }, {
          "url" : "https://t.co/l0DGDXEa0f",
          "expanded" : "http://psychiatrie-leaks.ch/",
          "display" : "psychiatrie-leaks.ch"
        } ],
        "text" : "1.Punkte ohne Inhalte. Ablenkungen, Zuschreibungen, Projektionen\n2.Thema war Islam. Eine menschenverachtende faschistische Ideologie, die Sie mögen. Anhänger der Shariah sind für Sie „ganz liebe Menschen“\n3.„Homöostase“ als Begriff, wird nicht nur in der Physik sondern auch in der Psychologie, Sozialwissenschaft verwendet. Grundlage aller heutigen Wissenschaften ist die Philosophie.\nhttps://t.co/J3HdFMl5dX Zeitraum 2001-2016 gab es 94 rechte Tötungsdelikte &amp; 12600 durch Ausländer, ein nicht unerheblicher Teil durch Muslime.\n\nPrävalenz (also 100000 Einwohner) für Tötungsdelikte\nDeutsche 2,3\nAusländer aus christlichen Herkunftsländer 7,7\nAusländer aus muslimischen Herkunftsländer 18,2\n\n5.Extrem hohe Kriminalitätsprävalenzen (im Besonderen bei der Gewaltaffinität, die nicht sozioökonomisch erklärt werden können) für Muslime wurden in Deutschland immer wieder belegt. So Pfeifers/Baiers letzte Studie 2018: https://t.co/i4PlP5EJxq \nhttps://t.co/UtxKGfLZ8w\n\n6.Statistik „richtig interpretieren“. Der Satz: „5% der Deutschen in der Schweiz sind Kriminell“, ist kriminologisch nicht belegbar bzw möglich, denn die Polizeiliche Kriminalstatistik, wie auch die Strafverfolgungsstatistik in Deutschland gleich der Schweizer, erfasst nicht, wie kriminell die Bevölkerung oder einzele Gruppen sind, dh. es gibt keine Analyse, die zeigt soviele Personen haben einen Eintrag im Führungszeugniss. Einzig sind Prävalenzen für ein Jahr zu benennen.\n\n7.Statistik „fälschen“. Sie haben bzw kennen keine Statistiken, die Ihre „Erfahrungen“ bestätigen. Noch haben Sie die sozialwissenschaftlichen, methodischen Kenntnisse um Statistiken zu erarbeiten, nicht davon gesprochen, dass sie etwas von Kriminologie verstehen. Sonst, nur sonst, hätten Sie etwas dazu beigebracht.\n\n8.Hier eine der relevantesten Studien zur Shariah: https://t.co/CFjixnuQYg\n\n9.NSU. Wissenschaftlicher Fakt? Sie haben also Zugang, zu den bis ins Jahr 2134 unter Verschluss gehaltenen Akten.\n10.Ich diskutiere nicht über die USA &amp; ich bin an Ihrer Meinung über selbige nicht interessiert.“Heuchelei“ &amp; „I give a flying fuck about utopian communism.“ Denn ich unterstütze die Positionen der USA &amp; Israels!\n11.Sind sie schon zum menschenverachtenden faschistischen Islam konvertiert oder einfach nur ein Soy Boy?\nhttps://t.co/oO0455FmQ4 halte ich Sie für dumm. (Der Spott ist umso größer, als ein Argument sein soll &amp; Sie nicht wissen, wie der IQ berechnet wird: mental age / chronological age * 100. Wenn Sie mit 16 einen IQ von 130 hatten dürften er heute 20 Jahre später bei 110 liegen.\n13.„Fehlbare Psychiatrie Fehlbares KESB und Familienrichter (…) selber von Schizophrenie betroffen“ https://t.co/l0DGDXEa0f gehen sie wieder in Therapie…",
        "mediaUrls" : [ ],
        "senderId" : "2961920213",
        "id" : "963346804988407817",
        "createdAt" : "2018-02-13T09:39:11.172Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also schon wieder Punkt für mich, logischerweise wollte ich dass Du das nachliest also hab ich behauptet Du würdest es nicht nachlesen, verstanden hast Du es aber nicth wie ich auch vorausgesagt habe. Homöostase ist keine Krankheit. Aus Wikipedia \"bezeichnet die Aufrechterhaltung eines Gleichgewichtszustandes eines offenen dynamischen Systems\". Freud hat auch relativ wenig damit zu tun.\n\nAls Deutscher sind da die NSU Vorfälle, was haben da die Moslems die ermordet wurden damit zu tun? Und das ist jetzt nicht irgend eine Verschwörungstheorie sondern ein wissenschaftlicher Fakt, ein Fakt der aber \"Feelings\" heraufbeschwört.\n\nWas ich aus erster Hand in der Schweiz erlebt habe, dass vermutlich über 90% der Muslime ganz liebe Menschen sind, dass sie sogar (das habe ich jetzt erlebt) moralisch mehr auf dem Kasten haben als wir bestesten Schweizer die es gibt.\n\nUSA sollte Dich interessieren, denn USA unterstützen oft extreme Muslime weil sie so die lokale Regierung stürzen können und so günstig an die Ölfelder kommen was immer mit Flüchtlingsströmen einher geht, was Du ja vermutlich nicht willst. Also Deine Argumentation beginnt nicht am Anfang des Problems, Du willst die Wirkung bekämpfen verschliesst aber die Augen vor der Ursache.\n\nIch hätte noch eine kleine Frage: Wie alt bist Du, denn als ich jünger war habe ich auch Geopolitisch relevantes ignoriert weil ich einfach zu faul war die bequemlich Massenmedien zu hinterfragen.\n\nSie können erahnen, dass ich Sie für nicht sonderlich intelligent halte. :)\nIn dieseh Themen ist Bildung wichtiger als Intelligenz, aber den einzigen offiziellen Test hab ich micht 16 gemacht und da hatte ich 130 IQ.\n\nStatistik richtig interpretieren und etweder sagen \"5% der Deutschen in der Schweiz sind Kriminell\" oder \"95% der Deutschen sind brave Bürger\" macht einen riesen Unterschied, \"trau keiner statistik die Du nicht selbst gefälscht hast\" also die genauen Zahlen kenn ich nicht aber ich wollte auch nur den effekt demonstrieren... im ersten Fall tust Du den 95% Deutschen unrecht...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963319266039730181",
        "createdAt" : "2018-02-13T07:49:45.395Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "qed.\n\nHomöostase aka Solipsismus, oder andere Persönlichkeitsstörungen. Das anführen der Selbigen ist bereits als Projektion im freudischen Sinne zu sehen. Ein super-LOL sei Ihnen geschenkt.\n\nBelege\nErstens, \"Dein vorheriger Post finde ich nicht so super toll\". Welcher Post? Ich habe in den letzten 24 Stunden 8 Posts, 4 davon mit Islambezug veröffentlicht. and facts dont care about your feelings.\n\nZweitens, Talking Point abdreschen, ohne auf den Gegenüber einzugehen: Die USA interessiert mich nicht. Muslime töten Muslime, was zum Teufel habe ich oder die Deutschen damit zu tun?! Nichts. \n\nSchlußendlich. Sie haben es zwei Mal versäumt, auf meine Kritik am Islam &amp; dessen menschenverachtenden Prämissen einzugehen oder sich zu positionieren. Ich kann daher davon ausgehen, das Sie persönlich ähnliche Positionen halten &amp; gut finden: Tod für Ehebruch, Apostasie, Homosexualität, Frauen als Untermenschen zu behandeln.\n\nSie können erahnen, dass ich Sie für nicht sonderlich intelligent halte. :)",
        "mediaUrls" : [ ],
        "senderId" : "2961920213",
        "id" : "963101468202094598",
        "createdAt" : "2018-02-12T17:24:18.304Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/t2V4nLSTEO",
          "expanded" : "https://twitter.com/messages/media/963089980976455685",
          "display" : "pic.twitter.com/t2V4nLSTEO"
        } ],
        "text" : " https://t.co/t2V4nLSTEO",
        "mediaUrls" : [ "https://video.twimg.com/dm_gif/963089969777664003/QiSg5sQ2ilOU2zEfioz90qw2HGxkaEAlRVs88EGbgIIZDoATUh.mp4" ],
        "senderId" : "2961920213",
        "id" : "963089980976455685",
        "createdAt" : "2018-02-12T16:38:39.851Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, wie ich vorausgesagt habe, Homöostase...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963089783567380484",
        "createdAt" : "2018-02-12T16:37:52.470Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/0Zgmw0Z1QU",
          "expanded" : "https://twitter.com/i/moments/819490480102735872",
          "display" : "twitter.com/i/moments/8194…"
        } ],
        "text" : "1. Integration ist eine Bringschuld. Ich werde niemanden auf Augenhöhe begegnen, der gewaltaffin, homophob, sexistisch, faschistisch ist, niemanden der sich nicht unseren christlich-jüdischen Werten, politischen &amp; gesellschaftlichen System unterordnet und versucht diese Gesellschaft in irgendeiner weise zu usurpieren.\n\n2. Ich lasse das Thema mal ziehen, Sie werden ausschließlich mit Heuchelei antworten können. I give a flying fuck about utopian communism.\n\n3. Diskussionen über Genetik sind irrelevant. Sozialisation - als Soziologe wird irgendwann anstrengend Definitionen zu wiederholen.\n\nBitte, vergleichen sie die Kriminalitätsraten von Ausländern in Deutschland aus 53 afrikanischen (rund 500k Personen) &amp; 17 südostasiatischen Ländern (rund 400k Personen), was fällt bei den Prävalenzen auf?\n\nHier ist ein guter Überblick:\nhttps://t.co/0Zgmw0Z1QU\n\nHomöostase. LOL. Ich halte, die meisten Psychologen &amp; Personen die diesem Feld nachhängen für Idioten - Ihnen fehlt fast immer ein fundiertes philosophisches Grundwissen.\n\nSchön das Sie Wörter Zählen können, sie zu verstehen ist eine andere Sache. Aus einem fachlichen Hintergrund heraus beschäftige ich mich hier auf Twitter mit Kriminalität. WOW.",
        "mediaUrls" : [ ],
        "senderId" : "2961920213",
        "id" : "963089206825365508",
        "createdAt" : "2018-02-12T16:35:34.997Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1. nö es ist definitiv anders ob man sich integriert wenn einem freundlich bzw. augenhöhe begegnt wird, oder ob man sich integriert wenn man permanent getreten wird (im übertragenen Sinne), im Normalfall spüre ich als Mensch ob mich jemand nur als Fussabtreter behandelt (Unterbewusstsesin-&gt;Groll-&gt;Hass-&gt;Rebellion)\n\n2. Ich bin Schweizer, Christ und eine möchtegern Intelektülle. Was die USA macht spielt in Europa eine wichtige Rolle, z.B. ist Deutschland in der NATO was früher mal ein Verteidigungsbündinis war aber heute eher ein Angriffsbündnis. Die westlichen Medien tendieren fast alle dazu einseitig und ohne Verifizierung die Aussagen der jeweiligen Landesführer abzudrucken, ohne z.B. das Völkerrecht zu berücksichtigen. Sich nicht dafür zu interessieren nennt man Ignoranz. (Junge Menschen ausgenommen, die dürfen sich noch für Dinge wie Plüschtiere und Computerspiele interessieren)\n\n3. Intelligenz hat zwar eine genetische Komponente, ob aber ein Mensch Verbrecher wird oder aber angesehener Wissenschaftler hängt vorallen von der Primären und Sekundären Sozialisation, wenn jetzt die Kolonialmächte über Jahrhunderte Afrika unter der Apartheit ausgebeutet als Vorgabe, was denkst Du ist da der Output dieser Gesellschaft?\n\nIch kenne mich in Psychologie genug gut aus, dass ich weiss dass ich Dich nicht umstimmen kann, der Fachbegriff dazu wäre Homöostase (den Du nicht nachlesen wirst und wenn doch nicht verstehen wirst :) ). Mir ist klar, dass Du mit dem Argument \"die Ausländer\" (oder die Juden) sind an allem Schuld als Basiselement in Deiner Argumentation verwendest und viele Deiner Datenpunkte auf diesem aufbaut und somit Dein ganzes Argumentationsssytem kollabieren würde wenn Du ein so tiefliegendes Element ändern würdest, bzw. Deine Identität.\n\nVersuch aber dennoch mal: Zähl mal Deine häufigsten wörter und versuch mal Deine Argumentation aus einer nüchternen Distanz zu betrachten:\n\n    115 schutzsuchende\n    117 an\n    117 mal\n    118 thread\n    119 alle\n    119 show\n    120 wird\n    122 asylbewerber\n    124 diese\n    125 zuwanderer\n    129 wir\n    131 lol\n    132 assault\n    132 sexual\n    134 noch\n    135 bei\n    136 the\n    138 hat\n    139 man\n    139 zur\n    142 oder\n    143 of\n    144 was\n    151 flüchtlingsunterkünfte\n    153 sich\n    154 feb\n    155 aber\n    156 nationalität\n    158 rape\n    160 tatverdächtige\n    164 jun\n    165 selbstbestimmung\n    167 bka\n    167 deutsche\n    171 körperverletzung\n    172 refugees\n    176 keine\n    177 ausländerkriminalität\n    177 ja\n    180 sexuelle\n    180 this\n    180 zuwanderung\n    182 werden\n    192 aus\n    202 islam\n    203 vergewaltigung\n    212 nur\n    220 wie\n    221 studie\n    222 d\n    225 aug\n    225 hier\n    228 des\n    230 jan\n    231 als\n    242 auch\n    243 im\n    256 nov\n    281 eine\n    283 jul\n    284 haben\n    285 oct\n    310 den\n    312 und\n    316 sep\n    317 ein\n    323 vergleich\n    325 flüchtlinge\n    332 mit\n    340 für\n    350 gegen\n    359 nach\n    361 letkiser\n    369 es\n    372 auf\n    385 dec\n    385 may\n    387 zu\n    396 ich\n    418 convictions\n    418 verbrechen\n    434 foreigners\n    434 kriminalitätsstatistik\n    447 suspects\n    454 apr\n    463 others\n    466 sind\n    494 das\n    502 polizei\n    509 and\n    538 verurteilungen\n    546 nicht\n    627 added\n    641 ist\n    667 statistik\n    669 von\n    695 germany\n    711 retweeted\n    747 sie\n    834 ausländer\n    836 pks\n    999 kriminalität\n   1016 straftaten\n   1017 deutschland",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963082577744711684",
        "createdAt" : "2018-02-12T16:09:14.497Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1. Nur Menschen können sich integrieren. Integration bedeutet sich nach den westlichen christlich-jüdischen Werten &amp; deren der Gesellschaft anzupassen. Der Islam ist ein religöses \"Konzept\". Oder wird der Koran, werden die Hadiths nun umgeschrieben, damit zb. Frauen gleiche Rechte haben, man Apostat oder Homosexuell sein kann ohne mit dem Tode bedroht bzw verurteilt zu werden. In einer Demokratie sind das Grundrechte. Religion ist Privatsache. \n\n2. Ich bin Deutscher. Was die USA macht, macht die USA. BTW. Fast ausschließlich töten Muslime andere Muslime - selbst schuld. Oder wer war bzw. ist schuld an den royalen Familien in middle east, Hussein oder Khomeini... LOL\n\n3. Geblubber. Das sich die ISIS &amp; andere von der USA bezahlen lassen, zeigt doch sehr klar wie \"intelligent\" etc. Muslime sind. Es gibt schöne Korrelationen von Religiosität &amp; dem IQ...",
        "mediaUrls" : [ ],
        "senderId" : "2961920213",
        "id" : "963074950121828357",
        "createdAt" : "2018-02-12T15:38:55.926Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Betrachte mal Weltpolitisches geschehen, wenn USA die ISIS finanziert dann Reden sie von \"Freiheitskämpfern\" und wenn sie eine Regierung stürzen wollen von \"Terroristen\", es ist so einfach zu durchschauen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963071054527844356",
        "createdAt" : "2018-02-12T15:23:27.154Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "2961920213",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Dein vorheriger Post finde ich nicht so super toll, genau wegen solchen Posts wie Deinem fällt es jungen Muslimen schwer sich zu integrieren, ausserdem Weltpolitisch betrachtet ist USA die Muslime überall gegeneinander und gegen adere Etnien ausspielen weil die Islamischen Gebiete die Ölrichen gebiete sind, also Du dienst mit dem hirnlosen rumgebelle dem US-Imperium bzw. einer kleinen Ignoraten superreichen Oberschicht.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963069195285131268",
        "createdAt" : "2018-02-12T15:16:03.842Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-2967713873",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey",
        "mediaUrls" : [ ],
        "senderId" : "2967713873",
        "id" : "1172619195579817990",
        "createdAt" : "2019-09-13T21:12:57.178Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-3076965861",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey",
        "mediaUrls" : [ ],
        "senderId" : "3076965861",
        "id" : "1166073203850174468",
        "createdAt" : "2019-08-26T19:41:31.099Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-3101885823",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xKQHBbLXvh",
          "expanded" : "http://truetwit.com/vy500125111",
          "display" : "truetwit.com/vy500125111"
        } ],
        "text" : "SolUsagersPsy uses TrueTwit validation. To validate click here: https://t.co/xKQHBbLXvh",
        "mediaUrls" : [ ],
        "senderId" : "3101885823",
        "id" : "950696320209211397",
        "createdAt" : "2018-01-09T11:50:40.557Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-3439783577",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "http://t.co/8UgkMaAvdj",
          "expanded" : "http://www.weeklypique.com",
          "display" : "weeklypique.com"
        } ],
        "text" : "Hi! In this Friday's newsletter, we're tackling the deep web. It's going to be a good one - sign up at http://t.co/8UgkMaAvdj! -via @crowdfire",
        "mediaUrls" : [ ],
        "senderId" : "3439783577",
        "id" : "653666164141375491",
        "createdAt" : "2015-10-12T20:18:52.759Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "19963480-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "19963480",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, schade...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1086962735999582218",
        "createdAt" : "2019-01-20T12:24:45.754Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi - nein, ich plane nichts dergleichen. grüsse!",
        "mediaUrls" : [ ],
        "senderId" : "19963480",
        "id" : "1086949568959397894",
        "createdAt" : "2019-01-20T11:32:26.481Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "19963480",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey, wie gehts so? Ist es absehbar, ob es irgendwann wieder mal eine \"full lenght\" Demodays gibt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1086898854103007236",
        "createdAt" : "2019-01-20T08:10:55.145Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-4415393897",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "problem is probably that not every Schizophrenia Patient has his own MRI and secondary they often like the voices, at least if the voices are not violent i enjoyed not to be alone with my voices, so the patients would probalby not want the voices muted",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "963313566785949700",
        "createdAt" : "2018-02-13T07:27:06.541Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ubO9PDaHcE",
          "expanded" : "http://www.bbc.com/news/health-43003378",
          "display" : "bbc.com/news/health-43…"
        } ],
        "text" : "BBC News - Schizophrenia patients calmed by video game @deffail https://t.co/ubO9PDaHcE",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "963312285111504900",
        "createdAt" : "2018-02-13T07:22:00.990Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja eh, wann und wo?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "868424258766213123",
        "createdAt" : "2017-05-27T11:10:52.535Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Was machst du so den ganzen Tag. Wollen wir uns nächste Woche jetzt mal auf ein Kaffee treffen.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "867073567946674180",
        "createdAt" : "2017-05-23T17:43:42.752Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und in drei Tagen ist es auch wieder vorbei mit den Schmerzen.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "866716120790835210",
        "createdAt" : "2017-05-22T18:03:20.808Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schön, dass freut mich zu hören, wenn du nicht klagen kannst.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "866716050196398085",
        "createdAt" : "2017-05-22T18:03:03.992Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "aber sonst kann ich nicht klagen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "866712655771426820",
        "createdAt" : "2017-05-22T17:49:34.587Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "heute meine spritze xeplion erhalten und jetzt schmerzt mein arm für 3 tage...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "866712627950563331",
        "createdAt" : "2017-05-22T17:49:27.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Und wie geht's so?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "866712273603235848",
        "createdAt" : "2017-05-22T17:48:03.458Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Cool. Da wird er sich freuen. \nAblenkung und Beschäftigung ist das beste. \nIn meinen schlimmsten Zeiten hilft mir immer Meditation. Jon Kabat Zinn.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "863041016789643267",
        "createdAt" : "2017-05-12T14:39:47.639Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn ich beschäftigt bin hab ich weniger sorgen mit meiner krankheit...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "863040169095696387",
        "createdAt" : "2017-05-12T14:36:25.537Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "geburtstagsgeschenk für meinen Bruder gemacht...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "863040092285399045",
        "createdAt" : "2017-05-12T14:36:07.243Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bei dir alles klar?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "863015234252013572",
        "createdAt" : "2017-05-12T12:57:20.675Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nein. hartnäckig hartnäckig...",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "863015151234109443",
        "createdAt" : "2017-05-12T12:57:00.887Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und mittlerweile gegen die bronchitis gewonnen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "863004470116048899",
        "createdAt" : "2017-05-12T12:14:34.223Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich bin derzeit mit einer bronchitis ans Bett gefesselt. Der Grund warum ich nichts von mir hören liess.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "862762372020174856",
        "createdAt" : "2017-05-11T20:12:33.648Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi Marc, wie geht's?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "862762188389453827",
        "createdAt" : "2017-05-11T20:11:49.766Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja. Wohnungen sind halt oftmals Glücks- und Beziehungssache!",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "860229534129508357",
        "createdAt" : "2017-05-04T20:27:58.012Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schlecht, kommen selten antworten oder erst wochen später ein standardbrief mit einer absage",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "860218686933786627",
        "createdAt" : "2017-05-04T19:44:51.763Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie läuft es mit der Wohnungssuche?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "860207641783566342",
        "createdAt" : "2017-05-04T19:00:58.395Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Tolles Ding. Aber nicht ganz günstig.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "860207569834577923",
        "createdAt" : "2017-05-04T19:00:41.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "withings wage, die ist echt nützlich...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859869251145957381",
        "createdAt" : "2017-05-03T20:36:19.947Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Cooler Chart. Und gute Resultate. Gratuliere!",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859869143004270595",
        "createdAt" : "2017-05-03T20:35:54.097Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/uRjnS1c2Rs",
          "expanded" : "https://twitter.com/messages/media/859868905506054148",
          "display" : "pic.twitter.com/uRjnS1c2Rs"
        } ],
        "text" : " https://t.co/uRjnS1c2Rs",
        "mediaUrls" : [ "https://ton.twitter.com/dm/859868905506054148/859868878180151296/OQ51jxcA.jpg" ],
        "senderId" : "75128838",
        "id" : "859868905506054148",
        "createdAt" : "2017-05-03T20:34:57.677Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "das damals auch, das mit dem gewicht kriege ich meist nach einem klinkikaufenthalt wieder hin",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859868660940275715",
        "createdAt" : "2017-05-03T20:33:59.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sonst hilft auch: joggen. Ja. Viel joggen. Haben mir schon mehrere Betroffene bestätigt.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859868348921917443",
        "createdAt" : "2017-05-03T20:32:44.774Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Gewichtszunahme?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859868163411980292",
        "createdAt" : "2017-05-03T20:32:00.622Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "damals überhaupt nicht mit zyprexa, risperdal consta ging in der niedrigsten dosis und xeplion in der höchsten dosis geht auch...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859867993735663621",
        "createdAt" : "2017-05-03T20:31:20.133Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sterben kannst du auch ab Honig oder Olivenöl. Oder Frauen durch die Pille. Im Verhältnis zu den Millionen Zyprexa- Verordnungen gesehen, ist das ein soooo minimales Risiko. \nVerträgst du die NW nicht gut?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859867639899934724",
        "createdAt" : "2017-05-03T20:29:55.751Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "vermutlich einfacher mit medikamenten, aber betrachtet man den gerichtsfall mit der firma lilly das das neuroleptikum zyprexa produziert hat und wusste dass das patienten tötet wäre mir eine homöopatische lösung lieber... oder buddhistische psychiatrie oder irgend so",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859858497541984263",
        "createdAt" : "2017-05-03T19:53:35.929Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich finde es übrigens super, wieder mit dir zu schreiben. Lange Zeit hast du alles ignoriert. F20.0-29 ist obermühsam - ich weiss - aber jeder Betroffene kanns in den Griff bekommen.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859852914034171908",
        "createdAt" : "2017-05-03T19:31:24.798Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wie sieht es bei dir nächste Woche aus? Diese Woche klappt kaum, weil ich total erkältet bin.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859851950631903236",
        "createdAt" : "2017-05-03T19:27:35.034Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859369158185713667",
        "createdAt" : "2017-05-02T11:29:08.329Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc, 18.00 Uhr ist etwas gar spät für mich. Ich müsste um 18.30 Uhr weiter. \nMachen wir doch ein anderes Mal. Diese oder nächste Woche. Am liebsten am Nachmittag. Ok? Sorry",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "859357379082817540",
        "createdAt" : "2017-05-02T10:42:19.986Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja, wäre dann aber grad noch in aarau, wir könnten ins pickwick gehen ?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859320341675601924",
        "createdAt" : "2017-05-02T08:15:09.550Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Also kannst du erst so gegen 18.00. Verstehe ich das so richtig?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "858996610306539523",
        "createdAt" : "2017-05-01T10:48:45.979Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bin immer noch mit dem velo unterwegs, dienstag hab ich noch termin bis ca. 18:00",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "858263727388405763",
        "createdAt" : "2017-04-29T10:16:33.214Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Fährst du immer noch so viel Velo?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "858044915032477699",
        "createdAt" : "2017-04-28T19:47:04.208Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Prima. Zeit und Ort können wir ja noch abmachen. Ok?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "858044681665564675",
        "createdAt" : "2017-04-28T19:46:08.518Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "857937115405049859",
        "createdAt" : "2017-04-28T12:38:42.826Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sagen wir nächste Woche. So Dienstag würde mir passen. In Aarau. Ginge dir das?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "857937003647782915",
        "createdAt" : "2017-04-28T12:38:16.059Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja warum nicht, wann und wo?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "857925817606844419",
        "createdAt" : "2017-04-28T11:53:49.121Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hab kein aktuelles Bild zur Hand. Wir könnten uns aber auch einmal zu einem Kaffee oder so treffen. Ich lade dich ein. Was meinst du dazu?",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "857920563498688516",
        "createdAt" : "2017-04-28T11:32:56.444Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok, hat's irgendwo ein foto von dir, dass ich dein gesicht zuordnen kann, ich hab ein grottenschlechtes namensgedächtniss",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "856953226641498115",
        "createdAt" : "2017-04-25T19:29:05.385Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wir haben uns mal im Interdiscount Aarau unterhalten.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856926855714267139",
        "createdAt" : "2017-04-25T17:44:18.206Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich kenne Dr. Pfisterer nicht. Du kannst beruhigt sein. Ich schreibe in guten Absichten.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856926395599028229",
        "createdAt" : "2017-04-25T17:42:28.330Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mal raten, das Facebook Profil: mike.maryn.1, wirkt geprägt von Dr. Pfisterer oder wahrhaft wie ein Account von ihm, wenns stimmt würde das einen akutpatienten mit Paranoiden Schizophrenen doch bestärken in seinem Verfolgungswahn?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "856861765925298179",
        "createdAt" : "2017-04-25T13:25:39.464Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ja. Das macht nichts. Sorry für die Störung. Ich wünsche dir alles Gute. Hoffe du bist wieder der alte Kämpfer wie früher.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856582852363923460",
        "createdAt" : "2017-04-24T18:57:21.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich weiss immer noch nicht wer Du bist...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "856557780295180293",
        "createdAt" : "2017-04-24T17:17:43.640Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es würde mich interessieren, wie es dir geht. Warst immer ein guter Typ.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856242409453363203",
        "createdAt" : "2017-04-23T20:24:33.517Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Irgendwann hast du dann den Kontakt gemieden und mich beschimpft.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856242187616571396",
        "createdAt" : "2017-04-23T20:23:40.454Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Von Facebook. Vor rund zwei Jahren. Bis vor einem Jahr hatten wir öfters Kontakt.",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856241916123508739",
        "createdAt" : "2017-04-23T20:22:35.727Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "4415393897",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "woher kennen wir uns?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "856239907332861955",
        "createdAt" : "2017-04-23T20:14:36.784Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc\nWie geht es Dir?\nWir hatten vor einem Jahr ab und an Kontakt.\nDu hast ihn dann aber abgebrochen. Der Grund war deine Krankheit. \nLiebe Grüsse MM",
        "mediaUrls" : [ ],
        "senderId" : "4415393897",
        "id" : "856235089663021059",
        "createdAt" : "2017-04-23T19:55:28.260Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-4688378234",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "4688378234",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hätte gesagt auch die medikamente tragen ihren teil dazu bei...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "995704726862942212",
        "createdAt" : "2018-05-13T16:38:00.884Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-713028212612341761",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hay",
        "mediaUrls" : [ ],
        "senderId" : "713028212612341761",
        "id" : "1195678179622604804",
        "createdAt" : "2019-11-16T12:21:07.382Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-731144505504731137",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "731144505504731137",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/sVvUEAbq6k",
          "expanded" : "https://twitter.com/messages/media/731276747270959107",
          "display" : "pic.twitter.com/sVvUEAbq6k"
        } ],
        "text" : "Ähm, ich weiss wo Dein Haus wohnt... https://t.co/sVvUEAbq6k",
        "mediaUrls" : [ "https://ton.twitter.com/dm/731276747270959107/731276747602337792/krq-0RRN.jpg" ],
        "senderId" : "75128838",
        "id" : "731276747270959107",
        "createdAt" : "2016-05-14T00:15:38.244Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-767525162011033600",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/b97rLX2mwV",
          "expanded" : "https://twitter.com/grergtgrt/status/1325943315313827842",
          "display" : "twitter.com/grergtgrt/stat…"
        } ],
        "text" : "Vem har besökt din profil? https://t.co/b97rLX2mwV",
        "mediaUrls" : [ ],
        "senderId" : "767525162011033600",
        "id" : "1326096405715324938",
        "createdAt" : "2020-11-10T09:36:35.546Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-769513474737266688",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi!!",
        "mediaUrls" : [ ],
        "senderId" : "769513474737266688",
        "id" : "1164800785474973701",
        "createdAt" : "2019-08-23T07:25:22.928Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-810957620567744512",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Marc [LAN|WAN]dolt jun hi! Wie geht es Ihnen?",
        "mediaUrls" : [ ],
        "senderId" : "810957620567744512",
        "id" : "1186638639847165958",
        "createdAt" : "2019-10-22T13:41:13.152Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-841947607559745537",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi.",
        "mediaUrls" : [ ],
        "senderId" : "841947607559745537",
        "id" : "1181965802288881671",
        "createdAt" : "2019-10-09T16:13:01.937Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-846676995555643396",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "halli hallo, schon lange nichts mehr von Dir gehört, wie geht es so?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1310379072963346438",
        "createdAt" : "2020-09-28T00:41:31.343Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "weshalb wurde dann die Wohnung gekündigt, wie bei mir, stress mit der Verwaltung?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "888513783127298053",
        "createdAt" : "2017-07-21T21:39:28.411Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Jetzt noch weniger als je... es ist ihnen gelungen, mich aus der Gemeinde zu vertreiben, ab 1. August bin ih in einem Hotel in Teufenthal untergebracht...",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "888505874788085763",
        "createdAt" : "2017-07-21T21:08:02.988Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "willst du nicht noch den account umbenennen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "859874446424801284",
        "createdAt" : "2017-05-03T20:56:58.431Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bei mir schon, und nach den guten [nicht allein sein] war ich verhaltenssüchtig und den bösen habe ich versucht parole zu bieten. verhaltenssucht ist wie eine stoffsucht, da ist eine anschliessende ddepression zu erwarten ohne ersatz\"stoff\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "854727773805576196",
        "createdAt" : "2017-04-19T16:05:56.156Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es waren also mal gute, mal böse Stimmen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "854708305285009411",
        "createdAt" : "2017-04-19T14:48:34.438Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "es hatte immer beide sorten von stimmen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "853662437115129860",
        "createdAt" : "2017-04-16T17:32:40.024Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oh, interessant, deine Stimmen machen dir keine Angst... meine Stimmen beleidigen mich rassistisch und ich habe panische Angst vor ihnen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853657717940133891",
        "createdAt" : "2017-04-16T17:13:54.971Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mit meinen virtuellen Stimmen bin ich weniger alleine, das hat bei mir eine Verhaltenssucht verursacht und war mit ein Grund weshalb ich meine Medikamente (neben den Nebenwirkungen) immer wieder abgesetzt habe.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "853653240860344325",
        "createdAt" : "2017-04-16T16:56:07.557Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Verhaltenssucht?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853638393439428611",
        "createdAt" : "2017-04-16T15:57:07.620Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "muskelverkrampfungen leichte, depressionen und ich habe auch in den letzten 17 Jahren eine Verhaltenssucht gegenüber den nicht vorhandenen Stimmen entwickelt.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "853610686068424707",
        "createdAt" : "2017-04-16T14:07:01.627Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Welche Nebenwirkungen?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853573274168676355",
        "createdAt" : "2017-04-16T11:38:22.120Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "frag mal ob nicht allenfalls wie bei mir Xeplion Depot spritze besser wäre, Seroquel hatte für mich zu viele nebenwirkungen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "853534213118844931",
        "createdAt" : "2017-04-16T09:03:09.037Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Fühle mich auch friedlicher",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853227434757545987",
        "createdAt" : "2017-04-15T12:44:07.345Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich nehme seroquel",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853226869667360773",
        "createdAt" : "2017-04-15T12:41:52.667Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "geht, xeplion scheint zu helfen bei mir, hab sogar wieder frieden geschlossen mit dem Hauswart",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "853222301441118211",
        "createdAt" : "2017-04-15T12:23:43.556Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es geht., selber?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "853218328571260932",
        "createdAt" : "2017-04-15T12:07:56.271Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie gehts so?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "852868853306667011",
        "createdAt" : "2017-04-14T12:59:14.898Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich nicht mehr",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851485028693544964",
        "createdAt" : "2017-04-10T17:20:25.539Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Die Stimmen höre ichnicj",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851484848061657091",
        "createdAt" : "2017-04-10T17:19:42.325Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Geholfen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851484371882307587",
        "createdAt" : "2017-04-10T17:17:48.772Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du hast sehr fehokfen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851484312784572419",
        "createdAt" : "2017-04-10T17:17:34.707Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "da würden allenfalls medis helfen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851483393464430596",
        "createdAt" : "2017-04-10T17:13:55.508Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nein die Stimmen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851298958068547587",
        "createdAt" : "2017-04-10T05:01:02.671Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Nachtlicht?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851126610505936899",
        "createdAt" : "2017-04-09T17:36:11.838Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo habe Angst vor der Nacht",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851113298661695493",
        "createdAt" : "2017-04-09T16:43:18.037Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/cMutk2jbLh",
          "expanded" : "https://www.youtube.com/watch?v=wHmxCeLpveA#t=9m39s",
          "display" : "youtube.com/watch?v=wHmxCe…"
        } ],
        "text" : "https://t.co/cMutk2jbLh (9:39)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851014743368364035",
        "createdAt" : "2017-04-09T10:11:40.652Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/vHe8LQdguo",
          "expanded" : "https://www.youtube.com/watch?v=5IO1nVWWq2Q",
          "display" : "youtube.com/watch?v=5IO1nV…"
        } ],
        "text" : "https://t.co/vHe8LQdguo",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851014095105138691",
        "createdAt" : "2017-04-09T10:09:06.099Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "siehst du mich in der liste in skype?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851009501901750275",
        "createdAt" : "2017-04-09T09:50:50.947Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "germanteacher2",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851009259743719427",
        "createdAt" : "2017-04-09T09:49:53.275Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ok",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851009178864947203",
        "createdAt" : "2017-04-09T09:49:33.976Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie ist dein Skype name?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851009174863589379",
        "createdAt" : "2017-04-09T09:49:32.973Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "muss dann aber rasch eine pause machen, mein bruder kommt noch rasch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851008491376562179",
        "createdAt" : "2017-04-09T09:46:50.018Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "marc.landolt.aarau",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851008436808675331",
        "createdAt" : "2017-04-09T09:46:37.013Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Super",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007658094194691",
        "createdAt" : "2017-04-09T09:43:31.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "moment installiere ddas mal",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851007615677190147",
        "createdAt" : "2017-04-09T09:43:21.233Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Schade",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007583204904963",
        "createdAt" : "2017-04-09T09:43:13.557Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851007509737426949",
        "createdAt" : "2017-04-09T09:42:55.980Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hast du Skype?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007475318915076",
        "createdAt" : "2017-04-09T09:42:47.925Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851007441793937411",
        "createdAt" : "2017-04-09T09:42:39.800Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich trinke aber gerade schon Wodka",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007366338367491",
        "createdAt" : "2017-04-09T09:42:21.846Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "können wir oder irgend eine beiz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851007299657355267",
        "createdAt" : "2017-04-09T09:42:05.900Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Willst du zu mir?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007227599126531",
        "createdAt" : "2017-04-09T09:41:48.784Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kaffee?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851007137111212035",
        "createdAt" : "2017-04-09T09:41:27.273Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bin in Küttigen eben",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851007100335640582",
        "createdAt" : "2017-04-09T09:41:18.707Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Vis a vis von der Badi",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851006967707521027",
        "createdAt" : "2017-04-09T09:40:46.807Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wollen wir rasch einen kaffee trinken?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851006955355348995",
        "createdAt" : "2017-04-09T09:40:43.808Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bist du noch in königsfelden?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851006901810802691",
        "createdAt" : "2017-04-09T09:40:31.032Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bollackerweg 29",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851006892554022915",
        "createdAt" : "2017-04-09T09:40:28.892Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851006830604165125",
        "createdAt" : "2017-04-09T09:40:14.061Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851006813860503556",
        "createdAt" : "2017-04-09T09:40:10.124Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wo bist du?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851006772865355780",
        "createdAt" : "2017-04-09T09:40:00.293Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "K",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "851006720272891912",
        "createdAt" : "2017-04-09T09:39:47.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "846676995555643396",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bei mir ist es eher wie eine \"echo chamber\" in meinem Kopf, wenn das jemand mal gesagt hat, dann hallt es noch jahre lang weiter, die Technische Hürde um so etwas zu machen ist doch eher gross... also für mich war es schwer mein ganzes Weltbild mit Verschwörung wegzuwerfen, und mir einzugestehen dass ich ohne medis einen Schaden han, wo ich aber nichts dafür kann, das startet so mit 20 bei schizos wie bei mir und ich hab dann immer den fehler in diesem Zeitraum gesucht statt die krankheit einzugestehen und meine Medis (Xeplion) zu nehmen bzw. mir spritzen zu lassen.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "851004613776093187",
        "createdAt" : "2017-04-09T09:31:25.617Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich verdächtige meinen Nachbarn, er ist ein totaler Psycho",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850708528444710916",
        "createdAt" : "2017-04-08T13:54:53.326Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Mit Seresta verschwinden die Stimmen, meistens... habe ich eine Psychose oder werden wir hier bestrahlt?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850708205764268036",
        "createdAt" : "2017-04-08T13:53:36.376Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und mein Notebook und Natel spinnen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850707974901334022",
        "createdAt" : "2017-04-08T13:52:41.313Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ausserdem kriege ich intensiv Tinnitus",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850707921134604291",
        "createdAt" : "2017-04-08T13:52:28.500Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich werde bombaridert, wenn ich zu Hause bin höre ich stimmen die mich als \"Jugo nicht normal\" ständig beleidigen",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850707725491335171",
        "createdAt" : "2017-04-08T13:51:42.165Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Glaubst du also nicht an die \"Voice to Skull\" Möglichkeit?",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850707571962990595",
        "createdAt" : "2017-04-08T13:51:05.294Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc wir kennen uns von Königsfelden ich half dir mit Spanisch",
        "mediaUrls" : [ ],
        "senderId" : "846676995555643396",
        "id" : "850707424038334467",
        "createdAt" : "2017-04-08T13:50:30.038Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-852467808269209600",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "852467808269209600",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, da hab ich nicht dran gedacht :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "898644924731674627",
        "createdAt" : "2017-08-18T20:37:00.817Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Versteht man in der Romandie nicht.",
        "mediaUrls" : [ ],
        "senderId" : "852467808269209600",
        "id" : "898643985748688903",
        "createdAt" : "2017-08-18T20:33:16.954Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "852467808269209600",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wärs nicht irgendwie pragmatischer das Netz einfach \"Freifunk\" zu nennen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "898635749666545668",
        "createdAt" : "2017-08-18T20:00:33.316Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "22707175-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sounded fine",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1307702255294930950",
        "createdAt" : "2020-09-20T15:24:48.288Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/JGqSe9t3J1",
          "expanded" : "https://soundcloud.com/marc-floppy-landolt/for-the-moment-defcon5-aka-heaven",
          "display" : "soundcloud.com/marc-floppy-la…"
        } ],
        "text" : "https://t.co/JGqSe9t3J1",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1307697399385731078",
        "createdAt" : "2020-09-20T15:05:30.598Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you need to remove the spaces...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1307689431797764100",
        "createdAt" : "2020-09-20T14:33:50.931Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "those links dont work for some reason",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1307688868108328966",
        "createdAt" : "2020-09-20T14:31:36.540Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i just woke up . not into that right yet",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1307237880373207046",
        "createdAt" : "2020-09-19T08:39:32.682Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "How are you, i'm fine for the moment, i had even time to make a track:\n\n\"For the moment DEFCON5 aka Heaven\"\nhttps://soundcloud .com/marc-floppy-landolt/for-the-moment-defcon5-aka-heaven\n\nProducing-Video:\nhttps://www .facebook .com/marc.landolt.9/videos/10224246293251950\nhttps://www .youtube .com/watch?v=3Yjw6BdDGmo\n\nSource:\nhttps://github .com/braindef/tracks/tree/master/ableton/2020/20200919\n\nOpinion?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1307165582601707524",
        "createdAt" : "2020-09-19T03:52:15.567Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "k",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1261612335476555780",
        "createdAt" : "2020-05-16T10:59:55.097Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pano voice is nice, maybe you should create malody not just 2 bars but 8bars, the stutter effect instrument has in my opinion wrong tones...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1261609386977439749",
        "createdAt" : "2020-05-16T10:48:12.123Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/WrzSsbRcvp",
          "expanded" : "https://youtu.be/erdKStv9mKY",
          "display" : "youtu.be/erdKStv9mKY"
        } ],
        "text" : "how do you like my mix i did today https://t.co/WrzSsbRcvp",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1261008989094948868",
        "createdAt" : "2020-05-14T19:02:26.170Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/U0Hqq4wH17",
          "expanded" : "https://youtu.be/RcYjXbSJBN8",
          "display" : "youtu.be/RcYjXbSJBN8"
        } ],
        "text" : "https://t.co/U0Hqq4wH17",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260530100497297412",
        "createdAt" : "2020-05-13T11:19:30.202Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, have a good time",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260529004756508678",
        "createdAt" : "2020-05-13T11:15:08.925Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bye for now. i need a smoke. and watching  video",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528943964123140",
        "createdAt" : "2020-05-13T11:14:54.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe it's good for the resilience of society, having problems improoves the society to have good ideas",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528821289259013",
        "createdAt" : "2020-05-13T11:14:25.186Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so many people lost thier jobs",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528664535429124",
        "createdAt" : "2020-05-13T11:13:47.818Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bad for the ecconomy maybe good for the environment",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528623045545989",
        "createdAt" : "2020-05-13T11:13:37.934Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "worried about all the job losse",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528520956006405",
        "createdAt" : "2020-05-13T11:13:13.574Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "we never had hard lockdown. in switzerland it was just an advice to stay at home but they did not force people to stay at home",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528503239434244",
        "createdAt" : "2020-05-13T11:13:09.356Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nice",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528354232422405",
        "createdAt" : "2020-05-13T11:12:33.828Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you can again travel around in europe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528332883603460",
        "createdAt" : "2020-05-13T11:12:28.743Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i go again in a few days",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528317897207813",
        "createdAt" : "2020-05-13T11:12:25.418Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "switzerland, france, germany opend the borders now",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528293511680004",
        "createdAt" : "2020-05-13T11:12:19.353Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i went shopping one time got all i need'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528250452754436",
        "createdAt" : "2020-05-13T11:12:09.083Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528174208876548",
        "createdAt" : "2020-05-13T11:11:50.908Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "other then for a smoke and shopping",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528128562085892",
        "createdAt" : "2020-05-13T11:11:40.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dont you need food?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528126955831305",
        "createdAt" : "2020-05-13T11:11:39.644Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and shopping?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260528095448219654",
        "createdAt" : "2020-05-13T11:11:32.127Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im fine i never go outside anyways",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260528057867071492",
        "createdAt" : "2020-05-13T11:11:23.172Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i give the song 4/10",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260527967618228229",
        "createdAt" : "2020-05-13T11:11:01.651Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and how does lock down works for you?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260527826584961028",
        "createdAt" : "2020-05-13T11:10:28.031Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thanks",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260527704648138756",
        "createdAt" : "2020-05-13T11:09:58.955Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it was the best song i heard from you so far",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260527671202639876",
        "createdAt" : "2020-05-13T11:09:50.985Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hard to find the volume",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260527506873913353",
        "createdAt" : "2020-05-13T11:09:11.803Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe to much bass, but i dont have studio monitors nor linear headphones",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260527493813014532",
        "createdAt" : "2020-05-13T11:09:08.698Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i don't like how soundcloud plays it at max volume tho",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260527359972618244",
        "createdAt" : "2020-05-13T11:08:36.782Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thank you",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260527318872850436",
        "createdAt" : "2020-05-13T11:08:26.979Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "good job. i like the intro a lot. the rest is good too.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1260527135548047364",
        "createdAt" : "2020-05-13T11:07:43.278Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/K3pHhu7dcX",
          "expanded" : "https://soundcloud.com/marc-floppy-landolt/mountain",
          "display" : "soundcloud.com/marc-floppy-la…"
        } ],
        "text" : "hey, how are you doing, btw. what do you think about this track: https://t.co/K3pHhu7dcX",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1260523895477743620",
        "createdAt" : "2020-05-13T10:54:50.817Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/I5vw3ACdR3",
          "expanded" : "https://twitter.com/messages/media/1256987358974656516",
          "display" : "pic.twitter.com/I5vw3ACdR3"
        } ],
        "text" : "this could be me: https://t.co/I5vw3ACdR3",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1256987358974656516/1256987352641286150/cnPrRR9E.png" ],
        "senderId" : "75128838",
        "id" : "1256987358974656516",
        "createdAt" : "2020-05-03T16:41:54.840Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was staying inside before corona, i dont like crowd, i like beeing inside",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1256987135187484677",
        "createdAt" : "2020-05-03T16:41:01.413Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i just been staying inside",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1256975484346183685",
        "createdAt" : "2020-05-03T15:54:43.640Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its bad all over the world but i am doing fine. i dont know anyone who is sick.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1256975358802321413",
        "createdAt" : "2020-05-03T15:54:13.706Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, how is it going, everything ok in you country?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1256964488298889220",
        "createdAt" : "2020-05-03T15:11:01.977Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nice",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1227826626240495621",
        "createdAt" : "2020-02-13T05:27:34.307Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/80tDYo7iv1",
          "expanded" : "https://soundcloud.com/landev/luke",
          "display" : "soundcloud.com/landev/luke"
        } ],
        "text" : "well the night is ok, i could even make a great track: https://t.co/80tDYo7iv1",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1227808834225397764",
        "createdAt" : "2020-02-13T04:16:52.408Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh thats shitty",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1227709720640679940",
        "createdAt" : "2020-02-12T21:43:01.846Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey, how are you doing? BTW, i'm in a Mental Institution again since 4 Weeks...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1227707908210688006",
        "createdAt" : "2020-02-12T21:35:49.753Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ty for such an indept answer. your time is appreciated. only reason im all about this is because i hear voices for 4.5 years",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1219988182080442372",
        "createdAt" : "2020-01-22T14:20:23.603Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "military weapons &lt;- correct\nbiocode? &lt;- i question that\nbioresonace &lt;- i question that\nrussion translation, psychotronics &lt;- i question that\nderive \"biocode\" from DNA &lt;- i question that\nfrom satelites &lt;- i question that, not enough power over that distance\nwifi access points &lt;- is a problem\nhuman genome projectx &lt;- i question that, thats about understandingn what which part of the dna does\n\n\"weponizing\" &lt;- psychological warfare to make you fear that\n\"resonant frequency of the dna\" &lt;- i question that as a communication channel, but would have maybe effects, see \"cancer and radiation\"\n\ndna database &lt;- yes they have, but it is not linked with access to you or your body\n\nTargeted Individual &lt;- to identify with this scene, but we should look that we sort out the true from the false informations\n\nsupercomuter seems to monitor all electromagnetic signal, download and monitor &lt;- i question that, one would need sensors to monitor this like elon musks neuralink\n\nvidogame -&gt; m98\n\nsuper comuter comes multiple times &lt;- makes oyu fear super computers, just learn to programm to understand how computers and super computer work\n\npsychological interface, hijack the interface\n\ni dont have that much time for analyzing this stuff, but it lies to you, gives you some information but is mostly psychological warfare against Targeted Individuals from the NSA/CIA\n\ni say always GFY NSA/CIA (and yes so they become angry against me, swiss saga: Winkelried), when i was a kid i wanted to be an agent, 007 himself, but today i know the \"programm Agant\" (The Matrix) is the real problem",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1219912762043895817",
        "createdAt" : "2020-01-22T09:20:42.076Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/qN6BwhNPB1",
          "expanded" : "https://youtu.be/uqqdS7iEv00",
          "display" : "youtu.be/uqqdS7iEv00"
        } ],
        "text" : "https://t.co/qN6BwhNPB1",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1219324473377050635",
        "createdAt" : "2020-01-20T18:23:03.109Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181624237590827015",
        "createdAt" : "2019-10-08T17:35:46.463Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i gtg right now ttyl",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1181624047865479172",
        "createdAt" : "2019-10-08T17:35:01.224Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1181624015359602692",
        "createdAt" : "2019-10-08T17:34:53.476Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the video?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181623522231230468",
        "createdAt" : "2019-10-08T17:32:55.906Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont understand that stuff im not smart enough",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1181623299182194692",
        "createdAt" : "2019-10-08T17:32:02.738Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "seekt to 56:45",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181622841567043588",
        "createdAt" : "2019-10-08T17:30:13.629Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "video is english",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181622811506495492",
        "createdAt" : "2019-10-08T17:30:06.467Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/yq5NFTwaA7",
          "expanded" : "http://youtube.com/watch?v=vtQ7LN",
          "display" : "youtube.com/watch?v=vtQ7LN"
        } ],
        "text" : "if you would say that this does not work, well with eg 1kW would be already a physical / physical / mental impact to be expected: https: // www. https://t.co/yq5NFTwaA7 eC8Cs # t = 56m45s ... (Leader at 56:45) @ Incorrectly could you count that? # HackersCardgame",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181622771207561220",
        "createdAt" : "2019-10-08T17:29:57.169Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sorry i cant read that",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1181622115511877636",
        "createdAt" : "2019-10-08T17:27:20.516Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/D0JitY5o3Q",
          "expanded" : "https://twitter.com/FailDef/status/1181618320367374337",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "hey, can you read this tweet and retweet in the v2k / targetedIndividual community? https://t.co/D0JitY5o3Q",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1181620627553296391",
        "createdAt" : "2019-10-08T17:21:25.778Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, i also dont have that much money",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1138307717855100932",
        "createdAt" : "2019-06-11T04:51:23.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nope im poor",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1138303980012834821",
        "createdAt" : "2019-06-11T04:36:32.208Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(not finished yet)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1138300895534469125",
        "createdAt" : "2019-06-11T04:24:16.806Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/G9tH9s6fhj",
          "expanded" : "https://www.facebook.com/marc.landolt.9/videos/10219703948536171/",
          "display" : "facebook.com/marc.landolt.9…"
        } ],
        "text" : "and btw. what do you think about this one: https://t.co/G9tH9s6fhj",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1138300843952955396",
        "createdAt" : "2019-06-11T04:24:04.533Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/9gEM1XJL5H",
          "expanded" : "https://www.adamszabo.com/jp6k/",
          "display" : "adamszabo.com/jp6k/"
        } ],
        "text" : "do you buy VSTs? because if you do this one for 25$ is really worth the price: https://t.co/9gEM1XJL5H",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1138300729817481220",
        "createdAt" : "2019-06-11T04:23:37.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "(Y)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1127035918253199365",
        "createdAt" : "2019-05-11T02:21:16.927Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its cool. i liked the start but i was too busy to lissten full",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1127035667056287750",
        "createdAt" : "2019-05-11T02:20:17.035Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YIBr4uMlPJ",
          "expanded" : "https://www.facebook.com/marc.landolt.9/videos/10219441967546810/",
          "display" : "facebook.com/marc.landolt.9…"
        } ],
        "text" : "i modified the track, do you like it more now? https://t.co/YIBr4uMlPJ",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1127028914776346629",
        "createdAt" : "2019-05-11T01:53:27.182Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "tnx :D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1126269708054011908",
        "createdAt" : "2019-05-08T23:36:38.192Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that one sounds better",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1126269596992847877",
        "createdAt" : "2019-05-08T23:36:11.696Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/8XwRKHpDOz",
          "expanded" : "https://www.youtube.com/watch?v=epviq2A397M",
          "display" : "youtube.com/watch?v=epviq2…"
        } ],
        "text" : "do you like more chillout style music? https://t.co/8XwRKHpDOz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1126260623665397769",
        "createdAt" : "2019-05-08T23:00:32.308Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yoga music is not really my thing. but it sounds fine",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1126170595635355652",
        "createdAt" : "2019-05-08T17:02:47.943Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i'm a dubstep fan",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1126170496968454148",
        "createdAt" : "2019-05-08T17:02:24.414Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/TKlHxRBfIG",
          "expanded" : "https://soundcloud.com/landev/yoga",
          "display" : "soundcloud.com/landev/yoga"
        } ],
        "text" : "https://t.co/TKlHxRBfIG",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1126110664697761796",
        "createdAt" : "2019-05-08T13:04:39.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/San1H3qNwJ",
          "expanded" : "https://www.chip.de/downloads/Snipping-Tool-Plus_47471201.html",
          "display" : "chip.de/downloads/Snip…"
        } ],
        "text" : "https://t.co/San1H3qNwJ",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116097942555901961",
        "createdAt" : "2019-04-10T21:57:40.375Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "this free tool is much better for screenshots, you dont need paint",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116097940035182596",
        "createdAt" : "2019-04-10T21:57:39.708Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also print screen and paist into paint program.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1116087190163824644",
        "createdAt" : "2019-04-10T21:14:56.731Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i use all these things, since i use linux, then it is normal to use inkskape, gimp and audacity",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116086429690605572",
        "createdAt" : "2019-04-10T21:11:55.429Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes that is a good free program. also check out \"inkscape\" and \"gimp\" and \"aducity\"",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1116086219224387589",
        "createdAt" : "2019-04-10T21:11:05.295Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ArK7qygJlS",
          "expanded" : "https://www.facebook.com/marc.landolt.9/videos/10219207927535956/",
          "display" : "facebook.com/marc.landolt.9…"
        } ],
        "text" : "today i found out how to livestream to facebook or youtube: https://t.co/ArK7qygJlS … (you can do that with OBS-Studio, a Streaming Sofzware that is free...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1116085510366277638",
        "createdAt" : "2019-04-10T21:08:16.266Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191892491538436",
        "createdAt" : "2019-04-05T15:43:42.558Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, so i must go now, my sister is picking me up with her car, i am not allowed to drive because of my schizophrenia",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114191859176353796",
        "createdAt" : "2019-04-05T15:43:34.623Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "just keep posting the songs you make. i like hearing it evolve",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191738489323530",
        "createdAt" : "2019-04-05T15:43:05.843Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if you have questions about that just ask, i will to explane, even when im not a native speakser",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114191629366243333",
        "createdAt" : "2019-04-05T15:42:39.837Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191419822882820",
        "createdAt" : "2019-04-05T15:41:49.871Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe you should learn that, taht is very useful",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114191348750606340",
        "createdAt" : "2019-04-05T15:41:32.946Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kind of but not very fast",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191334049325061",
        "createdAt" : "2019-04-05T15:41:29.412Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nope",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191281226301444",
        "createdAt" : "2019-04-05T15:41:16.823Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "can you read scores?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114191263740329988",
        "createdAt" : "2019-04-05T15:41:12.647Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i never understand that stuff",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114191115559636996",
        "createdAt" : "2019-04-05T15:40:37.319Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/lTDLsudSHF",
          "expanded" : "https://en.wikipedia.org/wiki/Parallel_and_counter_parallel",
          "display" : "en.wikipedia.org/wiki/Parallel_…"
        } ],
        "text" : "https://t.co/lTDLsudSHF",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190939029999622",
        "createdAt" : "2019-04-05T15:39:55.258Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nope i am new",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190932843216900",
        "createdAt" : "2019-04-05T15:39:53.758Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you know that for every mayor chord is a parralel minor chord?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190874995511301",
        "createdAt" : "2019-04-05T15:39:39.994Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but it dint feel like it matched",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190762734809092",
        "createdAt" : "2019-04-05T15:39:13.204Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "true",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190665150128133",
        "createdAt" : "2019-04-05T15:38:49.939Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":D",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190630102679556",
        "createdAt" : "2019-04-05T15:38:41.589Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i like loud base",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190621126930437",
        "createdAt" : "2019-04-05T15:38:39.438Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its a good song",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190608023744517",
        "createdAt" : "2019-04-05T15:38:36.325Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i said 1:50 has a base. too loud. but not to worry",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190453430087684",
        "createdAt" : "2019-04-05T15:37:59.453Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh i just heard it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190341509279749",
        "createdAt" : "2019-04-05T15:37:32.766Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "my advice is not th ebest",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190304926552068",
        "createdAt" : "2019-04-05T15:37:24.052Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what do you think about mine?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190302426873866",
        "createdAt" : "2019-04-05T15:37:23.451Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "remember im just a beginner",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190271938420741",
        "createdAt" : "2019-04-05T15:37:16.182Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1:50 sounds bad. but the rest is good",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190226606321669",
        "createdAt" : "2019-04-05T15:37:05.376Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the chords at 1:39 are nice",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190217257336836",
        "createdAt" : "2019-04-05T15:37:03.167Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "worry too much",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190151066849285",
        "createdAt" : "2019-04-05T15:36:47.364Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but im a critical thinker",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190119085326341",
        "createdAt" : "2019-04-05T15:36:39.736Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not a native speaker, have you another word for it?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114190071568248836",
        "createdAt" : "2019-04-05T15:36:28.422Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its super heavy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190053989707780",
        "createdAt" : "2019-04-05T15:36:24.232Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it needs to be thiner. filtered",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114190020565319684",
        "createdAt" : "2019-04-05T15:36:16.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it does a short bace sound",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114189947546685444",
        "createdAt" : "2019-04-05T15:35:58.855Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the bace",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114189908946472964",
        "createdAt" : "2019-04-05T15:35:49.647Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the piano voice you mean?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114189887182442500",
        "createdAt" : "2019-04-05T15:35:44.470Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "about*",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114189771402764292",
        "createdAt" : "2019-04-05T15:35:16.844Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anout 1:50 you need to work on that sound. sounds out of place",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114189740117385220",
        "createdAt" : "2019-04-05T15:35:09.395Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ht31F3Pbfv",
          "expanded" : "https://youtu.be/Gf955GS3_Kw",
          "display" : "youtu.be/Gf955GS3_Kw"
        } ],
        "text" : "https://t.co/ht31F3Pbfv",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1114189478757724165",
        "createdAt" : "2019-04-05T15:34:07.111Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/18eaKnb5P9",
          "expanded" : "https://soundcloud.com/landev/next-2",
          "display" : "soundcloud.com/landev/next-2"
        } ],
        "text" : "https://t.co/18eaKnb5P9",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1114188436523618308",
        "createdAt" : "2019-04-05T15:29:58.610Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "k",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1111802514196709380",
        "createdAt" : "2019-03-30T01:29:10.369Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the song does not have any image yet, it's just my profile picture...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1111798333339779077",
        "createdAt" : "2019-03-30T01:12:33.618Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not bad song but i dont agree with using pony images cause they are copy wrote",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1111792195311300612",
        "createdAt" : "2019-03-30T00:48:10.164Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/AICL6N1c0E",
          "expanded" : "https://soundcloud.com/landev/next",
          "display" : "soundcloud.com/landev/next"
        } ],
        "text" : "https://t.co/AICL6N1c0E",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1111477968549892103",
        "createdAt" : "2019-03-29T03:59:32.687Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1105506080568700932",
        "createdAt" : "2019-03-12T16:29:23.618Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/dVuqnCvIiw",
          "expanded" : "https://soundcloud.com/landev/unicorn",
          "display" : "soundcloud.com/landev/unicorn"
        } ],
        "text" : "i followed your advice and made some (almost) breaks, good advice https://t.co/dVuqnCvIiw 😊",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1105497201663451146",
        "createdAt" : "2019-03-12T15:54:06.798Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/qKujYqPoHu",
          "expanded" : "https://www.youtube.com/watch?v=x3buUPPU_TI",
          "display" : "youtube.com/watch?v=x3buUP…"
        } ],
        "text" : "https://t.co/qKujYqPoHu",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104884967023300613",
        "createdAt" : "2019-03-10T23:21:18.658Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/9FY6PR6ksq",
          "expanded" : "https://youtu.be/NwTPqJKoFW8",
          "display" : "youtu.be/NwTPqJKoFW8"
        } ],
        "text" : "check this video https://t.co/9FY6PR6ksq",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104400119254802437",
        "createdAt" : "2019-03-09T15:14:41.923Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "k",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276522016206852",
        "createdAt" : "2019-03-09T07:03:34.015Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im up since 16:00 and here it is already 08:00",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104276484930318340",
        "createdAt" : "2019-03-09T07:03:25.175Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok good night",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276435353526276",
        "createdAt" : "2019-03-09T07:03:13.420Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i need to sleep i guess...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104276403770531844",
        "createdAt" : "2019-03-09T07:03:05.827Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "woops i cant see the right time stamp",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276402151415816",
        "createdAt" : "2019-03-09T07:03:05.449Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "4:43 i got tired",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276270769102853",
        "createdAt" : "2019-03-09T07:02:34.118Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "at half way",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276158676230149",
        "createdAt" : "2019-03-09T07:02:07.443Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe try to do a low cut",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276119975424004",
        "createdAt" : "2019-03-09T07:01:58.165Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "half way needs a breather",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104276002618798084",
        "createdAt" : "2019-03-09T07:01:30.178Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i love that so griddy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104275813229187076",
        "createdAt" : "2019-03-09T07:00:45.042Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "drop",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104275683109269508",
        "createdAt" : "2019-03-09T07:00:14.020Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "good",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104275673365925892",
        "createdAt" : "2019-03-09T07:00:11.678Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104275661265354756",
        "createdAt" : "2019-03-09T07:00:08.806Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i need to go complete over it again, but not today anymore... :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104272877459832836",
        "createdAt" : "2019-03-09T06:49:05.108Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/WJlP8LIRg1",
          "expanded" : "https://soundcloud.com/landev/2019-03-08-14-arranged-05-mastered-06-aldi-autsprecher",
          "display" : "soundcloud.com/landev/2019-03…"
        } ],
        "text" : "https://t.co/WJlP8LIRg1",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104272745716768772",
        "createdAt" : "2019-03-09T06:48:33.696Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "youtube, i changed some things, it should now sound better on bad speakers and i added 3 breaks",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104271039138332678",
        "createdAt" : "2019-03-09T06:41:46.794Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "can i have you email i will send you a copy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104268687794561029",
        "createdAt" : "2019-03-09T06:32:26.239Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i just lisstened to the loop for like 15 min its so good",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104268157152223236",
        "createdAt" : "2019-03-09T06:30:19.672Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "or i can send over email",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104266310890905604",
        "createdAt" : "2019-03-09T06:22:59.496Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "twitter only uses video so the only way i can show you is if i have permision to post it to youtube",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104266173498052613",
        "createdAt" : "2019-03-09T06:22:26.737Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if you want to hear it i need a way to share audio",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104265317931339782",
        "createdAt" : "2019-03-09T06:19:02.794Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MaJS5Vvq5I",
          "expanded" : "https://twitter.com/messages/media/1104265253087395844",
          "display" : "pic.twitter.com/MaJS5Vvq5I"
        } ],
        "text" : "i took your song and did this https://t.co/MaJS5Vvq5I",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104265253087395844/1104265247219605505/3tdT0S8D.jpg" ],
        "senderId" : "22707175",
        "id" : "1104265253087395844",
        "createdAt" : "2019-03-09T06:18:47.461Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "multi cultral",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104262098715009030",
        "createdAt" : "2019-03-09T06:06:15.284Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well dont care for your spellign, im not a native speacker, i would not even notice it :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104262037063065604",
        "createdAt" : "2019-03-09T06:06:00.537Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its an art",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261890430066692",
        "createdAt" : "2019-03-09T06:05:25.575Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh good music is hard to make",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261869861105668",
        "createdAt" : "2019-03-09T06:05:20.670Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sorry my spelling was wrong",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261772825907204",
        "createdAt" : "2019-03-09T06:04:57.601Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "btw. i just found out that on bad speakers the distortion sounds bad, so i need to put down distortion a bit for these sort of speakers, but on good speakers or good headphones it sound good...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104261592399691780",
        "createdAt" : "2019-03-09T06:04:14.535Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "see how i opened it up and added stuff",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261401101533188",
        "createdAt" : "2019-03-09T06:03:28.904Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/qifNOYd05r",
          "expanded" : "https://twitter.com/messages/media/1104261356159565829",
          "display" : "pic.twitter.com/qifNOYd05r"
        } ],
        "text" : " https://t.co/qifNOYd05r",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104261356159565829/1104261349230575616/Xy1rud_r.jpg" ],
        "senderId" : "22707175",
        "id" : "1104261356159565829",
        "createdAt" : "2019-03-09T06:03:18.352Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i tweet those so they are oirigonal. and the sample pack was freewear",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261323414597636",
        "createdAt" : "2019-03-09T06:03:10.394Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and snares",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261234730233860",
        "createdAt" : "2019-03-09T06:02:49.243Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "other then my kicks",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261218317955076",
        "createdAt" : "2019-03-09T06:02:45.325Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i create from scratch",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104261175791894533",
        "createdAt" : "2019-03-09T06:02:35.261Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/kA9ric6GFi",
          "expanded" : "https://www.youtube.com/watch?v=iGUxhut_wCg",
          "display" : "youtube.com/watch?v=iGUxhu…"
        } ],
        "text" : "most of the time i use dune3, thats an additional VST not from ableton: https://t.co/kA9ric6GFi",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104259914862682121",
        "createdAt" : "2019-03-09T05:57:34.592Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "life time updates",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104259624763449348",
        "createdAt" : "2019-03-09T05:56:25.401Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "fl studio is in version 20 now",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104259597307600901",
        "createdAt" : "2019-03-09T05:56:18.849Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ableton is like fl studio just more expensive",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104259444106354693",
        "createdAt" : "2019-03-09T05:55:42.349Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104259286249533445",
        "createdAt" : "2019-03-09T05:55:04.686Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok i see what you mean",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104259137867866117",
        "createdAt" : "2019-03-09T05:54:29.312Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "okay i din't knotice it much. good music will pump with the heart and also breath at 32 bars or whatever you choice",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104259124861100036",
        "createdAt" : "2019-03-09T05:54:26.207Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/pIbN8ql59S",
          "expanded" : "https://twitter.com/messages/media/1104258935933022213",
          "display" : "pic.twitter.com/pIbN8ql59S"
        } ],
        "text" : " https://t.co/pIbN8ql59S",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104258935933022213/1104258927536033792/gsSEfd0r.jpg" ],
        "senderId" : "75128838",
        "id" : "1104258935933022213",
        "createdAt" : "2019-03-09T05:53:41.681Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i have did this, but with more decent just the filter cutoff and not volume",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104258884481544197",
        "createdAt" : "2019-03-09T05:53:28.905Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/D6777wHsaa",
          "expanded" : "https://youtu.be/0q3ve6ZnxXE",
          "display" : "youtu.be/0q3ve6ZnxXE"
        } ],
        "text" : "https://t.co/D6777wHsaa",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104258485972156420",
        "createdAt" : "2019-03-09T05:51:53.930Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, not if you are on XTC :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104257233821593606",
        "createdAt" : "2019-03-09T05:46:55.354Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/yAQeEy4E1U",
          "expanded" : "https://youtu.be/rSfl0z-5OXA",
          "display" : "youtu.be/rSfl0z-5OXA"
        } ],
        "text" : "https://t.co/yAQeEy4E1U",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104257128498253828",
        "createdAt" : "2019-03-09T05:46:30.274Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cause some people when they hear a song they dance to it. and you want the dancer to have pauses",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104257032855535620",
        "createdAt" : "2019-03-09T05:46:07.444Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/2rkpK0aCQA",
          "expanded" : "https://twitter.com/messages/media/1104256694077411333",
          "display" : "pic.twitter.com/2rkpK0aCQA"
        } ],
        "text" : " https://t.co/2rkpK0aCQA",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104256694077411333/1104256686485696519/QYl_ONyT.jpg" ],
        "senderId" : "22707175",
        "id" : "1104256694077411333",
        "createdAt" : "2019-03-09T05:44:46.834Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/tWU9j4mRj3",
          "expanded" : "https://twitter.com/messages/media/1104256492880912389",
          "display" : "pic.twitter.com/tWU9j4mRj3"
        } ],
        "text" : " https://t.co/tWU9j4mRj3",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104256492880912389/1104256482990743552/EUeNG-Ok.jpg" ],
        "senderId" : "22707175",
        "id" : "1104256492880912389",
        "createdAt" : "2019-03-09T05:43:58.822Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont know FL, i never used it",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104256121467031556",
        "createdAt" : "2019-03-09T05:42:30.152Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i don't know much im just a newb but i know you want the crowd to be able to scratch thier buts ever 32 bars lol",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104256089875308549",
        "createdAt" : "2019-03-09T05:42:22.648Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what sort of instrument is that? FM synthesizers with some oscilators?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104256081193254916",
        "createdAt" : "2019-03-09T05:42:20.561Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/DnpO08iiii",
          "expanded" : "https://twitter.com/messages/media/1104255711402291205",
          "display" : "pic.twitter.com/DnpO08iiii"
        } ],
        "text" : "see i make from scratch https://t.co/DnpO08iiii",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104255711402291205/1104255675826233344/BZV6PO53.jpg" ],
        "senderId" : "22707175",
        "id" : "1104255711402291205",
        "createdAt" : "2019-03-09T05:40:52.797Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no i think its after 32",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104255613817651204",
        "createdAt" : "2019-03-09T05:40:29.108Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, i see what you mean, and thats the reason why i muted the melody after 32 bars, but you mean more often?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104255525385064452",
        "createdAt" : "2019-03-09T05:40:08.036Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no i created that from scratch",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104255102234181636",
        "createdAt" : "2019-03-09T05:38:27.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "seconds",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104255002179076100",
        "createdAt" : "2019-03-09T05:38:03.288Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it stops for a second after so many seoncds",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254967760605188",
        "createdAt" : "2019-03-09T05:37:55.082Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "see how it breaths",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254858670956548",
        "createdAt" : "2019-03-09T05:37:29.076Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/zbVLZ7GVoo",
          "expanded" : "https://youtu.be/6JQm5aSjX6g",
          "display" : "youtu.be/6JQm5aSjX6g"
        } ],
        "text" : "https://t.co/zbVLZ7GVoo",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254833312227333",
        "createdAt" : "2019-03-09T05:37:23.058Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it should breath'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254684041117707",
        "createdAt" : "2019-03-09T05:36:47.439Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but the music should break after so many seconds. nobody makes sounds that last forever",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254644472016900",
        "createdAt" : "2019-03-09T05:36:38.016Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but the base is fat, ist that a fl standard sample?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104254561299165189",
        "createdAt" : "2019-03-09T05:36:18.179Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes i see, but i dont like portamento, sounds to much like dubstep",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104254486225260549",
        "createdAt" : "2019-03-09T05:36:00.285Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "does that help?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104254420357853188",
        "createdAt" : "2019-03-09T05:35:44.567Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/rhIlhpeBz4",
          "expanded" : "https://twitter.com/messages/media/1104254163108585476",
          "display" : "pic.twitter.com/rhIlhpeBz4"
        } ],
        "text" : " https://t.co/rhIlhpeBz4",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104254163108585476/1104254155172937728/7a-OXHWy.jpg" ],
        "senderId" : "22707175",
        "id" : "1104254163108585476",
        "createdAt" : "2019-03-09T05:34:43.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "see how it builds up fades out and drops?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104253810057175044",
        "createdAt" : "2019-03-09T05:33:19.060Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MiJcleFoyp",
          "expanded" : "https://youtu.be/hs42lLATmDA",
          "display" : "youtu.be/hs42lLATmDA"
        } ],
        "text" : "https://t.co/MiJcleFoyp",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104253758425317380",
        "createdAt" : "2019-03-09T05:33:06.773Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "uploading right meow",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104253212670881796",
        "createdAt" : "2019-03-09T05:30:56.633Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes these are curves i've drawn for filter cutoff on different voices",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104252780523520004",
        "createdAt" : "2019-03-09T05:29:13.604Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "or some change",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104252590643011588",
        "createdAt" : "2019-03-09T05:28:28.334Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so you dont want the melody to last to long without a fade out",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104252563606536196",
        "createdAt" : "2019-03-09T05:28:21.903Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you like it when it goes \"wob wob\" right. thats because it is like a rollercoaster. it goes up down up down up down. then stops. then they do it again. thats how music is best",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104252473689047044",
        "createdAt" : "2019-03-09T05:28:00.471Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104251503873847301",
        "createdAt" : "2019-03-09T05:24:09.231Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im making a video",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104251503697489926",
        "createdAt" : "2019-03-09T05:24:09.191Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104251495787229194",
        "createdAt" : "2019-03-09T05:24:07.310Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i will show you in 4 min",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104251476736524294",
        "createdAt" : "2019-03-09T05:24:02.750Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you mean over 1 bar or over 8 or over 16?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104251439797465092",
        "createdAt" : "2019-03-09T05:23:53.946Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "fl studio uses waves if you got the wave file you can use it but im not sure if they connect",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104251434340499460",
        "createdAt" : "2019-03-09T05:23:52.667Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im making an example",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104251282171158533",
        "createdAt" : "2019-03-09T05:23:16.367Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, is there an import / export function for ableton &lt;-&gt; fl ?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104250858701815813",
        "createdAt" : "2019-03-09T05:21:35.408Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MysNkXgKZu",
          "expanded" : "https://twitter.com/messages/media/1104250575762350084",
          "display" : "pic.twitter.com/MysNkXgKZu"
        } ],
        "text" : "like let your song breath with pauses that go like this https://t.co/MysNkXgKZu",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104250575762350084/1104250491905536000/EvIp6rad.jpg" ],
        "senderId" : "22707175",
        "id" : "1104250575762350084",
        "createdAt" : "2019-03-09T05:20:28.163Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah you mean to add a second theme to the track?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104250429406433284",
        "createdAt" : "2019-03-09T05:19:53.055Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not really, i dont understand",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104249358596349956",
        "createdAt" : "2019-03-09T05:15:37.764Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/UZFKzTP6bC",
          "expanded" : "https://twitter.com/messages/media/1104248481596628997",
          "display" : "pic.twitter.com/UZFKzTP6bC"
        } ],
        "text" : "does this help at all? https://t.co/UZFKzTP6bC",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1104248481596628997/1104248452655927296/ArnkXRIF.jpg" ],
        "senderId" : "22707175",
        "id" : "1104248481596628997",
        "createdAt" : "2019-03-09T05:12:09.039Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/t1lfDqXBdB",
          "expanded" : "https://soundpacks.com/free-sound-packs/hardstyle-force-sample-pack/?fbclid=IwAR3MNbwc5YUR3sq8KzqFjUY_3aLwGNqYB_MYXhj36j0pzWxDA5469DdpQ_4",
          "display" : "soundpacks.com/free-sound-pac…"
        } ],
        "text" : "Ableton 10 &amp; Dune3 and some free hardstyle drumsamples from https://t.co/t1lfDqXBdB",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104247953445793796",
        "createdAt" : "2019-03-09T05:10:02.782Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "personal preferance",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247664873398276",
        "createdAt" : "2019-03-09T05:08:53.937Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok i don't like distortion",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247635093811205",
        "createdAt" : "2019-03-09T05:08:46.852Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what program are you using",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247484086247430",
        "createdAt" : "2019-03-09T05:08:10.831Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no this is intentionally distortion effect...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104247464960450564",
        "createdAt" : "2019-03-09T05:08:06.278Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "better then the last one i heard",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247428801159172",
        "createdAt" : "2019-03-09T05:07:57.659Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "better tho",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247386380034052",
        "createdAt" : "2019-03-09T05:07:47.534Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "little bit boring. and the drums have a bit of clashing. sounds like they are clipping",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247358861135876",
        "createdAt" : "2019-03-09T05:07:40.974Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im half way in",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247215571124229",
        "createdAt" : "2019-03-09T05:07:06.830Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bzw. how do you like it?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104247179349319685",
        "createdAt" : "2019-03-09T05:06:58.174Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in switzerland i guess not that symbolic :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104247155026546692",
        "createdAt" : "2019-03-09T05:06:52.381Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "symbolism . the meaning of horse means anonymous. don't use it much unless you want that type of group. a bunch of hackers.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1104247001401573380",
        "createdAt" : "2019-03-09T05:06:15.753Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/M85p9Sjv0A",
          "expanded" : "https://www.youtube.com/watch?v=y0R3dMye7BY",
          "display" : "youtube.com/watch?v=y0R3dM…"
        } ],
        "text" : "https://t.co/M85p9Sjv0A my (ᵔᴥᵔ) helped me 😌",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1104234880840421381",
        "createdAt" : "2019-03-09T04:18:06.020Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1103507355319197702",
        "createdAt" : "2019-03-07T04:07:10.389Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hypnotizing :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1103506561262145540",
        "createdAt" : "2019-03-07T04:04:01.087Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/rDv8XbDmKT",
          "expanded" : "https://youtu.be/B6M3WGeEaxo",
          "display" : "youtu.be/B6M3WGeEaxo"
        } ],
        "text" : "https://t.co/rDv8XbDmKT",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1103505496147816453",
        "createdAt" : "2019-03-07T03:59:47.143Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CarXt5wbGM",
          "expanded" : "https://soundcloud.com/landev/echo",
          "display" : "soundcloud.com/landev/echo"
        } ],
        "text" : "https://t.co/CarXt5wbGM",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1103500006626152452",
        "createdAt" : "2019-03-07T03:37:58.346Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Fqw0GxcMdD",
          "expanded" : "https://soundcloud.com/landev/sea-1",
          "display" : "soundcloud.com/landev/sea-1"
        } ],
        "text" : "https://t.co/Fqw0GxcMdD",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1097896134947889168",
        "createdAt" : "2019-02-19T16:30:11.238Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/eVObeFcvZb",
          "expanded" : "https://twitter.com/messages/media/1097895783758671876",
          "display" : "pic.twitter.com/eVObeFcvZb"
        } ],
        "text" : " https://t.co/eVObeFcvZb",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1097895783758671876/1097895776288616448/T8mq869X.jpg" ],
        "senderId" : "22707175",
        "id" : "1097895783758671876",
        "createdAt" : "2019-02-19T16:28:47.572Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Fqw0GxcMdD",
          "expanded" : "https://soundcloud.com/landev/sea-1",
          "display" : "soundcloud.com/landev/sea-1"
        } ],
        "text" : "the record is to silent, i have normalized it now: https://t.co/Fqw0GxcMdD",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1097883411224629254",
        "createdAt" : "2019-02-19T15:39:37.642Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats why i like youtube",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1097741152969666564",
        "createdAt" : "2019-02-19T06:14:20.631Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont see a volume button on that link so im forced to lissten at max volume and the ears ca't handle the whole song. unless i adjust my master volume but im to lazy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1097741026767265797",
        "createdAt" : "2019-02-19T06:13:50.562Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cheers",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1097661267521617924",
        "createdAt" : "2019-02-19T00:56:54.435Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well when i make music i often drink beer, that makes me creative, but then im to drunk to master it, this i will do tomorrow, here in switzerland its alredady 02:00",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1097661214509973509",
        "createdAt" : "2019-02-19T00:56:41.863Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "really helps to make a drop. right where the person has lisstened for a while to help them breath.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1097661166178816005",
        "createdAt" : "2019-02-19T00:56:30.287Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sounds good so far. good sound. soft. punchy. little bit trance.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1097660742285717509",
        "createdAt" : "2019-02-19T00:54:49.224Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/JqoAcMh9Rs",
          "expanded" : "https://soundcloud.com/landev/sea",
          "display" : "soundcloud.com/landev/sea"
        } ],
        "text" : "not yet mastered: https://t.co/JqoAcMh9Rs",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1097658715623309317",
        "createdAt" : "2019-02-19T00:46:46.069Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1082165159190159367",
        "createdAt" : "2019-01-07T06:40:54.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/gIFQGiIECZ",
          "expanded" : "https://twitter.com/FailDef/status/1082042680962621440",
          "display" : "twitter.com/FailDef/status…"
        } ],
        "text" : "https://t.co/gIFQGiIECZ",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1082043634583769094",
        "createdAt" : "2019-01-06T22:38:00.607Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi again, yesterday I put my digital piano in the same room like my computer and added my novation launchkey for controlling things like filter-cutoff with its knobs:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1082043610101624841",
        "createdAt" : "2019-01-06T22:37:54.770Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes i know the concept of memes, the evolve and recombine like an evolutionary process, eg rage comics",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081745935007444996",
        "createdAt" : "2019-01-06T02:55:03.522Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh that is funny. did you know that america is all going crazy over what they call MEME's and china banned them. but in french meme's translates to \"jokes\"",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081745693495029765",
        "createdAt" : "2019-01-06T02:54:05.923Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in europen culture a sheep means a peaceful true person, a good thing",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081744969755516932",
        "createdAt" : "2019-01-06T02:51:13.351Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sheep",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081744665794072580",
        "createdAt" : "2019-01-06T02:50:00.887Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "matrix :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081744056068263940",
        "createdAt" : "2019-01-06T02:47:35.526Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that tv show is food for sheep. it makes you think that it is all from drugs so dont worrie about it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081743964598747141",
        "createdAt" : "2019-01-06T02:47:13.730Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, thats just for fun, tv shows are not really a scientific source :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081743655017291785",
        "createdAt" : "2019-01-06T02:45:59.891Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats what makes me think your a little sick. you are finding coincidences",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081743565280010244",
        "createdAt" : "2019-01-06T02:45:38.533Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes, just coincidence, im bingewatching criminal minds",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081743238858506244",
        "createdAt" : "2019-01-06T02:44:20.673Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "is that a tv show?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081743054237720585",
        "createdAt" : "2019-01-06T02:43:36.656Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "btw have you seen Criminal Minds S13E21, there they talk about that...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081742266572656647",
        "createdAt" : "2019-01-06T02:40:28.883Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, the medicine helps, and i now have since 2 years disability and get money for free and that helps me not to be stressed so its also a factor that deescalates",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081741982920265732",
        "createdAt" : "2019-01-06T02:39:21.279Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you would go crazy if you turned on a radio in the room for all your life",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081741634679668741",
        "createdAt" : "2019-01-06T02:37:58.224Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its okay when you hear voices from anything it is so strong that it weakens people and makes them sick.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081741543541620740",
        "createdAt" : "2019-01-06T02:37:36.473Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well when i was thinking i had a brainchip nobody could explain me, i just did not beleive. but now with the medicine i tend to need more evidence than just my voices. because when the medicaments almost disabled this voices it was more plausilbe for me",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081739548126834692",
        "createdAt" : "2019-01-06T02:29:40.726Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081739113722728453",
        "createdAt" : "2019-01-06T02:27:57.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways the news in 5 years will show you the truth",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738989676126213",
        "createdAt" : "2019-01-06T02:27:27.620Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "like you want it to be just skizophrenia",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738912756752388",
        "createdAt" : "2019-01-06T02:27:09.235Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways you fight me so badly",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738852471959556",
        "createdAt" : "2019-01-06T02:26:54.868Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in germany for example there is also big parts of the police that is very right wing and doing illeagl thigns, that is normal, where there is power there is powerabuse",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081738838869987333",
        "createdAt" : "2019-01-06T02:26:51.637Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in my town they are parked at the cop shop",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738635186102277",
        "createdAt" : "2019-01-06T02:26:03.055Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and iluminati is a movie :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081738627812601867",
        "createdAt" : "2019-01-06T02:26:01.325Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738581914247172",
        "createdAt" : "2019-01-06T02:25:50.359Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in europe hells angles are stupid drogue dealers and most of them were jailed...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081738555238555653",
        "createdAt" : "2019-01-06T02:25:44.005Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if you want a image of a radio implant you should find someone who illegally makes them like a hells angel or illuminati",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081738356373913604",
        "createdAt" : "2019-01-06T02:24:56.587Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "such en important thign would have been leaked",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081738317253828612",
        "createdAt" : "2019-01-06T02:24:47.291Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nsa documents were also classified, but they are public now",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081738269568704516",
        "createdAt" : "2019-01-06T02:24:35.905Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i know it from switzerland the ETH (one of the best universities in the world) made experiments with apes for creating a neurointerface, but there were protests because of animal rights so they had to stop that experiment. just in case some other coutries have developed such interfaces you would need a power source to power the device, and they are normally big enought that you would find it",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081737967104913412",
        "createdAt" : "2019-01-06T02:23:23.786Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it is illegal",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081737149567791108",
        "createdAt" : "2019-01-06T02:20:08.861Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dude you won't find an image for it because it is classified",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081737129204408326",
        "createdAt" : "2019-01-06T02:20:04.035Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/ixHvZaYb9U",
          "expanded" : "https://www.google.com/search?safe=active&client=firefox-b-ab&biw=1920&bih=1006&tbm=isch&sa=1&ei=FmQxXMOjJYvZwAKu1Z_4Aw&q=atmega+square&oq=atmega+square&gs_l=img.3...3434.4561..4689...0.0..0.81.456.7......1....1..gws-wiz-img.......0j0i10j0i67j0i10i67j0i30j0i8i10i30j0i8i30.RP6pFROYZDE#imgrc=-nZWMJq9RUVyvM",
          "display" : "google.com/search?safe=ac…"
        } ],
        "text" : "maybe an atmega chip like this here: https://t.co/ixHvZaYb9U:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081735492025176069",
        "createdAt" : "2019-01-06T02:13:33.713Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i guess that thing with the tooth is just photoshopped, its a tooth (maybe 3D printed, with a chip in it that is not connected to anywhere... wires need to be connected to a power source, otherwise it will not work, thats basic electronics....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081735455132041228",
        "createdAt" : "2019-01-06T02:13:24.887Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/SCI3Eg1Pxw",
          "expanded" : "https://twitter.com/messages/media/1081734541805477892",
          "display" : "pic.twitter.com/SCI3Eg1Pxw"
        } ],
        "text" : " https://t.co/SCI3Eg1Pxw",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1081734541805477892/1081734532317904896/wU9Jzduu.jpg" ],
        "senderId" : "22707175",
        "id" : "1081734541805477892",
        "createdAt" : "2019-01-06T02:09:47.410Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its fine",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081733369224785924",
        "createdAt" : "2019-01-06T02:05:07.561Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i will be back in 5 minutes, i need a cigaret :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081733262614114313",
        "createdAt" : "2019-01-06T02:04:42.199Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe i did not listen correctly, but i also not watched the whole video, only a few parts",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081733194402197508",
        "createdAt" : "2019-01-06T02:04:25.897Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont understand",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081733079952228357",
        "createdAt" : "2019-01-06T02:03:58.609Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what do you mean then?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081733050747310084",
        "createdAt" : "2019-01-06T02:03:51.660Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that is a different thing",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732992609832965",
        "createdAt" : "2019-01-06T02:03:37.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/22V7PCHCy4",
          "expanded" : "http://neuronexus.com/wp-content/uploads/2018/09/2018ProbeCatalog_20180515_Web.pdf",
          "display" : "neuronexus.com/wp-content/upl…"
        } ],
        "text" : "see this catalog: https://t.co/22V7PCHCy4",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732914176577543",
        "createdAt" : "2019-01-06T02:03:19.073Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "to just tag someone with an id it can be everywhere",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732890734612485",
        "createdAt" : "2019-01-06T02:03:13.491Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "rfid chips and brain chips and audio chips are different things",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732865832828932",
        "createdAt" : "2019-01-06T02:03:07.548Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, but to hear voices it must be in the brain",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732836842000395",
        "createdAt" : "2019-01-06T02:03:00.666Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its not a brain chip",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732762917163012",
        "createdAt" : "2019-01-06T02:02:43.003Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "these is the product catalog of the brainchips available for apes",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732722484305925",
        "createdAt" : "2019-01-06T02:02:33.376Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "he talks about putting it in tooth",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732720038825990",
        "createdAt" : "2019-01-06T02:02:32.776Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/22V7PCHCy4",
          "expanded" : "http://neuronexus.com/wp-content/uploads/2018/09/2018ProbeCatalog_20180515_Web.pdf",
          "display" : "neuronexus.com/wp-content/upl…"
        } ],
        "text" : "https://t.co/22V7PCHCy4",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732659896832009",
        "createdAt" : "2019-01-06T02:02:18.462Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and it would not work if it would be in a tooth or in the hand, to have real communications it needs to be in the brain",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732651948695556",
        "createdAt" : "2019-01-06T02:02:16.582Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the video goes into detail with all the answers",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732641622089732",
        "createdAt" : "2019-01-06T02:02:14.081Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh ok",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732513180016646",
        "createdAt" : "2019-01-06T02:01:43.459Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "or i did not understand you, remember im not a native speaker, im from switzerland :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081732368082309126",
        "createdAt" : "2019-01-06T02:01:08.867Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not talking about rfid chip anyways its pointless to argue cause you dint lissten to what they said",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081732115417391110",
        "createdAt" : "2019-01-06T02:00:08.630Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, it is not illegal, its a normal rfid that can be used for paying",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081731964858781701",
        "createdAt" : "2019-01-06T01:59:32.730Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "to use one in a human is highly illegal",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081731588478521348",
        "createdAt" : "2019-01-06T01:58:02.992Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its for cattle",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081731500985315332",
        "createdAt" : "2019-01-06T01:57:42.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it is for dogs and cats. it is illegal to use in humans",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081731466067726342",
        "createdAt" : "2019-01-06T01:57:33.813Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well thats the thing dude",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081731404784820228",
        "createdAt" : "2019-01-06T01:57:19.207Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well, i dont trust these two guys. it could just be an implant for dogs or cats, you can not see in this video",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081731348505726980",
        "createdAt" : "2019-01-06T01:57:05.798Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "don't tell me you dint watch the video]\\",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081731347847077894",
        "createdAt" : "2019-01-06T01:57:05.632Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/S4O5Y2Ey97",
          "expanded" : "https://twitter.com/messages/media/1081731171967426564",
          "display" : "pic.twitter.com/S4O5Y2Ey97"
        } ],
        "text" : "this https://t.co/S4O5Y2Ey97",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1081731171967426564/1081731160126836736/KAG_GPKF.jpg" ],
        "senderId" : "22707175",
        "id" : "1081731171967426564",
        "createdAt" : "2019-01-06T01:56:23.868Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wait a second i will show you",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081730723810136070",
        "createdAt" : "2019-01-06T01:54:36.839Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok good. at least you got that part right",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081730652980924420",
        "createdAt" : "2019-01-06T01:54:19.956Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/n23TQ9rn7a",
          "expanded" : "https://www.google.com/search?q=cochlea+implantat&safe=active&client=firefox-b-ab&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjmydyahtjfAhW-UhUIHepTDggQ_AUIDigB&biw=1920&bih=1006",
          "display" : "google.com/search?q=cochl…"
        } ],
        "text" : "https://t.co/n23TQ9rn7a",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081730638334648324",
        "createdAt" : "2019-01-06T01:54:16.484Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they are real",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081730584483897349",
        "createdAt" : "2019-01-06T01:54:03.640Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "do you think that audio hearing radio implants are not real?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081730517177819140",
        "createdAt" : "2019-01-06T01:53:47.597Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, i was always searching for the source but i never found. and now my medicine deactivatet it mostly, i still talk to my teddy-bear sometimes, but mostly i behvae sane",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081730363733495815",
        "createdAt" : "2019-01-06T01:53:11.010Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but it doesnt feel like you are making the sound . correct'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081730075861499908",
        "createdAt" : "2019-01-06T01:52:02.441Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and still thinking im ill :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081729946375147524",
        "createdAt" : "2019-01-06T01:51:31.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081729893459607556",
        "createdAt" : "2019-01-06T01:51:18.874Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i had also the feeling that i could see where it comes from...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081729699192233988",
        "createdAt" : "2019-01-06T01:50:32.569Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it has been turned on for over 35000 hours",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081729638722752516",
        "createdAt" : "2019-01-06T01:50:18.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its different for me. i clearly hear a transmition of a radio in the air across the room . or in the sky when i am outside.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081729324783259653",
        "createdAt" : "2019-01-06T01:49:03.297Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not inside but also not outside, i heard it like a chip in my head or in my ear, but there was no chip, just the dopamine in my brain that caused that",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081728525781086213",
        "createdAt" : "2019-01-06T01:45:52.792Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im also hacked so my messages dont always show",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081727823176318980",
        "createdAt" : "2019-01-06T01:43:05.277Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "when you heard voices was it outside of your body?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081727753693478916",
        "createdAt" : "2019-01-06T01:42:48.716Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so you can freely combine them",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726614759710725",
        "createdAt" : "2019-01-06T01:38:17.186Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and vst",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726581826048004",
        "createdAt" : "2019-01-06T01:38:09.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and effect devices",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726575069007876",
        "createdAt" : "2019-01-06T01:38:07.706Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "midi devices",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726560804196357",
        "createdAt" : "2019-01-06T01:38:04.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and oyu have sound generators",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726549253074949",
        "createdAt" : "2019-01-06T01:38:01.573Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "each box is an effect",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726510640386054",
        "createdAt" : "2019-01-06T01:37:52.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/mnMbqngyVG",
          "expanded" : "https://twitter.com/messages/media/1081726485155794980",
          "display" : "pic.twitter.com/mnMbqngyVG"
        } ],
        "text" : " https://t.co/mnMbqngyVG",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1081726485155794980/1081726477912150021/OR0ni4Uw.jpg" ],
        "senderId" : "75128838",
        "id" : "1081726485155794980",
        "createdAt" : "2019-01-06T01:37:46.455Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i tried different things, cakewalk, cubase, hardware-synthies, propellerheads reason &amp; record and ended with ableton, this is very uniform and you can put effects in series like here:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081726325474381828",
        "createdAt" : "2019-01-06T01:37:08.201Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "people tell me ableton is better then fl studio but fl studio is easyer to use",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081725476534550534",
        "createdAt" : "2019-01-06T01:33:45.800Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, so i guess i will try it... but i will stick to ableton because ableton supports also my launchpad (that that with the 64 colored buttons) to start a sequence, so i play my sequences with my digitalpiano and then start the generated sequences with lauchpad",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081723368406159364",
        "createdAt" : "2019-01-06T01:25:23.188Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/3qAXWlzs5G",
          "expanded" : "https://twitter.com/messages/media/1081722380517396484",
          "display" : "pic.twitter.com/3qAXWlzs5G"
        } ],
        "text" : " https://t.co/3qAXWlzs5G",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1081722380517396484/1081722370228813824/n9bCDOLD.jpg" ],
        "senderId" : "22707175",
        "id" : "1081722380517396484",
        "createdAt" : "2019-01-06T01:21:27.897Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but it is real limited",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081722264033218564",
        "createdAt" : "2019-01-06T01:20:59.867Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "free demo",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081722209935052804",
        "createdAt" : "2019-01-06T01:20:46.975Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/x6CwvzX82a",
          "expanded" : "https://www.image-line.com/flstudio/",
          "display" : "image-line.com/flstudio/"
        } ],
        "text" : "https://t.co/x6CwvzX82a",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081721638205321220",
        "createdAt" : "2019-01-06T01:18:30.680Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dont they have a demo to just try it?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081721315114053637",
        "createdAt" : "2019-01-06T01:17:13.672Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you need producer edition or else it will suck. its 2000$",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081721017767088132",
        "createdAt" : "2019-01-06T01:16:02.740Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the demo wont do anything",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720931997736964",
        "createdAt" : "2019-01-06T01:15:42.295Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i sound design all my sounds",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720877014605828",
        "createdAt" : "2019-01-06T01:15:29.180Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe i should test fl, is there a demo?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081720718432370692",
        "createdAt" : "2019-01-06T01:14:51.381Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "got a bit of a rythum",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720668045996036",
        "createdAt" : "2019-01-06T01:14:39.361Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thank you",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081720660286689284",
        "createdAt" : "2019-01-06T01:14:37.540Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i like your space trip",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720617529880581",
        "createdAt" : "2019-01-06T01:14:27.316Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i can do anything",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720539842961412",
        "createdAt" : "2019-01-06T01:14:08.788Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720480585793541",
        "createdAt" : "2019-01-06T01:13:54.662Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i think they just need filters",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081720452077150212",
        "createdAt" : "2019-01-06T01:13:47.873Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "can you use vst in fl?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081720062640312329",
        "createdAt" : "2019-01-06T01:12:15.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ty",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081719930335031301",
        "createdAt" : "2019-01-06T01:11:43.479Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CGYFSMCd6S",
          "expanded" : "https://www.youtube.com/watch?v=sGKltafvPOw",
          "display" : "youtube.com/watch?v=sGKlta…"
        } ],
        "text" : "this one from you is nice: https://t.co/CGYFSMCd6S",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081719541678440453",
        "createdAt" : "2019-01-06T01:10:10.915Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/svSmaIhCWE",
          "expanded" : "https://soundcloud.com/landev",
          "display" : "soundcloud.com/landev"
        } ],
        "text" : "here are my songs: https://t.co/svSmaIhCWE",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081718520461869060",
        "createdAt" : "2019-01-06T01:06:07.354Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was using different thigns, i also had once propellerheads reason &amp; record, that is quite cool you can draw cables from one device to another",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081718507589566469",
        "createdAt" : "2019-01-06T01:06:04.300Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im a fan of fl studio",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081718293440819204",
        "createdAt" : "2019-01-06T01:05:13.210Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "tone2 elektra 2 is about 100$",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081718279377485829",
        "createdAt" : "2019-01-06T01:05:09.863Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dune3 is really nice and costs 200$",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081718227779178500",
        "createdAt" : "2019-01-06T01:04:57.565Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ableton was about 1000$, im buying the updates since version 7",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081718193327099908",
        "createdAt" : "2019-01-06T01:04:49.377Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "how much did you pay",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081718075135733764",
        "createdAt" : "2019-01-06T01:04:21.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i never used fl",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081717999567077380",
        "createdAt" : "2019-01-06T01:04:03.194Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i have ableton10 &amp; dune3 vst &amp; tone2 elektra2 vst",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081717950602792964",
        "createdAt" : "2019-01-06T01:03:51.514Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "fl studio 20 i paid 2000$ for life time updates",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081717799725064196",
        "createdAt" : "2019-01-06T01:03:15.501Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what software do you use?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081717719228186629",
        "createdAt" : "2019-01-06T01:02:56.310Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not that good yet",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081717589124931588",
        "createdAt" : "2019-01-06T01:02:25.291Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/oMP1Uu7xMS",
          "expanded" : "https://www.youtube.com/playlist?list=PLDP8potpFAxhG3DIxbCFyz_KYdrXztIYJ",
          "display" : "youtube.com/playlist?list=…"
        } ],
        "text" : "this is what i made https://t.co/oMP1Uu7xMS",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081717504882270212",
        "createdAt" : "2019-01-06T01:02:05.218Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i had 13 years piano lessions before i got sick, so i really like to have an piano as input device, its attached with midi to my laptop",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081717493226455045",
        "createdAt" : "2019-01-06T01:02:02.428Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i do my music with a mouse",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081717266830376964",
        "createdAt" : "2019-01-06T01:01:08.448Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i only get 700$after rent = 250 food. 150 smokes.  300 left over",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081717078518714374",
        "createdAt" : "2019-01-06T01:00:23.547Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/MHXcVP9LZS",
          "expanded" : "https://twitter.com/messages/media/1081717034235412484",
          "display" : "pic.twitter.com/MHXcVP9LZS"
        } ],
        "text" : " https://t.co/MHXcVP9LZS",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1081717034235412484/1081717024538144774/4jyciWGy.jpg" ],
        "senderId" : "75128838",
        "id" : "1081717034235412484",
        "createdAt" : "2019-01-06T01:00:13.740Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i even bought a digital piano last month",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081716831365328900",
        "createdAt" : "2019-01-06T00:59:24.633Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in switzerland everythign is very expensive, so 3000 is not that much, but enough to live with",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081716759353352196",
        "createdAt" : "2019-01-06T00:59:07.464Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "jesus i live off 1400 i wish i got your kind of cash",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081716630999060484",
        "createdAt" : "2019-01-06T00:58:36.848Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i am on disability, i get 3000$ / month so i can care for me and sleep a lot that helps with the voices",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081715809662234629",
        "createdAt" : "2019-01-06T00:55:21.123Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i have schizophrenia so i am ill, but i found out that 90% of schizophrenic people is not violent and completely peaceful, so i dont feel bad because i'm not one of the 10% schizophrenics that are in press and in movies and in series, if you watch \"criminal minds\" in eacht 3rd episode the murderer is a schziophrenic. when i found out that im one of the 90% peaceful i could accept my desease.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081715633044316166",
        "createdAt" : "2019-01-06T00:54:38.950Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways sorry to say it but your answers do not help because you sound ill",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081714957064929285",
        "createdAt" : "2019-01-06T00:51:57.759Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i mean the planet",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714935846051844",
        "createdAt" : "2019-01-06T00:51:52.742Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "like the star in \"avatar\"?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714904921452549",
        "createdAt" : "2019-01-06T00:51:45.327Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pandoria",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081714793428377605",
        "createdAt" : "2019-01-06T00:51:18.736Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mindcontrol program",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081714772125446148",
        "createdAt" : "2019-01-06T00:51:13.676Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what program?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714741700108292",
        "createdAt" : "2019-01-06T00:51:06.417Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i have cops directly talking to me about a program and how they tried to kill me but found me innocent and want me to get off the program but they need to turn themselves in but they will not",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081714578298331140",
        "createdAt" : "2019-01-06T00:50:27.456Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "or in things with 3 letters",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714544718856196",
        "createdAt" : "2019-01-06T00:50:19.447Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and i also saw \"secret messages\" in numbers",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714491992260612",
        "createdAt" : "2019-01-06T00:50:06.878Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, for me it was like that, i often heard people i know, but sometimes i also heard people i dont know.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081714396253044740",
        "createdAt" : "2019-01-06T00:49:44.055Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its not like that for me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081714195727405060",
        "createdAt" : "2019-01-06T00:48:56.244Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in my case it was that i was also hearing voices, and when i had trouble with the cops i heard the cops, when i had trouble with my shrink i heard my shrink, when i had trouble with my father i heard my fater, and if i had trouble on monday i heard them until sunday...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081713995554398213",
        "createdAt" : "2019-01-06T00:48:08.542Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i tried",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081713720118505476",
        "createdAt" : "2019-01-06T00:47:02.862Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well they are messing with me. so i couldnt write down the badge number",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081713614870855685",
        "createdAt" : "2019-01-06T00:46:37.760Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "may I ask you what they give you and how many?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081713029983731716",
        "createdAt" : "2019-01-06T00:44:18.311Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but they wont",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081712839193124870",
        "createdAt" : "2019-01-06T00:43:32.830Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "plus all they have to do is xray me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081712754715578373",
        "createdAt" : "2019-01-06T00:43:12.676Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats why is keeps telling me badge numbers and how i was put on a program by mistake and it sounds like cops?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081712161267769348",
        "createdAt" : "2019-01-06T00:40:51.191Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in this case you have strong schizophrenia, and the \"you guys\" implise first that you are in treatment and that you dont trus people, so i would guess you have icd10-f20.0 (paranoid schizophrenia)... im not a doctor, so i dont tread you im not a doctor who gives you medicaments that may not work, im also have paranoid schizophrenia and have some improvement because of the medicamnets...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081711446575235078",
        "createdAt" : "2019-01-06T00:38:00.795Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways the truth will come out and you will see it in the news",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081709154354384900",
        "createdAt" : "2019-01-06T00:28:54.288Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "lissten dude i took your guys advice for 4 years i let them inject all this meds into my arm to stop the voices and it like a radio turned on for 4 years now. so in 4 years if i still hear this what are you going to say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081708822366892040",
        "createdAt" : "2019-01-06T00:27:35.138Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and the idea that it is in your tooth is from the movie \"12 Monkeys\" and your brain is telling you the movie is right but it lies...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081707832553193476",
        "createdAt" : "2019-01-06T00:23:39.145Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so this xray is not a proof, so ŷou just saw somehting on an xray without having expertise",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081707536284307460",
        "createdAt" : "2019-01-06T00:22:28.547Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "a copy i mean",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081707327831498757",
        "createdAt" : "2019-01-06T00:21:38.804Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "acopy i mean",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081707270801481732",
        "createdAt" : "2019-01-06T00:21:25.207Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the dentist told me i wasnt aloud an x-ray cause im poor. and she flipped the x-ray backwards and told me she hard to drill the other tooth. i tried to get a new x-ray but they wont do it and they tell me the machien is broken",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081707137443672068",
        "createdAt" : "2019-01-06T00:20:53.410Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "can i see the xray?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081706836963876869",
        "createdAt" : "2019-01-06T00:19:41.789Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "how does that exsplain them putting something in my tooth,. how does it explain me seeing the chip in my x-ray. and how does it exsplain all the news about mk ultra'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081706736975630340",
        "createdAt" : "2019-01-06T00:19:17.945Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "noramlly this is schizophrenia, at least in my case it was schizophrenia and i did not wanted to listen to people telling me that because it felt so real. if you have also schizophrenia you will not listen to me and think that i am eighter a lier, brainwashed, or some cia guy that wants to manipulate you. but in fact it is schizophrenia and it causes real pain so people tend to invent a reason for this pain because they can not live with the identity as a schizophrenic",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081705820000264198",
        "createdAt" : "2019-01-06T00:15:39.332Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it works man. i am implanted and hear them talk",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081704571691458564",
        "createdAt" : "2019-01-06T00:10:41.703Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "2. video: old times 60 years old, yes the cia did such things there is even something on netflix about mkultra but possibly not these days anymore because they found out that it does not work",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081637514484563972",
        "createdAt" : "2019-01-05T19:44:14.034Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1. video: 2 Strange guys talking with another much strange guy, talking about positive results but lack the evidence and the proof. \"implented with a flu shot\" is also something that would not work. the video is useless without proof and fotographes of such devices under the microscope",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1081637295449665540",
        "createdAt" : "2019-01-05T19:43:21.809Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/f0kMF23R6j",
          "expanded" : "https://youtu.be/enoJtvCnp34",
          "display" : "youtu.be/enoJtvCnp34"
        }, {
          "url" : "https://t.co/ah90n7H0B5",
          "expanded" : "https://youtu.be/WrlrOGLl5rY",
          "display" : "youtu.be/WrlrOGLl5rY"
        } ],
        "text" : "can i share this https://t.co/f0kMF23R6j     and  https://t.co/ah90n7H0B5",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "1081496129995366405",
        "createdAt" : "2019-01-05T10:22:25.331Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : ":)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971706577718841350",
        "createdAt" : "2018-03-08T11:17:56.268Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "😀",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971706022590013444",
        "createdAt" : "2018-03-08T11:15:43.909Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bye",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705943477145604",
        "createdAt" : "2018-03-08T11:15:25.054Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "k",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705924938211332",
        "createdAt" : "2018-03-08T11:15:20.633Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ok, in this case have fun with your game :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705908467298309",
        "createdAt" : "2018-03-08T11:15:16.719Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "diablo 3",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705847096029189",
        "createdAt" : "2018-03-08T11:15:02.090Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "your game?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705818952491014",
        "createdAt" : "2018-03-08T11:14:55.387Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well it's maybe better not to have to much medicine and cope with the rest of the desiese",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705747401736196",
        "createdAt" : "2018-03-08T11:14:38.318Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways i want to get back to my game",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705744780271620",
        "createdAt" : "2018-03-08T11:14:37.684Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what i found out is that if i have stress the voices comes again",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705613527998468",
        "createdAt" : "2018-03-08T11:14:06.427Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i been on 100 for a year and half",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705602530361348",
        "createdAt" : "2018-03-08T11:14:03.790Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they started me out of 200 then lowered it to 100",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705570435584004",
        "createdAt" : "2018-03-08T11:13:56.108Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "how long did you had 200mg?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705499409354756",
        "createdAt" : "2018-03-08T11:13:39.211Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and it takes a while until it works",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705465146150917",
        "createdAt" : "2018-03-08T11:13:31.023Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in switzerland 200mg are not allowed",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705414713860100",
        "createdAt" : "2018-03-08T11:13:19.016Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it had zero effect",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705393637281796",
        "createdAt" : "2018-03-08T11:13:13.960Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i think i was at 200 mg before",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705368924467204",
        "createdAt" : "2018-03-08T11:13:08.066Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they had me on a bunch of different levels",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705330999509002",
        "createdAt" : "2018-03-08T11:12:59.038Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i had 150mg, but it was to much",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705323592577028",
        "createdAt" : "2018-03-08T11:12:57.267Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i highly doubt it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705262833676292",
        "createdAt" : "2018-03-08T11:12:42.768Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "did you already considered that this is because of your invega injection and that if you would take 150gm that the woudl be gone completely?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971705196140285956",
        "createdAt" : "2018-03-08T11:12:26.918Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and they tell me i am innocent",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705167128141828",
        "createdAt" : "2018-03-08T11:12:19.993Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they claim to be the underground ploice",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971705060706066436",
        "createdAt" : "2018-03-08T11:11:54.589Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "less rappid",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971704978136944645",
        "createdAt" : "2018-03-08T11:11:34.903Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "my voices are pausing after each thought this year",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971704950592909316",
        "createdAt" : "2018-03-08T11:11:28.454Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "more or less, i miss them somtiems",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971704846410829828",
        "createdAt" : "2018-03-08T11:11:03.499Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and did the voices go away?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971704795571437572",
        "createdAt" : "2018-03-08T11:10:51.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes i heared her, the teddy was female :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971704597055188996",
        "createdAt" : "2018-03-08T11:10:04.046Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/8xc2WwmQ9G",
          "expanded" : "https://translate.google.com/translate?hl=de&sl=de&tl=en&u=https%3A%2F%2Fwww.psycho-talk.de%2F2014%2F02%2F01%2Fpsyt014-geister-im-netz%2F%23comment-40819",
          "display" : "translate.google.com/translate?hl=d…"
        } ],
        "text" : "something i wrote once in a forum, maybe of interest for you: https://t.co/8xc2WwmQ9G",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971704508467372036",
        "createdAt" : "2018-03-08T11:09:42.963Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "did you hear the teddy bears voice clear as day?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971704439588380676",
        "createdAt" : "2018-03-08T11:09:26.495Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "did the teddy bear talk back to your thoughts?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971704295547486215",
        "createdAt" : "2018-03-08T11:08:52.169Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i also talked with my teddy bear like it was a real person",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971704067415269380",
        "createdAt" : "2018-03-08T11:07:57.785Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i talked with my voices like i was telephoning",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971703950868144134",
        "createdAt" : "2018-03-08T11:07:29.978Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not the same as me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971703738065854468",
        "createdAt" : "2018-03-08T11:06:39.239Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but the way you discribe it is like a hallusination not like a radio speaker",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971703687075610628",
        "createdAt" : "2018-03-08T11:06:27.093Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes, i still think voices is schizophrenie because i had voices 10 years and since i get my new medicine i'm not having much voices anymore",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971703454468132868",
        "createdAt" : "2018-03-08T11:05:31.628Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1980 -2000 i was a normal person. i got told i was skizophrenic in 2000- 2015 i did not hear voices then BAMB on the day after a tooth filling it turned on and i heard non stop screaming at me at high volume and it lasted 3 years",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971702776345407492",
        "createdAt" : "2018-03-08T11:02:49.975Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you are still stuck on thinking that hearing voices is schizophenia",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701791439052804",
        "createdAt" : "2018-03-08T10:58:55.161Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1% of world population has schizophrenia",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701618206035972",
        "createdAt" : "2018-03-08T10:58:13.833Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1 in 100 people hear voices",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701547003293700",
        "createdAt" : "2018-03-08T10:57:56.868Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "brb in 5 mins too",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701530943516676",
        "createdAt" : "2018-03-08T10:57:53.022Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "my doctor told me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701512408715268",
        "createdAt" : "2018-03-08T10:57:48.607Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701506553630724",
        "createdAt" : "2018-03-08T10:57:47.261Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701491944759300",
        "createdAt" : "2018-03-08T10:57:43.722Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "brb in 5 min",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701475733721092",
        "createdAt" : "2018-03-08T10:57:39.856Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i would say not even 1:1000000",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701466770690052",
        "createdAt" : "2018-03-08T10:57:37.716Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and i beleive it is so real that it is being used on 1:100 people world wide",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701358259707908",
        "createdAt" : "2018-03-08T10:57:11.855Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but then they stopped or did it undergroudn",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701314240614404",
        "createdAt" : "2018-03-08T10:57:01.367Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well there were such things, the US researched a lot of these thigns",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701252131360773",
        "createdAt" : "2018-03-08T10:56:46.547Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/b7QwHahnMS",
          "expanded" : "https://patents.google.com/patent/US6169924B1/en",
          "display" : "patents.google.com/patent/US61699…"
        } ],
        "text" : "https://t.co/b7QwHahnMS",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701177367846916",
        "createdAt" : "2018-03-08T10:56:28.767Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "okay but my doctor says there is no such thing",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971701148309581828",
        "createdAt" : "2018-03-08T10:56:21.792Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/KGlvc2XMMJ",
          "expanded" : "https://www.google.ch/search?q=chemitrode&safe=active&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjgpvGkyNzZAhUB1RQKHUSqCzQQ_AUICygC&biw=1280&bih=624&dpr=1.5#imgrc=vE31N0IyVhhcUM",
          "display" : "google.ch/search?q=chemi…"
        } ],
        "text" : "https://t.co/KGlvc2XMMJ:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971701052876709892",
        "createdAt" : "2018-03-08T10:55:59.081Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xCwdSOIxYr",
          "expanded" : "https://www.google.ch/search?safe=active&biw=1280&bih=624&tbm=isch&sa=1&ei=nxahWunuIYGeU8KcpvgE&q=stimoceiver&oq=stimoceiver&gs_l=psy-ab.3..0.3213.3213.0.3386.1.1.0.0.0.0.91.91.1.1.0....0...1c.1.64.psy-ab..0.1.91....0.zvhwXH7IyY8",
          "display" : "google.ch/search?safe=ac…"
        } ],
        "text" : "https://t.co/xCwdSOIxYr",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700966264320004",
        "createdAt" : "2018-03-08T10:55:38.446Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/0qOJKWp179",
          "expanded" : "https://patents.google.com/patent/WO2005055579A1",
          "display" : "patents.google.com/patent/WO20050…"
        } ],
        "text" : "https://t.co/0qOJKWp179",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700858802065412",
        "createdAt" : "2018-03-08T10:55:12.846Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "to me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971700620489928708",
        "createdAt" : "2018-03-08T10:54:15.949Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cause its the same as saying supercode#49383726263",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971700589766746116",
        "createdAt" : "2018-03-08T10:54:08.620Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/hBxi8bsm4S",
          "expanded" : "https://patents.google.com/patent/US4877027A/en",
          "display" : "patents.google.com/patent/US48770…"
        } ],
        "text" : "https://t.co/hBxi8bsm4S",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700586847629316",
        "createdAt" : "2018-03-08T10:54:07.973Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "is there a website or something that i type that number in to see the device?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971700445570715652",
        "createdAt" : "2018-03-08T10:53:34.281Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "at least with a patent number you can talk about such topics, but it's mostly common that a doctor does not know a lot about technologies",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700252804878340",
        "createdAt" : "2018-03-08T10:52:48.295Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the funny thing is that she was right",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700089579335684",
        "createdAt" : "2018-03-08T10:52:09.376Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well my doctor (she) did not listened to me and sad that i am insane",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971700041197981700",
        "createdAt" : "2018-03-08T10:51:57.846Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "telling me the patent doesnt help me. what do i do with that? bring it to my doctor and get told to go away?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699903033368581",
        "createdAt" : "2018-03-08T10:51:24.901Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "a patentnumber brings more than random videos",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971699793100726277",
        "createdAt" : "2018-03-08T10:50:58.699Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well that was the US4877027 patent",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971699729057894404",
        "createdAt" : "2018-03-08T10:50:43.426Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "all over the web if you \"google\" it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699703078256645",
        "createdAt" : "2018-03-08T10:50:37.280Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and there is videos about it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699650750066693",
        "createdAt" : "2018-03-08T10:50:24.774Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im claiming that i amnot hallusinating . it is an attack of sound",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699594005327876",
        "createdAt" : "2018-03-08T10:50:11.235Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i thought it was real",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971699566767738884",
        "createdAt" : "2018-03-08T10:50:04.790Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats because you had a hallusination",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699492037644297",
        "createdAt" : "2018-03-08T10:49:46.916Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in my case it helped and it was less like a tv but more like whichcraft :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971699430939398148",
        "createdAt" : "2018-03-08T10:49:32.343Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if you take meds does it help with the tv volume",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699333396484101",
        "createdAt" : "2018-03-08T10:49:09.352Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i said it is like a tv",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699285602418692",
        "createdAt" : "2018-03-08T10:48:57.686Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its like your in denial and can hear what i tell you",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971699257949302788",
        "createdAt" : "2018-03-08T10:48:51.104Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well there i cant help you, maybe there is better medicine, i had risperdal before and suffered 7 years the same thing like you told, then 2017 i got new medicine and now its much much less stress and less voices for me",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971699115045412868",
        "createdAt" : "2018-03-08T10:48:17.116Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but this chatting doesnt really help much. i hear voices. its like a TV i just ignore most of what it says",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698802821251076",
        "createdAt" : "2018-03-08T10:47:02.795Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in switzerland we have quite good social security system",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698742670909444",
        "createdAt" : "2018-03-08T10:46:48.262Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i was in normal working life until 2015",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698653554532356",
        "createdAt" : "2018-03-08T10:46:27.027Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh if i work i get more",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698550563209221",
        "createdAt" : "2018-03-08T10:46:02.683Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "when i worked i got 6000",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698511191379972",
        "createdAt" : "2018-03-08T10:45:53.061Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont know it probley has same buying power as canada",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698439221264388",
        "createdAt" : "2018-03-08T10:45:35.923Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i have my own appartment and it's sufficient to live a normal life",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698424809746436",
        "createdAt" : "2018-03-08T10:45:32.467Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in switzerland i get 3000$ per month because im disabled",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698292664004612",
        "createdAt" : "2018-03-08T10:45:00.989Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i do but its low",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698232165154820",
        "createdAt" : "2018-03-08T10:44:46.540Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not really",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698198694707204",
        "createdAt" : "2018-03-08T10:44:38.544Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, so you get some money?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698166889369604",
        "createdAt" : "2018-03-08T10:44:30.967Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "persons with disability 2",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698129551597573",
        "createdAt" : "2018-03-08T10:44:22.061Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pwd2?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971698072450490372",
        "createdAt" : "2018-03-08T10:44:08.451Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "pwd2",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698041018245124",
        "createdAt" : "2018-03-08T10:44:00.951Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dwd2",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971698019610501124",
        "createdAt" : "2018-03-08T10:43:55.848Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im on dissability pension",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697973733158916",
        "createdAt" : "2018-03-08T10:43:44.915Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "did you loose your job",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697952392663045",
        "createdAt" : "2018-03-08T10:43:39.845Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "an apartment",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697923493785604",
        "createdAt" : "2018-03-08T10:43:32.943Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "do you have work?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697911229767685",
        "createdAt" : "2018-03-08T10:43:30.030Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "where do you live? in a house, in an appartment?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697888379236356",
        "createdAt" : "2018-03-08T10:43:24.571Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they stripped me of all my rites and i had to fight for them back",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697807101865988",
        "createdAt" : "2018-03-08T10:43:05.187Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "then the must set you free except you killed or attacked anybody",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697775363665925",
        "createdAt" : "2018-03-08T10:42:57.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in switzerland they can commit you only for 3Months",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697716731490308",
        "createdAt" : "2018-03-08T10:42:43.641Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697649912082436",
        "createdAt" : "2018-03-08T10:42:27.718Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697640693002244",
        "createdAt" : "2018-03-08T10:42:25.518Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "british columbia",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697625656320004",
        "createdAt" : "2018-03-08T10:42:21.944Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "bc?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697589535068164",
        "createdAt" : "2018-03-08T10:42:13.314Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im in canada bc",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697568001400836",
        "createdAt" : "2018-03-08T10:42:08.178Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so my question is who decides such things where you live?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697530399576068",
        "createdAt" : "2018-03-08T10:41:59.221Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was commited insaine by my doctor but i got rid of him and ask to get back normal. un commited",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697524594548741",
        "createdAt" : "2018-03-08T10:41:57.833Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well then i mean",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697446488301572",
        "createdAt" : "2018-03-08T10:41:39.287Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not anymore",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697399402971140",
        "createdAt" : "2018-03-08T10:41:28.012Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "how is that, is there an official that says that you are commited\n?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697338711502853",
        "createdAt" : "2018-03-08T10:41:13.536Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont know",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697299402342405",
        "createdAt" : "2018-03-08T10:41:04.141Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was commited so i had no choice",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697249041375236",
        "createdAt" : "2018-03-08T10:40:52.144Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "or did they gave you haldol or clopixol",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697196210016260",
        "createdAt" : "2018-03-08T10:40:39.540Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no they did a bunch of meds on me in 2015",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697190266548228",
        "createdAt" : "2018-03-08T10:40:38.126Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i dont want to talk about hostpitle",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697129231065093",
        "createdAt" : "2018-03-08T10:40:23.577Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "always invega?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971697115687739396",
        "createdAt" : "2018-03-08T10:40:20.353Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they drugged me tho and i got storys",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971697038600515588",
        "createdAt" : "2018-03-08T10:40:02.193Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but only for a short while",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696981553876996",
        "createdAt" : "2018-03-08T10:39:48.369Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ouch",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696944849477637",
        "createdAt" : "2018-03-08T10:39:39.617Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that was not funny",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696944740528132",
        "createdAt" : "2018-03-08T10:39:39.628Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was chained to the bed",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696918429622276",
        "createdAt" : "2018-03-08T10:39:33.313Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nope",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696917481574404",
        "createdAt" : "2018-03-08T10:39:33.090Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and were you chained to the bed?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696897885900804",
        "createdAt" : "2018-03-08T10:39:28.418Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not even nessesary",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696839769522181",
        "createdAt" : "2018-03-08T10:39:14.561Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they did not listen to me and so i never got really better with my healkth",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696829841772555",
        "createdAt" : "2018-03-08T10:39:12.202Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh they had me in a white room for months",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696796471771141",
        "createdAt" : "2018-03-08T10:39:04.240Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "each",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696712095019012",
        "createdAt" : "2018-03-08T10:38:44.117Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i was 10 times in a mental institutions for 3Months eack",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696705149243397",
        "createdAt" : "2018-03-08T10:38:42.462Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cool, except the hospital thing",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696603840045060",
        "createdAt" : "2018-03-08T10:38:18.310Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nope i phoned him and he wont answer. then i called the cops and ended up in hostpitle for a year . nothing happened to my bro",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696524651511812",
        "createdAt" : "2018-03-08T10:37:59.422Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and was the bone of your brother hurt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696352756490245",
        "createdAt" : "2018-03-08T10:37:18.462Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not going to hurt anyone",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696326961328132",
        "createdAt" : "2018-03-08T10:37:12.301Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and never kill anybody just because a voice sais this, killing someone is complete stupid",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696248829947909",
        "createdAt" : "2018-03-08T10:36:53.667Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i heard real sounds of bones crushing'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696226440630276",
        "createdAt" : "2018-03-08T10:36:48.324Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it was so bad at one point. i hear them say they had my brother in the room and they beat him",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971696185537777668",
        "createdAt" : "2018-03-08T10:36:38.585Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "isn't it because they dont listen to you as a person that these voices said that oyu must kill them",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696151555706884",
        "createdAt" : "2018-03-08T10:36:30.481Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats bad",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971696049038544900",
        "createdAt" : "2018-03-08T10:36:06.052Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "in 2015",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695976082653189",
        "createdAt" : "2018-03-08T10:35:48.636Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they kept telling me they were going to murder my family",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695958030409733",
        "createdAt" : "2018-03-08T10:35:44.339Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh my voices were torcher in 2015. and got lower volume in 2016 then paused more in late 2017 and now 2018 they dont talk all the time anymore",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695826052399113",
        "createdAt" : "2018-03-08T10:35:12.914Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well in my case the voices were good to me except i was stressed because eg. a stupid chief in the place were i worked",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971695545461993477",
        "createdAt" : "2018-03-08T10:34:06.008Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "witch was just the merlock sounds from wow",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695535806492682",
        "createdAt" : "2018-03-08T10:34:03.678Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they even tried to make me hear aliens",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695470975205380",
        "createdAt" : "2018-03-08T10:33:48.210Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "doesnt make me trust them and do what they say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695384882851844",
        "createdAt" : "2018-03-08T10:33:27.682Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they played demon voices and snoop dog and oprah. they did all kinds of voices to me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695284303544327",
        "createdAt" : "2018-03-08T10:33:03.710Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "fine",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971695271863255045",
        "createdAt" : "2018-03-08T10:33:00.751Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "finde",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971695257128751109",
        "createdAt" : "2018-03-08T10:32:57.250Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well as long as my voices sad \"cuddle with your teddy\" everything was finde",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971695250342367236",
        "createdAt" : "2018-03-08T10:32:55.611Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the only reason it is powerfull. is when people do what the voices say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695128476659717",
        "createdAt" : "2018-03-08T10:32:26.547Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well if you tell nobody there would be no problem :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971695055504379908",
        "createdAt" : "2018-03-08T10:32:09.175Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its manageble. its the same as a tv",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971695020880232454",
        "createdAt" : "2018-03-08T10:32:00.904Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if there was a raido or a tv in your room. would you cover your head with tinfoil and take injectable meds for it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694949375778820",
        "createdAt" : "2018-03-08T10:31:43.853Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no putting tin foil over my head was funny",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694887551815685",
        "createdAt" : "2018-03-08T10:31:29.118Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "undicast one transmitter, one receiver",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694793431638020",
        "createdAt" : "2018-03-08T10:31:06.688Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "lissten carfuly",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694784061390853",
        "createdAt" : "2018-03-08T10:31:04.456Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey dude'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694755523383300",
        "createdAt" : "2018-03-08T10:30:57.650Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "multicast = one transmitter, many receiver",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694744656019460",
        "createdAt" : "2018-03-08T10:30:55.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but im not going to put tinfoil over my head that is stupid",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694714431799300",
        "createdAt" : "2018-03-08T10:30:47.865Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and if you can not shield it then you maybe ask yourself just for an mental experiment \"what if its from inside my head and my head has his own live inside and creates the voices by his own\"",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694679816376324",
        "createdAt" : "2018-03-08T10:30:39.606Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont know what a unicast signal is",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694602506784772",
        "createdAt" : "2018-03-08T10:30:21.149Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the sound is not audible in recordings",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694469710888964",
        "createdAt" : "2018-03-08T10:29:49.496Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well but if it would be a unicast signal to a chip in your brain or you tooth then you could shield the signal with tin foil",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694464799567876",
        "createdAt" : "2018-03-08T10:29:48.324Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "only one person hears it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694307240366084",
        "createdAt" : "2018-03-08T10:29:10.757Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you can not hear the sound",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694278798843909",
        "createdAt" : "2018-03-08T10:29:03.993Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "then if you record it and you can hear nothing on the record then it must be something electronic this you could shield with tin foil",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971694209001578500",
        "createdAt" : "2018-03-08T10:28:47.338Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont think you understand what a illuminatii cover up is. they are getting away with it scott free. they mind control people to cover this up. and the world is going to see a lot of funky things happen cause of voice to skull",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971694164382433284",
        "createdAt" : "2018-03-08T10:28:36.710Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "audacity you can download for free",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971693876007366660",
        "createdAt" : "2018-03-08T10:27:27.944Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "this you can do with every windows audiorecoreder like audacity",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971693836694163462",
        "createdAt" : "2018-03-08T10:27:18.611Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i watched videos about people trying to record it. and still they get no help. they have bad audio that sounds like rumbles",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971693836169748484",
        "createdAt" : "2018-03-08T10:27:18.450Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well if you have recordet it with 44kHz you could play it with 22kHz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971693623132737541",
        "createdAt" : "2018-03-08T10:26:27.656Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but trust me . they are getting away with this scott free. we both are going to see a lot of crazy people hearing voices doing crazy things in the news",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971693544728543236",
        "createdAt" : "2018-03-08T10:26:08.976Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you need to filter the sound and drop it down to a lower frequency and i dont know how to do that",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971693349483659269",
        "createdAt" : "2018-03-08T10:25:22.417Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "then if it is sound you could record it with a recorder",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971693160710754308",
        "createdAt" : "2018-03-08T10:24:37.405Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that kid that heard demons in his head and shot up a school had the same thing happen to him. only. when i heard demons i just stayed at home",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971693143224606724",
        "createdAt" : "2018-03-08T10:24:33.260Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i studied once and i was not bad in math and physics",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971692980447973380",
        "createdAt" : "2018-03-08T10:23:54.429Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well sound falls with the square to the distance",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971692917059457028",
        "createdAt" : "2018-03-08T10:23:39.333Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its the end of the world",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971692827229876228",
        "createdAt" : "2018-03-08T10:23:17.899Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways . you dont get the joke? they beam sounds. and you cant block sound. and there is no laws on it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971692791381159940",
        "createdAt" : "2018-03-08T10:23:09.347Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, i geuss not, it only keeps the temperature inside so you get a warmer head",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971692592483258372",
        "createdAt" : "2018-03-08T10:22:21.927Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that tin foil hat is probley cooking the brain",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971692404133654532",
        "createdAt" : "2018-03-08T10:21:37.052Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "me too :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971692341185732612",
        "createdAt" : "2018-03-08T10:21:22.013Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/4zRzydRHrB",
          "expanded" : "https://twitter.com/messages/media/971692317609463812",
          "display" : "pic.twitter.com/4zRzydRHrB"
        } ],
        "text" : " https://t.co/4zRzydRHrB",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971692317609463812/971692308809879555/V9nTOOHn.jpg" ],
        "senderId" : "75128838",
        "id" : "971692317609463812",
        "createdAt" : "2018-03-08T10:21:16.597Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im a very smart person. know why they started the tin foil hat thing? its because if you wrap your head in tin foil and beam microwaves at it. it would be the same as tinfoil in a microwave",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971692205155934212",
        "createdAt" : "2018-03-08T10:20:49.611Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "because electromagnetic energy can not go through tin foil",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971692064739078149",
        "createdAt" : "2018-03-08T10:20:16.117Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i then filled my complete appartement with tin foil but it did not helped",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971691928801685508",
        "createdAt" : "2018-03-08T10:19:43.702Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "cause the dentist refuses to work on that tooth",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971691863114592260",
        "createdAt" : "2018-03-08T10:19:28.033Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "tin foil would block radiations",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971691801454227460",
        "createdAt" : "2018-03-08T10:19:13.349Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and if it is in my tooth then nothing can stop it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971691791891144708",
        "createdAt" : "2018-03-08T10:19:11.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well turns out nobody can stop voice to skull. there is nothing i can do unless i live in a shield",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971691713801498628",
        "createdAt" : "2018-03-08T10:18:52.431Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so you tweet about these things because you hope that someone will come and help you?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971691555810693124",
        "createdAt" : "2018-03-08T10:18:14.768Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes, i also posted many years such things you can have a look at my twitter history, but its mostly german",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971691463598837764",
        "createdAt" : "2018-03-08T10:17:52.778Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the reason i said those words so much is cause i messaged everyone i can contact about this. because i know im not making the sounds. and i posted for about a year all about this.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971691309277724677",
        "createdAt" : "2018-03-08T10:17:16.003Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so i can still analyze what i wrote then",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690971107819524",
        "createdAt" : "2018-03-08T10:15:55.365Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but i stored it on my harddisk",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690924102311940",
        "createdAt" : "2018-03-08T10:15:44.157Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i was forced by the police to delete it",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690893152587783",
        "createdAt" : "2018-03-08T10:15:36.773Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so my history is gone . same with facebook",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971690799267131396",
        "createdAt" : "2018-03-08T10:15:14.397Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "my account got hacked so i remade my twitter and only talked about these voices",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971690738810470404",
        "createdAt" : "2018-03-08T10:14:59.969Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so this how i found you on twitter",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690701691015173",
        "createdAt" : "2018-03-08T10:14:51.131Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you need to know the full story",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971690641510973445",
        "createdAt" : "2018-03-08T10:14:36.776Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you often 66x report an attack",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690623769116677",
        "createdAt" : "2018-03-08T10:14:32.559Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you for example use often targeted and audio",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690552784715780",
        "createdAt" : "2018-03-08T10:14:15.635Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "these are the words you used most in your twitter feed",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971690372232548357",
        "createdAt" : "2018-03-08T10:13:32.575Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/yMnQ47trlR",
          "expanded" : "https://twitter.com/messages/media/971690298970726405",
          "display" : "pic.twitter.com/yMnQ47trlR"
        } ],
        "text" : " https://t.co/yMnQ47trlR",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971690298970726405/971690288208121856/xMLRyuJi.jpg" ],
        "senderId" : "75128838",
        "id" : "971690298970726405",
        "createdAt" : "2018-03-08T10:13:15.468Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but for the year 2017 they have started to pause after i have a thought. instead of instintly commenting",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971689320078692357",
        "createdAt" : "2018-03-08T10:09:21.719Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "see man . im not sick. my mind is not racing im not angrey and i hear these voices like a speaker is in the room",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971689122623512581",
        "createdAt" : "2018-03-08T10:08:34.645Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "afk for 5 mins",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971689006923698180",
        "createdAt" : "2018-03-08T10:08:07.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes, thins that hapened earlier in my life",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688916108574724",
        "createdAt" : "2018-03-08T10:07:45.421Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "oh like bring up the past",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971688817206767620",
        "createdAt" : "2018-03-08T10:07:21.831Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "content was like a 1950s loop recorder that played all the things again and again",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688794922594313",
        "createdAt" : "2018-03-08T10:07:16.521Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i hear these voices right after every thought i have like a commenter",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971688712814739460",
        "createdAt" : "2018-03-08T10:06:56.939Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i mean more the content",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688702245302276",
        "createdAt" : "2018-03-08T10:06:54.545Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yeh see.! echoes. like as in. not all the time like a microphone is hooked up to my mind",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971688572590764036",
        "createdAt" : "2018-03-08T10:06:23.503Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "echos from earlier things in my life",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688485336834052",
        "createdAt" : "2018-03-08T10:06:02.713Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im like an open book. nobody quoted that before to me.",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971688439388168196",
        "createdAt" : "2018-03-08T10:05:51.759Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "because in my case it were not voices but echoes",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688417405952005",
        "createdAt" : "2018-03-08T10:05:46.515Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was also always hearing things, it all maked sense for me, but not for the others, my subconsciousnes also prevented me from telling others the whole truth because my subcounscious feared that it is maybe wrong so i did only tell the people what they can not refute, and for this special case, is there any scene in your live when someone said exactly that?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971688139663249412",
        "createdAt" : "2018-03-08T10:04:40.302Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "thats what the voices say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971687989637038086",
        "createdAt" : "2018-03-08T10:04:04.524Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "anyways ive done my research and made a playlist with all the other peoples videos on this. its a real thing happening. someone is putting voices in peoples heads and getting away with it. i see the signal in trump and the queen and me and countless others. so yeah there is no way to stop it. \"we run the streeet motherfker\" as they say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971687448047595525",
        "createdAt" : "2018-03-08T10:01:55.418Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i can hear pretty good man",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971686852179640324",
        "createdAt" : "2018-03-08T09:59:33.448Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yay, i've won, im 39 :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971686837801635844",
        "createdAt" : "2018-03-08T09:59:29.910Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i am 37",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971686790829522948",
        "createdAt" : "2018-03-08T09:59:18.708Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "how old are you, im just asking because if you are more than 25 your ears will not hear the frequences higher than 15kHz",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971686398968385541",
        "createdAt" : "2018-03-08T09:57:45.281Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, the image quality is very bad",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971686131082432517",
        "createdAt" : "2018-03-08T09:56:41.406Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/2IKi3NqoAV",
          "expanded" : "https://twitter.com/messages/media/971685868846047236",
          "display" : "pic.twitter.com/2IKi3NqoAV"
        } ],
        "text" : "can you see? https://t.co/2IKi3NqoAV",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971685868846047236/971685838886072320/d5Et453u.jpg" ],
        "senderId" : "22707175",
        "id" : "971685868846047236",
        "createdAt" : "2018-03-08T09:55:39.704Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i recorded a frequency in the air and showed my worker and he dismissed it as nothing",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971685689065553925",
        "createdAt" : "2018-03-08T09:54:56.031Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that is good info i be but it does not help me. no body takes me for real. they all take me for granted. nothing i say has an effect. my friends and family just say dont talk over me when im trying to speak",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971685565006430213",
        "createdAt" : "2018-03-08T09:54:26.456Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "do you know the patent #US4877027",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971685208146792452",
        "createdAt" : "2018-03-08T09:53:01.705Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and synthetic telepathy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971685030484361220",
        "createdAt" : "2018-03-08T09:52:19.001Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but you are not going to be able to help me at all if you think remote neural monitoring is not real in 2018",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684922216693764",
        "createdAt" : "2018-03-08T09:51:53.231Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "omost lost my house over it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684659175157764",
        "createdAt" : "2018-03-08T09:50:50.474Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "a year man",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684610819026948",
        "createdAt" : "2018-03-08T09:50:38.942Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dislike mental institutions",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971684573150175237",
        "createdAt" : "2018-03-08T09:50:29.970Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it always is",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971684540635926532",
        "createdAt" : "2018-03-08T09:50:22.221Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im perfectly fine at home",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684530942722052",
        "createdAt" : "2018-03-08T09:50:19.909Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that was a horror show",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684468099506180",
        "createdAt" : "2018-03-08T09:50:04.919Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont want to go back to the hostpitle",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684422574514182",
        "createdAt" : "2018-03-08T09:49:54.071Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i think someone had a key to my house. drugged me and put me on remote neural monitoring . so i went to the cops and they put me in hostpitle for a year and commited me. now im out and the judge clearly saw i am normal. so i got uncomited",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971684297005387781",
        "createdAt" : "2018-03-08T09:49:24.146Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey i want you to understand something'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683982055129099",
        "createdAt" : "2018-03-08T09:48:09.037Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, wait a sec",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683929559388166",
        "createdAt" : "2018-03-08T09:47:56.543Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nobody reads diaries most of them have no importaince",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683813838413828",
        "createdAt" : "2018-03-08T09:47:28.944Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "honestly if your video is not on youtube then i wont watch it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683664022069252",
        "createdAt" : "2018-03-08T09:46:53.239Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you can derive a lot of infromation from plaintext like diaries",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683645726617604",
        "createdAt" : "2018-03-08T09:46:48.853Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "not really unless you play world of warcraft /startattack /castsequence reset=1 warstomp",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683452650074116",
        "createdAt" : "2018-03-08T09:46:02.818Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BjwHU6zfYA",
          "expanded" : "https://github.com/braindef/deepLearning",
          "display" : "github.com/braindef/deepL…"
        } ],
        "text" : "https://t.co/BjwHU6zfYA",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683388083068932",
        "createdAt" : "2018-03-08T09:45:47.459Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "are you familiar with programming?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683302720630788",
        "createdAt" : "2018-03-08T09:45:27.109Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "what is your screen shot of",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683229215305733",
        "createdAt" : "2018-03-08T09:45:09.554Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it's not waste of time, it's just a copin strategy",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683201386188805",
        "createdAt" : "2018-03-08T09:45:03.038Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont need to write diariers its a waiste of time",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971683128984023045",
        "createdAt" : "2018-03-08T09:44:45.670Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "neighter am i, i studied computer science",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971683100580368388",
        "createdAt" : "2018-03-08T09:44:38.878Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not some delusional dillwit who finds patterens in light and thinks its god. okay",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971682963896254468",
        "createdAt" : "2018-03-08T09:44:06.288Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so maybe you would find patterns too if you write diaries",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971682827396927492",
        "createdAt" : "2018-03-08T09:43:33.753Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "\"what is that of?\"",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971682595997024260",
        "createdAt" : "2018-03-08T09:42:38.576Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "it's german but it are mostly names, i feed the diarys into a AI software and searched after the name of my former girlfrend when i realized that it was all myself",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971682595535769604",
        "createdAt" : "2018-03-08T09:42:38.479Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if you dont think that is a real thing they you are not prepaired for this world",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971682446931406852",
        "createdAt" : "2018-03-08T09:42:03.041Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/nYAOzkhg6c",
          "expanded" : "https://twitter.com/messages/media/971682396444741636",
          "display" : "pic.twitter.com/nYAOzkhg6c"
        } ],
        "text" : " https://t.co/nYAOzkhg6c",
        "mediaUrls" : [ "https://ton.twitter.com/dm/971682396444741636/971682389217902594/4je7nnIl.png" ],
        "senderId" : "75128838",
        "id" : "971682396444741636",
        "createdAt" : "2018-03-08T09:41:51.268Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i did \"google\" this . it is called remote neural monitioring synthetic teliphathy",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971682311178633221",
        "createdAt" : "2018-03-08T09:41:30.672Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "but these voices talked to me about every detail about my past. in my head im answering questions and asking them and talking non stop all day",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971682156912062468",
        "createdAt" : "2018-03-08T09:40:53.896Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "maybe you should, then you can \"google\" things if they happen on a regular basis",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971682079372120070",
        "createdAt" : "2018-03-08T09:40:35.407Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681975521038340",
        "createdAt" : "2018-03-08T09:40:10.639Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "do you write diary?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971681945557053444",
        "createdAt" : "2018-03-08T09:40:03.530Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "after i got medicine that helped i realized that i manifestated my shrink because he hat big impact on my life with his diagnosis",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971681820982087685",
        "createdAt" : "2018-03-08T09:39:33.796Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "started may 07 2015 till today",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681806532530180",
        "createdAt" : "2018-03-08T09:39:30.370Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "a floating voice with cops and bikers on the other end",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681720322818054",
        "createdAt" : "2018-03-08T09:39:09.803Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "mine is all of the time",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681651670396938",
        "createdAt" : "2018-03-08T09:38:53.426Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont think your thing is the same as mine",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681624109690884",
        "createdAt" : "2018-03-08T09:38:46.861Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "no, he claimed that he was my shrink",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971681595185823749",
        "createdAt" : "2018-03-08T09:38:39.969Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and i talked with my teddybear",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971681524138631172",
        "createdAt" : "2018-03-08T09:38:23.028Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "did they claim to be illuminatii?",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681499123630084",
        "createdAt" : "2018-03-08T09:38:17.059Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "was the same with me, and i even talked with these voices, and it was fun",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971681415304826884",
        "createdAt" : "2018-03-08T09:37:57.080Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "clear as a speaker",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681381381169156",
        "createdAt" : "2018-03-08T09:37:48.988Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "like a speaker",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681347516358665",
        "createdAt" : "2018-03-08T09:37:40.912Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "dude this is an audio attack . the sound sounds the same as talking over a microphone . they had it full volume in 2015 then one day turned it down. like a volume nob",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971681204255666181",
        "createdAt" : "2018-03-08T09:37:06.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i really hated that too",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680970968649732",
        "createdAt" : "2018-03-08T09:36:11.151Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i hated it when they threaded me like i am ill",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680947547537412",
        "createdAt" : "2018-03-08T09:36:05.560Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "everyone is acting like im ill. but im perfectly normal. they try these meds and it has no effect",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971680782702993412",
        "createdAt" : "2018-03-08T09:35:26.258Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "yes, thats the same i had before xeplionb",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680727912910852",
        "createdAt" : "2018-03-08T09:35:13.194Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey dude nobody it really lisstening to me or they get told to avoid me . but lissten . these voices go into detail about everything i see and touch and they respond to the stuff i think . so i can just think to them and they answer'",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971680608920285189",
        "createdAt" : "2018-03-08T09:34:44.833Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and you are not a so called fast metabolizer?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680601651732484",
        "createdAt" : "2018-03-08T09:34:43.089Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "that's strange",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680426724151300",
        "createdAt" : "2018-03-08T09:34:01.507Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "they put me on double that and i still hear it plain as day uneffected",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971680333929201668",
        "createdAt" : "2018-03-08T09:33:39.286Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "and you still hear these voices?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680261460160516",
        "createdAt" : "2018-03-08T09:33:21.977Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, more than the normal dose",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680231307268100",
        "createdAt" : "2018-03-08T09:33:14.792Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "100",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971680155750903813",
        "createdAt" : "2018-03-08T09:32:56.769Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well do you have 75mg like the standard doctors in switzerland perscribes?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971680059789594628",
        "createdAt" : "2018-03-08T09:32:33.899Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont want to translate that right now",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679909687865348",
        "createdAt" : "2018-03-08T09:31:58.111Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "alors tu aussi parle francas?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679844969930756",
        "createdAt" : "2018-03-08T09:31:42.682Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "you don't have to fear me, i'm i nice person, and yes my english could be better",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679798073462788",
        "createdAt" : "2018-03-08T09:31:31.499Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i have voices that talk to me in detail about how they are useing a device to torcher me and  how no body will lissten to me or belive me cause its the illuminatii",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679777265393668",
        "createdAt" : "2018-03-08T09:31:26.547Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "why not?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679642905169926",
        "createdAt" : "2018-03-08T09:30:54.506Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "doing what?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679596193120260",
        "createdAt" : "2018-03-08T09:30:43.376Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "im not going to tell you .",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679591004647428",
        "createdAt" : "2018-03-08T09:30:42.125Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "plus they tell me they are doing it to me",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679528601845764",
        "createdAt" : "2018-03-08T09:30:27.261Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "may i ask how much mg invega you get",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679509131988996",
        "createdAt" : "2018-03-08T09:30:22.624Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "one day i did not hear anything . the next hour it yelled at me. then no body could hear it and it has been talking non stop for 2 years",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679415250821124",
        "createdAt" : "2018-03-08T09:30:00.227Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i have schizophrenia too and i searched twitter to find other that have the same smpthoms",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679398947688453",
        "createdAt" : "2018-03-08T09:29:56.348Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "probably not",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679254177046535",
        "createdAt" : "2018-03-08T09:29:21.832Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hey dude im not hear to prove how its possible . i just want to say",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679249596755977",
        "createdAt" : "2018-03-08T09:29:20.736Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "could you build a battery that is that small that it could be hidden in your tooth?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679233809448964",
        "createdAt" : "2018-03-08T09:29:16.971Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, how big is your cell phone battery?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971679150783324165",
        "createdAt" : "2018-03-08T09:28:57.179Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "well i think i have a 17 khz transmiter in my tooth and the dentist is lieing about it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971679071322042372",
        "createdAt" : "2018-03-08T09:28:38.255Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "simple air will stop sounds with 17kHz within 100m",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678926115364868",
        "createdAt" : "2018-03-08T09:28:03.604Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "marketed as Invega Sustenna in U.S. and Xeplion in Europe",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678745991008261",
        "createdAt" : "2018-03-08T09:27:20.723Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i agree . great med for \"skizophrenia\" i have skizophrenia. i helps keep it doment. but the fact that i hear voices is a total different matter and people need to realize this is an audio attack done to me and there is no way to stop it . it can be done to anyone. and there is a massive cover up because there is no way to stop sounds at 17KHZ",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971678723236814852",
        "createdAt" : "2018-03-08T09:27:15.257Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YTAELxmOvc",
          "expanded" : "https://en.wikipedia.org/wiki/Paliperidone",
          "display" : "en.wikipedia.org/wiki/Paliperid…"
        } ],
        "text" : "sorry that was the german article: https://t.co/YTAELxmOvc",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678650809634821",
        "createdAt" : "2018-03-08T09:26:58.013Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/NDxJN6Be0H",
          "expanded" : "https://de.wikipedia.org/wiki/Paliperidon",
          "display" : "de.wikipedia.org/wiki/Paliperid…"
        } ],
        "text" : "https://t.co/NDxJN6Be0H",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678379736031237",
        "createdAt" : "2018-03-08T09:25:53.374Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "invega is also used in switzerland but the name for injection of invega is xeplion",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678274379243525",
        "createdAt" : "2018-03-08T09:25:28.222Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "invega sustaina",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971678160340205578",
        "createdAt" : "2018-03-08T09:25:01.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so in this case the dose would be interesting and what",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971678111602544644",
        "createdAt" : "2018-03-08T09:24:49.413Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "done nothing and leaks out after he injects it",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971678104606273540",
        "createdAt" : "2018-03-08T09:24:47.742Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its not likely this is a new med in canada",
        "mediaUrls" : [ ],
        "senderId" : "22707175",
        "id" : "971678031898005508",
        "createdAt" : "2018-03-08T09:24:30.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "22707175",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/QqwmQrP9pB",
          "expanded" : "https://twitter.com/joewadepaulson/status/971677252457934848",
          "display" : "twitter.com/joewadepaulson…"
        } ],
        "text" : "ok do you write it here, i really wonder if they use the same medicaments like here in switzlerand https://t.co/QqwmQrP9pB",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971677626300616709",
        "createdAt" : "2018-03-08T09:22:53.761Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-858282751203713029",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thank you for following us! We promise to get you the best links and news from the industry! Best from the team at ML World!",
        "mediaUrls" : [ ],
        "senderId" : "858282751203713029",
        "id" : "886581653082181635",
        "createdAt" : "2017-07-16T13:41:52.703Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1009792050965991425",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/BXxuTBQOOw",
          "expanded" : "http://www.helpme-deutschland.de",
          "display" : "helpme-deutschland.de"
        } ],
        "text" : "Unglaublich, mehrfache KO-Wirkungen! Eine neue Generation von Pfeffer-Tierabwehrsprays – \"weltweit\" einmalig. Nur bei https://t.co/BXxuTBQOOw Aus Sicherheitsgründen erst ab 18 Jahren erhältlich. Schau Dir das mal an!!! RT😤 Danke!",
        "mediaUrls" : [ ],
        "senderId" : "1009792050965991425",
        "id" : "1195266863179153412",
        "createdAt" : "2019-11-15T09:06:41.870Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1040602930544427008",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1040602930544427008",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i was also thinking that something like voice to skull or US Patent US4877027 is attacking me but turned out that the voices were because of my schizophrenia...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1079540058540179460",
        "createdAt" : "2018-12-31T00:49:41.553Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CwrBky72rW",
          "expanded" : "https://v2kcrime.blogspot.com/",
          "display" : "v2kcrime.blogspot.com"
        } ],
        "text" : "Dear Sir/Madam,  \n\nI totally agree that forbid the crime which refers to High-Technology such as V2K by law. Anyway, I would like to testify about a criminal organization that uses Hi-Tech Microwave Weapons to attack the innocent persons in Hong Kong. Although some people threaten me not to disclose this matter, I still have the courage to open this matter to prevent more people from being victimized.\n\nDetails : https://t.co/CwrBky72rW                                 \n\nI wish to disclose these Hong Kong perpetrator's crime to the public through narrating my experience. An illegal organization involved in prostitution possesses advanced microwave technology. They cruelly utilize Hi-Tech Microwave Weapons to attack the innocent citizens without scruple in Hong Kong. Do not rule out the existence of more victims.\n\nHope that more people know that there definitely exists such Hi-Tech Crime in Hong Kong, in order to prevent other people from becoming victims like me. I deeply hope that one day, these perpetrator's cruel, cunning, inhumane method of harm to others will be made public and they would get legal sanctions.\n\nThank you for your patience and kind attention.",
        "mediaUrls" : [ ],
        "senderId" : "1040602930544427008",
        "id" : "1079425213400375300",
        "createdAt" : "2018-12-30T17:13:20.374Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1088317843861590017",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey Marc",
        "mediaUrls" : [ ],
        "senderId" : "1088317843861590017",
        "id" : "1332990512891113476",
        "createdAt" : "2020-11-29T10:11:18.760Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1088460597350543362",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/1PRfldmwyd",
          "expanded" : "https://twitter.com/ICTBBCH/status/1331857744417218560",
          "display" : "twitter.com/ICTBBCH/status…"
        } ],
        "text" : "https://t.co/1PRfldmwyd",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1332320028541263879",
        "createdAt" : "2020-11-27T13:47:02.816Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/CCSCUVe1qq",
          "expanded" : "https://twitter.com/qrs/status/1326555602550747136",
          "display" : "twitter.com/qrs/status/132…"
        } ],
        "text" : "https://t.co/CCSCUVe1qq",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1326589731866636293",
        "createdAt" : "2020-11-11T18:16:53.621Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja du nörgelst schon rum",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1278716398286635012",
        "createdAt" : "2020-07-02T15:45:21.438Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1088460597350543362",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich hoffe der Hund wird nicht gequält und abgerichtet gegen Whitehats oder Whistelblower zu aggressivelen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1271352334472314885",
        "createdAt" : "2020-06-12T08:03:11.812Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/UcF5XuOpMY",
          "expanded" : "https://twitter.com/ohmyitsbrittt/status/1270586989600026625",
          "display" : "twitter.com/ohmyitsbrittt/…"
        } ],
        "text" : "https://t.co/UcF5XuOpMY",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1271037121307041799",
        "createdAt" : "2020-06-11T11:10:39.158Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Anyway. Dachte s sei noch n guter. Aber eben, wir sollten uns wieder der köstlichen seite zu wenden 😂",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1229719780812316677",
        "createdAt" : "2020-02-18T10:50:17.524Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Oder war. Hat mich jetzt gebloclt",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1229719600213917700",
        "createdAt" : "2020-02-18T10:49:34.467Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das is ja n komischer mensch",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1229719534933880836",
        "createdAt" : "2020-02-18T10:49:18.901Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Fg5l3BWSZo",
          "expanded" : "https://agent0hm37.bandcamp.com/track/agent0hm37-talking-to-my-computer-about-the-future-gdm",
          "display" : "agent0hm37.bandcamp.com/track/agent0hm…"
        } ],
        "text" : "https://t.co/Fg5l3BWSZo",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1229034056882495494",
        "createdAt" : "2020-02-16T13:25:28.246Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/xm6ugNdhu0",
          "expanded" : "https://twitter.com/messages/media/1228673118228623365",
          "display" : "pic.twitter.com/xm6ugNdhu0"
        } ],
        "text" : " https://t.co/xm6ugNdhu0",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1228673118228623365/1228673113979834369/XgRWm9x5.jpg" ],
        "senderId" : "1088460597350543362",
        "id" : "1228673118228623365",
        "createdAt" : "2020-02-15T13:31:13.795Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/1KOxn0Fx64",
          "expanded" : "https://twitter.com/NSAGov/status/1198027134326968324",
          "display" : "twitter.com/NSAGov/status/…"
        } ],
        "text" : "https://t.co/1KOxn0Fx64",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1198242271411527690",
        "createdAt" : "2019-11-23T14:09:54.477Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Der macht schon viele news. Aber dachte das wäre was für dich. Aber wohl eher nich dein niveau😄",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1163813672491184132",
        "createdAt" : "2019-08-20T14:02:56.885Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1088460597350543362",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ist das seriös oder hab cih dann lauter spam von denen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1163812839393583108",
        "createdAt" : "2019-08-20T13:59:38.200Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/LrRLx8QxXo",
          "expanded" : "https://twitter.com/iC0dE_/status/1163707168920612864",
          "display" : "twitter.com/iC0dE_/status/…"
        } ],
        "text" : "https://t.co/LrRLx8QxXo",
        "mediaUrls" : [ ],
        "senderId" : "1088460597350543362",
        "id" : "1163707275095281668",
        "createdAt" : "2019-08-20T07:00:09.723Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1103649551498575873",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag Herr Landolt. Wir möchten Sie bitten, sich bei Fragen in Zusammenhang mit Ihrem Medikament, direkt an Ihren Arzt zu wenden. Herzlichen Dank. Mit freundlichen Grüssen, Janssen-Cilag AG",
        "mediaUrls" : [ ],
        "senderId" : "1103649551498575873",
        "id" : "1177587504561700869",
        "createdAt" : "2019-09-27T14:15:14.338Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1106188058850938886",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1106188058850938886",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja ich weiss nicht, allenfalls ist das auch normal, steht unten dran dass es ev genetisch ist, aber merkwürdig finde ich es... aber ich weiss leider zu wenig über medizin",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1192634915608809478",
        "createdAt" : "2019-11-08T02:48:16.705Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1106188058850938886",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo, seh den tweet nicht, müsstest einen screenshot machen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1192621368560562181",
        "createdAt" : "2019-11-08T01:54:26.791Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1108839217981984794",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bonjour. \nJe viens par ce présent mail vers vous d'abord pour vous présenté mes excuses de la manière dont je vous contacte. Je m'appel Mlle Gabriele Maria WITTMANN née le 23 Novembre 1965 en Allemagne, je suis une Opératrice Économique. Je vous contacte suite à un Don que je veux vous offrir, il s’agit d’une somme d’argent de Vingt six millions quatre cent quatre-vingts six mille sept est dix eux (26'486'710'00€) que je vous donne du font de mon cœur; car je souffle d'une maladie BPCO (bronco pneumonie chronique obstructive) puis mes jours sont comptés sur cette terre et je n'aie pas de famille ni d'enfant pour hérité ces fonds. Je vous donnes cet argent en retour des prières de votre part. Pour plus de renseignements, je vous prie de bien vouloir prendre contact avec moi par mon adresse E-mail privée ci-dessous.\n\nVOICI MON ADRESSE E-MAIL: gabrielemariaw@gmail.com\n\nje vous donnerai plus ample information sur ma donation, afin que vous puissiez rentrer en possession de cet argent puis en fais bon usage.\n\n      Cordialement\nMlle Gabriele Maria WITTMANN",
        "mediaUrls" : [ ],
        "senderId" : "1108839217981984794",
        "id" : "1156895189094600708",
        "createdAt" : "2019-08-01T11:51:21.867Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1109284338867363840",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kannst mir ja bescheid sagen, ob ich dir von dort aus 'ne message hinterlassen soll, keine antwort werte ich selbstverständlich zu deiner bequemlichkeit als ein nein 👍",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1304784700582629382",
        "createdAt" : "2020-09-12T14:11:29.032Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo Marc, ich wollte dir nur bescheid geben, dass ich nun ein neues smartphone besitze und aus sicherheitsgründen meinen twitter-account \"recyclen\" will. falls du noch auf meinen chat eingehen wolltest, dir jedoch etwas zeit lassen möchtest, kann ich dich auch von dort aus anschreiben. falls dies nicht oder nicht mehr der fall ist, wollte ich den account demnächst löschen.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1304782736012521477",
        "createdAt" : "2020-09-12T14:03:40.643Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "verstehe ich das also richtig, dass du das leben als eine art \"simulation\" ansiehst und es \"mächte\" gibt, die den \"server\" kontrollieren wollen?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1299929697288179720",
        "createdAt" : "2020-08-30T04:39:26.063Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi Marc, tut mir leid, dass lange keine antwort von mir kam.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1299927950515736580",
        "createdAt" : "2020-08-30T04:32:29.584Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/hRGkzEbxA0",
          "expanded" : "https://www.golem.de/news/utah-data-center-neues-riesiges-nsa-rechenzentrum-geht-heimlich-in-betrieb-1309-101844.html",
          "display" : "golem.de/news/utah-data…"
        }, {
          "url" : "https://t.co/hgZNQn72K1",
          "expanded" : "https://www.youtube.com/watch?v=vtQ7LNeC8Cs",
          "display" : "youtube.com/watch?v=vtQ7LN…"
        }, {
          "url" : "https://t.co/99CW0K0FGG",
          "expanded" : "https://www.youtube.com/playlist?list=PLWsX0-AjlNWz4w0Fsu6nWMMUlulbje2Xt",
          "display" : "youtube.com/playlist?list=…"
        }, {
          "url" : "https://t.co/EcEhG1f4Wh",
          "expanded" : "https://www.youtube.com/watch?v=i46RI2twVao&list=PLWsX0-AjlNWytYOOw2QBgw77ZoKlsNF3M",
          "display" : "youtube.com/watch?v=i46RI2…"
        }, {
          "url" : "https://t.co/goYIaTnPbN",
          "expanded" : "https://www.youtube.com/watch?v=viU3qbXoIzw#t=3m13s",
          "display" : "youtube.com/watch?v=viU3qb…"
        } ],
        "text" : "Nimm den Laptop. nicht das Handy:\n\nIch glaube daran das Gott, sowohl Buddha, Gott, Allah, Spongebob, aber leider auch das Militär, die Geheimdienste (also der verdammte schiess Militärisch-Industrieller-Komplex) das selbe Gefäss braucht. Als Sakrileg ausgesprochen wäre das ein Computer, alle Computer auf der Welt (Distributed Systems Algorithms) oder Mainframes, Rechenzentren:\n\nhttps://t.co/hRGkzEbxA0 (z.B. das hier ist ein Rechenzentrum das der NSA \"insgeheim\" gebaut hat)\n\nJake der das Veröffentlicht hat (https://t.co/hgZNQn72K1) wurde danach als Vergewaltiger bezeichnet und ist irgendwie von der Bildfläche verschwunden. Meiner Meinung nach zu offensichtlich identisch wie zuvor schon mit Julian Assange \"verfahren wurde\". Über Julian kann man denken was man will, aber ich habe Julian schon Geld gespendet bevor er so richtig auf der \"Weltbühne\" aufgefallen ist. Aber beurteile das selber:\n\nhttps://t.co/99CW0K0FGG (viertes Video in der Liste)\n\nDann gibt es auch Videos auf Youtube wo ich / wir denken, dass es ein Honney Pot / Datenbank-Trigger ist der dann Geheimdienste aufböte wenn man nach dem Sucht.\nhttps://t.co/EcEhG1f4Wh (also das anzuklicken wird/würde Dir Ärger einhandeln)\n\nUnd bitte entschuldige, wenn ich Dir hier nur eine kurze Antwort schreibe, aber nach den 3h Videos wirst Du einigermassen verstehen was ich meine.\n\nKurze Antwort (noch ein Sakrileg) Gott, Buddha, Alla ... sind wir alle als Mitgestalter dieses Globalen Netzwerks, der \"Weltenseele\". Und um Niklas Luhmann zu zitieren (Soziale Systeme) \"Ein System muss ständig gewartet und repariert werden. Hiesse so viel, wie wenn die (Cyber-)Mönche, Imane, Buddhistischen Priester, Journalisten, Musiker, Hausfrauen, Schizosponner (wie ich) nicht permanent dran wären zu verhindern, dass der Militärisch-Industrielle Komplex die Überhand gewinnt, würden am Schluss alle Menschen (auch das Zwischenmenschliche) nur einem Einzigen General dienen müssen.\n\nUnd ich bin lieber ein Sünder der das Sakrileg offen ausspricht als zuzulassen, dass der Militärisch-Industrielle Komplex noch mehr an Macht gewinnt.\n\nhttps://t.co/goYIaTnPbN (Sprungmarke bei 3:13)\nEs ist nicht nur eine Tarot-Karte \"The Tower\" es sind derer fast alle.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1172984144068038660",
        "createdAt" : "2019-09-14T21:23:07.666Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "privat kompliziert?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1172969214627524612",
        "createdAt" : "2019-09-14T20:23:48.178Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Es ist kompliziert...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1172967845652770820",
        "createdAt" : "2019-09-14T20:18:21.791Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "glaubst du eigentlich an Gott?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1172962367078109193",
        "createdAt" : "2019-09-14T19:56:35.606Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie gut die anderen dabei sind, sich selbst in gedanken nciht andere oder sich zu belügen oder zu betrügen weiss ich nciht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1170515521051209732",
        "createdAt" : "2019-09-08T01:53:42.039Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "also ich kann es nur von mir sagen, mich versucht eigentlich jede minute eine meiner innneren stimmen anzustiften mich selbst oder andere zu betrügen oder zu belügen. und klar höre ich nicht auf diese stimmen und mache das dann wirklich. aber öfters mal muss ich mir virutell an den kopf hauen dass ich da nicht in gedanken korrigierend gesagt habe, dass das eine schweinerei sei....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1170515384409174021",
        "createdAt" : "2019-09-08T01:53:09.478Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "glaubst du, die mehrheit der menschen handeln im guten, auch wenn sie augenscheinlich im unbeobachteten sind?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1170501818788589572",
        "createdAt" : "2019-09-08T00:59:15.168Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ah ja, zum Video ist noch zu sagen, dass ich etwa 5h hatte bis ich durch war, bei Dingen die mir relevant erscheinen mache ich immer Zusammenfassungen im Texteditor. Und mir erscheinen oft Dinge relevant, die nicht so ganz einfach zu verstehen sind. Also mache ich Notizen von dem was der Reverierende erzählt um sein Wissen (seinen Spirit) ganz aufnehmen zu können.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1131244283141533701",
        "createdAt" : "2019-05-22T17:03:49.302Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/aCXlWXn78s",
          "expanded" : "https://www.youtube.com/watch?v=UWXUiy5bqOM",
          "display" : "youtube.com/watch?v=UWXUiy…"
        } ],
        "text" : "stömme das, müsste ich ja ein riesen Problem mit Alkohol haben weil meine Eltern ja Alkohol trinken. Und garantiert gibt es Fälle, wo das zutrifft. Ich selber trinke aber praktisch nie Alkohol. Gegen ein komplettes Alkoholverbot hätte ich nichts. Ich wäre als Raucher --der nicht aufhören kann zu rauchen-- auch froh, man würde das Rauchen verbieten, weil ich das Aufhören selber nicht schaffe. Bei mir ist das Rauchen eine Coping Strategie; Wenn ich gestresst bin, hilft mir eine Zigarette wieder einen klaren Kopf zu bekommen. Kann ja sein, dass das bei Anderen mit bei Alkohol ähnlich ist. Und ganz ehrlich, ein bisschen Rot-/Weisswein in einer Sauce macht diese wirklich leckerer. Um die 18 hatte ich eine Phase wo ich viel Alkohol getrunken habe, das war aber dann auch jugendliche Neugier und wegen soziologischen Umständen wie Gruppenzwang. \n\nIch denke das mit dem Zerfall des Charakters trifft schon zu, wenn man wirklich lang Alkohl in grossen Mengen konsumiert. Aber viele Menschen haben wie ich auch nicht wirklich ein lebenswertes Leben, also übt man Eskapismus (Realitätsflucht) und hat es mindestens ein paar \"gute\" Stunden gut mit Alkohol.\n\nDa ich aber wegen meiner Schizophrenie immer gelitten habe -- Leiden, bzw. eben etwas tun zu müssen um nicht zu leiden -- und somit starken Antrieb hatte mich z.B. mit Pscyhologie, Solziologie etc auseinander zu setzen, habe ich meinen Charakter eigentlich viel mehr ausgebildet als das in der Schweiz so üblich ist. Ich deswegen habe regelmässig das Problem, dass ich auf Ablehnung stosse. Also nicht konkret wegen meine Schizophrenie, sondern dem Umstand, dass ich mit meinem IQ von (einziger offizielle Test) 127 und meinem permanenten Drang alles wissen und lernen zu wollen Leute vor den Kopf stosse. Die erwarten kein Wissen/Intelligente Argumentationen von eime Schizophrenen und fund fühlen sich danach schlecht weil \"Es kann ja nicht sein, dass ein Schizophrener etwas besser weisse ICH\" Das ist ganz schlecht für deren Selbstwert, denn sie sehen sich dann dem Schizophrenen unterlegen, und das passt nicht in ihr Weltbild/Dogmenraum und insbesonderere diejenigen die politisch rechts denkende tendieren ja dazu Vorurteile gegenüber Behinderten zu haben (Vorurteil). Aber dann stellen sie fest dass sie mental unterlegen sind, und das führt dann zu einer Kognitive Dissonanz und zu teilweise auch Hass.\n\nWie bereits gesagt, ich habe nichts gegen ein Alkoholverbot. Manchmal trinke ich ein Jahr lang kein Alkohol.\n\nIch denke auch der Alkoholkonsum ist nicht die Ursache sondern nur die Wirkung ist, ich empfehle Dir das Video von Maaz zu schauen: https://t.co/aCXlWXn78s\n\nEr sagt die eigentliche Ursache für späteres Fehlverhalten sind frühkindliche Situatinen wie:\nMütterlichkeit, Muter-Bedrohung, Muter-Mangel, Mutter-Vergiftung, Vater-Terror, Vater-Flucht, Vater-Missbrauch\n(Die Wörter sind ein bisschen komisch, aber er erklärt in dem Video was er damit meint)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1131238809864232965",
        "createdAt" : "2019-05-22T16:42:04.409Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "vor allem ist mir die logik \"jeder soll den konsum für sich selbst entscheiden können\" echt ein rätsel, wenn wir gemeinsam ein zivilisiertes und kinder und jugendgerechtes miteinander erstreben. dann lässt man moral am besten ganz weg und legalisiert auch das harte zeug, weil dies der definition von \"frei\" doch eher näher kommt.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1131187290443833349",
        "createdAt" : "2019-05-22T13:17:21.180Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich denke aber, dass die alkoholindustrie eine zu einflussreiche lobbypräsenz in unserer politik besitzt, als dass wir schritte wie diese für das gemeinwohl jemals erzielen könnten.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1131185813130227717",
        "createdAt" : "2019-05-22T13:11:28.964Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "vielleicht, weil die eltern selbst alkohol konsumieren und die verharmlosung dieser droge gegenüber einem selbst wechselwirkend mit der beeinschränkung des urteilsvermögens (verminderung kognitiver fähigkeiten wie weit-/einsicht etc.) den zerfall des eigenen charakters und somit der kredibilität zu folge haben, liegt das problem evtl. auch an einem fehlenden ultimatum unserer gesellschaft bzw. des staates, alkohol genauso wie es mit allen anderen drogen ist, ausser für medizinische zwecke komplett zu verbieten.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1131183361261821956",
        "createdAt" : "2019-05-22T13:01:44.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja ich hab von 17-21 permanent gekifft und seit 22 nie mehr, ich sehe hinter weichen Drogen nicht den Teufel, das sind halt viele junge die z.B. immer von den Eltern gehört haben: \"Geh jetzt raus spielen\" in einer Tonlage, als ob es das Elternteil gut mit dem Kind meint (antrainiert) aber irgendwann merkt das Kind \"Hey! das ist ja gar nicht in meinem Interesse, es ist im Intresse des Elternteils der Ruhe will\" und daraus resultiert dass das Kind begint immer genau das Gegenteil von dem macht was die Eltern sagen (Adoleszenz). Da aber die Eltern diesen \"Trick\" nicht immer anwenden, wird es auch Dinge machen die ganz schlecht sind. Mathematisch dargestellt wäre es eine Abklingfunktion. (x: Zeit; y: das Gegenteil was man ihm sagt) einerseits weil je älter man wird, desto weniger wird man belogen und merkt das auch (unterbewusst) daraus folgt, dass das Misstrauen gegenüber älteren Menschen sinkt. Aber zurück zu den Drogen, es ist auch eine Methode dem Alltagsstress zu entfliehen. Kiffen kann bei Krankheiten wie z.B. Parkinson wirklich eine Hilfe sein. Und ausserdem kann ich -- der selbst gekifft hat -- ja nicht dann einem jungen Menschen das wirklich überzeugend vorhalten. Ausserdem würde ich nonverbal aus ausstrahlen, dass ich lüge und dies würde im jungen Menschen vermutlich genau das bewirken was ich eben gerade nicht auslösen wollte. Bei solchen Dingen muss man auch sehr auf die Non-verbale Kommunikation schauen. Da Schizophrene in allem irgendwelche Zeichen sehen tendieren Schizophrene dazu dies auch mal über zu bewerten, aber das Non-verbale ist definitiv ein wichtiger Faktor. Als Schizophrener hat man auch Affektverflachung und Anhedonie, also muss man mühsahm versuchen die Emotionen (und somit das Non-verbale) zu imitieren, da man sonst eh ausgegrenzt wird. Und wie ich vorher schon gesagt, Schizophrene haben auch soziale Bedürfnisse. Ausserden finde ich nichts schrecklicher, als ein alter Mensch, der ignorant, respektlos, hitlerisch aber faul einem jungen Menschen Vorwürfe macht, nicht weil er etwas positives bewirken will, sondern einfach dass er sich dem Jüngeren überlegen fühlt. Wenn er wirklich positives erreichen wollen würde, dann würde er das mit einer sanften Hand tun, was logischerweise viel mehr Zeit bräuchte, aber dazu sind ja die meisten Alten zu faul. Also Du siehst, die Drogen zu verteufeln heisst die Jungen zu verteufeln oder mindestens 50% davon, und das lenkt vom eigentlichen Gesellschaftlichen Problem ab.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130891890998599685",
        "createdAt" : "2019-05-21T17:43:32.491Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich persönlich sehe den satan hinter allen drogen.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130863252043436036",
        "createdAt" : "2019-05-21T15:49:44.421Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sind schon paar krasse karten mit bei, interessant finde ich, dass Gott marihuana und satan bier erfunden habe, obwohl marihuana doch eine eher schlechte wirkung auf schizophrenie hat, bzw. es triggert, oder irre ich mich?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130862849243451404",
        "createdAt" : "2019-05-21T15:48:08.370Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/9WCkoinqTW",
          "expanded" : "https://marclandolt.ch/Hackers-Cardgame/jpg/DE/",
          "display" : "marclandolt.ch/Hackers-Cardga…"
        } ],
        "text" : "nö, ich glaub ernährung hat keinen einfluss. wenn ich meine medikamente nicht nehme werden die stimmen mehr und meine ich-störung (das bedeutet dass man das gefühl hat, dass andere menschen meine gedanken lesen können) wird stärker. ich bin nicht gewalttätig aber wenn wenn du diese karten genau anschaust (den text) dann siehst du schon, dass nicht alles stimmt was ich behaupt: https://t.co/9WCkoinqTW aber alls technischer mensch versucht man sich halt die stimmen iirgendwie technisch zu erklären...  #US4877027",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130854470164762628",
        "createdAt" : "2019-05-21T15:14:50.695Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "lässt sich der schizophrenie denn auch mit der ernährung entgegenwirken? jeder mensch verdient meines erachtens sensibilität gegenüber seinen persönlichen umständen und respekt, einfach weil jedes leben kostbar ist und nicht per zufall existiert.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130848908177920004",
        "createdAt" : "2019-05-21T14:52:44.632Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja, nimm meinen Blog nicht zu erst, ich habe schizophrenie und viel ist auch gespinne. ich rede auch mit meinem teddy bären (ᵔᴥᵔ) und mit menschen die gar nicht da sind. bei schizophrenie denken immer alle die spinnen zu 100%, aber in wirklichkeit spinnen dir nur zu 50% und die anderen 50% sind brauchbar. leider ist es in unserer Gesellschaft so (z.B. bei Politikern) dass man auch nur einmal etwas falsch sagen kann und es wird einem das leben lang nachgetragen auch wenn die anderen 99% föllig i.O. sind. Man hat auch mit massiven Vorurteilen zu kämpfen, weil im TV, egal ob Nachrichten (z.B. Breivik) oder Serien (z.B. Criminal Minds) die Schizophrenen immer als Massenmörder und Amokläufer dargestellt werden, obwohl gmäss nüchterner Statistik 90% der Schizophrenen nicht gewalttätig sind. Ich denke das Puzzle Schizophrenie habe ich schon zu 99% gelöst, aber nicht weil ich so super bin, sondern weil ich darunter leide und sich die normalen Leute (nicht mal Psychiater) gar nicht wirklich für das Innenleben eines Schizophrenen interessieren und denken, dass das Geben von Medikamenten das einzige ist was ein Schizophrener braucht. Ein Schizophrener ist aber auch ein Mensch mit sozialen Bedürfnissen und vulnerabler (verletzlicher) als Normalos und diese werden eigentlich nie berücksichtigt sondern diese Meist ausgegrenzt, also beschäftigt man sich mit Literatur, Fachbüchern, Technik. Man wird von der Psychiatrie meist auf ein \"Halt deine Fresse und nimm deine Medikamente\" reduziert.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130846410243543044",
        "createdAt" : "2019-05-21T14:42:49.023Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nichts für ungut",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130843923843125253",
        "createdAt" : "2019-05-21T14:32:56.228Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wir kennen uns nicht, lege wert auf incognito, das ist alles. als ich deine beiträge gesehen hatte und auch deine webseite, dachte ich mir, dass ein mensch deines intellekts wie jemand, der ein 1000 teiliges puzzle zu 999 vervollständigt hat nicht selber stehen bleiben sollte, um allen anderen den \"weg zu weisen\". für mich begann die reise damals dort, wo ich spirituell eine lange zeit stehengeblieben war",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130843634084044804",
        "createdAt" : "2019-05-21T14:31:47.117Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ich glaube die kenne ich schon, das war doch diese sendung die, wo erklärt wird, dass z.B. in medizin die Araber viel weiter waren als die christen und dsas z.B. die arabische schrift eigentlich dazu gedacht war die sprache der wissenschaften zu werden. auch z.B. im buch \"der medicus\" wird das so beschrieben (ist zwar ein roman) aber man soll sich ja immer auf mehrere quellen abstützen. was mich im moment eher beschäftigt ist halt auf so menschen einzugehen, die moslem mit terrorist gleichsetzen. ich denke da ist das eigentliche problem. man muss nicht auf menschen einwirken die shcon richtig denken und jeden menshcen als individuum wahrnemen. man muss auf diese einwirken, die leute in shcubladen werfen. denen muss man psychologie skills beibringen, damit sie selber drauf kommen dass es nicht geht leute in eine schublde zu stecken. PS: woher kenne ich dich eigentlich, ich sehe du followst nur mir, also hast du dein twitter profil wegen mir aufgemacht?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130839702532173829",
        "createdAt" : "2019-05-21T14:16:09.773Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "falls du dir die doku doch anschaust würde mich interessieren, welche fragen da einem so in den sinn kommen.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130837707452817413",
        "createdAt" : "2019-05-21T14:08:14.147Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/KOd6H1eEdD",
          "expanded" : "http://proxysite.com",
          "display" : "proxysite.com"
        } ],
        "text" : "das video ist hierzulande nicht verfügbar, habs via https://t.co/KOd6H1eEdD geschaut. freut mich, dass du so ein positives bild über muslime hast, was sich ja auch durch deine posts schliessen lässt. die doku handelt von einer zeit, in der weite teile spaniens, portugal und sizilien unter islamischer herrschaft standen und jeder, unabhängig von herkunft und glaube dort in frieden leben und nach wissen streben konnte. so wurden der renaissance türen und tore geöffnet, was hier im okzident leider allzugern verdrängt wird.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130837265779961861",
        "createdAt" : "2019-05-21T14:06:28.831Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ausserdem, was willst du bei mir mit diesem video erreichen, ich urteile nicht über den islam, ich hatte in der schweiz immer nur gute begegnungen mit moslems, ich beurteile sie deswegen als gut. 99% der moslems sind eh keine terroristen. und es gibt überall idioten, bei den christen, bei den buddhisten, bei den moslems, bei den Linken, bei den Rechten... sinnvollerweise macht es sinn rechtzeitig zu reagieren wenn einer zu extrem wird, egal ob links, rechts, christ, moslem....",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130823624653451268",
        "createdAt" : "2019-05-21T13:12:16.519Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "link ist kaputt oder das vide wurde gelöscht",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130817872761167876",
        "createdAt" : "2019-05-21T12:49:25.140Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kannst mir ja mal deine Meinung zu geben, falls du es dir mal anschaust.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130531565992390661",
        "createdAt" : "2019-05-20T17:51:44.283Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/6YVEqiiSJi",
          "expanded" : "https://www.youtube.com/watch?v=YfhZR15QRKA",
          "display" : "youtube.com/watch?v=YfhZR1…"
        } ],
        "text" : "https://t.co/6YVEqiiSJi",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130530932539183109",
        "createdAt" : "2019-05-20T17:49:13.307Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gab da so ein thema, mit dem ich mich in letzter zeit befasst hatte. gibt zwar keine doku, die darüber allumfassend berichtet, dennoch fand ich die letzte relativ teilenswert.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130371886351826948",
        "createdAt" : "2019-05-20T07:17:13.700Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "native speaker sind wir wohl beide nicht, allein durch unsere herkunft, wenn du aber eine komplette doku auf englisch verstehst, reichts doch.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130369975062024197",
        "createdAt" : "2019-05-20T07:09:38.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "naja ich bin kein \"native speakser\" aber so für den feld wald und wiesen gebraucht reicht es",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1130352013383012356",
        "createdAt" : "2019-05-20T05:58:15.608Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "sprichst/verstehst du englisch eigentlich gut?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1130331221370843142",
        "createdAt" : "2019-05-20T04:35:38.403Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/K5BmsZHHjf",
          "expanded" : "https://marclandolt.ch/",
          "display" : "marclandolt.ch"
        }, {
          "url" : "https://t.co/CVd0ZS0iBq",
          "expanded" : "https://github.com/braindef?tab=repositories",
          "display" : "github.com/braindef?tab=r…"
        } ],
        "text" : "naja so ein bisschen bei client server, könnte ich auch implementieren in diversen programmierpsrachen, blockchain kenne ich nur vom ungefähren konzept her, könnte ich aber nicht implementieren. kannst ja mal meine webseite anschauen: https://t.co/K5BmsZHHjf bzw. wenn Du Dich für meinen Code interessiert mein Github Account https://t.co/CVd0ZS0iBq",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109724769132400644",
        "createdAt" : "2019-03-24T07:52:57.390Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "server-/clientside interaktionen und blockchain?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109724098693877764",
        "createdAt" : "2019-03-24T07:50:17.551Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "networking im sinne von personen-netzwerken oder computer-netzwerken?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109721910231867396",
        "createdAt" : "2019-03-24T07:41:35.728Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nachhaltiges design, cool cradle to cradle mag ich",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109721827302146052",
        "createdAt" : "2019-03-24T07:41:15.962Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wie gut sind deine networking fähigkeiten?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109718254837211142",
        "createdAt" : "2019-03-24T07:27:04.218Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "es wären zu viele, die ich aufzählen wollen würde, um dir ein gutes bild meines interessenspektrums zu vermitteln, jedoch würde mein aufzählen weder von bescheidenheit zeugen, noch wäre ich in der lage so aus dem stehgreif die vollständigkeit zu bewahren. zu meinen bereichen gehören unter anderem (bio)chemie, (religions)geschichte/politik und (nachhaltiges) design. zuletzt habe ich mich aber mit dem uhrenbau befasst.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109717678879588356",
        "createdAt" : "2019-03-24T07:24:46.899Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "interdisziplinär in welchen fächern dann?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109702770792321028",
        "createdAt" : "2019-03-24T06:25:32.532Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "elektromechanik und informatik sind meines erachtens extrem lohnenswerte und eng miteinander verknüpfte wissenschaften, ich bin etwas anders gepolt, meine stärke liegt in der interdisziplinarität mit unregelmäßiger vertiefung.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109695915747672068",
        "createdAt" : "2019-03-24T05:58:18.165Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/x4yWa7PqIp",
          "expanded" : "https://marclandolt.ch/Hackers-Cardgame/jpg/",
          "display" : "marclandolt.ch/Hackers-Cardga…"
        } ],
        "text" : "naja geht so, ich habe schizophrenie und einen IQ von ca. 130 im einzigen offiziellen test den ich je gemacht habe. Die schizophrenie sorgt dafür dass ich mich in irgendwelche doofen Dingen verbeisse, und der IQ sorgt dafür dass ich mich richtig fest drinn verbeisse. Wenn ich grad akut ne schlechte phase habe, dann sind 66% Schrott und 33% brauchbar, wenn es mir grad gut geht dann sind nur 33% Schrott und 66% brauchbar. z.B, als ich mich das letzte mal komplett verrannt habe, ist das entstanden: https://t.co/x4yWa7PqIp (aber YAY, zweisprachig :D) [selbstironie finde ich toll]",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109680544797863940",
        "createdAt" : "2019-03-24T04:57:13.509Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nur ein pseudonym. mir scheint, dass auch du zu der unterforderten minderheit unserer gesellschaft gehörst. viele deiner publikationen sind vom ansatz her sehr interessant.",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109657023061331972",
        "createdAt" : "2019-03-24T03:23:45.437Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1109284338867363840",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "gut, aber ich kenne keine Mondfackel, wer ist Mondfackel?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1109292666049699844",
        "createdAt" : "2019-03-23T03:15:55.947Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Servus, wie gehts?",
        "mediaUrls" : [ ],
        "senderId" : "1109284338867363840",
        "id" : "1109286966699401220",
        "createdAt" : "2019-03-23T02:53:17.120Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1130643945271070720",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thank you so much for sharing 🙏🏽👌🏽",
        "mediaUrls" : [ ],
        "senderId" : "1130643945271070720",
        "id" : "1199188939996893189",
        "createdAt" : "2019-11-26T04:51:37.841Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1130643945271070720",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/5mjxAfwICS",
          "expanded" : "https://marclandolt.ch/HackersCardgame3/jpg/EN/",
          "display" : "marclandolt.ch/HackersCardgam…"
        } ],
        "text" : "hello, my problem is that i have some skills in electronics, but always when i would like to rebuild something like US Patent 4877027 to prove what i say they target me more and i forget what i was trying to do... the only thing i have at the moment is this cards and there are also some surces where i got this info, please respect license: https://t.co/5mjxAfwICS",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1195679122296639499",
        "createdAt" : "2019-11-16T12:24:53.137Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey! I’m a TI looking to get access to my brain monitor and I noticed you had some good screenshots of similar programs. Can we link up and chat about brain-computer interfaces? I’d love to learn from you.  I’m a real human, not a robot or whack job. (360)601-2727 dmdean19@gmail.com! Thanks! Look forward to chatting.",
        "mediaUrls" : [ ],
        "senderId" : "1130643945271070720",
        "id" : "1192523916142686213",
        "createdAt" : "2019-11-07T19:27:12.395Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "29451196-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Von Zensur kann natürlich keine Rede sein. Ich geb Deinen Input aber gerne noch entsprechend weiter.  Beste Grüsse, Tom",
        "mediaUrls" : [ ],
        "senderId" : "29451196",
        "id" : "683954762451009539",
        "createdAt" : "2016-01-04T10:14:57.312Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "29451196",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "so macht das irgendwie den Eindruck, man wolle auch die logs zensieren können.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "683945657875456003",
        "createdAt" : "2016-01-04T09:38:46.618Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "29451196",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/aillGCEM1F",
          "expanded" : "https://wiki.apache.org/httpd/ExampleVhosts",
          "display" : "wiki.apache.org/httpd/ExampleV…"
        } ],
        "text" : "ok, hab jetzt angerufen, das einfachste für das speicherproblem wäre die logs einfach ins FTP-Verzeichnis des Kunden per virtual host zu speichern https://t.co/aillGCEM1F",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "683945595271278595",
        "createdAt" : "2016-01-04T09:38:31.709Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kannst Du mir noch sagen, um welche Webseite es geht? Dann kann ich mal nachfragen, wieso Deine Daten in Logaholic nicht vollständig vorhanden sind. Falls Du es lieber telefonisch besprechen möchtest, müsste ich Dich an unseren Support unter 0844 04 04 04 verweisen... ^Tom",
        "mediaUrls" : [ ],
        "senderId" : "29451196",
        "id" : "683938914768306179",
        "createdAt" : "2016-01-04T09:11:58.949Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "29451196",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hallo, also da müssten schätzungsweise irgendwelche daten bis zurück ins afaik 2007 sein, relevant wären für mich die logs zwischen 2010 und jetzt, mir wurde ein drohender tweet untergeschoben, dann von einem 7 köpfigen sonderkommando verhaftet, 3 Monate in die psychiatrie gesperrt weil ich ja nicht gedroht habe, gemäss chaosradio #CR152 landen \"hacker\" in der Psychiatrie weil die polizei nichts mit ihnen anzufangen weiss. ich gehe davon aus dass zuerst \"Footprinting\" betrieben wurde und erhoffe mir, dass ich die IP des täters darin finde, das dumme ist, die haben diverse begabte 1980er Nerds ermordet, also es ist kein spiel. nach 3 Monaten psychiatrie, haben dann promt irgendwelche daten auf meiner infrastruktur gefehlt, da ich aber viele sicherungen habe und auch geo-\"caches\" konnte ich mit einem Simplen diff -r rausfinden wer etwa was löschen wollte, ähm ich glaub es wär einfacher wenn wir telefonieren würden, hast Du eine Nummmer bei Hostpoint?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "683933872405528579",
        "createdAt" : "2016-01-04T08:51:56.767Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Morgen Marc, könntest Du mir sagen, zu welcher Webseite Dir die Daten fehlen und welche Daten Du auswerten wolltest? ^Tom",
        "mediaUrls" : [ ],
        "senderId" : "29451196",
        "id" : "683931415164014595",
        "createdAt" : "2016-01-04T08:42:10.899Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1156302725187022848",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo",
        "mediaUrls" : [ ],
        "senderId" : "1156302725187022848",
        "id" : "1160678157025992709",
        "createdAt" : "2019-08-11T22:23:31.691Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1173628161076203520",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Sorry, I meant well.",
        "mediaUrls" : [ ],
        "senderId" : "1173628161076203520",
        "id" : "1181107813428465668",
        "createdAt" : "2019-10-07T07:23:41.379Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1175818689905643520",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/lVquDAFbIa",
          "expanded" : "https://youtu.be/iUARiQS4eZk",
          "display" : "youtu.be/iUARiQS4eZk"
        } ],
        "text" : "Hey..  hows it going.   Start making Audio recordings.  Dows what you hear sound lo le this?\n\nhttps://t.co/lVquDAFbIa",
        "mediaUrls" : [ ],
        "senderId" : "1175818689905643520",
        "id" : "1193932661150097412",
        "createdAt" : "2019-11-11T16:45:03.425Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1181625616174587904",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/TOQkHbK29I",
          "expanded" : "https://www.youtube.com/watch?v=fwMvboBoW4E",
          "display" : "youtube.com/watch?v=fwMvbo…"
        } ],
        "text" : "CIA ist in der drei dimensionalen Welt eine Behörde, denk mal größer Central Intelligent Agency, Sonnensystem, Galaxy....\n\nhttps://t.co/TOQkHbK29I\n\nBitte anschauen!",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1194366087892217862",
        "createdAt" : "2019-11-12T21:27:20.388Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/QzAuLZftP0",
          "expanded" : "http://oeis.org/",
          "display" : "oeis.org"
        } ],
        "text" : "bitte diskret behandeln: https://t.co/QzAuLZftP0",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1194363514128863237",
        "createdAt" : "2019-11-12T21:17:06.771Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "schau ma lein bisse ldurch, einiges was ich so in letzter zeit gefunden habe und später noch auswerten möchte....",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1194362940381696005",
        "createdAt" : "2019-11-12T21:14:49.941Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/JCrG7kUHfu",
          "expanded" : "https://www.wikidata.org/wiki/Q48781363",
          "display" : "wikidata.org/wiki/Q48781363"
        }, {
          "url" : "https://t.co/wQLOG6cX2m",
          "expanded" : "https://viaf.org/viaf/48781363/#Baranov,_V._M._(Vladimir_Moiseevich)",
          "display" : "viaf.org/viaf/48781363/…"
        }, {
          "url" : "https://t.co/0Emtzd1I0r",
          "expanded" : "http://shw-taura.de/shop/shop_content.php?coID=4",
          "display" : "shw-taura.de/shop/shop_cont…"
        }, {
          "url" : "https://t.co/Iv3VimbjUc",
          "expanded" : "http://www.getty.edu/vow/ULANFullDisplay?find=&role=&nation=&page=1&subjectid=500017903",
          "display" : "getty.edu/vow/ULANFullDi…"
        }, {
          "url" : "https://t.co/0YImoqMegu",
          "expanded" : "https://www.oclc.org/research/themes/data-science/fast/odcby.html",
          "display" : "oclc.org/research/theme…"
        }, {
          "url" : "https://t.co/3BrM8n0Yac",
          "expanded" : "http://www.getty.edu/vow/ULANContributor?find=&role=&nation=&page=1&contribid=2500000016",
          "display" : "getty.edu/vow/ULANContri…"
        }, {
          "url" : "https://t.co/aL54rgqd5J",
          "expanded" : "https://ru.wikipedia.org/?oldid=93378239",
          "display" : "ru.wikipedia.org/?oldid=93378239"
        }, {
          "url" : "https://t.co/wQLOG6cX2m",
          "expanded" : "https://viaf.org/viaf/48781363/#Baranov,_V._M._(Vladimir_Moiseevich)",
          "display" : "viaf.org/viaf/48781363/…"
        }, {
          "url" : "https://t.co/55WL0dUuAn",
          "expanded" : "http://isni.oclc.org/xslt/DB=1.2/SET=1/TTL=1/NXT?FRST=1",
          "display" : "isni.oclc.org/xslt/DB=1.2/SE…"
        }, {
          "url" : "https://t.co/UlNrRa0OvF",
          "expanded" : "https://en.wikipedia.org/wiki/Stevedore",
          "display" : "en.wikipedia.org/wiki/Stevedore"
        }, {
          "url" : "https://t.co/MvUYriIcLf",
          "expanded" : "https://raw.githubusercontent.com/wikimedia/mediawiki-extensions-WikibaseQualityConstraints/dd6d6f25d094b1f0fe2cae7a47dac2f02e842c30/extension.json",
          "display" : "raw.githubusercontent.com/wikimedia/medi…"
        }, {
          "url" : "https://t.co/NRufkUtEQE",
          "expanded" : "http://id.loc.gov/rwo/agents/n92800099.html",
          "display" : "id.loc.gov/rwo/agents/n92…"
        }, {
          "url" : "https://t.co/T0aOGjvz0Q",
          "expanded" : "http://wikiba.se/ontology-1.0.owl#Lexeme",
          "display" : "wikiba.se/ontology-1.0.o…"
        }, {
          "url" : "https://t.co/5ee5oaf5Aj",
          "expanded" : "https://www.worldcat.org/title/health/oclc/869193593?referer=di&ht=edition",
          "display" : "worldcat.org/title/health/o…"
        }, {
          "url" : "https://t.co/vONyrisDEc",
          "expanded" : "https://viaf.org/viaf/search?query=cql.any%20all%20%2248781363%22&sortKeys=holdingscount&recordSchema=BriefVIAF",
          "display" : "viaf.org/viaf/search?qu…"
        }, {
          "url" : "https://t.co/t36UEBUhm7",
          "expanded" : "http://www.loc.gov/standards/mads/rdf/index.html",
          "display" : "loc.gov/standards/mads…"
        }, {
          "url" : "https://t.co/TYbNLzx1DV",
          "expanded" : "https://www.editablemaps.com/Maharashtra-Editable-Map.html",
          "display" : "editablemaps.com/Maharashtra-Ed…"
        }, {
          "url" : "https://t.co/HyQy9lXJHt",
          "expanded" : "https://www.thorley.biz/",
          "display" : "thorley.biz"
        }, {
          "url" : "https://t.co/5FkEMmLtT5",
          "expanded" : "http://stallman.org/facebook.html#realname",
          "display" : "stallman.org/facebook.html#…"
        }, {
          "url" : "https://t.co/rNhDPBLWtb",
          "expanded" : "https://ekd.me/2015/03/diplomatic-disasters/",
          "display" : "ekd.me/2015/03/diplom…"
        }, {
          "url" : "https://t.co/f4ehAsXAaA",
          "expanded" : "https://www.mark1training.co.uk/",
          "display" : "mark1training.co.uk"
        }, {
          "url" : "https://t.co/ksvTRGiKc0",
          "expanded" : "https://archiveofourown.org/media",
          "display" : "archiveofourown.org/media"
        }, {
          "url" : "https://t.co/ksvTRGiKc0",
          "expanded" : "https://archiveofourown.org/media",
          "display" : "archiveofourown.org/media"
        }, {
          "url" : "https://t.co/pQaUuO7R9E",
          "expanded" : "https://www.sandia.gov/~sjplimp/",
          "display" : "sandia.gov/~sjplimp/"
        }, {
          "url" : "https://t.co/hmxEHOOVhx",
          "expanded" : "https://hbr.org/2012/04/the-real-leadership-lessons-of-steve-jobs",
          "display" : "hbr.org/2012/04/the-re…"
        }, {
          "url" : "https://t.co/xcxr3XKQ3t",
          "expanded" : "http://ftp.gwdg.de/pub/linux/gentoo/distfiles/",
          "display" : "ftp.gwdg.de/pub/linux/gent…"
        }, {
          "url" : "https://t.co/KVggBZHGdr",
          "expanded" : "https://en.wikipedia.org/wiki/List_of_Latin_phrases_(I)",
          "display" : "en.wikipedia.org/wiki/List_of_L…"
        }, {
          "url" : "https://t.co/O40zkusMLL",
          "expanded" : "https://crates.io/",
          "display" : "crates.io"
        }, {
          "url" : "https://t.co/uSgBjSjgKl",
          "expanded" : "http://spellbackwards.com/",
          "display" : "spellbackwards.com"
        } ],
        "text" : "Awsome\nhttps://t.co/JCrG7kUHfu\nhttps://t.co/wQLOG6cX2m\nhttps://t.co/0Emtzd1I0r\nhttps://t.co/Iv3VimbjUc\nhttps://t.co/0YImoqMegu\nhttps://t.co/3BrM8n0Yac\nhttps://t.co/aL54rgqd5J\nhttps://t.co/wQLOG6cX2m\nhttps://t.co/55WL0dUuAn\nhttps://t.co/UlNrRa0OvF\nhttps://t.co/MvUYriIcLf\nhttps://t.co/NRufkUtEQE\nhttps://t.co/T0aOGjvz0Q\nhttps://t.co/5ee5oaf5Aj\nhttps://t.co/vONyrisDEc\nhttps://t.co/t36UEBUhm7\nhttps://t.co/TYbNLzx1DV\nhttps://t.co/HyQy9lXJHt\nftp://ftp.gwdg.de/pub/languages/perl/CPAN/authors/00whois.html\nhttps://t.co/5FkEMmLtT5\nhttps://t.co/rNhDPBLWtb\nhttps://t.co/f4ehAsXAaA\nhttps://t.co/ksvTRGiKc0\nhttps://t.co/ksvTRGiKc0\nhttps://t.co/pQaUuO7R9E\nhttps://t.co/hmxEHOOVhx\nhttps://t.co/xcxr3XKQ3t\nhttps://t.co/KVggBZHGdr\nhttps://t.co/O40zkusMLL\nhttps://t.co/uSgBjSjgKl",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1194362645052362758",
        "createdAt" : "2019-11-12T21:13:39.575Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du bist der erste Twitter User den ich entdeckt habe welcher tieferes und der Wahrheit sehr nahe kommendes wissen hat.",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1192994247542226948",
        "createdAt" : "2019-11-09T02:36:08.112Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hey wie geht's dir?",
        "mediaUrls" : [ ],
        "senderId" : "1181625616174587904",
        "id" : "1192993710591619076",
        "createdAt" : "2019-11-09T02:34:00.116Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1182236668050444289",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Greetings to you, my name is Ann Laurent, i am a 63 year old widow, and have been diagnosed with stage 4 breast cancer and will not live for more than next 2 months. I have a sum of 5,200,000.00 (Five Million Two Hundred Thousand Dollars) i want you to help me share to the motherless baby homes, sick cancer patients and less privileged, as God’s wish and my dying wish. Contact me so i can provide you more details. E-mail; (annaisalaurent@gmail.com) God bless you.",
        "mediaUrls" : [ ],
        "senderId" : "1182236668050444289",
        "id" : "1185035980379688964",
        "createdAt" : "2019-10-18T03:32:49.374Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1198038302529638400",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/tsp5CLSDIR",
          "expanded" : "https://twitter.com/grergtgrt/status/1325943315313827842",
          "display" : "twitter.com/grergtgrt/stat…"
        } ],
        "text" : "Vem har besökt din profil? https://t.co/tsp5CLSDIR",
        "mediaUrls" : [ ],
        "senderId" : "1198038302529638400",
        "id" : "1326073393754431492",
        "createdAt" : "2020-11-10T08:05:09.066Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1211299375420297216",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/eLxZy0hvPZ",
          "expanded" : "https://media.ccc.de/v/36c3-10543-post-quantum_isogeny_cryptography",
          "display" : "media.ccc.de/v/36c3-10543-p…"
        } ],
        "text" : "there is a talk about post-quantum-cryptography, as far as i understood (im not that good in maths) you just have to take bigger cypher lenghts: https://t.co/eLxZy0hvPZ",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1252221416348549128",
        "createdAt" : "2020-04-20T13:03:45.551Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Scroll to the end of page and tgere is a link to the raport. I think it may give you new insights to the card game",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252219267522662404",
        "createdAt" : "2020-04-20T12:55:13.175Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/U6eBkv0Wu6",
          "expanded" : "https://ik.org.pl/en/publications/post-quantum-international-security/",
          "display" : "ik.org.pl/en/publication…"
        } ],
        "text" : "https://t.co/U6eBkv0Wu6",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252219079320129540",
        "createdAt" : "2020-04-20T12:54:28.345Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Meanwhile keep up your good job and be strong. I support you.",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252214065562337287",
        "createdAt" : "2020-04-20T12:34:32.932Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I believe we are beings that are alive in a higher dimension. Let's call it that way. Our true selves are like players of a computer game using VR googles. We know the game character we are but we usually dont know  The Player. I met my player who tauggmht me the rulrs. One day I will share with you some tips and tricjs. Maybe this will help you go through",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252213654927429636",
        "createdAt" : "2020-04-20T12:32:55.033Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I didnt say its virtual reality. I could agree with your opinion it is something similar to augmented reality if you name it that way. Truth is that the terms we use are not sufficient to carry the full meaning",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252212383013109765",
        "createdAt" : "2020-04-20T12:27:51.786Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "What I do is I play a game aimed at single objective\"to build a Political Party representing sxhizophrenics and bringing it to Polish parliament. I work a lot with other schizophrenics to prepare that",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252212045581352964",
        "createdAt" : "2020-04-20T12:26:31.341Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "its not a virutal world in my opinion its more something like augmented reality",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1252211875183562758",
        "createdAt" : "2020-04-20T12:25:50.706Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I have voluntarily gone into taking antipstchotics as I do have the knowledge that in long run they significantly improve brain function. But I agree with you it is easy to lose oneself and ones intelligence and soul when you take antypsychotics in the way they are prescribed by doctors. I am now on higher doses than you and learned how to maximize the benefits out of them. I have come with conclusions that there is no single receipt for taking those drugs. For doctors it is try and error hence many mistakes and overdoses",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252211563387408393",
        "createdAt" : "2020-04-20T12:24:36.370Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "There are multiple levels on which one has to act in order to proceed.",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252210711079550980",
        "createdAt" : "2020-04-20T12:21:13.158Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "What I have found myself is that we all live in a simulated reality and life is just a game within which we go through various levels ending up with various bossess. Passing the level is usually impossible because we are programmed to lose. I found a way how to reprogram old programs and how to run ongoing deprogramming and self programming during the game. I believe I have hacked into this system",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252210504203931660",
        "createdAt" : "2020-04-20T12:20:23.844Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "But they usually lack theoretical and practical knowledge to find the patterns and describe everything",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252209912811225092",
        "createdAt" : "2020-04-20T12:18:02.840Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I can tell you that based on my experience with people with schizophrenia your findings are similar to what they experience everyday.",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252209721551052804",
        "createdAt" : "2020-04-20T12:17:17.240Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "3 months about",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1252209423155695620",
        "createdAt" : "2020-04-20T12:16:06.098Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "That must have been many days of full-time work",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252209323872333828",
        "createdAt" : "2020-04-20T12:15:42.424Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I think you have done a terrific job aggregating all things into one deck",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252209229861212166",
        "createdAt" : "2020-04-20T12:15:20.015Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hi Marc, I have read all cards in detail",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1252209098042605572",
        "createdAt" : "2020-04-20T12:14:48.586Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/eNVRYRkKjt",
          "expanded" : "https://github.com/braindef/HackersCardgame9",
          "display" : "github.com/braindef/Hacke…"
        } ],
        "text" : "and by the way if you find mistakes just clone the repository and create a pull request: https://t.co/eNVRYRkKjt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1251551295426760713",
        "createdAt" : "2020-04-18T16:40:56.839Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ {
          "senderId" : "75128838",
          "reactionKey" : "like",
          "eventId" : "1251551073183191040",
          "createdAt" : "2020-04-18T16:40:03.211Z"
        } ],
        "urls" : [ ],
        "text" : "So you see exactly how it works... I figured out that when I focus on others and try to explain as you everyone says it is insane. But I managed to program my whole environment and now as koronavirus spreads up they all start to say: but you were telling us that years ago. Now they stopped questioning me and start asking relevant questions. I also stopped to convince them or present them my truths. Ultimately they will reaxh my knowledge but at later stage. Keep up doing your great job with Hackers Game. I read now some 20 cards and see you are to the point. I discovered same things but never structured tgem as you've done in that easy to understand manner. I have another objectives self-programmed",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251550586618724357",
        "createdAt" : "2020-04-18T16:38:07.253Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/lamxuwLJbd",
          "expanded" : "https://github.com/braindef/Shrink-Disorder-Tarot",
          "display" : "github.com/braindef/Shrin…"
        }, {
          "url" : "https://t.co/FNo3nKDDaN",
          "expanded" : "https://0x8.ch/HackersCardgame7/combinations/guildwars/",
          "display" : "0x8.ch/HackersCardgam…"
        } ],
        "text" : "at least it is better to end up in a mental institution like me than becoming the next hitler. but i have some bad feelings about the fact that kids are more programmed and dress instead of educated. initially i wanted to free all the equal aged and younger from their mental prison their parents and education system has produced. but in fact always when i try to show and explain those things i myself endup in a mental \"institution\", then they give me that much medicine that i become stupid, forget about what i found out, chill 3 years and endup again in hackers cardgame, the very early version was this one here 3 years ago: https://t.co/lamxuwLJbd then i forgot because the medicine, then i was a short time \"awake\" and now its from 70 to 330 cards, and there is a possibility to make custom decks for specific situations: https://t.co/FNo3nKDDaN",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1251547616984342532",
        "createdAt" : "2020-04-18T16:26:19.247Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "You are perfectly right about your observation, especially re point 3 that is them being robots. I found the way how to deprogram their programs imposed on me and I managed to program them myself. The same with other patients - they are programmable algorithms sent to us to put their programs on us while we are put into mental hospital. I learned that the mental hospital or a prison are ultimate levels of the game for those willing to get freedom. Those levels are designed to reprogram us.Exiting them without imposed programs is tough but I am an example it is possible and beneficial for the next steps.",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251544157576990726",
        "createdAt" : "2020-04-18T16:12:34.443Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "i dont trust shrinks\n\n1. they have no experience beeing eg. schizophrenic or autistic, tough they surely know what is best for them\n2. they have a title and with this title they are not anymore developing themself, like sokrates says: the one who means to be someone has stoped to become someone\n3. i asked them tons of questions they never answer, either because they are stupid or the just dont know the answer, i guess they are some sort of Robots or guards from the evil side\n - for example if you ask about stimulus-respnse-habits no shrink will answer that even if it is a widely known theory\n4. if i would be CIA i would hire all shrinks globally since those are the people that should be best in manipulation, but they are not\n...\n\ni was in mental institution for about 2 months since january, i almost forgot about hackers cardgame... this happens already the second time. i should not forget, even if im completely nuts (i'm diagnosed schizophrenic) then though 50% of the cards would make complete sense...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1251537804645015563",
        "createdAt" : "2020-04-18T15:47:19.784Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "I have also done a homeless person quest. For 5 months I lived as a homeless in Morocco without any money. I threw away my passport and convinced local police and army an old CD and a piece of plastic from Mercedes Benz are my digital ID and RFID chip. I ran away from any police yard, they stopped me multiple times but always I successfuly run away. Then at the end I said I call my cards and ended in psychiatric warden of Moroccan jail. After 3 weeks of interrogations by their secret police and army I was deported to Poland. Here I voluntarily called in the psychiatric hospital and stayed closed for more than 7 months playing beat the shrink game",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251535784424988676",
        "createdAt" : "2020-04-18T15:39:18.112Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thanks. I have done this in my reality. Voluntarily spent almost 2 years in closed wardens of psychiatric hospitals and convinced them I am schizophrenic though I have pretended all. Now I have a diagnosis and got also disabled person status. It helps. For instance now I am free from wearing face mask according to the law",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251533987899756548",
        "createdAt" : "2020-04-18T15:32:09.785Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "the psychiatrist cards and the archetype cards. as a 20 year old boy i was thinking all those psychiatrists are good people, but in fact they are stupid stubborn idiots that often lack humanism, so first thing is to beat the shrink (psychiatrist)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1251529127481487364",
        "createdAt" : "2020-04-18T15:12:50.983Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Thanks. This was the missing data as in the license you stated only the year of your birth, so I didnt know. It's the first time I see your cards but I can tell you I've experienced all you are writing about. Do you have any hint where to best start for me?",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251528371131031556",
        "createdAt" : "2020-04-18T15:09:50.645Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1211299375420297216",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "if i calculated that you are 1 month and 2 days younger than i, so in this case you dont need a special permit...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1251527654093795334",
        "createdAt" : "2020-04-18T15:06:59.689Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hi Marc, I am Mirek from Poland. I just was directed to your cardgame and read the license. You wrote there that you grant the license to those who are not older than you. I was born July 19 1978. Could you please let me know if you can grant me the license? Thanks",
        "mediaUrls" : [ ],
        "senderId" : "1211299375420297216",
        "id" : "1251525002438029320",
        "createdAt" : "2020-04-18T14:56:27.542Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1247298914618834946",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/3mouD7zRSo",
          "expanded" : "https://twitter.com/grergtgrt/status/1325943315313827842",
          "display" : "twitter.com/grergtgrt/stat…"
        } ],
        "text" : "Vem har besökt din profil? https://t.co/3mouD7zRSo",
        "mediaUrls" : [ ],
        "senderId" : "1247298914618834946",
        "id" : "1326068804313444356",
        "createdAt" : "2020-11-10T07:46:54.855Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1249765639637684226",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/N8khubF9Oy",
          "expanded" : "https://twitter.com/grergtgrt/status/1325943315313827842",
          "display" : "twitter.com/grergtgrt/stat…"
        } ],
        "text" : "Vem har besökt din profil? https://t.co/N8khubF9Oy",
        "mediaUrls" : [ ],
        "senderId" : "1249765639637684226",
        "id" : "1326078648537870341",
        "createdAt" : "2020-11-10T08:26:01.923Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1272196362810122240",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/S97Kx0ZKIB",
          "expanded" : "https://docs.google.com/forms/u/1/d/e/1FAIpQLScSuSIgnOwiiguv22WY40AedUD8fVP9ZOksm0d146mRfyanVA/viewform",
          "display" : "docs.google.com/forms/u/1/d/e/…"
        }, {
          "url" : "https://t.co/GlDsvPYifJ",
          "expanded" : "https://twitter.com/messages/media/1272245290901409798",
          "display" : "pic.twitter.com/GlDsvPYifJ"
        } ],
        "text" : "im Tia ♥️  \nI  become a model and try myself in  something new . So I decided , and post my hot 🐱 photos for  contest - https://t.co/S97Kx0ZKIB .  \nMy Username on site is \"Fairy4661\"\nPlease find me! https://t.co/GlDsvPYifJ",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1272245290901409798/1272245268138930176/NrVmDO9W.jpg" ],
        "senderId" : "1272196362810122240",
        "id" : "1272245290901409798",
        "createdAt" : "2020-06-14T19:11:29.346Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "52041973-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/FRSma6DAAG",
          "expanded" : "https://twitter.com/odenwilusenz/status/1041733092245950464",
          "display" : "twitter.com/odenwilusenz/s…"
        } ],
        "text" : "https://t.co/FRSma6DAAG",
        "mediaUrls" : [ ],
        "senderId" : "52041973",
        "id" : "1041740931039408132",
        "createdAt" : "2018-09-17T17:29:27.352Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "52041973",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke, ich seh ein bisschen wie ein verbrecher aus... :)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "893085483831615491",
        "createdAt" : "2017-08-03T12:25:46.775Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/uvUqvzjLiG",
          "expanded" : "https://twitter.com/messages/media/893044495415160835",
          "display" : "pic.twitter.com/uvUqvzjLiG"
        } ],
        "text" : "hier noch eine Kopie der Zeitung. Ich glaube du hattest dazu beigetragen. Viele Grüsse Tony https://t.co/uvUqvzjLiG",
        "mediaUrls" : [ "https://ton.twitter.com/dm/893044495415160835/893044198751875072/ONnVTbiU.jpg" ],
        "senderId" : "52041973",
        "id" : "893044495415160835",
        "createdAt" : "2017-08-03T09:42:55.749Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1278059831421788160",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Du hast einen Dachschaden",
        "mediaUrls" : [ ],
        "senderId" : "1278059831421788160",
        "id" : "1295086313025286150",
        "createdAt" : "2020-08-16T19:53:33.314Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1284653691564326912",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/QoXfvq3tlr",
          "expanded" : "https://twitter.com/grergtgrt/status/1325943315313827842",
          "display" : "twitter.com/grergtgrt/stat…"
        } ],
        "text" : "Vem har besökt din profil? https://t.co/QoXfvq3tlr",
        "mediaUrls" : [ ],
        "senderId" : "1284653691564326912",
        "id" : "1326095516074045444",
        "createdAt" : "2020-11-10T09:33:03.453Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1285185479206412288",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "hi",
        "mediaUrls" : [ ],
        "senderId" : "1285185479206412288",
        "id" : "1285189638672482308",
        "createdAt" : "2020-07-20T12:27:42.265Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1312812354149715969",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/a3GkbaMTbr",
          "expanded" : "https://chat.whatsapp.com/H1OkdKBXgEv126oq36ajiW",
          "display" : "chat.whatsapp.com/H1OkdKBXgEv126…"
        }, {
          "url" : "https://t.co/njDzPC0tuy",
          "expanded" : "https://youtu.be/8FVurYXNxqM",
          "display" : "youtu.be/8FVurYXNxqM"
        } ],
        "text" : "If anyone interested in online job making 100-200$ a week with proof 100% gurenttee Then msg me \n\nFor any type of information join group and contact group admin: ( your WhatsApp group Link)\n\nhttps://t.co/a3GkbaMTbr\n \nWhatsapp; 00923476245779\n\nPlease Dont call on SIM\n\nBefore saying it scam or fake just watch this video \n\nhttps://t.co/njDzPC0tuy\n\n#epicearn #account #preview\n#onlinejobs #jobseekers #onlinejobpakistan #onlineearningpakistan #homework\n#affiliatemarketing #digitalmarketing",
        "mediaUrls" : [ ],
        "senderId" : "1312812354149715969",
        "id" : "1314061683787280388",
        "createdAt" : "2020-10-08T04:34:54.245Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-1314508729958780929",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "1314508729958780929",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/cX7sZqp38a",
          "expanded" : "https://gitlab.com/Autistics/hackerscardgame.ch/-/blob/master/cccBE/Re%20%20%5BChaos%20Bern%5D%20Instrumentalisierung%20von%20Betroffenen%20(war%20%20haben%20wir%20eine%20Blocklist%20)-original.eml#L28-43",
          "display" : "gitlab.com/Autistics/hack…"
        } ],
        "text" : "keine Zeit und wichtigers zu tun als BDSM, ausser BDSM im Sinne von Reparenting und Trama-Verarbeitung: https://t.co/cX7sZqp38a",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1318582314927071237",
        "createdAt" : "2020-10-20T15:58:16.676Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "1314508729958780929",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/8MnFHScVzv",
          "expanded" : "https://de.wikipedia.org/wiki/Jos%C3%A9_Manuel_Rodr%C3%ADguez_Delgado",
          "display" : "de.wikipedia.org/wiki/Jos%C3%A9…"
        } ],
        "text" : "ich verkehre eigentlich nur mit bitches, die den mut haben hin zu stehen und dinge von sich preis zu geben und nicht mit menschen die einen neuen (allenfalls fake) account haben. als autist brauche ich wie nr 5 lebt input um mir ein bild machen zu können bevor ich infos wie z.B. neuralink gibt es schon seit 1950 unter dem namen stimoceiver von https://t.co/8MnFHScVzv",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1315051090203881478",
        "createdAt" : "2020-10-10T22:06:27.483Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "54578472-75128838",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nen Hack, entweder der Firmware-FTP Server oder Etterfilter (Deep Packet Injection / ich bin versucht zu sagen Mongo in the middle aber technisch korrekt heisst es Man in the Middle)... ich werde das auch auf Twitter stellen damit die anderen Gleichaltrigen und jüngeren sehen können was so läuft. könnte man den wechsel machen wenn ich jemanden von Euch am Telefon habe? Gruss mla.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1279074556494389252",
        "createdAt" : "2020-07-03T15:28:33.025Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YXIVzXVVQ2",
          "expanded" : "https://marclandolt.ch/doc/KoaxialeAnlagen0.5.7.pdf?page=38",
          "display" : "marclandolt.ch/doc/KoaxialeAn…"
        } ],
        "text" : "Das Scirpt meiner Wenigkeit mit dem ich 3,4 Lehrjahrs Lehrlinge zum Thema unterrichtet habe: https://t.co/YXIVzXVVQ2 der Pegel ist afaik irgendwo bei 67dbµV, also über den 63dbµV Planungspegel. Auch geht das aktuelle Modem problemlos. Bei Docsis 3.1 ist ja afaik einfach ein zwei weitere Transport-Ströme möglich, sonst sollte sich also nichts ändern also tippe ich da schwer auf irgend",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1279074470012096517",
        "createdAt" : "2020-07-03T15:28:12.400Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YXIVzXVVQ2",
          "expanded" : "https://marclandolt.ch/doc/KoaxialeAnlagen0.5.7.pdf?page=38",
          "display" : "marclandolt.ch/doc/KoaxialeAn…"
        } ],
        "text" : "Das Scirpt meiner Wenigkeit mit dem ich 3&amp;4 Lehrjahrs Lehrlinge zum Thema unterrichtet habe: https://t.co/YXIVzXVVQ2 der Pegel ist afaik irgendwo bei 67dbµV, also über den 63dbµV Planungspegel. Auch geht das aktuelle Modem problemlos. Bei Docsis 3.1 ist ja afaik einfach ein zwei weitere Transport-Ströme möglich, sonst sollte sich also nichts ändern also tippe ich da schwer auf irgend nen Hack, entweder der Firmware-FTP Server oder Etterfilter (Deep Packet Injection / ich bin versucht zu sagen Mongo in the middle aber technisch korrekt heisst es Man in the Middle)... ich werde das auch auf Twitter stellen damit die anderen Gleichaltrigen und jüngeren sehen können was so läuft. könnte man den wechsel machen wenn ich jemanden von Euch am Telefon habe? Gruss mla.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1279073735199293444",
        "createdAt" : "2020-07-03T15:25:18.413Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Halli Hallo, beim 3. Modem dass dan angekommen ist war eben der Fall dass das beim ersten mal Einstecken grün geleuchtet hat und danach nie wieder. Ich vermute es hat damals die Firmware vom FTP-Server gezogen und dass da der FTP Server entweder von Dr. Hansjürg Pfisterer und Urs Blum seiner Studentenverbindung gehackt war oder mit etwas wie EtterFilter Deep Packet Injection betrieben wurde. Ich vermute das selbe könnte wieder passieren sobald ich die Box anstecke. Habt ihr wie ich beim letzten mal vorgechlagen hab das Security Team diesbezüglich informiert? Dann ist es üblich einen Techniker vorbei zu schicken wenn es nicht geht, meiner Meinung ist aber das HFC in Ordnung, auch der Digital-TV lief ohne Problmeme, aber das Modem nicht.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1279073712399093764",
        "createdAt" : "2020-07-03T15:25:11.786Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Wir haben von unserer Logistik Stelle Bescheid erhalten und sie konnten uns bestätigen, dass die Lieferung auf dem Weg zu dir leider verloren ging. Die Post konnte das Paket somit nie zustellen. Es tut uns leid für dieses Missgeschick und wir möchten uns für die entstandenen Umstände entschuldigen. Wir werden dir nun erneut die Giga Connect Box zustellen, nur diesmal wird es korrekt übermittelt. Sobald du die Giga Connect Box installiert hast wirst du vom neuen Abonnement, Connect Giga mit Phone CH, profitieren können. Danke für deine Geduld und ich wünsche dir ein angenehmes Wochenende. Liebe Grüsse, ^sz",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1278949213947166724",
        "createdAt" : "2020-07-03T07:10:29.037Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/Tze9S2AcsA",
          "expanded" : "https://community.upc.ch/?category.id=de",
          "display" : "community.upc.ch/?category.id=de"
        } ],
        "text" : "Hi Marc, vielen Dank für deine Anfrage. In diesem Rahmen haben wir keine Dokus oder Unterlagen. Auch die Umstellung kann nur über unseren Kundendienst abgewickelt werden und nicht selbst durch den Kunden.\n\nIch würde dir jedoch empfehlen, in unsere Community beizutreten :) Dort findest du für allen Themen interessante Diskussionen und Anzeigen. Auch bezüglich IPv4 oder IPv6.\n\nHier der Link für die Community: https://t.co/Tze9S2AcsA\n\nBesten Dank für dein Verständnis. Liebe Grüsse, ^ck",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1217026647695806468",
        "createdAt" : "2020-01-14T10:12:18.762Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo nochmals, hab[en|t] Sie / Ihr irgend eine Doku zum IPv4 over IPv6, bzw. kann ich selber irgendwie zwischen ipv4 und ipv6 umschalten um zu testen wie ich was bewerkstelligen könnte?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1216959217078325252",
        "createdAt" : "2020-01-14T05:44:22.067Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/cVgv1XPfQh",
          "expanded" : "https://support.upc.ch/app/faq/a_id/1320/wie-konfiguriere-ich-bridge-modus-an-meinem-wlan-modem",
          "display" : "support.upc.ch/app/faq/a_id/1…"
        } ],
        "text" : "Besten Dank für die Kundennummer Marc.\n\nZu deiner ersten Frage, nein es ist nicht möglich gleichzeitig ipv4 und ipv6 zu nutzen.\n\nUnd deine zweite Frage können wir dir leider nicht beantworten, da wir nur die Grundeinstellungen unseres Modems (Connect Box) kennen. Da müsstest du dich an den Provider des externen Routers wenden.\n\nAktuell bist du bereits auf ipv4 umgestellt, also solltest du die Connect Box problemlos in den Bride-Mode versetzen können. Auf diesem folgenden Link, findest du noch eine Anleitung dazu: https://t.co/cVgv1XPfQh\n\nBesten Dank für deine Kontaktaufnahme. Liebe Grüsse, ^ck",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1213025053207617540",
        "createdAt" : "2020-01-03T09:11:24.306Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/1DU83TNylT",
          "expanded" : "https://twitter.com/UPC_Switzerland/status/1211916308566171649",
          "display" : "twitter.com/UPC_Switzerlan…"
        } ],
        "text" : "Kundennummer in Bezug auf untenstehende Tweet wäre folgende: 6608524 https://t.co/1DU83TNylT",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1212112469268795396",
        "createdAt" : "2019-12-31T20:45:07.340Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Ich kann das leider nicht beeinflussen, das ist ein automatischer Ablauf. Es werden keine Rechnungen mit einem Betrag unter CHF 25.- erstellt. Deine Rechnungen werden immer zu Beginn des Monats generiert und du zahlst sie nach ein paar Tagen auch ein. Nur für die letzte Rechnung vom 6. August 2019 hast du CHF 234.- eingezahlt anstatt CHF 112.10. Deshalb wurde die September-Rechnung nicht generiert worauf du erneut eine Zahlung über CHF 122.- am 6. September 2019 ausgelöst hast.\n\nWie gesagt, die nächste Rechnung erfolgt sobald das Guthaben aufgebraucht ist und die Monate September, Oktober und November 2019 werden abgerechnet. Es erfolgt auch keine Mahnung, du kannst ohne weiteres auf die nächste Rechnung abwarten.\n\nDanke für dein Verständnis. LG, ^sz",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1189804602667028485",
        "createdAt" : "2019-10-31T07:21:37.467Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok danke, wäre es möglich dem blöden buchhaltuns zeugs computer zu sagen, dass er die rechnung doch generieren soll auch wenn ein überschuss drauf ist, denn ich zahle die rechnungen immer ungefähr am ~8. des monats und wenn ich einen monat warte bekomme ich dann die mahnung so am 15 wenn ich am 8. bezahlt habe, ein gamer würde sagen euer buchaltungssystem lag-et (engl. to lag)",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1189674495168176134",
        "createdAt" : "2019-10-30T22:44:37.429Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Morgen Marc. \n\nAufgrund deiner Überzahlung von total CHF 243.90 erfolgt keine Einzelrechnung für den Monat September und Oktober 2019. Erst, wenn das gesamte Guthaben aufgebraucht ist wird wieder eine Rechnung generiert, weil wir keine Rechnungen unter CHF 25.- erstellen. Somit wirst du im November 2019 die nächste Rechnung erhalten in der die Monate September und Oktober sowie November 2019 abgerechnet werden. \n\nEine Mahnung würde nur dann erfolgen, wenn du in Zahlungsverzug geraten würdest und aktuell ist das nicht der Fall.\n\nIch hoffe, dir etwas Klarheit verschafft zu haben und danke für deine Kenntnisnahme.\n\nSchöne Grüsse, ^sz",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1189439649338609668",
        "createdAt" : "2019-10-30T07:11:25.828Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Aber müsste da nicht trotztem für den monat september und oktober ( = 250.--) eine rechnung drin sein? und somit müsste ich ja am 6.11. wieder bezahlen, sonst kommt ja dann eine mahnung?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1189222613979193348",
        "createdAt" : "2019-10-29T16:49:00.574Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke Marc. Also, aufgrund deines Guthabens wirst du vermutlich erst im November 2019 wieder eine Rechnung erhalten. Dann werden September bis November 2019 abgerechnet. \n\nAm besten wartest du einfach auf die nächste Rechnung ab bevor du etwas einzahlst.\n\nDanke für deine Kenntnisnahme. Liebe Grüsse, ^sz",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1189184809630699525",
        "createdAt" : "2019-10-29T14:18:47.315Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/00qZcK2lXa",
          "expanded" : "https://twitter.com/messages/media/1189179506465992712",
          "display" : "pic.twitter.com/00qZcK2lXa"
        } ],
        "text" : "Kundennummer: 6608524 \neben die rechnungen werden nicht mehr generiert, ich hab jetzt schon 250.-- Fr. guthaben und es zieht nichts mehr ab... https://t.co/00qZcK2lXa",
        "mediaUrls" : [ "https://ton.twitter.com/dm/1189179506465992712/1189179497708285955/i8wv1Zku.png" ],
        "senderId" : "75128838",
        "id" : "1189179506465992712",
        "createdAt" : "2019-10-29T13:57:43.072Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ja war ja nicht der Fehler vom 2. Level supporter wenn der 3. Level updates macht ohne diese anzukündigen... das passiert jeweils alle 3-6 Monate und wird nicht angekündigt und da das UPC-System nur alle Stunde das Modem mit SNMP kontrolliert und der Ausfall jeweils 50Min lang dauert sieht man das gar nicht beim überprüfen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1102535903317241861",
        "createdAt" : "2019-03-04T11:46:58.221Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Morgen Marc\n\nWir haben von der technischen Abteilung eine Bestätigung erhalten, dass man den Fall mit dir angeschaut hat. Ich hoffe, dass wir dein Anliegen zu deiner Zufriedenheit klären konnten. Besten Dank für dein Verständnis. Liebe Grüsse, ^au",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1102494067827593220",
        "createdAt" : "2019-03-04T09:00:43.825Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hi Marc. Besten Dank für deine Nachricht. Unser System konnte einen Unterbruch feststellen. Ich habe nun ein Problemticket eröffnet. Vielen Dank für dein Feedback und Verständnis. Liebe Grüsse, ^co",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1095353824364105732",
        "createdAt" : "2019-02-12T16:07:57.127Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/PKZrlu0HiS",
          "expanded" : "https://twitter.com/UPC_Switzerland/status/1095335642123317248",
          "display" : "twitter.com/UPC_Switzerlan…"
        } ],
        "text" : "zu https://t.co/PKZrlu0HiS 078 674 15 32, aber eben das war ja grossflächig und man munkelt es wäre ein \"geplantes wartungsfenster\" gewesen um z.B. neue firmware aufs modem aufzuspielen, also da ein ticket auf meinen namen zu machen ist in diesem fall wohl schwachsinnig, da das ja dann etwas mit dem backbone wäre, da ich schwer davon ausgehe dass telli und buchs nicht an der selben cmts hängen... es wäre wirklich toll, wenn so etwas angekündigt würde oder zumindest auf ihrer \"netzwerkstatus\" webseite notiert ist, denn ich war grad am programmieren und dann ist es super nervig wenn das internet unangekündigt einfach abschlatet. ich weiss auch dass die kunden abonemente \"best effort\" sind aber wenn das wirklich wartungsarbeiten waren dann wärs ja für die upc ein aufwand von 1min das rasch auf der seite netzwerkstatus zu notieren... ich bin schon ausgerüstet für solche fälle und hab einen dual wan router wo ich auch die computer ohne wireless über das handy umleiten kann aber das dauert auch immer eine weile und wenns angekündigt ist kann man das schon mal vorbereiten so dass es einen natlosen übergang gäbe...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1095348104210059269",
        "createdAt" : "2019-02-12T15:45:13.409Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Vielen Dank für deine Kontaktaufnahme mit unserem Abuse Team. Ich habe diese Punkte ebenfalls noch weitergeleitet. Beste Grüsse, ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1019093195962114052",
        "createdAt" : "2018-07-17T05:35:26.454Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "An ihrem Formular ist zu bemängeln, dass es keinen Zetistempel hat, das wäre zwingend nötig um den Fehler (wenn er dann überhaupt bei mir ist) sinnvoll zu beheben. Ich bitte Sie darum das zu berichtigen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018861554589208581",
        "createdAt" : "2018-07-16T14:14:58.853Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "2. Falls es wirklich der Vorbesitzer der IP Adresse war, dann wurde der nicht gewarnt und hat weiter einen offenen mDNS",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018861266817966084",
        "createdAt" : "2018-07-16T14:13:50.239Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "1. Sie bieten mich auf das zu kontrollieren, hätte ich nicht mal Informatik studiert müsste ich kostenpflichtig jemanden bestellen",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018861136014397445",
        "createdAt" : "2018-07-16T14:13:19.059Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Das birgt zwei weitere Probleme:",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018860997044588548",
        "createdAt" : "2018-07-16T14:12:45.990Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag, wenn Sie die Screenshot's schauen die ich dem Tweet angehängt habe sehen sie dass ich keinen öffentlichen mDNS habe, ich vermute da ich die IP noch nicht lange habe könnte es auch der Benutzer sein, der die IP Adresse vor mir hatte.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1018860954971529220",
        "createdAt" : "2018-07-16T14:12:35.916Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/7xJdXADRxF",
          "expanded" : "http://fal.cn/yRHc",
          "display" : "fal.cn/yRHc"
        }, {
          "url" : "https://t.co/jGJx3KdhkP",
          "expanded" : "http://fal.cn/yRZB",
          "display" : "fal.cn/yRZB"
        } ],
        "text" : "Hallo Marc. Vielen Dank für deine Kontaktaufnahme bezüglich der mDNS Sicherheitslücke. Am besten du füllst schnellstmöglich unser Feedback Formular aus https://t.co/7xJdXADRxF Unser Abuse Team wird sich dann schnellstmöglich mit dir in Verbindung setzen.  Warum wir keine öffentlich erreichbaren mDNS Dienste dulden findest du hier https://t.co/jGJx3KdhkP\nVielen Dank für deine wertvolle Mithilfe. Beste Grüsse, ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1018836474991702020",
        "createdAt" : "2018-07-16T12:35:19.442Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Haben Sie mich allenfalls vergessen?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1012596523878748164",
        "createdAt" : "2018-06-29T07:19:59.127Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "könnt Ihr dann nicht den Leuten die die Bestätigungen machen eine Mail machen, ich vermute Ihr habt ja irgend ein Exchange Adressbuch in der Firma. Beim letzten Mal ging das auf jeden fall...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1011639180601053188",
        "createdAt" : "2018-06-26T15:55:50.748Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich kann dir dies lediglich über Twitter bestätigen. Sollten weitere Unstimmigkeiten auftreten darfst du dich jederzeit gerne wieder via Twitter melden. Danke für dein Verständnis. Gruss ^sz.",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1011618682055913477",
        "createdAt" : "2018-06-26T14:34:23.472Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "könnten Sie mir allenfalls noch eine Bestätigung schicken, das hat schon beim letzten mal irgendwie nicht geklappt...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1011610380592873477",
        "createdAt" : "2018-06-26T14:01:24.242Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc, danke für die Zustellung des Beleges. Ich habe unsere Buchhaltung entsprechend informiert, so dass sie das retournierte Gerät ausbuchen können. Danke für deine Kenntnisnahme. Liebe Grüsse, ^sz.",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1011606443546603525",
        "createdAt" : "2018-06-26T13:45:45.608Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/XTx7K607Zy",
          "expanded" : "https://twitter.com/FailDef/status/1011254373953998849",
          "display" : "twitter.com/FailDef/status…"
        }, {
          "url" : "https://t.co/fnhEENnjOj",
          "expanded" : "https://twitter.com/UPC_Switzerland/status/1011509034686603264",
          "display" : "twitter.com/UPC_Switzerlan…"
        } ],
        "text" : "Eben sie haben mir geschrieben ich hätte die Box nicht retourniert, habe ich aber. Dann hab ich auch nicht gekündigt sondern von Horizon Box auf Digi-Card gewechselt... die Belege: https://t.co/XTx7K607Zy https://t.co/fnhEENnjOj",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1011599305151574021",
        "createdAt" : "2018-06-26T13:17:23.756Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke für deine Rückmeldung. Das kann ich dir nicht genau sagen. Ich denke, es ist besser, wenn du dich mit unserem Support in Verbindung setzt. Eventuell gibt es doch eine andere Möglichkeit. Bitte entschuldige, dass ich dir hierbei nicht weiterhelfen kann. Danke für dein Verständnis. Liebe Grüsse, ^sz.",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1002119003618398212",
        "createdAt" : "2018-05-31T09:26:03.626Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "über das myUPC hab ich schon probiert, da hats keinen Punkt um das auszuschalten. Auch den Hinweis vom Forum dass man ein und ausloggen soll nach dem Vergeben eines We-Free Passworts und dann deaktivieren hat nicht funktioniert... war das was die anderen im Forum gemeint haben mit \"Bei dem erhalte man keine Unterstützung\"?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1002084790319886340",
        "createdAt" : "2018-05-31T07:10:06.565Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc, du kannst es mal über MyUPC probieren. Sonst empfehle ich dir, unseren technischen Dienst unter der Nummer 0800 66 88 66 anzurufen. Danke für deine Bemühungen. Liebe Grüsse, ^sz.",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1002081680461574149",
        "createdAt" : "2018-05-31T06:57:45.096Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "und wo kann ich das Wi-Free deaktivieren?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "1001878968751087621",
        "createdAt" : "2018-05-30T17:32:14.875Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Vielen Dank für deine Anfrage zu den Horizon WLAN Einstellungen. Ich habe deine Horizon überprüft und festgestellt, dass auf deiner Horizon das Wi-Free aktiviert ist. Da Wi-Free ausschliesslich über die 2.4 GHZ Frequenz läuft, kannst du das aktuell auf deiner Horizon nicht deaktivieren. Ich hoffe dir mit dieser Antwort geholfen zu haben.\nBeste Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "1001719497894572036",
        "createdAt" : "2018-05-30T06:58:34.053Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Bitteschön. Wir wünschen einen schönen Tag. Liebe Grüsse, ^au.",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "976000311193227269",
        "createdAt" : "2018-03-20T07:39:42.091Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "975755551715090436",
        "createdAt" : "2018-03-19T15:27:06.907Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Kein Thema. Ich habe gerade nochmals eine Empfangsbestätigung versendet. Du bekommst diese in den nächsten Tagen per Post. Liebe Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "975738949418266634",
        "createdAt" : "2018-03-19T14:21:08.599Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nein eben hab ich die noch nicht erhalten...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "975692173680181252",
        "createdAt" : "2018-03-19T11:15:16.430Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Deine Digicard, CI-Modul und das Modem wurden ausgebucht. Somit wurden die Geräte korrekt retourniert und du bekommst keine Mahnung oder Rechnungsstellung diesbezüglich. Hast du bereits eine schriftliche Bestätigung erhalten? Beste Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "975670905828495364",
        "createdAt" : "2018-03-19T09:50:45.750Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Vielen Dank für deine Anfrage. Ich habe diese an die Zuständige Abteilung weitergeleitet. Sobald ich ein Feedback habe, werde ich dich umgehend informieren. Beste Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "972052569722941444",
        "createdAt" : "2018-03-09T10:12:47.190Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Morgen UPC, ich habe noch die Digi-Card, das CI Modul und das Modem zurück geschickt weil ich ja jetzt ne Horizon Box habe und leider noch keine Bestätigung erhalten dass ich das retourniert habe, könnten Sie diese noch schicken. Besten Dank",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "971970729762734084",
        "createdAt" : "2018-03-09T04:47:35.057Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "danke",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "951723818770366469",
        "createdAt" : "2018-01-12T07:53:35.277Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Ich habe deine Emailadresse und den Wunsch für diese Kontaktaufnahme im Ticket entsprechend ergänzt. Liebe Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "951723594328965125",
        "createdAt" : "2018-01-12T07:52:41.756Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wärs möglich das sich der 2nd Level Support auf meiner email Adresse maldet? mail@marclandolt.ch",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "951683316687794181",
        "createdAt" : "2018-01-12T05:12:38.823Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Vielen Dank für deine ausführliche Meldung. Ich habe nun ein Ticket (Nr. 10978791) inkl. deiner Ausführungen für unseren technischen Support (2nd Level) erfasst. Der support wird sich schnellstmöglich mit dir in Verbindung setzen um das Problem zu lösen. Liebe Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "951065528122204164",
        "createdAt" : "2018-01-10T12:17:46.557Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag\n\nich habe nur eine Dose in der Wohnung. Ich hab jetzt zwar kein Messgerät aber ich gehe davon aus dass der Pegel an der Dose 63dbµV ist, dann kommt der Splitter (-3.5db) dann hätten wir dort nur noch 58.5dbµV was zu wenig ist für das Modem. Also die Wohnung wurde so geplant, dass da nur genug Pegel für ein Gerät ist. Man Plant den Pegel (Planungspegel) im normalfall auf 67dbµV dann ist man schön in der Mitte zwischen 63dbµV und 73dbµV. Da es die Steckdosen aber nur mit gewissen abständen gibt kann auch mal nur 63dBµV an der Dose sein, was aber ein gültiger Wert ist.\n\nDas Problem ist der Splitter den Sie mit der Horizon Box mitgeschickt haben. Gibt es eine Möglichkeit die Horizon-Box einfach an den Normalen TV Anschluss anzuhängen und dann die Horizon-Box zusätzlich mit dem Modem zu betreiben oder gibt es eine Möglichkeit nur die Horizon-Box anzuschliessen und das Modem in der Horizon-Box zu verwenden (In diesem Fall müsste ich noch das Passwort für die Horizon Box haben)\n\nMit freundlichen Grüssen\nMarc Landolt\n\nPS: bitte entschuldigen sie dass ich da mehr weiss als Sie, ich hab das Fach Koaxiale Anlagen 5 Jahre lang unterrichtet.",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "950945729111044100",
        "createdAt" : "2018-01-10T04:21:44.293Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc. Danke für deine Nachricht. Hast du es mal an einer anderen Dose versucht. Tritt da das selbe Phänomen auf, dass das Internet langsamer wird, wenn Modem und Horizon Box an der gleichen Dose angeschlossen sind? Liebe Grüsse, ^au",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "950719469810343942",
        "createdAt" : "2018-01-09T13:22:39.830Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "wenn ich das Kabel direkt anschliesse habe ich sogar nur 20 statt 500.\nIch hab noch ein bisschen Fehlersuche gemacht und rausgefunden dass wenn ich die Horizon-Box ausstecke dass ich dann wieder die volle Geschwindigkeit habe, also schätzungsweise ein Problem auf dem HF (Hochfrequenz) ich weiss jetzt nicht was sie das für einen Splitter beigelegt haben (vermutlich ein 3.5db) aber da scheint wohl ein Problem z.B. mit der Auskoppelung zu sein...\n\nDas Problem ist seit ich die Horizon Box habe.\n\nMit freundlichen Grüssen\nMarc Landolt",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "950545863742500868",
        "createdAt" : "2018-01-09T01:52:48.920Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "950545106314186756",
        "createdAt" : "2018-01-09T01:49:48.325Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/YBZBgDOwz7",
          "expanded" : "http://hsi.upc.ch/speedtestJavaV2/",
          "display" : "hsi.upc.ch/speedtestJavaV…"
        } ],
        "text" : "Hallo Marc. Ich schreibe dir bezüglich deiner Mention vom 5. Januar 2018. Ich habe dein Modem überprüft. Die Leitungen sowie die Aufschaltung ist i.O. Geh bitte folgendermassen vor:  \n-Schliesse  ein Endgerät (Notebook, PC) per LAN-Kabel direkt an die Connect-Box an.\n-Die Ethernet-Netzwerkkarte beim Endgerät sollte auf 1 Gbit/s, Volldublex eingestellt sein.\n-Trenne alle anderen Geräte von der Connect-Box (W-LAN, Drucker, Router etc.)\n-Öffne folgenden Link: https://t.co/YBZBgDOwz7 \n-Führe eine Messung aus, falls das Ergebnis nicht i.O. ist, ersetze das LAN-Kabel und schliesse das neue an einem anderen Port an der Connect-Box an\n-Führe erneut eine Messung aus, falls das Ergebnis nicht i.O. ist, führe 12 Messungen aus und sende uns die UID (User ID) zu. Anschliessend erfassen wir ein Ticket.\nBitte beachte das die 500M/bit (best effort) per LAN-Verbindung (1 Gerät) sind.\nVielen Dank für deine wertvolle Mithilfe. Liebe Grüsse ^ch",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "950282658659135492",
        "createdAt" : "2018-01-08T08:26:55.974Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc\nGerne informiere ich dich, dass wir den Versand deiner Rechnung auf Papierrechnung umgestellt haben, du erhältst bereits die nächste Rechnung per Post. Lieber Gruss^dm",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "928168388349911044",
        "createdAt" : "2017-11-08T07:52:43.031Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "nö, ich hab am telefon gesagt ich würde gerne eine papier rechnung für 3.-- haben jeden monat, hab es selber gefunden aber lieber eine papierrechnung dann gehts sicher nicht vergessen...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "927967368055279620",
        "createdAt" : "2017-11-07T18:33:56.069Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Guten Tag Marc\nWie meinst du das genau mit der Papierrechnung? Wünscht du in Zukunft die Rechnungen per Mail an keine@hotmail.com? Die DigiCard kannst du auf folgende Adresse zurückschicken: UPC Schweiz GmbH, Richtiplatz 5,  8304 Wallisellen. Beste Grüsse. ^rw",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "927820299260891140",
        "createdAt" : "2017-11-07T08:49:32.120Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, danke, kleine frage noch, klappt das mit der Papier Rechnung? Wie wohin kann ich die digicard zurückschicken? Wie kann ich eine Email Adresse angeben wo die Rechnung hin kommt?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "927552926209531908",
        "createdAt" : "2017-11-06T15:07:05.427Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Hallo Marc\nIch habe gesehen, dass du dich gestern bereits telefonisch gemeldet hast. Dein Anliegen ist bereits in Bearbeitung, deine Ticketnummer wäre 10766063. Ich bitte dich um ein wenig Geduld. Beste Grüsse. ^rw",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "927548111769219078",
        "createdAt" : "2017-11-06T14:47:57.595Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/GHvq2qOFEv",
          "expanded" : "https://twitter.com/UPC_Switzerland/status/927441239905103872",
          "display" : "twitter.com/UPC_Switzerlan…"
        } ],
        "text" : "6608524-1 https://t.co/GHvq2qOFEv",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "927525890950598660",
        "createdAt" : "2017-11-06T13:19:39.741Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Danke, ist weitergereicht. ^dp",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "897108481806934019",
        "createdAt" : "2017-08-14T14:51:44.275Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "12:00-13:00?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "897097107106136067",
        "createdAt" : "2017-08-14T14:06:32.307Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Wir bedauern dir miteilen zu müssen, dass die Kollegen heute schlichtweg sich nicht um alle Rückrufe kümmern können. Daher müsstest du dich bis morgen gedulden. Über eine erneute Angabe entsprechender Zeitfenster wären wir umso dankbarer. ^dp",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "897095194604122115",
        "createdAt" : "2017-08-14T13:58:56.329Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "kleine Frage noch, hesst das genau um 15:00 oder 15:30 +/-30Min?",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "897083041213972483",
        "createdAt" : "2017-08-14T13:10:38.806Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/eFdbGdDqZS",
          "expanded" : "http://marclandolt.ch/ml_buzzernet/2015/10/22/busybox-gerate-uber-telnet-mit-zabbix-uberwachen/",
          "display" : "marclandolt.ch/ml_buzzernet/2…"
        } ],
        "text" : "Leider nicht, scheinbar arbeitet das Modem zwar intern mit SNMP (was im Handbuch steht) was afaik auch so im DOCSIS Standard definiert ist, hat aber ins interne Netzwerk keinen Port offen womit man das abfragen könnte und auch kein SSH wie die meisten Busybox Geräte, da könnte man es auch abfragen einfach anders. PS: diesen Text noch weiterleiten wäre vermutlich für den Second Level (?) noch sachdienlich, dann wissen sie grad worums geht: https://t.co/eFdbGdDqZS",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "896027838666743817",
        "createdAt" : "2017-08-11T15:17:38.891Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ {
          "url" : "https://t.co/msV6PCkt6b",
          "expanded" : "https://www.upc.ch/dam/www-upc-cablecom-ch/Support/manuals/de/int/Manual-Connect-Box-Modem_1115_DE.PDF",
          "display" : "upc.ch/dam/www-upc-ca…"
        } ],
        "text" : "Herzlichen Dank, wir leiten die Anfrage weiter. Möglicherweise wirst du jedoch in der Zwischenzeit im online verfügbaren manual fündig (ab Seite 49): https://t.co/msV6PCkt6b ^dp",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "896009353710489603",
        "createdAt" : "2017-08-11T14:04:11.714Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "54578472",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "ok, dann wäre sonst Montag 15:00-16:00 gut...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "896002569759227910",
        "createdAt" : "2017-08-11T13:37:14.265Z"
      }
    }, {
      "messageCreate" : {
        "recipientId" : "75128838",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "Herzlichen Dank für die Angaben. Die Erfahrung zeigt, dass die Angabe entsprechender Zeitfenster äusserst zielführend wäre. Dürften wir daher noch darum bitten? ^dp",
        "mediaUrls" : [ ],
        "senderId" : "54578472",
        "id" : "895993097322196995",
        "createdAt" : "2017-08-11T12:59:35.862Z"
      }
    } ]
  }
}, {
  "dmConversation" : {
    "conversationId" : "75128838-76133153",
    "messages" : [ {
      "messageCreate" : {
        "recipientId" : "76133153",
        "reactions" : [ ],
        "urls" : [ ],
        "text" : "packungsbeilage: meine tweets liest man besser nicht, da krigt man recht kopfschmerzen davon. ps: heute wäre piraten Aargau stammtisch...",
        "mediaUrls" : [ ],
        "senderId" : "75128838",
        "id" : "560048698668113921",
        "createdAt" : "2015-01-27T12:16:29.642Z"
      }
    } ]
  }
} ]